package idsoft.inspectiondepot.wdo.supportclass;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.R.integer;
import android.webkit.URLUtil;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class Validation {
CommonFunction cf;
	public Validation(CommonFunction cf) {
	// TODO Auto-generated constructor stub
		this.cf=cf;
	}
	
	public boolean validate(EditText ed,String title)
	{
		
		if(ed.getTag()!=null)
		{
			String validationstring=ed.getTag().toString().trim();
			if(validationstring.contains("required"))
			{
				if(ed.getText().toString().trim().equals(""))
				{
					cf.show_toast("Please enter value for "+title, 0);
					ed.requestFocus();
					return false;
				}
			
			}
			if(validationstring.contains("email"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  // TODO Auto-generated method stub
					  final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
					  Pattern pattern = Pattern.compile(EMAIL_PATTERN);
					  Matcher matcher = pattern.matcher(ed.getText().toString().trim());
					  if(!matcher.matches())
					  {
						  cf.show_toast("Please enter valid "+title, 0);
						  ed.requestFocus();
					    return false;
					  }
					
				}
			
			}
			if(validationstring.contains("number"))
			{
				try
				{
				if(!ed.getText().toString().trim().equals(""))
				{
					if(validationstring.contains("number_"))
					{
						String[] val=validationstring.split("number_");
						if(val[1]!=null)
						{
							String length[]=val[1].split(",");
							if(!length[0].equals(""))
							{
								int len= Integer.parseInt(length[0]);
								if(ed.length()!=len)
								{
									cf.show_toast("Please enter valid "+title, 0);
									ed.requestFocus();
									return false;	
								}
							}
						}
							
					}
					
					
					
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			if(validationstring.contains("phone_no"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(ed.getText().toString().length()!=13)
					  {
						  cf.show_toast("Please enter valid  "+title, 0);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("website"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(!URLUtil.isValidUrl(ed.getText().toString()))
					  {
						  cf.show_toast("Please enter valid "+title, 0);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("Date_past"))
			{
				if(checkfortodaysdate(ed.getText().toString().trim()))
				{
					return true;
				}
				else
				{
					 cf.show_toast("Date should not be greater than todays date "+title, 0);
					 ed.requestFocus();
					  return false;
				}
				
			}
			
			
		}
		return true;
	}
	public boolean validate_other(EditText ed,String title,String option_other)
	{
		
		if(ed.getTag()!=null)
		{
			String validationstring=ed.getTag().toString().trim();
			if(validationstring.contains("required"))
			{
				if(ed.getText().toString().trim().equals(""))
				{
					cf.show_toast("Please enter  value "+option_other+" for "+title, 0);
					ed.requestFocus();
					return false;
				}
			
			}
			if(validationstring.contains("email"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  // TODO Auto-generated method stub
					  final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
					  Pattern pattern = Pattern.compile(EMAIL_PATTERN);
					  Matcher matcher = pattern.matcher(ed.getText().toString().trim());
					  if(!matcher.matches())
					  {
						  cf.show_toast("Please enter valid "+title, 0);
						  ed.requestFocus();
					    return false;
					  }
					
				}
			
			}
			if(validationstring.contains("number"))
			{
				try
				{
				if(!ed.getText().toString().trim().equals(""))
				{
					if(validationstring.contains("number_"))
					{
						String[] val=validationstring.split("number_");
						if(val[1]!=null)
						{
							String length[]=val[1].split(",");
							if(!length[0].equals(""))
							{
								int len= Integer.parseInt(length[0]);
								if(ed.length()!=len)
								{
									cf.show_toast("Please enter valid "+title, 0);
									ed.requestFocus();
									return false;	
								}
							}
						}
							
					}
					
					
					
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			if(validationstring.contains("phone_no"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(ed.getText().toString().length()!=13)
					  {
						  cf.show_toast("Please enter valid  "+title, 0);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("website"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(!URLUtil.isValidUrl(ed.getText().toString()))
					  {
						  cf.show_toast("Please enter valid "+title, 0);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			
			
		}
		return true;
	}

	public boolean validate(Spinner sp,String title,String defaults) {
		// TODO Auto-generated method stub
		if(sp.getTag()!=null)
		{
			String validationstring=sp.getTag().toString().trim();
			System.out.println("validation string "+validationstring);
			if(validationstring.contains("required"))
			{
				if(sp.getSelectedItem().toString().equals(defaults))
				{
					 cf.show_toast("Please select value for "+title, 0);
					return false;
				}
			}
		}
		
		return true;
	}

	public boolean validate(RadioButton[] call_info_rd, String title) {
		// TODO Auto-generated method stub
		
		for (int i=0;i<call_info_rd.length;i++)
		{
			if(call_info_rd[i].isChecked()){
				
				return true;
			}
		}
		 cf.show_toast("Please select value for "+title, 0);
		return false;
	}

	public boolean validate(CheckBox[] check, String title) {
		// TODO Auto-generated method stub
		for (int i=0;i<check.length;i++)
		{
			if(check[i].isChecked()){
				
				return true;
			}
		}
		 cf.show_toast("Please select value for "+title, 0);
		 return false;
	}

	public boolean validate(RadioGroup radioGroup, String title) {
		// TODO Auto-generated method stub
		for (int i=0;i<radioGroup.getChildCount();i++)
		{
			if(((RadioButton)radioGroup.getChildAt(i)).isChecked()){
				
				return true;
			}
		}
		 cf.show_toast("Please select value for "+title, 0);
		 return false;
		
	}

	public boolean validate(TextView wdos_tv, String title, String defaults) {
		// TODO Auto-generated method stub
		if(wdos_tv.getTag().toString().contains("required"))
		{
			String s=wdos_tv.getText().toString().trim();
			s=s.replace(defaults, "");
			if(s.equals(""))
			{
				 cf.show_toast("Please select value for "+title, 0);
				 return false;
			}
		}
		return true;
	}
	public boolean checkfortodaysdate(String string) {
		// TODO Auto-generated method stub
		int i1 = string.indexOf("/");
		String result = string.substring(0, i1);
		int i2 = string.lastIndexOf("/");
		String result1 = string.substring(i1 + 1, i2);
		String result2 = string.substring(i2 + 1);
		result2 = result2.trim();
		int smonth= Integer.parseInt(result);
		int sdate = Integer.parseInt(result1);
		int syear = Integer.parseInt(result2);
	    cf.getCalender();
	    cf.mMonth = cf.mMonth + 1;
	    if ( syear  < cf.mYear || (smonth < cf.mMonth && syear  <= cf.mYear) || (smonth <= cf.mMonth && syear <=cf.mYear && sdate <= cf.mDay )) {
	    	return true;
		} else {
			return false;
		}
		
	}

	public boolean validate(Option_list option_list, String title,
			String defaults) {
		if(option_list.get_selected().trim().equals(""))
		{
			 cf.show_toast("Please select value for "+title, 0);
			return false;
		}
		else
		{
			return true;
		}
		// TODO Auto-generated method stub
		
	}

}
