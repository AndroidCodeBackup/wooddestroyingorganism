package idsoft.inspectiondepot.wdo.supportclass;

import android.graphics.Color;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class TextWatchLimit implements TextWatcher
{
	EditText ed;
	int length; 
	TextView textView;
	String addendum="";
	
	public TextWatchLimit(EditText ed, int max_length, TextView showing_textview)
	{
		this.ed=ed;
		this.length=max_length;
		this.textView=showing_textview;
		textView.setText("   "+0+" %");
		textView.setTextColor(Color.parseColor("#212839"));
		
	}
	public TextWatchLimit(EditText ed, int max_length, TextView showing_textview,String addnedum)
	{
		this.ed=ed;
		this.length=max_length;
		this.textView=showing_textview;
		textView.setText("   "+0+" %,       "+addnedum);
		textView.setTextColor(Color.parseColor("#212839"));
		this.addendum=addnedum;
	}
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		int string_len=ed.getText().toString().length();
		if(string_len>0)
		{
			int percent=(int)((string_len)*(100.00/Double.parseDouble(String.valueOf(length))));
		    
		    if(string_len==length)
		    {
		    	if(!addendum.equals(""))
		    		textView.setText(Html.fromHtml("   "+percent+" % Exceed limit,        "+addendum));
		    	else
		    		textView.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
		    	textView.setTextColor(Color.RED);
		    }
		    else
		    {
		    	if(!addendum.equals(""))
		    		textView.setText(Html.fromHtml("   "+percent+" %,        "+addendum));
		    	else
		    		textView.setText("   "+percent+" % ");
		    	textView.setTextColor(Color.parseColor("#212839"));
		    }
		}
		else
		{
			textView.setTextColor(Color.parseColor("#212839"));
			if(!addendum.equals(""))
	    		textView.setText(Html.fromHtml("   "+0+" %,         "+addendum));
	    	else
	    		textView.setText("   "+0+" %");
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before,
			int count) {
		// TODO Auto-generated method stub
		
	}
	
}
     