package idsoft.inspectiondepot.wdo.supportclass;

import java.util.List;



import android.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MultiSpinner extends Spinner implements
        OnMultiChoiceClickListener, OnCancelListener {
	
    private static final int visibility = 0;
	private List<String> items;
    private boolean[] selected;
    boolean selected_t[];
    private String defaultText,defspinnertxt="";
    public String spinnerText;
    private MultiSpinnerListener listener;
    int cancel=0;
   // String doublequotes="\"";
    /**For show the selected  values **/
    private TextView TV1;

    public MultiSpinner(Context context) {
        super(context);
     
    }

    public MultiSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
      
      
    }

    public MultiSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
       
    }

    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
    	System.out.println("the sele"+which);
        if (isChecked)
            selected[which] = true;
        else
        	selected[which] = false;
    }

    public void onCancel(DialogInterface dialog) {
        // refresh text on spinner
    	
        StringBuffer spinnerBuffer = new StringBuffer();
        boolean someUnselected = false;
        boolean Nothingselected = true;
         for (int i = 0; i < items.size(); i++) {
            if (selected[i] == true) {
            	if(!items.get(i).equals("-Select-"))
            	{
            		spinnerBuffer.append(items.get(i));
                    spinnerBuffer.append("~ ");
            	}
                Nothingselected=false;
            } else {
            	someUnselected = true;
            }
        }
        
      if(Nothingselected)
        {
        	spinnerText=this.defaultText;
        }
        else
	     {
            spinnerText = spinnerBuffer.toString();            
	        if (spinnerText.length() > 2)
	            spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        }
        /**We can set the default selection when all has been selected  **/
       
        /**We can set the default selection when all has been selected  **/
        ArrayAdapter<String> adapter;
        /**set the selcted text to the New Text view **/
        
        
        if(TV1!=null)
        {
        	if(spinnerText.equals(defaultText))
        	{
        	 		TV1.setText("");
        	}
        	else
        	{
        		spinnerText=spinnerText.replace("~", ",");
        		TV1.setText("You have selected "+spinnerText);
        	}
        	
        	 adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item,
                    new String[] { "-Select-" });
        	 
        }
        else
        {
        	adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item,
                    new String[] { spinnerText });
        }
        setAdapter(adapter);
        listener.onItemsSelected(selected);
    
        /**set the selcted text to the New Text view ends **/
    }

    @Override
    public boolean performClick() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        selected_t =new boolean[selected.length];
        for(int i =0;i<selected_t.length;i++)
        {
        	selected_t[i]=selected[i];
        }
        
         builder.setMultiChoiceItems(items.toArray(new CharSequence[items.size()]), selected, this);
        /** to add the Ok button in the spinner  **/
         builder.setNegativeButton("Cancel",
                 new DialogInterface.OnClickListener() {

                     public void onClick(DialogInterface dialog, int which) {
                     	
                     	 for(int i =0;i<selected_t.length;i++)
                          {
                     		 selected[i]=selected_t[i];
                          }  
                     	builder.setCancelable(true);
                     	  dialog.cancel();
                     }
                 });
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                    	  builder.setCancelable(true);
                    	 dialog.cancel();
                    }
                });
      
      
        builder.setOnCancelListener(this);
        builder.setCancelable(false);
        builder.show();
        return true;
    }

    public void setItems(List<String> items, String allText,
            MultiSpinnerListener listener,TextView tv) {
        this.items = items;
        this.defaultText = allText;
        this.listener = listener;
        this.TV1=tv;
        // all selected by default
        selected = new boolean[items.size()];
        for (int i = 0; i < selected.length; i++)
            selected[i] = false;

        // all text on the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, new String[] { allText });
        setAdapter(adapter);
    }
    public void setItems(List<String> items, String allText,
            MultiSpinnerListener listener,TextView tv,String atrarr) {
    	System.out.println("atrarr="+atrarr);
    	defspinnertxt="";
        this.items = items;
        this.defaultText = allText;
        this.listener = listener;
        this.TV1=tv;
        // all selected by default
        String[] str = new String[items.size()];
        items.toArray(str);
        selected = new boolean[items.size()];System.out.println("atrarr="+atrarr+"selected.length"+selected.length);
        for (int i = 0; i < selected.length; i++)
        {
        	selected[i] = false;
        }
        
      for (int i = 0; i < selected.length; i++)
       {    	   
    	   String spliarr[] = atrarr.split(",");
    	   for(int j=0;j<spliarr.length; j++)
        	{
        		if(str[i].trim().equals(spliarr[j].trim()))
        		{
        			selected[i] = true;
        			defspinnertxt += spliarr[j].trim()+",";        			
        		}        		
        	}        
        }System.out.println("defspinnertxt="+defspinnertxt);
      if(defspinnertxt.length()>1)
      {
    	  defspinnertxt=defspinnertxt.substring(0,defspinnertxt.length()-1);
    	  System.out.println("ends="+defspinnertxt);
      }
      this.TV1.setText("You have selected "+defspinnertxt);
        // all text on the spinner
       ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
               android.R.layout.simple_spinner_item, new String[] { allText });
       setAdapter(adapter);
    }
    
    public void setcheckboxvalue(String value)
    {
    	defspinnertxt="";
    	 String[] str = new String[items.size()];
         items.toArray(str);
         clearchk();
    	 for (int i = 0; i < selected.length; i++)
         {    	   
      	   String spliarr[] = value.split(",");
      	   for(int j=0;j<spliarr.length; j++)
          	{
          		if(str[i].trim().equals(spliarr[j].trim()))
          		{
          			selected[i] = true;
          			defspinnertxt += spliarr[j].trim()+",";        			
          		}        		
          	}        
          }
    	 if(defspinnertxt.length()>1)
         {
       	  	defspinnertxt=defspinnertxt.substring(0,defspinnertxt.length()-1);
         }
    	 this.TV1.setText("You have selected "+defspinnertxt);

    }
    public void clearchk()
    {
    	for (int i = 0; i < selected.length; i++)
        {
    		selected[i] = false;
        }
    }
    
    public interface MultiSpinnerListener {
        public void onItemsSelected(boolean[] selected);
    }
	
	
}