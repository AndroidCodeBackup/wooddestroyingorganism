package idsoft.inspectiondepot.wdo.supportclass;

import idsoft.inspectiondepot.wdo.HomeScreen;
import idsoft.inspectiondepot.wdo.InspectionsFindings;
import idsoft.inspectiondepot.wdo.R;

import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunction {
public static final int loadcomment_code = 14;

Context  con;
public String selected_insp_type="",selectedhomeid="",deviceId,model,manuf,devversion,apiLevel;
public String type_id_carr="23",type_id="22";
public String elev[]={"--Select--","Front Elevation","Left Elevation","Back Elevation","Right Elevation","Wood Decay","Drill Holes","Mud Tubes","Interior"};
public int wd,ht,identityval=0,mYear,mMonth,mDay,typeidentity=1,windmitinstypeval,k1=0,mDayofWeek,selmDayofWeek,hours,minutes,amorpm,seconds,ipAddress;
public  ProgressDialog pd;
//public String apkrc="RC U 05-16-2013";
public String apkrc="RC U 01-08-2015";
public String datewithtime;
public String addendm_txt="Use addendum page if necessary";

public boolean application_sta=false;

	public CommonFunction(Context cont)
	{
		con=cont;
		DateFormat df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
	
	}
	public void get_appstat()
	{
		try
		{
		final PackageManager pm = con.getPackageManager();
        //get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);
		
        for (ApplicationInfo packageInfo : packages) {

	          
			  if(packageInfo.packageName.toString().trim().equals("idsoft.inspectiondepot.IDMA")) // for checking the main app has installed  or not 
	            {
				  application_sta=true; // check if the main appliccation IDinspection has installed  or not 
	            }
			
        }
		}catch(Exception e)
		{
			System.out.println("Problem in the application sta"+e.getMessage());
		}
	}
	
	public void show_toast(String msg,int type)
	{
		String colorname = "#000000"; 
		switch(type)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		final Toast toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(msg);
		toast.setView(layout); 
		Thread t = new Thread() {
            public void run() {
                int count = 0;
                try {
                    while (true && count < 10) {
                        toast.show();
                        sleep(3);
                        count++;

                        // do some logic that breaks out of the while loop
                    }
                } catch (Exception e) {
                   
                }
            }
        };
        t.start();
		
	}
	public void go_home()
	{
		con.startActivity(new Intent(con,HomeScreen.class));
	}
	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);System.out.println("mDay"+mDay);
			
			mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
			hours = c.get(Calendar.HOUR);
			minutes = c.get(Calendar.MINUTE);
			seconds = c.get(Calendar.SECOND);
			amorpm = c.get(Calendar.AM_PM);
	
	}
	public void showDialogDate(EditText edt) {
			// TODO Auto-generated method stub
		    getCalender();
			Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        DatePickerDialog dialog= new DatePickerDialog(con, new mDateSetListener(edt),
	                   mYear, mMonth, mDay);
	        dialog.show();
		}
	public void show_help(String string) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
		LinearLayout li=(LinearLayout)dialog1.findViewById(R.id.help_alert);
		li.setVisibility(View.VISIBLE);
		TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
		TextView txt = (TextView) li.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(string));
		ImageView btn_helpclose = (ImageView) li.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				
			}
			
		});
		Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
		btn_ok.setVisibility(View.VISIBLE);
		btn_ok.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
			dialog1.dismiss();
			
			}
			
		});
		
		dialog1.setCancelable(false);
		dialog1.show();
	}
	public class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			getCalender();
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			selmDayofWeek = mDayofWeek ;
			
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(pad(mMonth)).append("/").append(pad(mDay)).append("/")
					.append(mYear).append(" "));
		}
		
	}
	public String pad(int c) {
		// TODO Auto-generated method stub		
		return c>=10 ? ""+c : "0"+c;   
	}
	public void getInspectorId() {
		// TODO Auto-generated method stub
		
	}
	public void go_back(Class c)
	{
		Intent in = new Intent(con,c);
		in.putExtra("SRID", selectedhomeid);
		
		con.startActivity(in);
	}
	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}
	public void setTouchListener(View v) {
		// TODO Auto-generated method stub
		if(v instanceof ViewGroup)
		{
			ViewGroup vg=(ViewGroup)v;
			for(int i=0;i<vg.getChildCount();i++)
			{
				if((vg.getChildAt(i)) instanceof ViewGroup)
				{
					setTouchListener(vg.getChildAt(i));
				}
				else if((vg.getChildAt(i)) instanceof EditText)
				{
					EditText ed=(EditText)(vg.getChildAt(i));
					ed.setOnTouchListener(new TouchFoucs(ed));
					
				}
			}
		}
	}
	public void setValuetoCheckbox(CheckBox[] checkbox, String value) {
		// TODO Auto-generated method stub
		System.out.println("valu="+value);
		if(value.contains("^"))
		{
			value=value.replace("^", ",");
			System.out.println("valu1="+value);
			String[] val= value.split(",");
			System.out.println("valu length ="+val.length);
			for(int i=0;i<checkbox.length;i++)
			{
				for(int j=0;j<val.length;j++)
				{
					if(checkbox[i].getText().toString().equals(val[j].trim()))
					{
						checkbox[i].setChecked(true);
					}
				}
			}
		}
		else
		{
			System.out.println("comes else");
			for(int i=0;i<checkbox.length;i++)
			{
				
					if(checkbox[i].getText().toString().equals(value.trim()))
					{
						checkbox[i].setChecked(true);
					}
				
			}
		}
	}
	public void setValuetoRadio(RadioButton[] radio, String value) {
		// TODO Auto-generated method stub
		for(int i=0;i<radio.length;i++)
		{
			if(radio[i].getText().toString().equals(value.trim()))
				{
					radio[i].setChecked(true);
					return;
				}
		}
	}
	public String getvaluefromchk(CheckBox[] chk) {
		// TODO Auto-generated method stub
		String txt="";
		for(int i=0;i<chk.length;i++)
		{
			if(chk[i].isChecked())
				txt+=chk[i].getText().toString().trim()+"^";
		}
		if(txt.length()>1)
		{
			txt=txt.substring(0,txt.lastIndexOf("^"));
		}
		return txt;
	}
	public String getvaluefromchk(RadioButton[] rd) {
		// TODO Auto-generated method stub
		for(int i=0;i<rd.length;i++)
		{
			if(rd[i].isChecked())
				return rd[i].getText().toString().trim();
		}
		return "";
	}
	public String getvaluefromRG(RadioGroup radioGroup) {
		// TODO Auto-generated method stub
		return (radioGroup.getCheckedRadioButtonId()==-1)?"":((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
		
	}
	public void setvaluerd(RadioGroup radioGroup, String string) {
		// TODO Auto-generated method stub
		for(int i=0;i<radioGroup.getChildCount();i++)
		{
			if(radioGroup.getChildAt(i) instanceof RadioButton)
			{
				if(((RadioButton)radioGroup.getChildAt(i)).getText().toString().trim().equals(string))
				{
					((RadioButton)radioGroup.getChildAt(i)).setChecked(true);
				}
			}
		}
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = ((Activity) con).managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	
	}
	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>"+string+" . Please wait...</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}
	public void Device_Information()
	{
		 
		deviceId = Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}
	public String check_values(String pH_Status) {
		// TODO Auto-generated method stub
		if(pH_Status!=null)
		{
			if(!pH_Status.equals("N/A"))
			{
				return pH_Status; 
			}
		}
		return "";
	}
	public String removeNOformat(String no)
	{
		no=no.replace("(", "");
		no=no.replace(")", "");
		return no=no.replace("-", "");
	}
	public boolean checkforassigndate(String getinsurancedate,
			String getinspectiondate) {
		try
		{
		if (!getinsurancedate.trim().equals("")
				|| getinsurancedate.equals("N/A")
				|| getinsurancedate.equals("Not Available")
				|| getinsurancedate.equals("anytype")
				|| getinsurancedate.equals("Null")) {
			boolean chkdate = true;
			int i1 = getinsurancedate.indexOf("/");
			String result = getinsurancedate.substring(0, i1);
			int i2 = getinsurancedate.lastIndexOf("/");
			String result1 = getinsurancedate.substring(i1 + 1, i2);
			String result2 = getinsurancedate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = getinspectiondate.indexOf("/");
			String result3 = getinspectiondate.substring(0, i3);
			int i4 = getinspectiondate.lastIndexOf("/");
			String result4 = getinspectiondate.substring(i3 + 1, i4);
			String result5 = getinspectiondate.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);

			if (j > k) {
				chkdate = false;
			} else if (j < k) {
				chkdate = true;
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = false;
				} else if (j1 < k1) {
					chkdate = true;
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = false;
					} else if (j2 < k2) {
						chkdate = true;
					} else if (j2 == k2) {

						chkdate = true;
					}

				}

			}

			return chkdate;
		} else {
			return true;
		}
		}
		catch (Exception e)
		{
			return true;
		}
	}
	
	public boolean eMailValidation(CharSequence stringemailaddress1) {
		// TODO Auto-generated method stub
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(stringemailaddress1);

		return matcher.matches();
		
		

	}
	
	public void hidekeyboard(EditText editText){
        InputMethodManager imm = (InputMethodManager)con.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

	public void show_toast(Spanned fromHtml, int type) {
		// TODO Auto-generated method stub
		String colorname = "#000000"; 
		switch(type)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		final Toast toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(fromHtml);
		toast.setView(layout); 
		Thread t = new Thread() {
            public void run() {
                int count = 0;
                try {
                    while (true && count < 10) {
                        toast.show();
                        sleep(3);
                        count++;

                        // do some logic that breaks out of the while loop
                    }
                } catch (Exception e) {
                   
                }
            }
        };
        t.start();
	}
	
}
