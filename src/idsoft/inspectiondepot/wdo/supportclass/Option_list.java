package idsoft.inspectiondepot.wdo.supportclass;

        
import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Option_list {
	Context con;
	Spinner sp;
	MultiSpinner msp;
	TextView tv;
	EditText ed;
	int mtype,qtype;
	DataBaseHelper db;
	String option[],selected_txt;
	ArrayAdapter ad;
	int selected = 0;
	String def_selected = "";
	CommonFunction cf=null;
	private List<String> items= new ArrayList<String>();
	public Option_list(Context con,Spinner sp,int mtype,int qtype,EditText ed)
	{
		this.con=con;
		this.sp=sp;
		this.mtype=mtype;
		this.qtype=qtype;
		db=new DataBaseHelper(con);
		this.ed=ed;
		Set_RDoption();
		
	}
	public Option_list(Context con,MultiSpinner sp_wdo, int mtype, int qtype, EditText ed, TextView tv ,String selected_txt) {
		// TODO Auto-generated constructor stub
		this.con=con;
		this.msp=sp_wdo;
		this.mtype=mtype;
		this.qtype=qtype;
		this.ed=ed;
		this.tv=tv;
		this.selected_txt=selected_txt;
		db=new DataBaseHelper(con);
		Set_chkoption() ;
	}
	public Option_list(Context con,MultiSpinner sp_wdo, int mtype, int qtype, EditText ed, TextView tv ,String selected_txt,CommonFunction cf) {
		// TODO Auto-generated constructor stub
		this.con=con;
		this.msp=sp_wdo;
		this.mtype=mtype;
		this.qtype=qtype;
		this.ed=ed;
		this.tv=tv;
		this.selected_txt=selected_txt;
		this.cf=cf;
		db=new DataBaseHelper(con);
		Set_chkoption() ;
	}
	public void Set_chkoption() {
		// TODO Auto-generated method stub
		
		items.clear();
		Cursor c=db.SelectTablefunction(db.config_option, "  WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+mtype+"' AND CP_Question='"+qtype+"' ORDER by CP_Order");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			option=new String[c.getCount()];
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
			
				option[i]=db.decode(c.getString(c.getColumnIndex("CP_Option")));
				/***Start change dynamic for policy holder information agent not present**/
				if(mtype==11 &&  (qtype==1 || qtype==2))
				{
					Cursor c1=db.SelectTablefunction(db.Agent_tabble, " WHERE AI_SRID='"+cf.selectedhomeid+"'");
					if(c1.getCount()<=0)
					{
						if(!option[i].equals("Agent"))
						{
							items.add(option[i]);
						}
						
					}
					else
					{
						items.add(option[i]);
					}
					if(c1!=null)
						c1.close();
					
				}
				else
				{
					items.add(option[i]);
				}
				/***Start change dynamic for policy holder information agent not present ends**/
				if(c.getString(c.getColumnIndex("CP_Default_op")).equals("1"))
				{System.out.println("defaul");
					def_selected+=option[i]+",";
					
				}
			}
			System.out.println("def_selected"+def_selected);
			//set_selectionCHk(def_selected);
			
			if(def_selected.length()>1)
			{
				def_selected=def_selected.substring(0,def_selected.length()-1);
			}
			
			if(!selected_txt.equals(""))
			{
				msp.setItems(items,"-Select-",new multi1(),tv,selected_txt);
			}
			else if(!def_selected.equals(""))
			{System.out.println("enters");
				msp.setItems(items,"-Select-",new multi1(),tv,def_selected);
			}
			else
			{
				msp.setItems(items,"-Select-",new multi1(),tv);
			}
			
			if(tv.getText().toString().trim().equals("You have selected"))
			{
				tv.setVisibility(View.GONE);
			}
			else if(tv.getText().toString().trim().contains("Other"))
			{
				
				if(ed!=null)
				{
				ed.setVisibility(View.VISIBLE);
				}
				
			}
				
		}
		if(c!=null)
			c.close();
		
	}
	public void Set_RDoption() {
		// TODO Auto-generated method stub
		
		Cursor c=db.SelectTablefunction(db.config_option, "  WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+mtype+"' AND CP_Question='"+qtype+"' ORDER by CP_Order");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			option=new String[c.getCount()+1];
			option[0]="-Select-";
			for(int i=1;i<=c.getCount();i++,c.moveToNext())
			{
				option[i]=db.decode(c.getString(c.getColumnIndex("CP_Option")));
				if(c.getString(c.getColumnIndex("CP_Default_op")).equals("1"))
				{
					selected=i;
				}
			}
			ad = new ArrayAdapter(con, android.R.layout.simple_spinner_item,option);
			ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp.setAdapter(ad);
			sp.setOnItemSelectedListener(new sp_listenert());
			sp.setSelection(selected);
			
			
		}
		if(c!=null)
			c.close();
	}
	public void set_selectionRD(String txt)
	{
		int i=ad.getPosition(txt);
		if(i!=-1)
		{
			
			sp.setSelection(i);
			if(txt.equals("Other"))
			{
				if(ed!=null)
				{
				ed.setVisibility(View.VISIBLE);
				}
			}
		}
	}
	class multi1 implements idsoft.inspectiondepot.wdo.supportclass.MultiSpinner.MultiSpinnerListener{
		public void onItemsSelected(boolean[] selected) {
			// TODO Auto-generated method stub
			try
			{
				if(tv.getText().toString().trim().equals("You have selected"))
				{
					tv.setVisibility(View.GONE);
					if(ed!=null)
					{
					ed.setVisibility(View.GONE);
					ed.setText("");
					}
				}
				else if(tv.getText().toString().trim().contains("Other"))
				{
					tv.setVisibility(View.VISIBLE);
					if(ed!=null)
					{
					ed.setVisibility(View.VISIBLE);
					ed.setTag("required");
					}
					
				}
				else
				{
					tv.setVisibility(View.VISIBLE);
					if(ed!=null)
					{
					ed.setVisibility(View.GONE);
					ed.setText("");
					ed.setTag("");
					}
				}
					
				
			}
			catch (Exception e) {
				// TODO: handle exception
			}
}
	}
	class sp_listenert implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(sp.getSelectedItem().toString().trim().equals("Other"))
			{
				if(ed!=null)
				{
					ed.setTag("required");
					ed.setVisibility(View.VISIBLE);
				}
			}
			else
			{
				if(ed!=null)
				{
					ed.setTag("");
					ed.setText("");
					ed.setVisibility(View.GONE);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	public void clear_selectionRD()
	{
		sp.setSelection(0);
	}
	public void setDefaultRD()
	{
		sp.setSelection(selected);
	}
	public void clear_selectionCHK()
	{
		msp.setcheckboxvalue("");
		tv.setVisibility(View.GONE);
		if(ed!=null)
		{
			ed.setText("");
			ed.setTag("");
			ed.setVisibility(View.GONE);
		}
	}
	public void setDefaultCHK()
	{
		msp.setcheckboxvalue(def_selected);
		if(def_selected.equals(""))
		{
		tv.setVisibility(View.GONE);
		
			if(ed!=null && !def_selected.contains("Other"))
			{
				ed.setText("");
				ed.setTag("");
				ed.setVisibility(View.GONE);
			}
		}
	}
	public void set_selectionCHk(String txt)
	{
		
		if(tv!=null)
		{
			msp.setcheckboxvalue(txt);	
			if(!txt.trim().equals(""))
			{
				tv.setVisibility(View.VISIBLE);
			}
		}
		if(ed!=null)
		{
			if(txt.contains("Other"))
			{
				
				ed.setTag("required");
				ed.setVisibility(View.VISIBLE);
			}
			else
			{
				ed.setText("");
				ed.setTag("");
				ed.setVisibility(View.GONE);
			}
		}
		
	}
	public String get_selected()
	{
		if(tv!=null)
		{
			String s =tv.getText().toString().trim();
			s=s.replace("You have selected", "");
			return s;
		}
		else if(sp!=null)
		{
			return sp.getSelectedItem().toString().trim();
		}
		return "";
	}
}
