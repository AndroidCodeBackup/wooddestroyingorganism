package idsoft.inspectiondepot.wdo.supportclass;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;

public class DataBaseHelper {
	public String MY_DATABASE_NAME = "WDODatabases.db";
	public static final String inspectorlogin = "inspectorlogin",policyholder = "Policyholder",Onlinepolicyholder = "Online_Policyholder",
	wdoVersion="wdoVersion",MailingPolicyHolder = "MailingPolicyHolder",Agent_tabble = "Agent_information",Realtor_table="Realtor_Information",Additional_table = "Additional_information",
	Companyinformation="companyinformation",inspection_finding="inspection_findings",treatment="Treatment",no_access="No_Access",
	CommentsandFinancials="Comments_financial",QA_question="QA_question",QA_question_dynamic="QA_question_dynamic",ImageTable="Photos",
	ImageCaption="Photo_caption",feedback_document="feedback_document",feedback_infomation="Feedback_informaion",To_addcomments="To_addcomments",config_option="Option_list",IDMAVersion="IDMA_Version";

	public String  Insp_id,Insp_firstname,Insp_lastname,Insp_address,Insp_companyname,Insp_email,Insp_ext;
	public static SQLiteDatabase wdo_db;
    Context con;
    public DateFormat df;
	public CharSequence datewithtime;
	public boolean application_sta;
	public static final String LoadCompany = "LoadCompany";
	public static final String LoadbuildingType = "LoadbuildingType";
	public static final String LoadAgency = "LoadAgency";
	public static final String LoadAgent = "LoadAgent";
	public static final String LoadRealEstateCompany = "LoadRealEstateCompany";
	public static final String LoadRealtor = "LoadRealtor";
	public static final String ReportsReady = "ReportsReady";
	public static final String Addendum = "Addendum";
	public boolean value;
	public static final String Version = "Version";
	public String status="40",substatus="0",state=""; 
	
	
    public DataBaseHelper(Context con) 
    {
		wdo_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		try
		{
			final PackageManager pm = con.getPackageManager();
	        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
			for (ApplicationInfo packageInfo : packages) 
			{
				if(packageInfo.packageName.toString().trim().equals("idsoft.inspectiondepot.IDMA")) // for checking the main app has installed  or not 
				{
					application_sta=true; // check if the main appliccation IDinspection has installed  or not 
				 }
			}
		}catch(Exception e)
		{
			System.out.println("Problem in the application sta"+e.getMessage());
		}
		CreateTable(16);
        getInspectorId();
       
	}
    public void open()
    {
    	if(!wdo_db.isOpen())
    	{
    		System.out.println("opened successfully");
    		wdo_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
    	}
    }
    public void getInspectorId() {
		// TODO Auto-generated method stub
		try {
			CreateTable(1);
			Cursor cur = this.SelectTablefunction(this.inspectorlogin," where Fld_InspectorFlag='1'");
			System.out.println(" the quesry ");
			cur.moveToFirst();
			if (cur.getCount()>0) {
				do {
					Insp_id = decode(cur.getString(cur.getColumnIndex("Fld_InspectorId")));System.out.println("inspe"+Insp_id);
					
					Insp_firstname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorFirstName")));
					Insp_lastname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorLastName")));
					//System.out.println("comes correctly"+Insp_firstname+Insp_firstname);
					Insp_address = decode(cur.getString(cur.getColumnIndex("Fld_InspectorAddress")));
					Insp_companyname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyName")));
					Insp_email = decode(cur.getString(cur.getColumnIndex("Fld_InspectorEmail")));
					Insp_ext=decode(cur.getString(cur.getColumnIndex("Fld_InspectorPhotoExtn")));
				} while (cur.moveToNext());
				CreateTable(15);
			}
			else
			{
				//if(con.)
			}
			cur.close();
		} catch (Exception e) {System.out.println("erro"+e.getMessage());
	//	Insp_id="2826";
		
		}
	}
	public void CreateTable(int i) {
		// TODO Auto-generated method stub
	switch (i) 
	{
		case 0:
		/** CREATING ARR VERSION TABLE **/
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ wdoVersion
						+ "(VId INTEGER PRIMARY KEY Not null,VersionCode varchar(50),VersionName varchar(50),ModifiedDate varchar(50));");
	
			} catch (Exception e) {}
		break;
		case 1:
			/*INSPECTOR LOGIN*/
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspectorlogin
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Fld_InspectorId varchar(50)," +
						"Fld_InspectorFirstName varchar(50),Fld_InspectorMiddleName varchar(50)," +
						"Fld_InspectorLastName varchar(50),Fld_InspectorAddress varchar(150)," +
						"Fld_InspectorCompanyName varchar(150),Fld_InspectorCompanyId varchar(100)," +
						"Fld_InspectorUserName varchar(50),Fld_InspectorPassword varchar(50)," +
						"Ins_rememberpwd varchar(5) DEFAULT('0'), Fld_InspectorPhotoExtn Varchar(10)," +
						"Android_status bit,Fld_InspectorFlag bit,Fld_InspectorEmail varchar(100)," +
						"Fld_InspectorPEmail varchar(100),Fld_signature varchar(100) DEFAULT('')," +
						"Fld_licenceno varchar(100) DEFAULT(''),Fld_licencetype varchar(100) DEFAULT('')," +
						"Fld_Phone varchar(25) DEFAULT(''));");	
				} catch (Exception e){}
			
		break;
		case 2:
			/* POLICYHOLDER */
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ policyholder
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,PH_InspectorId varchar(50) NOT NULL," +
						"PH_SRID varchar(50) NOT NULL,PH_FirstName varchar(100) NOT NULL," +
						"PH_LastName varchar(100) NOT NULL,PH_Address1 varchar(100),PH_Address2 varchar(100)," +
						"PH_City varchar(100),PH_Zip varchar(100),PH_State varchar(100),PH_County varchar(100)," +
						"PH_Policyno varchar(100),PH_Inspectionfees varchar(100),PH_InsuranceCompany varchar(100)," +
						"PH_HomePhone varchar(100),PH_WorkPhone varchar(100),PH_CellPhone varchar(100)," +
						"PH_NOOFSTORIES varchar(5),PH_WEBSITE varchar(100),PH_Email varchar(100)," +
						"PH_EmailChkbx Integer DEFAULT (0),PH_InspectionTypeId Integer,PH_IsInspected Integer," +
						"PH_IsUploaded Integer,PH_Status Integer,PH_SubStatus Integer," +
						"Schedule_ScheduledDate varchar(100) DEFAULT ('')," +
						"Schedule_InspectionStartTime varchar(100) DEFAULT ('')," +
						"Schedule_InspectionEndTime varchar(100) DEFAULT ('')," +
						"Schedule_Comments varchar(500) DEFAULT ('')," +
						"Schedule_ScheduleCreatedDate varchar(100) DEFAULT ('')," +
						"Schedule_AssignedDate varchar(100) DEFAULT (''),ScheduleFlag Integer DEFAULT ('')," +
						"YearBuilt varchar(100) DEFAULT (''),ContactPerson Varchar(50) DEFAULT ('')," +
						"BuidingSize Varchar(20) DEFAULT (''),fld_latitude varchar(50)," +
						"fld_longitude varchar(50),fld_homeownersign varchar(250)," +
						"fld_homewonercaption varchar(150),fld_paperworksign varchar(250)," +
						"fld_paperworkcaption varchar(150),fld_noofbuildings varchar(50)," +
						"R_Requestedby varchar(100) DEFAULT (''),R_Requestedby_other varchar(150) DEFAULT ('')," +
						"R_Requestedto varchar(100) DEFAULT (''),R_Requestedto_other varchar(150) DEFAULT ('')," +
						"structure varchar(100) DEFAULT (''),structure_other varchar(150) DEFAULT (''),hand_out varchar(5) DEFAULT ('false')," +
						"preferdate varchar(100),prefertime varchar(100),Inspectionfee varchar(1000),discount varchar(1000),totalfee varchar(1000));");
			} catch (Exception e) {}
		break;
		case 3:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MailingPolicyHolder
						+ " (MA_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,ML_PH_InspectorId varchar(50) NOT NULL,ML_PH_SRID varchar(50) NOT NULL,ML Integer DEFAULT (0),ML_NA Integer DEFAULT (0),ML_PH_Address1 varchar(100),ML_PH_Address2 varchar(100),ML_PH_City varchar(100),ML_PH_Zip varchar(100),ML_PH_State varchar(100),ML_PH_County varchar(100));");
			} catch (Exception e) {}
		break;
		case 4: 
			/** creating Addiitonal information table **/
			try
			{
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Additional_table + " (AD_id INTEGER PRIMARY KEY Not null,AD_SRID  Varchar(50) ,AD_INSPID Varchar(50),AD_IsRecord boolean,AD_UserTypeName Varchar(50),AD_ContactEmail varchar(50),AD_PhoneNumber varchar(15),AD_MobileNumber varchar(15),AD_BestTimetoCallYou varchar(50),AD_Bestdaycall varchar(50),AD_FirstChoice varchar(100),AD_SecondChoice varchar(50),AD_ThirdChoice varchar(50),SF_AD_FeedbackComments varchar(500));");
				
			} catch (Exception e) {}
			
			
		break;
		case 5: 
			/** creating Agent information table **/
			try
			{
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Agent_tabble + " (AI_id INTEGER PRIMARY KEY Not null,AI_SRID  Varchar(50) ,AI_AgencyName  Varchar(50) ,AI_AgentName  Varchar(50) ,AI_AgentAddress  Varchar(50) ,AI_AgentAddress2  Varchar(50) ,AI_AgentCity  Varchar(50) ,AI_AgentCounty  Varchar(50) ,AI_AgentRole  Varchar(50) ,AI_AgentState  Varchar(50) ,AI_AgentZip  Varchar(50) ,AI_AgentOffPhone  Varchar(50) ,AI_AgentContactPhone  Varchar(50) ,AI_AgentFax  Varchar(50) ,AI_AgentEmail  Varchar(50) ,AI_AgentWebSite Varchar(50));");
			} catch (Exception e) {}

			
		break;
		case 6:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Companyinformation
						+ " (Id INTEGER PRIMARY KEY Not null,Ins_Id varchar(100),WDO_status varchar(5) DEFAULT('false'),sign_with_wdo varchar(5) DEFAULT('false'),Company_name varchar(100) DEFAULT(''),Company_address varchar(100) DEFAULT(''),Company_city varchar(50) DEFAULT(''),Company_state varchar(50) DEFAULT(''),Company_county varchar(50) DEFAULT(''),Company_zipcode varchar(10) DEFAULT(''),business_lic_no varchar(50) DEFAULT(''),PH_no varchar(20) DEFAULT('')," +
						" Issue_date DATETIME DEFAULT(CURRENT_TIMESTAMP),Expiry_date DATETIME DEFAULT(CURRENT_TIMESTAMP),License_path varchar(100) DEFAULT(''),comments varchar(200) DEFAULT(''),insp_idcard_no varchar(20) DEFAULT(''),insp_lic_no varchar(20) DEFAULT(''),insp_lic_path varchar(50) DEFAULT(''),insp_lic_isue_date DATETIME DEFAULT(CURRENT_TIMESTAMP),insp_lic_exp_date DATETIME DEFAULT(CURRENT_TIMESTAMP));");
			} catch (Exception e) { System.out.println("the erro "+e.getMessage());}
			
			//WDO_status ,sign_with_wdo ,Company_name,Company_address,Company_city,Company_state,Company_county,Company_zipcode ,business_lic_no,PH_no,Issue_date ,Expiry_date ,License_path,comments,insp_idcard_no,insp_lic_no ,insp_lic_path ,insp_lic_isue_date ,insp_lic_exp_date 
		break;
		case 7:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
						
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspection_finding
						+ " (IF_Id INTEGER PRIMARY KEY AUTOINCREMENT,IF_InspectorId varchar(50) NOT NULL,IF_SRID varchar(50) NOT NULL,FI_Visiblesignofwdo varchar(5),FI_wdo_present varchar(5),FI_Livewdo varchar(50),FI_Livewdo_other varchar(100),FI_Location varchar(100)" +
						",FI_Evidence_present varchar(5),FI_Evidence varchar(50),FI_Evidence_other varchar(100),FI_Evidence_Location varchar(100)" +
						",FI_Damage_present varchar(5),FI_Damage varchar(50),FI_Damage_other varchar(100),FI_Damage_Location varchar(100));");
			} catch (Exception e) { System.out.println("the erro "+e.getMessage());}
		break;
		case 8:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ treatment
						+ " (T_Id INTEGER PRIMARY KEY AUTOINCREMENT,T_Enable varchar(5) NOT NULL,T_InspectorId varchar(50) NOT NULL,T_SRID varchar(50) NOT NULL,T_observed varchar(150),T_noticeofinspection varchar(150),T_notice_other varchar(150),T_typet varchar(150),T_typet_other varchar(150), T_companyhastreated varchar(5),T_organism varchar(100),T_organism_other varchar(100),T_nameofpesticide varchar(150),T_termsandcon varchar(50),T_methodoftreat varchar(50),T_comments varchar(150),T_Treatlocation varchar(50));");
			} catch (Exception e) { System.out.println("the erro "+e.getMessage());}
		break;
		case 9:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ no_access
						+ " (N_Id INTEGER PRIMARY KEY AUTOINCREMENT,N_InspectorId varchar(50) NOT NULL,N_SRID varchar(50) NOT NULL,N_Area varchar(100),N_Area_other varchar(150),N_SArea varchar(150),N_SArea_other varchar(150),N_Reason varchar(150));");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 10:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ CommentsandFinancials
						+ " (CF_Id INTEGER PRIMARY KEY AUTOINCREMENT,CF_InspectorId varchar(50) NOT NULL,CF_SRID varchar(50) NOT NULL,CF_Commnents varchar(500));");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 11:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ QA_question
						+ " (QA_Id INTEGER PRIMARY KEY AUTOINCREMENT,QA_InspectorId varchar(50) NOT NULL,QA_SRID varchar(50) NOT NULL,kitchen varchar(50)," +
						"living_R varchar(50),Formal_R varchar(50),Dining_R varchar(50),Master_BR varchar(50),Master_BaR varchar(50),Bedroom1 varchar(50)," +
						"Bedroom2 varchar(50),Bedroom3 varchar(50),Bedroom4 varchar(50),Bedroom5 varchar(50),Bedroom6 varchar(50),Hallways varchar(50),Closets varchar(50),Hall_BaR varchar(50)," +
						"Half_BaR varchar(50),Florida_R varchar(50),Garage varchar(50),Storage varchar(50),Outbuildings varchar(50),PoolHouse varchar(50));");
			CreateTable(111);
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 111:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ QA_question_dynamic
						+ " (QA_D_Id INTEGER PRIMARY KEY AUTOINCREMENT,QA_D_InspectorId varchar(50) NOT NULL,QA_D_SRID varchar(50) NOT NULL,QA_D_title varchar(150),QA_D_option varchar(50));");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 12:
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+ImageTable+" (IM_Id INTEGER PRIMARY KEY AUTOINCREMENT,IM_InspectorId varchar(50) NOT NULL,IM_SRID varchar(50) NOT NULL,IM_Elevation varchar(5) NOT NULL,IM_path varchar(250) NOT NULL,IM_Description varchar(150) NOT NULL,IM_ImageOrder varchar(5) NOT NULL,IM_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				CreateTable(121);   
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 121:
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+ImageCaption+" (IM_C_Id INTEGER PRIMARY KEY AUTOINCREMENT,IM_C_InspectorId varchar(50) NOT NULL,IM_C_Elevation varchar(5) NOT NULL,IM_C_caption varchar(250) NOT NULL,IM_C_ImageOrder varchar(5) NOT NULL,IM_C_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 13:
			try {
				
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+feedback_infomation+" (FD_Id INTEGER PRIMARY KEY AUTOINCREMENT,FD_InspectorId varchar(50) NOT NULL,FD_SRID varchar(50) NOT NULL,FD_CSC varchar(15) NOT NULL,FD_whowas varchar(15) NOT NULL,FD_whowas_other varchar(50),FD_issatisfied varchar(15) NOT NULL,FD_comments varchar(500) NOT NULL,FD_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				CreateTable(131);
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 131:
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+feedback_document+" (FD_D_Id INTEGER PRIMARY KEY AUTOINCREMENT,FD_D_InspectorId varchar(50) NOT NULL,FD_D_SRID varchar(50) NOT NULL,FD_D_doctit varchar(15) NOT NULL,FD_D_doctit_other varchar(50), FD_D_path varchar(100) NOT NULL,FD_D_type varchar(50),FD_D_imageorder  varchar(50) DEFAULT('0'),FD_D_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 14:
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+To_addcomments+" (TC_Id INTEGER PRIMARY KEY AUTOINCREMENT,TC_InspectorId varchar(50) NOT NULL,TC_SRID varchar(50) NOT NULL,TC_type varchar(15) NOT NULL,TC_Question varchar(15) NOT NULL,TC_Comments varchar(500),TC_Status varchar(5) NOT NULL,TC_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				Cursor c =SelectTablefunction(To_addcomments, " WHERE TC_InspectorId='"+Insp_id+"'");
				if(c.getCount()<=0)
				{
					int i1=1;
					wdo_db.execSQL("Insert into "+To_addcomments+" SELECT   (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+") as TC_Id, '"+Insp_id+"' as TC_InspectorId,'' as TC_SRID,'1' as TC_type,'1' as TC_Question,'"+encode("Not Applicable")+"' as TC_Comments,'true' as TC_Status,'' as TC_CreatedOn" +
							//schedule comments
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Home is on lock box")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Call client before you leave inspection")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Call agent if any problems")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Closing within one week")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Prior termites disclosed")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','2','"+encode("Prior treatment disclosed")+"','true',''"+
							
							//Locations
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Exterior")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Front Door")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Garage Door")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Door Frame")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Siding along rear")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Interior")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','3','"+encode("Baseboard")+"','true',''"+
							
							//Treatment observed
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','4','"+encode("Drill holes noted")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','4','"+encode("Baiting stations present")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','4','"+encode("Past termite activity disclosed")+"','true',''"+
							
							//treatemnt comments
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','5','"+encode("Sticker noted only partial treatment to")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','5','"+encode("Partial treatment to")+"','true',''"+
							
							//Reason
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','6','"+encode("No clearance due to limited headroom")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','6','"+encode("Extensive storage present")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','6','"+encode("Storage/belongings impeded inspection")+"','true',''"+
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','6','"+encode("Furniture present within home. Not moved as part of inspection")+"','true',''"+
							
							//comments
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1',7','"+encode("Not Applicable")+"','true',''"+
							
							//addendum
							" UNION SELECT  (Select max(TC_Id)+"+(i1++)+" from "+To_addcomments+"),'"+Insp_id+"','','1','9','"+encode("Not Applicable")+"','true',''");
							
					
				}
				if(c!=null)
					c.close();
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 15:
			try {
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+config_option+" (CP_Id INTEGER PRIMARY KEY AUTOINCREMENT,CP_InspectorId varchar(50) NOT NULL,CP_Mtype varchar(15) NOT NULL,CP_Question varchar(15) NOT NULL,CP_Option varchar(150),CP_Default_op varchar(2) DEFAULT('0'),CP_Order INTEGER ,CP_optiontype varchar(3),CP_editable varchar(3)  DEFAULT('1'),CP_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				Cursor c =SelectTablefunction(config_option, " WHERE CP_InspectorId='"+Insp_id+"'");
				if(c.getCount()<=0)
				{
					int i1=1; 
					
					//
					wdo_db.execSQL("Insert into "+config_option+" SELECT   (Select max(CP_Id)+"+(i1++)+" from "+config_option+") as CP_Id, '"+Insp_id+"' as CP_InspectorId,'22' as CP_Mtype,'1' as CP_Question,'"+encode("Subterranean Termites")+"' as CP_Option,'0' as CP_Default_op,'1' as CP_Order,'chk' as CP_optiontype,'0' as CP_editable,'' as CP_CreatedOn" +
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Powder Post Beetles")+"','0','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Wood Boring Beetles")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Dry wood termites")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Wood Decaying Fungus")+"','0','5','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Other")+"','0','1117','chk','0',''"+// We will give Other order as 1117 for always remain as the last in the option list
							
//							" UNION SELECT  (Select max(CP_Id)+"+(i++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("-Select-")+"','0','1','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("Electrical Panel")+"','0','2','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("Under Kitchen Sink")+"','0','3','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("Water Heater")+"','0','4','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("Not Applicable")+"','0','5','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','1','"+encode("Other")+"','0','1117','radio','0',''"+// We will give Other order as 1117 for always remain as the last in the option list
							
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Subterranean Termites")+"','0','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Powder Post Beetles")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Wood Boring Beetles")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Dry Wood Termites")+"','0','5','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Wood Decaying Fungus")+"','0','6','chk','0',''"+														
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Drill holes")+"','0','7','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Bait Station")+"','0','8','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Treatment Sticker")+"','0','9','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Bond Documentation")+"','0','10','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Treatment Documentation")+"','0','11','chk','0',''"+
							/*" UNION SELECT  (Select max(CP_Id)+"+(i++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Not Applicable")+"','0','7','chk','0',''"+*/
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','2','"+encode("Other")+"','0','1117','chk','0',''"+// We will give Other order as 1117 for always remain as the last in the option list
							
							
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Subterranean temites")+"','0','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Powder Post Beetles")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Wood Boring Beetles")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Dry wood termites")+"','0','5','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Wood Decaying Fungus")+"','0','6','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','23','3','"+encode("Other")+"','0','1117','chk','0',''"+// We will give Other order as 1117 for always remain as the last in the option list
										
							
							//Report Requested By
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','1','"+encode("Agent")+"','0','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','1','"+encode("Real Estate Agent")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','1','"+encode("Client/Owner")+"','1','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','1','"+encode("Other")+"','0','1117','chk','0',''"+
							//Report Requested By
							
							//Structures on Property Inspected
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','3','"+encode("Main Dwelling")+"','1','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','3','"+encode("Detached Garage")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','3','"+encode("Carport")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','3','"+encode("Other")+"','0','1117','chk','0',''"+
							//Structures on Property Inspected
							
							//Report sent to
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','2','"+encode("Client/Owner")+"','1','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','2','"+encode("Agent")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','2','"+encode("Real Estate Agent")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','11','2','"+encode("Other")+"','0','1117','chk','0',''"+
							//Report sent to
							
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','1','"+encode("Attic")+"','0','2','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','1','"+encode("Interior")+"','0','3','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','1','"+encode("Exterior")+"','0','4','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','1','"+encode("Crawlspace")+"','0','5','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','1','"+encode("Other")+"','0','1117','radio','0',''"+
							
//							" UNION SELECT  (Select max(CP_Id)+"+(i++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("-Select-")+"','0','1','radio','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Generally Throughout")+"','0','2','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Around Perimeter")+"','0','3','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Under Kitchen")+"','0','4','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Under Rear")+"','0','5','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Areas Where headroom Less then 18 inches")+"','0','6','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Attic Areas")+"','0','7','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Eaves and Corners")+"','0','8','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Ceiling Joists")+"','0','9','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Under tubs and behind showers")+"','0','10','chk','0',''"+
							" UNION SELECT  (Select max(CP_Id)+"+(i1++)+" from "+config_option+"),'"+Insp_id+"','24','2','"+encode("Other")+"','0','1117','chk','0',''");// We will give Other order as 1117 for always remain as the last in the option list
							
				}
				if(c!=null)
					c.close();
				
				
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		case 16:
			CreateTable(2);
			CreateTable(3);
			CreateTable(4);
			CreateTable(5);
			CreateTable(6);
			CreateTable(7);
			CreateTable(8);
			CreateTable(9);
			CreateTable(10);
			CreateTable(11);
			CreateTable(12);
			CreateTable(13);
			CreateTable(26);
			
			wdo_db.execSQL("CREATE TRIGGER IF NOT EXISTS Policyholder_delete BEFORE Delete ON "+policyholder+" FOR EACH ROW " +
					"BEGIN " +
					" DELETE FROM "+MailingPolicyHolder+" WHERE ML_PH_SRID=old.PH_SRID; " +
					" DELETE FROM "+Additional_table+" WHERE AD_SRID=old.PH_SRID; " +
					" DELETE FROM "+Agent_tabble+" WHERE AI_SRID=old.PH_SRID; " +
					" DELETE FROM "+inspection_finding+" WHERE IF_SRID=old.PH_SRID; " +
					" DELETE FROM "+treatment+" WHERE T_SRID=old.PH_SRID; " +
					" DELETE FROM "+no_access+" WHERE N_SRID=old.PH_SRID; " +
					" DELETE FROM "+CommentsandFinancials+" WHERE CF_SRID=old.PH_SRID; " +
					" DELETE FROM "+QA_question+" WHERE QA_SRID=old.PH_SRID; " +
					" DELETE FROM "+QA_question_dynamic+" WHERE QA_D_SRID=old.PH_SRID; " +
					" DELETE FROM "+ImageTable+" WHERE IM_SRID=old.PH_SRID; " +
					" DELETE FROM "+feedback_infomation+" WHERE FD_SRID=old.PH_SRID; " +
					" DELETE FROM "+feedback_document+" WHERE FD_D_SRID=old.PH_SRID; " +
					" DELETE FROM "+Addendum+" WHERE AD_D_SRID=old.PH_SRID; " +
					
					"END;");  
		break;
		
		case 17:
			try {
				wdo_db.execSQL("create table if not exists " + LoadCompany
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 18:
			try {
				wdo_db.execSQL("create table if not exists " + LoadbuildingType
						+ " (id varchar2(50),buildingtype varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 19:
			try {
				wdo_db.execSQL("create table if not exists " + LoadAgency
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 20:
			try {
				wdo_db.execSQL("create table if not exists " + LoadAgent
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 21:
			try {
				wdo_db.execSQL("create table if not exists " + LoadRealEstateCompany
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 22:
			try {
				wdo_db.execSQL("create table if not exists " + LoadRealtor
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 23:
			wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
					+ IDMAVersion
					+ "(VId INTEGER PRIMARY KEY Not null,ID_VersionCode varchar(50),ID_VersionName varchar(50),ID_VersionType varchar(50));");

			break;
			
		case 24:
			try {
				wdo_db.execSQL("create table if not exists "
						+ ReportsReady
						+ " (id integer primary key autoincrement,srid varchar2,firstname varchar2,lastname varchar2,address1 varchar2,address2 varchar2,state varchar2,county varchar2,city varchar2,zip varchar2,policyno varchar2,email varchar2,status varchar2,pdfpath varchar2,created_Date DATETIME DEFAULT(CURRENT_TIMESTAMP))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 25: 
			/** creating Agent information table **/
			try
			{
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Realtor_table + " (AI_id INTEGER PRIMARY KEY Not null,AI_SRID  Varchar(50) ,AI_AgencyName  Varchar(50) ,AI_AgentName  Varchar(50) ,AI_AgentAddress  Varchar(50) ,AI_AgentAddress2  Varchar(50) ,AI_AgentCity  Varchar(50) ,AI_AgentCounty  Varchar(50) ,AI_AgentRole  Varchar(50) ,AI_AgentState  Varchar(50) ,AI_AgentZip  Varchar(50) ,AI_AgentOffPhone  Varchar(50) ,AI_AgentContactPhone  Varchar(50) ,AI_AgentFax  Varchar(50) ,AI_AgentEmail  Varchar(50) ,AI_AgentWebSite Varchar(50));");
			} catch (Exception e) {}

			
		break;
		case 26:
			try {
				
				wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+Addendum+" (AD_D_Id INTEGER PRIMARY KEY AUTOINCREMENT,AD_D_InspectorId varchar(50) NOT NULL,AD_D_SRID varchar(50) NOT NULL,AD_comments varchar(5000) NOT NULL,AD_D_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				//wdo_db.execSQL("CREATE TABLE IF NOT EXISTS "+Addendum+" (AD_D_Id INTEGER PRIMARY KEY AUTOINCREMENT,AD_D_InspectorId varchar(50) NOT NULL,AD_D_SRID varchar(50) NOT NULL,AD_D_doctit varchar(15) NOT NULL,AD_D_doctit_other varchar(50), AD_D_path varchar(100) NOT NULL,AD_D_imageorder  varchar(50) DEFAULT('0'),AD_D_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
			} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
		break;
		
	}
		
	}
	public Cursor SelectTablefunction(String inspectorlogin2, String string) {
		// TODO Auto-generated method stub
		try
		{
		Cursor cur = wdo_db.rawQuery("select * from " + inspectorlogin2 + " " + string, null);
		return cur;
		}
		catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public String encode(String oldstring) 
	{
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;

	}
	public String decode(String newstring) 
	{
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		catch (Exception e) {
			// TODO: handle exception
			return "";
		}
		return newstring;
	}
	public String convert_null(String newstring)
	{
		return (newstring==null)?"N/A":(newstring.trim().equals(""))? "N/A":(newstring.equals("anyType{}"))? "":newstring;
		
	}
	public boolean check_value_inTable(String tbl_name,String Where)
	{
		Cursor c =SelectTablefunction(tbl_name, Where);
		if(c.getCount()>0)
		{
			if(c!=null)
				c.close();
			return true;
		}
		else
		{
			if(c!=null)
				c.close();
			return false;
		}
		
	}
	public void getpolicyholderstatus(String home_id)
	{
		Cursor c = wdo_db.rawQuery("Select PH_Status,PH_SubStatus,PH_State from "+policyholder+" WHERE PH_SRID='" + encode(home_id)+ "' and PH_InspectorId='"+ encode(Insp_id)+"'", null);
		c.moveToFirst();
		status=c.getString(c.getColumnIndex("PH_Status"));
		substatus=c.getString(c.getColumnIndex("PH_SubStatus"));
		state=decode(c.getString(c.getColumnIndex("PH_State")));
		if(c!=null)
			c.close();
	}
}