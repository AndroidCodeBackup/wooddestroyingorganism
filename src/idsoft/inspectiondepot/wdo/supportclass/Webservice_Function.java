package idsoft.inspectiondepot.wdo.supportclass;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
 
public class Webservice_Function {
	/*public static final String URL = "http://idmaws.paperless-inspectors.com/WDOWebservice.asmx";
	public static final String URL_IDMA = "http://72.15.221.151:89/AndroidWebService.asmx";*//**UAT**/
	public static final String URL = "http://wdo.paperlessinspectors.com/IDMAWebService.asmx";
	public static final String URL_IDMA = "http://72.15.221.153:91/AndroidWebService.asmx";/**LIVE**/
	public String URL_sink = "http://72.15.221.153:91/AndroidWebService.asmx"; // LIVE SH
	Context con;
	public String NAMESPACE = "http://tempuri.org/";
	public SoapSerializationEnvelope envelope;
	public String status,result,deviceId,model,manuf,devversion,apiLevel,versionname;
	public int ipAddress;
	
	public Webservice_Function(Context con)
	{
		this.con=con;
	}
	public boolean isInternetOn()
	{
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) this.con.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
		//return false;
	}
	public SoapObject export_header(String string)
	{
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		SoapObject add_property = new SoapObject(NAMESPACE,string);
		return add_property;
	}
	public String export_footer(SoapSerializationEnvelope envelope22,
			SoapObject add_property, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		// TODO Auto-generated method stub
		MarshalBase64 marshal = new MarshalBase64();
		marshal.register(envelope22);
		envelope22.setOutputSoapObject(add_property);
		HttpTransportSE androidHttpTransport11 = new HttpTransportSE(URL);
		try {
			androidHttpTransport11.call(NAMESPACE+string, envelope22);
			String result = String.valueOf(envelope22.getResponse());
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "false";
		}
		
		
	}
	public SoapObject Calling_service(String insp_id, String string)throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("inspectorid",insp_id);
		envelope.setOutputSoapObject(request);
		System.out.println("the property ="+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE+string,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("result "+result);
		return result;
		
		
	}
	public String IsCurrentInspector(String inspid,String srid,String methodname) throws SocketException,IOException, XmlPullParserException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		// TODO Auto-generated method stub
		String currrentinsp,resultres;
		SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope1.dotNet = true;
		SoapObject ad_property1 = new SoapObject(NAMESPACE,methodname);
		ad_property1.addProperty("InspectorID", inspid);
		ad_property1.addProperty("Srid", srid);
		envelope1.setOutputSoapObject(ad_property1);
		System.out.println(" ad_property1 "+ad_property1);
		HttpTransportSE androidHttpTransport2 = new HttpTransportSE(URL);
		androidHttpTransport2.call(NAMESPACE+methodname,envelope1);//System.out.println("httptrans");
		resultres = String.valueOf(envelope1.getResponse());//System.out.println("resultres="+resultres);
		System.out.println(" resultIsCurrentInspector "+resultres);
		
		return resultres;
	}
	public boolean check_result(String current) {
		// TODO Auto-generated method stub
		if(current!=null)
		{
			if(!current.equals(""))
			{
				if(!current.trim().equals("false"))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public SoapObject Calling_WS_LoadInspectionType_Discount(String couponcode,
			String inspectionfee, String companyid, String inspectionid,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {

		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CouponCode", couponcode);
		request.addProperty("InspectionFees", inspectionfee);// Float.parseFloat(inspectionfee)
		request.addProperty("CompanyID", Integer.parseInt(companyid));
		request.addProperty("InspTypeID", Integer.parseInt(inspectionid));
		envelope.setOutputSoapObject(request);
		System.out.println("The request for LoadInspectionType_Discount is "
				+ request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("The result is " + result);
		return result;

	}

	public SoapObject Calling_WS_LoadInspectionTypeDescription(String string,
			String string2, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspTypeID", Integer.parseInt(string));
		request.addProperty("CompanyID", Integer.parseInt(string2));
		System.out.println("The request for LoadInspectionTypeDetails is "+ request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadCompany(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public String Calling_WS_CheckCouponcode(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Couponcode", string);
		System.out.println("Coupon code Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}

	public SoapObject Calling_WS_GETADDRESSDETAILS(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Zipcode", string);
		System.out.println("Zipcode Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadAgency(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		System.out.println("GETAGENCYNAME request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadAgent(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("AgencyID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("GETAGENTNAME request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadRealEstateCompany(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		System.out.println("GETREALTORCOMPANYDETAILS request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadRealtor(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CompanyID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("GETREALTORNAME request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadBuildingTypes(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CategoryID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("Fill building type request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public String Calling_WS_ViewCustomerPDF(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID", string);
		System.out.println("ViewCustomerPDF request is " + request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	
	public String Calling_WS_EmailReport(String inspectorid, String to,
			String subject, String body, String path, String string3,String srid) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		Device_Information();
		versionname = getdeviceversionname();
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		 
		request.addProperty("InspectorID", inspectorid);
		request.addProperty("To", to);
		request.addProperty("CC", "");
		request.addProperty("Subject", subject);
		request.addProperty("Body", body);
		request.addProperty("Pdfpath", path);
		request.addProperty("SRID", srid);
		
		/*request.addProperty("ApplicationVersion",versionname);
		request.addProperty("DeviceName",model);
		request.addProperty("APILevel",apiLevel);*/
		System.out.println("Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	
	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(
					this.con.getPackageName(), 0).versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
		}
		return versionname;
	}
	
	public void Device_Information()
	{
		deviceId= Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}
	
}
