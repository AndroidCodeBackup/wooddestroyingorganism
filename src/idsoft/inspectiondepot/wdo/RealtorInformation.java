package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RealtorInformation extends Activity{
	CommonFunction cf;
	DataBaseHelper db;
	TextView tv[]=new TextView[14];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.realtor_info);
		cf =new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		  
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Realtor Info",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,3,cf));
		declaration();
		db.CreateTable(25);
		Cursor c =db.SelectTablefunction(db.Realtor_table, " WHERE AI_SRID='"+cf.selectedhomeid+"' ");
		if(c.getCount()>0)
		{
			showsaved_data(c);
		}
		else
		{
			/*try
			{
			db.wdo_db.execSQL("INSERT INTO "+db.Realtor_table+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite)" +
					" VALUES ('"+cf.selectedhomeid+"','test agency name','test agent name','test agent address','test agent address2','test city','miami','test','florida','34210','(962)910-8196','(962)910-8197','(962)910-8198','rakki.bsc2009@gmail.com','http://www.google.com') ");
				Cursor c1 =db.SelectTablefunction(db.Realtor_table, " WHERE AI_SRID='"+cf.selectedhomeid+"' ");
				showsaved_data(c1);
			}catch (Exception e) {
				// TODO: handle exception
			}*/
		}
		if(c!=null)
			c.close();
		
	}

private void declaration() {
		// TODO Auto-generated method stub
		tv[0]=(TextView) findViewById(R.id.efirstname);
		tv[1]=(TextView) findViewById(R.id.econtactperson);
		tv[2]=(TextView) findViewById(R.id.txtaddr1);
		tv[3]=(TextView) findViewById(R.id.txtaddr2);
		tv[4]=(TextView) findViewById(R.id.txtcity);
		tv[5]=(TextView) findViewById(R.id.txtstate);
		tv[6]=(TextView) findViewById(R.id.txtcountry);
		tv[7]=(TextView) findViewById(R.id.txtzip);
		tv[8]=(TextView) findViewById(R.id.txtoffphn);
		tv[9]=(TextView) findViewById(R.id.txtmail);
		tv[10]=(TextView) findViewById(R.id.txtconphn);
		tv[11]=(TextView) findViewById(R.id.txtweb);
		tv[12]=(TextView) findViewById(R.id.txtfax);
		tv[13]=(TextView) findViewById(R.id.txtagncyname);
	}

private void showsaved_data(Cursor c) {
		// TODO Auto-generated method stub
		c.moveToFirst();
		tv[0].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentName"))));
		tv[1].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentRole"))));
		tv[2].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentAddress"))));
		tv[3].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentAddress2"))));
		tv[4].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentCity"))));
		tv[5].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentState"))));
		tv[6].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentCounty"))));
		tv[7].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentZip"))));
		tv[8].setText(db.decode(db.convert_null(c.getString(c.getColumnIndex("AI_AgentOffPhone")))));
		tv[9].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentEmail"))));
		tv[10].setText(db.decode(db.convert_null(c.getString(c.getColumnIndex("AI_AgentContactPhone")))));
		tv[11].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentWebSite"))));
		tv[12].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgentFax"))));
		tv[13].setText(db.convert_null(c.getString(c.getColumnIndex("AI_AgencyName"))));
		if(db.convert_null(c.getString(c.getColumnIndex("AI_AgentRole"))).toLowerCase().contains("supervisor"))
		{
			((TextView)findViewById(R.id.txtfirst)).setText("Real Estate supervisor Name");
		}
		/*if(db.convert_null(c.getString(c.getColumnIndex("AI_AgentRole"))).toLowerCase().contains("company"))
		{
			((TextView)findViewById(R.id.txtfirst)).setVisibility(View.GONE);
			findViewById(R.id.efirstname).setVisibility(View.GONE);
		}*/
	}

public void clicker(View v)
{
	switch(v.getId())
	{
		case R.id.hme:
			cf.go_home();
		break;
		case R.id.save:
			cf.show_toast("Saved successfully", 1);
			Intent in =new Intent(this,RealtorInformation.class);
			in.putExtra("SRID", cf.selectedhomeid);
			startActivity(in);
			//cf.go_home();
		break;
		default:
			cf.show_toast("Button under construction", 0);
		break;
	}
}
public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		//cf.goback(12);
		cf.go_back(policyholderInformation.class);
		
		return true;
	}
	return super.onKeyDown(keyCode, event);
}


}
