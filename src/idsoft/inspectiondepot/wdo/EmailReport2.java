package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmailReport2 extends Activity {
	CheckBox ckinspector, ckcustomer, ckother, ckattachreport,chk_agentmail;
	EditText etinspector, etcustomer, etother, etto, etsubject, etbody,et_agent_mail;
	CommonFunction cf;
	DataBaseHelper db;
	Webservice_Function wf;
	String inspectorid, inspectorname, inspectoremail, srid, chklogin, path,
			to, classidentifier, mailid, ownersname, policynumber,status;
	boolean report;
	int show_handler;
	LinearLayout llreport;
	TextView tvpercentage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_report2);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		wf = new Webservice_Function(this);
		
		cf.setTouchListener(findViewById(R.id.widget54));
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(EmailReport2.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		ckinspector = (CheckBox) findViewById(R.id.e_report2_ckinspectormailid);
		ckcustomer = (CheckBox) findViewById(R.id.e_report2_ckcustomermailid);
		chk_agentmail=(CheckBox) findViewById(R.id.e_report2_ckagentmail);
		ckother = (CheckBox) findViewById(R.id.e_report2_ckothermailid);
		ckattachreport = (CheckBox) findViewById(R.id.e_report2_ckattachreport);
		etinspector = (EditText) findViewById(R.id.e_report2_etinspectormail);
		etcustomer = (EditText) findViewById(R.id.e_report2_etcustomermail);
		et_agent_mail = (EditText) findViewById(R.id.e_report2_etagentmail);
		etother = (EditText) findViewById(R.id.e_report2_etothermail);
		etto = (EditText) findViewById(R.id.e_report2_etto);
		etsubject = (EditText) findViewById(R.id.e_report2_etsubject);
		etbody = (EditText) findViewById(R.id.e_report2_etbody);
		llreport = (LinearLayout) findViewById(R.id.e_report2_llreport);
		tvpercentage = (TextView) findViewById(R.id.e_report2_tvpercentage);

		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		policynumber = bundle.getString("policynumber");
		status = bundle.getString("status");
		mailid = bundle.getString("mailid");
		classidentifier = bundle.getString("classidentifier");
		ownersname = bundle.getString("ownersname");
		cf.selectedhomeid=bundle.getString("SRID");
		System.out.println("slected home id"+cf.selectedhomeid);
		if(status.equals("10")||status.equals("20")||status.equals("30"))
		{
			status = "Awaiting";
		}
		else if(status.equals("40"))
		{
			status = "Scheduled";
		}
		else if(status.equals("120"))
		{
			status = "Suspended";
		}
		else if(status.equals("1"))
		{
			status = "Inspected";
		}
		else if(status.equals("50")||status.equals("60")||status.equals("70")||status.equals("80")||status.equals("90")||status.equals("110"))
		{
			status = "Other";
		}
		else if(status.equals("2")||status.equals("5"))
		{
			status = "ReportsReady";
		}
		

		etbody.setText("");
		
		Cursor c =db.SelectTablefunction(db.Agent_tabble, " WHERE AI_SRID='"+cf.selectedhomeid+"'");
				if(c.getCount()>0)
				{
					c.moveToFirst();
					String agent_email=db.decode(c.getString(c.getColumnIndex("AI_AgentEmail")));
					System.out.println(" the email values ="+agent_email);
					if(!agent_email.equals("") && agent_email!=null)
					{
						et_agent_mail.setText(agent_email);
						findViewById(R.id.email_LIagent).setVisibility(View.VISIBLE);
					}
					else
					{
						findViewById(R.id.email_LIagent).setVisibility(View.GONE);
					}
				}
				else
				{
					findViewById(R.id.email_LIagent).setVisibility(View.GONE);
				}
		
		/*etbody.setText("Owners Name =" + ownersname + "\n" + "Policy Number = "
				+ policynumber + "\n" + "Status = " + status);*/
		Call_Pdf_Available();
		if(c!=null)
			c.close();
		db.CreateTable(1);
		Cursor cur = db.wdo_db.rawQuery("select * from " + db.inspectorlogin,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			inspectorid = db.decode(cur.getString(cur
					.getColumnIndex("Fld_InspectorId")));
			inspectorname = db.decode(cur.getString(cur
					.getColumnIndex("Fld_InspectorFirstName")));
			inspectorname += " "
					+ db.decode(cur.getString(cur
							.getColumnIndex("Fld_InspectorLastName")));
			inspectoremail = db.decode(cur.getString(cur
					.getColumnIndex("Fld_InspectorEmail")));
			System.out.println("Agent Email " + inspectoremail);
		}
		cur.close();

		etinspector.setText(inspectoremail);
		etcustomer.setText(mailid);

		cf.Device_Information();

		ckinspector.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String inspectormailid = etinspector.getText().toString();
				if (isChecked) {
					if (cf.eMailValidation(inspectormailid)) {
						etto.append(inspectormailid + ",");
						etinspector.setEnabled(false);
						etinspector.setBackgroundColor(getResources().getColor(R.color.disables));
					} else {
						cf.show_toast("Agent Mail Id is in invalid format", 0);
						// etagent.setText("");
						etinspector.requestFocus();
						ckinspector.setChecked(false);
					}
				} else {
					String agency = etto.getText().toString();
					String inspector = etinspector.getText().toString();
					if (agency.contains(inspector + ",")) {
						agency = agency.replace(inspector + ",", "");
						etto.setText(agency);
						etinspector.setEnabled(true);
						etinspector.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		ckcustomer.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String customermailid = etcustomer.getText().toString();
				if (isChecked) {
					if (cf.eMailValidation(customermailid)) {
						etto.append(customermailid + ",");
						etcustomer.setEnabled(false);
						etcustomer.setBackgroundColor(getResources().getColor(R.color.disables));
					} else {
						cf.show_toast("Customer Mail Id is in invalid format",
								0);
						// etcustomer.setText("");
						etcustomer.requestFocus();
						ckcustomer.setChecked(false);
					}
				} else {
					String agency = etto.getText().toString();
					String customer = etcustomer.getText().toString();
					if (agency.contains(customer + ",")) {
						agency = agency.replace(customer + ",", "");
						etto.setText(agency);
						etcustomer.setEnabled(true);
						etcustomer.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});
		chk_agentmail.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String customermailid = et_agent_mail.getText().toString();
				if (isChecked) {
					if (cf.eMailValidation(customermailid)) {
						etto.append(customermailid + ",");
						et_agent_mail.setEnabled(false);
						et_agent_mail.setBackgroundColor(getResources().getColor(R.color.disables));
					} else {
						cf.show_toast("Customer Mail Id is in invalid format",
								0);
						// etcustomer.setText("");
						et_agent_mail.requestFocus();
						chk_agentmail.setChecked(false);
					}
				} else {
					String agency = etto.getText().toString();
					String customer = et_agent_mail.getText().toString();
					if (agency.contains(customer + ",")) {
						agency = agency.replace(customer + ",", "");
						etto.setText(agency);
						et_agent_mail.setEnabled(true);
						et_agent_mail.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		ckother.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					if (etother.getText().toString().trim().equals("")) {
						cf.show_toast("Please enter Mail Id", 0);
						// etother.setText("");
						etother.requestFocus();
						ckother.setChecked(false);
						
					} else {
						String othermailid = etother.getText().toString();
						if (othermailid.contains(",")) {
							String[] splitvalue = othermailid.split(",");
							String valid = "";
							for (int i = 0; i < splitvalue.length; i++) {
								if (cf.eMailValidation(splitvalue[i])) {
									valid += "true";
								} else {
									valid += "false";
								}
							}
							if (valid.contains("false")) {
								cf.show_toast("Mail Id is in invalid format", 0);
								// etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
								
							} else {
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
								etother.setBackgroundColor(getResources().getColor(R.color.disables));
							}
						} else {
							if (cf.eMailValidation(etother.getText().toString())) {
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
								etother.setBackgroundColor(getResources().getColor(R.color.disables));
								
							} else {
								cf.show_toast("Mail Id is in invalid format", 0);
								// etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
								
							}
						}
					}
				} else {
					String agency = etto.getText().toString();
					String other = etother.getText().toString();
					if (agency.contains(other + ",")) {
						agency = agency.replace(other + ",", "");
						etto.setText(agency);
						etother.setEnabled(true);
						etother.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		etinspector.addTextChangedListener(new CustomTextWatcher(etinspector));
		etcustomer.addTextChangedListener(new CustomTextWatcher(etcustomer));
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		// etbody.addTextChangedListener(new CustomTextWatcher(etbody));
		etsubject.addTextChangedListener(new CustomTextWatcher(etsubject));
		etbody.addTextChangedListener(new TextWatchLimit(etbody, 300, tvpercentage));
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.e_report2_btnsend:
			Check_Validation();
			break;

		case R.id.e_report2_btncancel:
			
			if(classidentifier.equals("EmailReport"))
			{
				Intent intenthome = new Intent(EmailReport2.this, EmailReport.class);
				intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intenthome);
				finish();
			}
			else if(classidentifier.equals("Export"))
			{
				Intent intenthome = new Intent(EmailReport2.this, Export.class);
				intenthome.putExtra("type", "emailreport");
				intenthome.putExtra("classidentifier", classidentifier);
				startActivity(intenthome);
				finish();
			}
			break;

		case R.id.e_report2_btnviewreport:
			String[] filenamesplit = path.split("/");
			final String filename = filenamesplit[filenamesplit.length - 1];
			System.out.println("The File Name is " + filename);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ filename);
			if (file.exists()) {
				View_Pdf_File(filename);
			} else {
				if (wf.isInternetOn() == true) {
					cf.show_ProgressDialog("Downloading ");
					new Thread() {
						public void run() {
							Looper.prepare();
							try {
								String extStorageDirectory = Environment
										.getExternalStorageDirectory()
										.toString();
								File folder = new File(extStorageDirectory,
										"DownloadedPdfFile");
								folder.mkdir();
								File file = new File(folder, filename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path, file);
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								show_handler = 2;
								handler.sendEmptyMessage(0);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("The error is "
										+ e.getMessage());
								e.printStackTrace();
								show_handler = 1;
								handler.sendEmptyMessage(0);

							}
						}

						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								cf.pd.dismiss();
								// dialog1.dismiss();
								if (show_handler == 1) {
									show_handler = 0;
									cf.show_toast(
											"There is a problem on your application. Please contact Paperless administrator.",
											0);

								} else if (show_handler == 2) {
									show_handler = 0;

									View_Pdf_File(filename);

								}
							}
						};
					}.start();
				} else {
					cf.show_toast("Internet connection not available", 0);

				}
			}
			break;

		case R.id.e_report2_toclear:
			ckinspector.setChecked(false);
			ckcustomer.setChecked(false);
			ckother.setChecked(false);
			etto.setText("");
			break;

		case R.id.e_report2_home:
			Intent intent = new Intent(EmailReport2.this, HomeScreen.class);
			startActivity(intent);
			finish();
			break;

		}
	}

	private void View_Pdf_File(String filename) {
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/" + filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try {
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			// Toast.makeText(VehicleInspection.this,
			// "No Application Available to View PDF",
			// Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(EmailReport2.this, ViewPdfFile.class);
			intentview.putExtra("path", this.path);
			startActivity(intentview);
			// finish();
		}
	}

	private void Check_Validation() {
		if (!etto.getText().toString().trim().equals("")) {
			if (!etsubject.getText().toString().trim().equals("")) {
				if (!etbody.getText().toString().equals("")) {
					Send_Mail();
				} else {
					cf.show_toast("Please enter Body", 0);
				}
			} else {
				cf.show_toast("Please enter Subject", 0);
			}
		} else {
			cf.show_toast("Please select Mail Id", 0);
		}
	}

	private void Send_Mail() {
		to = etto.getText().toString();
		to = to.substring(0, to.length() - 1);
		if (!ckattachreport.isChecked()) {
			path = "";
		}

		if (wf.isInternetOn() == true) {
			cf.show_ProgressDialog("Sending Mail... ");
			new Thread() {
				String chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wf.Calling_WS_EmailReport(inspectorid, to,
								etsubject.getText().toString(), etbody
										.getText().toString(), path,
								"REPORTSREADYMAIL",cf.selectedhomeid);
						System.out.println("response EmailReport" + chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast(
									"There is a problem on your Network. Please try again later with better Network.",
									0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast(
									"There is a problem on your application. Please contact Paperless administrator.",
									0);

						} else if (show_handler == 5) {
							show_handler = 0;

							if (chklogin.toLowerCase().equals("true")) {
								Intent intenthome = new Intent(
										EmailReport2.this, HomeScreen.class);
								startActivity(intenthome);
								finish();
								cf.show_toast("Email sent successfully.", 0);
							} else {
								Intent intenthome = new Intent(
										EmailReport2.this, HomeScreen.class);
								startActivity(intenthome);
								finish();
								cf.show_toast(
										"There is a problem on your application. Please contact Paperless administrator.",
										0);
							}
						}
					}
				};
			}.start();
		} else {
			cf.show_toast("Internet connection not available", 0);
		}
	}

	private void Call_Pdf_Available() {
		/*Cursor cur = db.wdo_db.rawQuery("select * from " + db.policyholder
				+ " where PH_SRID='" + cf.selectedhomeid + "'", null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {*/
			//srid = db.decode(cur.getString(cur.getColumnIndex("PH_SRID")));
		srid =cf.selectedhomeid;
			System.out.println("The srid is " + srid);
		/*}
		cur.close();*/

		if (wf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>"
					+ "Processing... Please wait" + "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(EmailReport2.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {

					Looper.prepare();
					try {
						chklogin = wf.Calling_WS_ViewCustomerPDF(srid,
								"ViewCustomerPDF");
						System.out.println("response ViewCustomerPDF"
								+ chklogin);

						if (!chklogin.toLowerCase().equals("false")) {

							report = true;
							path = chklogin;
						} else {
							report = false;
							path = "N/A";
						}

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {

					@Override
					public void handleMessage(Message msg) {

						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast(
									"There is a problem on your Network. Please try again later with better Network.",
									0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast(
									"There is a problem on your application. Please contact Paperless administrator.",
									0);

						} else if (show_handler == 5) {
							show_handler = 0;
							if (report == true) {
								llreport.setVisibility(View.VISIBLE);
							} else {
								llreport.setVisibility(View.GONE);
							}

						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(classidentifier.equals("EmailReport"))
		{
			Intent intenthome = new Intent(EmailReport2.this, EmailReport.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intenthome);
			finish();
		}
		else 
		{
			Intent intenthome = new Intent(EmailReport2.this, Dashboard.class);
			intenthome.putExtra("type", "emailreport");
			//intenthome.putExtra("classidentifier", classidentifier);
			startActivity(intenthome);
			finish();
		}
	}
	
}
