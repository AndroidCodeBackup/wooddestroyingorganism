package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Map extends FragmentActivity implements Runnable{
	
	static GeoPoint point;
	final static int MAX_RESULT = 10;
	private static final String TAG = null;
	Drawable drawable;
    GoogleMap googleMap;
    CommonFunction cf;
    static String addr, addr1;
	static String straddr;
	static String strcty;
	int ichk, k;
	String strstatnam, strcntry;
	static String newstraddr;
	ProgressDialog pd;
	static String newstrcty;
	String newstrstatnam, newstrcntry, InspectionType, status;
	TextView policyholderinfo;
	DataBaseHelper db;
	static Double lon = new Double(0);
	static Double lat = new Double(0);
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		db= new DataBaseHelper(this);
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Maps",0,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,6,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-20;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            try {
    			Cursor c2 = db.wdo_db.rawQuery("SELECT * FROM  "
    					+ db.policyholder + " WHERE PH_SRID='" + cf.selectedhomeid
    					+ "'", null);
    			c2.moveToFirst();
    			if (c2 != null) {
    				do {
    					//straddr = "2401+NW+49TH+LN,BOCA+RATON,Florida,PALM+BEACH";//cf.getsinglequotes(c2.getString(Column5));
    					newstraddr=straddr =db.decode(c2.getString(c2.getColumnIndex("PH_Address1")));
    					newstrcty=strcty = db.decode(c2.getString(c2.getColumnIndex("PH_City")));
    					newstrstatnam=strstatnam =db.decode(c2.getString(c2.getColumnIndex("PH_State")));
    					newstrcntry=strcntry =db.decode(c2.getString(c2.getColumnIndex("PH_County")));
    					
    				} while (c2.moveToNext());
    			}
    			c2.close();

    		} catch (Exception e) {
    			
    		}

    		try {
    			
    			if (straddr.contains(" ")) {
    				newstraddr = straddr.replace(" ", "+");
    			}
    			if (strcty.contains(" ")) {
    				newstrcty = strcty.replace(" ", "+");
    			}
    			if (strstatnam.contains(" ")) {
    				newstrstatnam = strstatnam.replace(" ", "+");
    			}
    			if (strcntry.contains("-")) {
    				newstrcntry = strcntry.replace("-", "+");
    				if (strcntry.contains(" ")) {
    					newstrcntry = newstrcntry.replace(" ", "+");
    				}

    			} else {
    				if (strcntry.contains(" ")) {
    					newstrcntry = strcntry.replace(" ", "+");
    				}
    			}
    		} catch (Exception e) {
    			
    		}


    		addr = newstraddr + "," + newstrcty + "," + newstrstatnam + ","
    				+ newstrcntry;
    		System.out.println("the address"+addr);
    		//addr="4/16+Amman+koil+street+pannimadai,coimbatore,tamilnadu,india";
    		String source = "<b><font color=#00FF33>Loading map. Please wait..."
    				+ "</font></b>";
    		pd = ProgressDialog.show(Map.this, "", Html.fromHtml(source), true);
    		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo info = conMgr.getActiveNetworkInfo();
    		if (info != null && info.isConnected()) {
    			ichk = 0;
    		} else {
    			ichk = 1;
    		}

    		Thread thread = new Thread(Map.this);
    		thread.start();
            // Getting LocationManager object from System Service LOCATION_SERVICE
            
        }
    }
   
    public void run() {
		// TODO Auto-generated method stub
		if (ichk == 0) {
			getLocationInfo(addr);
			k = 1;
		} else {
			k = 2;
		}
		handler.sendEmptyMessage(0);
	}

	public Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			System.out.println("comes comes");
			pd.dismiss();
			if (k == 1) {
				System.out.println("no more ieuse "+point);
				if(googleMap!=null)
				{   
					System.out.println("comes correc2");
					googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,lon)).title("Home Owner").icon(BitmapDescriptorFactory.fromResource(R.drawable.iconmarker)).snippet(straddr+","+strcty+","+strcntry+","+strstatnam));
					googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat,lon)));
				}
     
			} else if (k == 2) {
				cf.show_toast("Sorry, There is no Network availability.",1);

			}

		}
	};
	public static JSONObject getLocationInfo(String address) {
		HttpGet httpGet = new HttpGet(
				"http://maps.google.com/maps/api/geocode/json?address="
						+ address + "&sensor=false");
	System.out.println("url"+"http://maps.google.com/maps/api/geocode/json?address="
						+ address + "&sensor=false");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
			Log.i(TAG, "ClientProtocolException" + e.getMessage());
		} catch (IOException e) {
			Log.i(TAG, "IOException" + e.getMessage());
		}

		JSONObject jsonObject = new JSONObject();
		try {
			Log.i(TAG, "fdfgd");
			jsonObject = new JSONObject(stringBuilder.toString());
			Log.i(TAG, "fds" + jsonObject);
			getGeoPoint(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}
	public static GeoPoint getGeoPoint(JSONObject jsonObject) {

		 
		Log.i(TAG, "latsdgfsdf");
		try {

			lon = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lng");

			lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lat");
			Log.i(TAG, "lat=" + lat + lon);
			//point = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
			//showmap(lat, lon);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	}
public void clicker(View v)
{
	switch (v.getId()) {
	case R.id.hme:
		cf.go_home();
	break;

	default:
		break;
	}
}

}
