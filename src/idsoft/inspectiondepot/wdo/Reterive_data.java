package idsoft.inspectiondepot.wdo;



import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Reterive_data extends Activity {
	CommonFunction cf;
	DataBaseHelper db;
	Webservice_Function wb;
	private int vcode,usercheck,mState,delay=40,RUNNING=1;
	double total;
	private ProgressDialog progDialog;
	public ProgressThread progThread; 
	String progress_text="Data";
	PowerManager.WakeLock wl=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 progDialog = new ProgressDialog(this);
		 wb=new Webservice_Function(this);
		 db=new DataBaseHelper(this);
		 cf=new CommonFunction(this);
		 total = 0;
		showDialog(1);
		total=5;
		Bundle b =getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		start_import();
	}
	 private void start_import() {
		// TODO Auto-generated method 
		 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		 wl.acquire();
		 new Thread() {
				public void run() {
					
					try {
					
					total=5;
					progress_text="Inspection detail.";
					 getpolicyholder_info();
					 total=15;
					 progress_text="Agent detail.";
					 GetAgentinformation();
					 total=20;
					 progress_text="Real Estate Agent detail.";
					 GetRealtorinformation();
					 total=25;
					 progress_text="Inspection findings detail.";
					 Getfindinginspection();
					 total=30;
					 progress_text="Treatment  detail.";
					 Gettreament();
					 total=35;
					 progress_text="No access areas.";
					 GetNoAccessArea();
					 total=40;
					 progress_text="Comments and Financial.";
					 Getcommentsandfina();
					 total=45;
					 progress_text="QA Question.";
					 GetQA_question();
					 total=50;
					 progress_text="Image.";
					 Getimage();
					 total=75;
					 progress_text="Feedback information.";
					 Getfeedback();
					 progress_text="Status change.";
					 changestatus();
					 usercheck = 4;
					 total = 100;
					 handler.sendEmptyMessage(0);
				}
				catch (SocketTimeoutException s) {
					System.out.println("s "+s.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
						
				 } catch (NetworkErrorException n) {
					 System.out.println("n "+n.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
					   
				 } catch (IOException io) {
					 System.out.println("io "+io.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
					    
				 } catch (XmlPullParserException x) {
					 System.out.println("x "+x.getMessage());
					 	usercheck = 2;
					 	total = 100;
					 	handler.sendEmptyMessage(0);
					    
				 }
					 catch (Exception e) {
						 System.out.println("e "+e.getMessage());
						 	usercheck = 2;
						 	total = 100;
						 	handler.sendEmptyMessage(0);
						 System.out.println("commeonexcep"+e.getMessage());
					}
				}

				

				

				
		 }
		 .start();
	}
	 
	 protected void changestatus() {
		// TODO Auto-generated method stub
		 
		 String Sql="UPDATE "
					+ db.policyholder
					+ " SET  PH_IsInspected ='1',"+
					       " PH_SubStatus='1', PH_Status='1' where PH_SRID='" + db.encode(cf.selectedhomeid)+ "' and PH_InspectorId='"+ db.encode(db.Insp_id)+"'";
		
		 db.wdo_db.execSQL(Sql);
	}
	private void getpolicyholder_info()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException {
			// TODO Auto-generated method stub
		 SoapObject request = new SoapObject(wb.NAMESPACE,"UpdateMobileDB_SRID");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"UpdateMobileDB_SRID",envelope);
			SoapObject objInsert = (SoapObject) envelope.getResponse();
			 String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
		     PH_Fname,PH_Lname,PH_Mname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees = "",PH_CONTPERSON,CompID,YearBuilt,
		     PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,PH_Noofstories,IsInspected,BuildingSize,
		     PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0",PH_InspectionName,insp_name="",latitude="",longtitude="",PH_MainInspectionTypeId="",
		    		  MailingAddress="",MailingAddress2="",CommercialFlag="",Mailingcity="",MailingState="",MailingCounty="",Mailingzip="";
			 int Cnt;
			 db.CreateTable(2);
			 if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
			 {
					Cnt=0;total = 15;
			 }
			 else
			 {
				Cnt = objInsert.getPropertyCount();			
				
				for (int i = 0; i < Cnt; i++) 
				{
					SoapObject obj = (SoapObject) objInsert.getProperty(i);  
					try 
					{
						//System.out.println( "property "+obj.toString());
						 HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
						
						 PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
						 PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
						 PH_Mname = (String.valueOf(obj.getProperty("MiddleName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("NA"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("N/A"))?"":String.valueOf(obj.getProperty("MiddleName"));
						 PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
						 PH_Address2 = (String.valueOf(obj.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address2")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address2")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address2"));
						 PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
						 PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
						 PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
						 PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
						 PH_HPhone = (String.valueOf(obj.getProperty("HomePhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("HomePhone"));
						 PH_CPhone = (String.valueOf(obj.getProperty("CellPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("CellPhone"));
						 PH_WPhone = (String.valueOf(obj.getProperty("WorkPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("WorkPhone"));
						 PH_Email = (String.valueOf(obj.getProperty("Email")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Email")).equals("NA"))?"":(String.valueOf(obj.getProperty("Email")).equals("N/A"))?"":String.valueOf(obj.getProperty("Email"));
						 PH_CONTPERSON = (String.valueOf(obj.getProperty("ContactPerson")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("NA"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("N/A"))?"":String.valueOf(obj.getProperty("ContactPerson"));
						 PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
						 PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
						 PH_SubStatus =(String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
						 CompID = (String.valueOf(obj.getProperty("CompanyId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("NA"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("N/A"))?"":String.valueOf(obj.getProperty("CompanyId"));
						 InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
						 ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
						 YearBuilt = (String.valueOf(obj.getProperty("YearBuilt")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("NA"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("N/A"))?"":String.valueOf(obj.getProperty("YearBuilt"));
						 PH_Noofstories = (String.valueOf(obj.getProperty("Nstories")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("NA"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("N/A"))?"":String.valueOf(obj.getProperty("Nstories"));
						 ScheduledCreatedDate = (String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduleCreatedDate"));
						 AssignedDate = (String.valueOf(obj.getProperty("AssignedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("AssignedDate"));
						 InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
						 InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
						 Schedulecomments = (String.valueOf(obj.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionComment"));
						 PH_IsInspected= (String.valueOf(obj.getProperty("IsInspected")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("NA"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("N/A"))?"":String.valueOf(obj.getProperty("IsInspected"));
						 PH_InsuranceCompany = (String.valueOf(obj.getProperty("InsuranceCompany")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuranceCompany"));
						 BuildingSize = (String.valueOf(obj.getProperty("BuildingSize")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingSize"));
						 latitude = (String.valueOf(obj.getProperty("Latitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Latitude"));
						 longtitude = (String.valueOf(obj.getProperty("Longitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Longitude"));				
						// PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
						 MailingAddress= (String.valueOf(obj.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress"));
						 MailingAddress2 = (String.valueOf(obj.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress2"));
						 Mailingcity = (String.valueOf(obj.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingcity"));
						 MailingState = (String.valueOf(obj.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingState"));
						 MailingCounty = (String.valueOf(obj.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingCounty"));				
						 Mailingzip = (String.valueOf(obj.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingzip"));
						 Cursor c1 = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.encode(db.Insp_id) + "' and PH_SRID='"+db.encode(HomeId)+"'");
						 
						 if(PH_Email.equals(""))
						 {
							 PH_EmailChk="1";
						 }
						if(c1.getCount() >= 1)
						 {
							 c1.moveToFirst();
									 
										 String Sql="UPDATE "
													+ db.policyholder
													+ " SET  PH_FirstName ='"+ db.encode(PH_Fname)+"'," +
													       " PH_LastName  ='"+ db.encode(PH_Lname)+"'," +
													       " PH_Address1 ='"+ db.encode(PH_Address1)+"'," +
													       " PH_Address2 ='"+ db.encode(PH_Address2)+"'," +
													       " PH_City ='"+ db.encode(PH_City)+"'," +
													       " PH_Zip ='"+ db.encode(PH_Zip)+"'," +
													       " PH_State ='"+ db.encode(PH_State)+"', "+
													       " PH_County ='"+ db.encode(PH_County)+"',"+
													       " PH_Policyno ='"+ db.encode(PH_Policyno)+"',"+
													       " PH_Inspectionfees ='"+ db.encode(PH_Inspectionfees)+"',"+
													       " PH_InsuranceCompany ='"+ db.encode(PH_InsuranceCompany)+"',"+
													       " PH_HomePhone ='"+ db.encode(PH_HPhone)+"',"+
													       " PH_WorkPhone ='"+ db.encode(PH_WPhone)+"',"+
													       " PH_CellPhone ='"+ db.encode(PH_CPhone)+"', "+
													       " PH_NOOFSTORIES ='"+db.encode(PH_Noofstories)+"',"+
													       " PH_WEBSITE  ='',"+
													       " PH_Email ='"+ db.encode(PH_Email)+"', "+										       
													       " PH_EmailChkbx ='"+ db.encode(PH_EmailChk)+"', "+
													       " PH_IsInspected ='0',"+
													       " PH_IsUploaded ='0',"+
													       " PH_Status='"+ db.encode(PH_Status)+"',"+
													       " PH_SubStatus='"+ db.encode(PH_SubStatus)+"'," +
													       " Schedule_ScheduledDate ='"+ db.encode(ScheduledDate)+"'," +
													       " Schedule_InspectionStartTime='"+db.encode(InspectionStartTime)+"'," +
													       " Schedule_InspectionEndTime ='"+db.encode(InspectionEndTime)+"'," +
													       " Schedule_Comments ='"+db.encode(Schedulecomments)+"',"+
													       " Schedule_ScheduleCreatedDate ='"+db.encode(ScheduledCreatedDate)+"',"+
													       " Schedule_AssignedDate ='"+db.encode(AssignedDate)+"',"+
													       " ScheduleFlag =0 ," +
													       " YearBuilt ='"+YearBuilt+"',"+
													       " ContactPerson ='"+db.encode(PH_CONTPERSON)+"',BuidingSize='"+db.encode(BuildingSize)+"',fld_latitude='"+latitude+"',fld_longitude='"+longtitude+"' "+
													       " where PH_SRID='" + db.encode(HomeId)+ "' and PH_InspectorId='"+ db.encode(InspectorId)+"'";
										
										 db.wdo_db.execSQL(Sql);
										 
									 }
							 else
							 {
								 /*INSERTING THE TABLE - POLICYHOLDER*/
								
								 db.wdo_db.execSQL("INSERT INTO "
											+ db.policyholder
											+ " (PH_InspectorId,PH_SRID,PH_FirstName,PH_LastName,PH_Address1,PH_Address2," +
											" PH_City,PH_Zip,PH_State,PH_County,PH_Policyno,PH_Inspectionfees,PH_InsuranceCompany," +
											" PH_HomePhone,PH_WorkPhone,PH_CellPhone,PH_Email,PH_EmailChkbx," +
											" PH_IsInspected,PH_IsUploaded,PH_Status,PH_SubStatus,Schedule_ScheduledDate,Schedule_InspectionStartTime,"+
										  " Schedule_InspectionEndTime,Schedule_Comments,Schedule_ScheduleCreatedDate,Schedule_AssignedDate,ScheduleFlag,YearBuilt,ContactPerson,BuidingSize,PH_NOOFSTORIES,PH_WEBSITE,fld_latitude,fld_longitude,fld_homeownersign,fld_homewonercaption,fld_paperworksign,fld_paperworkcaption)"
											+ " VALUES ('"+ db.encode(InspectorId)+"','"
											+ db.encode(HomeId)+"','"
											+ db.encode(PH_Fname)+"','"
											+ db.encode(PH_Lname)+"','"
											+ db.encode(PH_Address1)+"','"
											+ db.encode(PH_Address2)+"','"
											+ db.encode(PH_City)+"','"
											+ db.encode(PH_Zip)+"','"
											+ db.encode(PH_State)+"','"
											+ db.encode(PH_County)+"','"
											+ db.encode(PH_Policyno)+"','"
											+ db.encode(PH_Inspectionfees)+"','"
											+ db.encode(PH_InsuranceCompany)+"','"
											+ db.encode(PH_HPhone)+"','"
											+ db.encode(PH_WPhone)+"','"
											+ db.encode(PH_CPhone)+"','"
											+ db.encode(PH_Email)+"','"
											+ db.encode(PH_EmailChk)+"','1','0','"
											+ db.encode(PH_Status)+"','"
											+ db.encode(PH_SubStatus)+"','"
											+ db.encode(ScheduledDate)+"','"+db.encode(InspectionStartTime)+"','"+db.encode(InspectionEndTime)+"','"
											+ db.encode(Schedulecomments)+"','"+db.encode(ScheduledCreatedDate)+"','"+db.encode(AssignedDate)+"','0','"+YearBuilt+"','"+db.encode(PH_CONTPERSON)+"','"+db.encode(BuildingSize)+"','"+db.encode(PH_Noofstories)+"','','"+latitude+"','"+longtitude+"','','','','')");
							 }
						if(c1!=null)
							c1.close();
						 db.CreateTable(3);
					//	 System.out.println("the mail address"+MailingAddress);
						        Cursor c21 = db.SelectTablefunction(db.MailingPolicyHolder," where ML_PH_InspectorId='" + db.encode(db.Insp_id) + "' and ML_PH_SRID='"+db.encode(HomeId)+"'");
								int rws = c21.getCount();
								if(rws >= 1)
								{
									db.wdo_db.execSQL("UPDATE " + db.MailingPolicyHolder
											+ " SET ML_PH_Address1='" + db.encode(MailingAddress)
											+ "',ML_PH_Address2='"
											+ db.encode(MailingAddress2)
											+ "',ML_PH_City='"
											+ db.encode(Mailingcity)
											+ "',ML_PH_Zip='"
											+ db.encode(Mailingzip) + "',ML_PH_State='"
											+ db.encode(MailingState) + "',ML_PH_County='"
											+ db.encode(MailingCounty) + "' WHERE ML_PH_SRID ='" + db.encode(HomeId)
											+ "'");
								}
								else
								{
									db.wdo_db.execSQL("INSERT INTO "
											+ db.MailingPolicyHolder
											+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
											+ "VALUES ('"+db.encode(HomeId)+"','"+db.encode(InspectorId)+"','0','"+db.encode(MailingAddress)+"','"
										    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
										    + db.encode(Mailingzip)+"','"+db.encode(MailingState)+"','"
										    + db.encode(MailingCounty)+"')");
								}
								if(c21!=null)
									c21.close();

					}
					catch(Exception e)
					{
						System.out.println("Exception"+e.getMessage());
					}
				}
				}
		}
	 public void GetAgentinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException {
			// TODO Auto-generated method stub
		try
		{
			 	int Cnt;
				String SRID,AgencyName,AgentName,AgentAddress,AgentAddress2,AgentCity,AgentCounty,AgentRole,AgentState,AgentZip,AgentOffPhone,AgentContactPhone,AgentFax,AgentEmail,AgentWebSite;
			 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadAgentInformation_SRID");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("SRID",cf.selectedhomeid);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				androidHttpTransport.call(wb.NAMESPACE+"LoadAgentInformation_SRID",envelope);
				
				SoapObject agentinfo = (SoapObject) envelope.getResponse();
				
				System.out.println("the agent info result is "+agentinfo);
				Cnt=agentinfo.getPropertyCount();
				
				if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
				{
					Cnt=0;total = 20;
				}
				else
				{
				 	
					db.CreateTable(5);
					
					for(int i=0;i<agentinfo.getPropertyCount();i++)
					{
						SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
						SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
						AgencyName=(String.valueOf(temp_result.getProperty("AgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgencyName"));
						AgentName=(String.valueOf(temp_result.getProperty("AgentName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentName"));
						AgentAddress=(String.valueOf(temp_result.getProperty("AgentAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress"));
						AgentAddress2=(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress2"));
						AgentCity=(String.valueOf(temp_result.getProperty("AgentCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCity"));
						AgentCounty=(String.valueOf(temp_result.getProperty("AgentCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCounty"));
						AgentRole=(String.valueOf(temp_result.getProperty("AgentRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentRole"));
						AgentState=(String.valueOf(temp_result.getProperty("AgentState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentState"));
						AgentZip=(String.valueOf(temp_result.getProperty("AgentZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentZip"));
						AgentOffPhone=(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentOffPhone"));
						AgentContactPhone=(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentContactPhone"));
						AgentFax=(String.valueOf(temp_result.getProperty("AgentFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentFax"));
						AgentEmail=(String.valueOf(temp_result.getProperty("AgentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentEmail"));
						AgentWebSite=(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentWebSite"));
						System.out.println("comes corredctly0");
						if(!AgencyName.equals("") && !AgencyName.equals("N/A"))
						{
						try
						{
							Cursor c2=db.SelectTablefunction(db.Agent_tabble," where AI_SRID ='"+SRID+"'");
							if(c2.getCount()==0)
							{
								db.wdo_db.execSQL("Insert into "+db.Agent_tabble+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
										"('"+SRID+"','"+AgencyName+"','"+AgentName+"','"+AgentAddress+"','"+AgentAddress2+"','"+AgentCity+"','"+AgentCounty+"','"+AgentRole+"','"+AgentState+"','"+AgentZip+"','"+db.encode(AgentOffPhone)+"','"+db.encode(AgentContactPhone)+"','"+AgentFax+"','"+AgentEmail+"','"+AgentWebSite+"') ");
							}
							else if(c2.getCount()>=1)
							{
								db.wdo_db.execSQL("UPDATE "+db.Agent_tabble+" SET AI_SRID='"+SRID+"',AI_AgencyName='"+AgencyName+"',AI_AgentName='"+AgentName+"',AI_AgentAddress='"+AgentAddress+"',AI_AgentAddress2='"+AgentAddress2+"'," +
								"AI_AgentCity='"+AgentCity+"',AI_AgentCounty='"+AgentCounty+"',AI_AgentRole='"+AgentRole+"',AI_AgentState='"+AgentState+"',AI_AgentZip='"+AgentZip+"',AI_AgentOffPhone='"+db.decode(AgentOffPhone)+"'," +
								"AI_AgentContactPhone='"+db.decode(AgentContactPhone)+"',AI_AgentFax='"+AgentFax+"',AI_AgentEmail='"+AgentEmail+"',AI_AgentWebSite='"+AgentWebSite+"' WHERE AI_SRID='"+SRID+"'");
							}
							if(c2!=null)
								c2.close();
						}
						catch(Exception e)
						{
							System.out.println("he issues is "+e.getMessage());
						}
						}
						
					}
				 }
		}
		catch (XmlPullParserException e) {
			// TODO: handle exception
		}
		}
	 public void GetRealtorinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException  {
			// TODO Auto-generated method stub
		 /***Start manula coding **/
			
		    /*String envelope="<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope     xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><LoadRealtorInformation_SRID xmlns=\"http://tempuri.org/\"><SRID>%s</SRID></LoadRealtorInformation_SRID></soap:Body></soap:Envelope>";

		    String myEnvelope = String.format(envelope, "SRID "+cf.selectedhomeid);
		    String url = "http://wdo.paperlessinspectors.com/IDMAWebService.asmx";
		    String soapAction = "http://tempuri.org/LoadRealtorInformation_SRID"; 
		    
		    String response = CallWebService(url, soapAction, myEnvelope);
		    Log.v("response", response);
		    Toast.makeText(getApplicationContext(), "this"+response,Toast.LENGTH_LONG).show();
		    String xml=response;*/
			/***Start manula coding **/
		 try
		 {
			 	int Cnt;
				String RealtorSRID,RealtorAgencyName,RealtorName,RealtorAddress,RealtorAddress2,RealtorCity,RealtorCounty,RealtorRole,RealtorState,RealtorZip,RealtorOffPhone,RealtorContactPhone,RealtorFax,RealtorEmail,RealtorWebSite;
			 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadRealtorInformation_SRID");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("SRID",cf.selectedhomeid);
				envelope.setOutputSoapObject(request);
				System.out.println("the realtor info request is "+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				androidHttpTransport.call(wb.NAMESPACE+"LoadRealtorInformation_SRID",envelope);
				System.out.println(" the envlop "+envelope.toString());
				SoapObject agentinfo = (SoapObject) envelope.getResponse();
				System.out.println("comes corredctly");
			
				if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
				{
					Cnt=0;total = 25;
				}
				else
				{
				 	 
					db.CreateTable(25);
					
					for(int i=0;i<agentinfo.getPropertyCount();i++)
					{
						SoapObject temp_result=agentinfo;//(SoapObject) agentinfo.getProperty(i);
						System.out.println("the realtor info result is "+temp_result);
						RealtorSRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
						RealtorAgencyName=(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAgencyName"));
						RealtorName=(String.valueOf(temp_result.getProperty("RealtorName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorName"));
						RealtorAddress=(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress"));
						RealtorAddress2=(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress2"));
						RealtorCity=(String.valueOf(temp_result.getProperty("RealtorCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCity"));
						RealtorCounty=(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCounty"));
						System.out.println("comes correct");
						RealtorRole=(String.valueOf(temp_result.getProperty("RealtorRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorRole"));
						System.out.println("comes correct ok");
						RealtorState=(String.valueOf(temp_result.getProperty("RealtorState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorState"));
						RealtorZip=(String.valueOf(temp_result.getProperty("RealtorZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorZip"));
						RealtorOffPhone=(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorOffPhone"));
						RealtorContactPhone=(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorContactPhone"));
						RealtorFax=(String.valueOf(temp_result.getProperty("RealtorFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorFax"));
						RealtorEmail=(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorEmail"));
						RealtorWebSite=(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorWebSite"));
						System.out.println("comes correct15");
						try
						{
							Cursor c2=db.SelectTablefunction(db.Realtor_table," where AI_SRID ='"+RealtorSRID+"'");
							if(c2.getCount()==0)
							{
								db.wdo_db.execSQL("Insert into "+db.Realtor_table+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
										"('"+RealtorSRID+"','"+RealtorAgencyName+"','"+RealtorName+"','"+RealtorAddress+"','"+RealtorAddress2+"','"+RealtorCity+"','"+RealtorCounty+"','"+RealtorRole+"','"+RealtorState+"','"+RealtorZip+"','"+db.encode(RealtorOffPhone)+"','"+db.encode(RealtorContactPhone)+"','"+RealtorFax+"','"+RealtorEmail+"','"+RealtorWebSite+"') ");
							}
							else if(c2.getCount()>=1)
							{
								db.wdo_db.execSQL("UPDATE "+db.Realtor_table+" SET AI_SRID='"+RealtorSRID+"',AI_AgencyName='"+RealtorAgencyName+"',AI_AgentName='"+RealtorName+"',AI_AgentAddress='"+RealtorAddress+"',AI_AgentAddress2='"+RealtorAddress2+"'," +
								"AI_AgentCity='"+RealtorCity+"',AI_AgentCounty='"+RealtorCounty+"',AI_AgentRole='"+RealtorRole+"',AI_AgentState='"+RealtorState+"',AI_AgentZip='"+RealtorZip+"',AI_AgentOffPhone='"+RealtorOffPhone+"'," +
								"AI_AgentContactPhone='"+RealtorContactPhone+"',AI_AgentFax='"+RealtorFax+"',AI_AgentEmail='"+RealtorEmail+"',AI_AgentWebSite='"+RealtorWebSite+"' WHERE AI_SRID='"+RealtorSRID+"'");
							}
							if(c2!=null)
								c2.close();
						}
						catch(Exception e)
						{
							System.out.println("he issues is "+e.getMessage());
						
						}

						System.out.println("comes correct ends");
					}
				 }
		 }catch (XmlPullParserException e) {
			// TODO: handle exception
		}
		}	 
	 String CallWebService(String url,String soapAction,String envelope) 
	 {

	   final DefaultHttpClient httpClient=new DefaultHttpClient();
	   // request parameters
	   HttpParams params = httpClient.getParams();
	      HttpConnectionParams.setConnectionTimeout(params, 10000);
	      HttpConnectionParams.setSoTimeout(params, 15000);
	      // set parameter
	   HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

	   // POST the envelope
	   HttpPost httppost = new HttpPost(url);
	   // add headers
	   System.out.println("comes correct1");
	      httppost.setHeader("soapaction", soapAction);
	      System.out.println("comes correct2");
	      httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
	      String responseString="";
System.out.println(" the http post"+httppost);
	      try 
	      {
	             // the entity holds the request
	             HttpEntity entity = new StringEntity(envelope);
	             httppost.setEntity(entity);
	             System.out.println(" the http post2"+httppost);	             
	             // Response handler
	             
	             ResponseHandler<String> rh=new ResponseHandler<String>() {
	     // invoked when client receives response
	            	 	 
	     public String handleResponse(HttpResponse response)
	       throws ClientProtocolException, IOException {

	      // get response entity
	      HttpEntity entity = response.getEntity();

	      // read the response as byte array
	            StringBuffer out = new StringBuffer();
	            byte[] b = EntityUtils.toByteArray(entity);

	            // write the response byte array to a string buffer
	            out.append(new String(b, 0, b.length));
	            return out.toString();
	     }
	    };

	    responseString=httpClient.execute(httppost, rh); 

	   }
	      catch (Exception e) {
	       Log.v("exception", e.toString());
	   }

	      // close the connection
	   httpClient.getConnectionManager().shutdown();
	   return responseString;
	 }
	 private void Getfindinginspection() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
			
			
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"ImportInspectionFindings");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			//System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportInspectionFindings",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			
			//System.out.println("the findin info result is "+temp_result);
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				
			      
			      String visiblesign,wdo_pre,livewdo,wdo_other,wdo_location,
					eve_pre,eve,eve_other,eve_location,
					dam_pre,dam,dam_other,dam_location,
					ReportReqby,Structureonpdf,Reportssendto;
					visiblesign =db.encode(db.convert_null(temp_result.getProperty("VisibleSignofWDO").toString()));
					wdo_pre=db.encode(db.convert_null(temp_result.getProperty("LiveWDO_present").toString()));
					livewdo=db.encode(db.convert_null(temp_result.getProperty("Livewdo_organism").toString()));
					wdo_other=db.encode(db.convert_null(temp_result.getProperty("LiveWDO_organismother").toString()));
					wdo_location=db.encode(db.convert_null(temp_result.getProperty("WDO_Location").toString()));
					
					eve_pre=db.encode(db.convert_null(temp_result.getProperty("Evidence_present").toString()));
					eve=db.encode(db.convert_null(temp_result.getProperty("Evidence_organism").toString()));
					eve_other=db.encode(db.convert_null(temp_result.getProperty("Evidence_organismother").toString()));
					eve_location=db.encode(db.convert_null(temp_result.getProperty("Evidence_Location").toString()));
					
					dam_pre=db.encode(db.convert_null(temp_result.getProperty("Damage_present").toString()));
					dam=db.encode(db.convert_null(temp_result.getProperty("Damage_organism").toString()));
					dam_other=db.encode(db.convert_null(temp_result.getProperty("Damage_organismother").toString()));
					dam_location=db.encode(db.convert_null(temp_result.getProperty("Damage_Location").toString()));
					/*livewdo =db.encode(db.convert_null(temp_result.getProperty("LiveWDO").toString()));
					livewdo_other =db.encode(db.convert_null(temp_result.getProperty("LiveWDO_other").toString()));
					location =db.encode(db.convert_null(temp_result.getProperty("Location").toString()));
					evidence =db.encode(db.convert_null(temp_result.getProperty("Evidence").toString()));
					damage =db.encode(db.convert_null(temp_result.getProperty("Damage").toString()));*/
					ReportReqby =db.convert_null(temp_result.getProperty("ReportReqby").toString());
					Structureonpdf =db.convert_null(temp_result.getProperty("Structureonpdf").toString());
					Reportssendto =db.convert_null(temp_result.getProperty("Reportssendto").toString());
					String Addendum=db.encode(db.convert_null(temp_result.getProperty("Addendumcomments").toString()));
					
					try
					{
						db.CreateTable(26);
						Cursor c=	db.SelectTablefunction(db.Addendum, " WHERE AD_D_SRID='"+cf.selectedhomeid+"'");
						if(c.getCount()>0)
						{
							db.wdo_db.execSQL("UPDATE "+db.Addendum+" SET AD_comments='"+Addendum+"' WHERE AD_D_SRID='"+cf.selectedhomeid+"'");
						}
						else
						{
							db.wdo_db.execSQL("INSERT INTO "+db.Addendum+" (AD_D_InspectorId,AD_D_SRID,AD_comments) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+Addendum+"')");
						}
						if(c!=null)
							c.close();
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					try
					{
						Cursor c =db.SelectTablefunction(db.inspection_finding, " Where IF_SRID='"+cf.selectedhomeid+"'");
						if(c.getCount()>0)
						{
							/*(IF_Id INTEGER PRIMARY KEY AUTOINCREMENT,IF_InspectorId varchar(50) NOT NULL,IF_SRID varchar(50) NOT NULL,FI_Visiblesignofwdo varchar(5),FI_wdo_present varchar(5),FI_Livewdo varchar(50),FI_Livewdo_other varchar(100),FI_Location varchar(100)," +
									",FI_Evidence_present varchar(5),FI_Evidence varchar(50),FI_Evidence_other varchar(100),FI_Evidence_Location varchar(100)," +
									",FI_Damage_present varchar(5),FI_Damage varchar(50),FI_Damage_other varchar(100),FI_Damage_Location varchar(100));");*/
							
							db.wdo_db.execSQL(" UPDATE "+db.inspection_finding+" SET FI_Visiblesignofwdo='"+visiblesign+"',FI_wdo_present='"+wdo_pre+"',FI_Livewdo='"+livewdo+"',FI_Location='"+wdo_location+"',FI_Livewdo_other='"+wdo_other+"'" +
									",FI_Evidence_present='"+eve_pre+"',FI_Evidence='"+eve+"',FI_Evidence_Location='"+eve_location+"',FI_Evidence_other='"+eve_other+"'" +
									",FI_Damage_present='"+dam_pre+"',FI_Damage='"+dam+"',FI_Damage_Location='"+dam_location+"',FI_Damage_other='"+dam_other+"' " +
									"WHERE IF_SRID='"+cf.selectedhomeid+"'");
						}
						else
						{
							db.wdo_db.execSQL(" INSERT INTO "+db.inspection_finding+" (IF_SRID,IF_InspectorId,FI_Visiblesignofwdo,FI_wdo_present,FI_Livewdo,FI_Location,FI_Livewdo_other,FI_Evidence_present,FI_Evidence,FI_Evidence_Location,FI_Evidence_other,FI_Damage_present,FI_Damage,FI_Damage_Location,FI_Damage_other) VALUES " +
									"('"+cf.selectedhomeid+"','"+db.Insp_id+"','"+visiblesign+"','"+wdo_pre+"','"+livewdo+"','"+wdo_location+"','"+wdo_other+"','"+eve_pre+"','"+eve+"','"+eve_location+"','"+eve_other+"','"+dam_pre+"','"+dam+"','"+dam_location+"','"+dam_other+"')");
						}
						if(c!=null)
							c.close();
						String ReportReqby_other,Structureonpdf_other,Reportssendto_other;
						
						if(ReportReqby.contains("Other"))
						{
							ReportReqby_other=ReportReqby.substring(ReportReqby.lastIndexOf("(")+1,ReportReqby.length()-1);
							ReportReqby=ReportReqby.substring(0,ReportReqby.lastIndexOf("("));
						}
						else
						{
							ReportReqby_other="";
						}
						
						if(Structureonpdf.contains("Other"))
						{
							Structureonpdf_other=Structureonpdf.substring(Structureonpdf.lastIndexOf("(")+1,Structureonpdf.length()-1);
							Structureonpdf=Structureonpdf.substring(0,Structureonpdf.lastIndexOf("("));
						}
						else
						{
							Structureonpdf_other="";
						}
						if(Reportssendto.contains("Other"))
						{
							Reportssendto_other=Reportssendto.substring(Reportssendto.lastIndexOf("(")+1,Reportssendto.length()-1);
							Reportssendto=Reportssendto.substring(0,Reportssendto.lastIndexOf("("));
						}
						else
						{
							Reportssendto_other="";
						}
						String handout=db.convert_null(temp_result.getProperty("Handout").toString());
						db.wdo_db.execSQL("UPDATE "+ db.policyholder+ " SET R_Requestedby='"+db.encode(ReportReqby)+"',R_Requestedby_other='"+db.encode(ReportReqby_other)+"'" +
						",R_Requestedto='"+db.encode(Reportssendto)+"',R_Requestedto_other='"+db.encode(Reportssendto_other)+"',structure='"+db.encode(Structureonpdf)+"'" +
						",structure_other='"+db.encode(Structureonpdf_other)+"',hand_out='"+handout+"' where PH_SRID='" + db.encode(cf.selectedhomeid)+ "'");
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}

					
				
			}
		}
	 private void Gettreament() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
			
			
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"ImportTreatment");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportTreatment",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			System.out.println(" the result"+temp_result);
		
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					
					String T_Enable = "",T_observed,T_notice,T_notice_other,T_termitt,T_termitt_other,T_company,T_organism,T_organism_other,T_pesticide,T_terms,T_methodof,T_comments,T_Treatlocation;
					T_Enable=db.convert_null(temp_result.getProperty("Enable").toString());
					T_observed=db.encode(db.convert_null(temp_result.getProperty("Observed").toString()));
					T_notice=db.encode(db.convert_null(temp_result.getProperty("NoticeofInspection").toString()));
					T_notice_other=db.encode(db.convert_null(temp_result.getProperty("Notice_Other").toString()));
					T_company=db.convert_null(temp_result.getProperty("CompanyTreated").toString());
					T_organism=db.encode(db.convert_null(temp_result.getProperty("Organism").toString()));
					T_organism_other=db.encode(db.convert_null(temp_result.getProperty("Organism_Other").toString()));
					T_termitt=db.encode(db.convert_null(temp_result.getProperty("Type").toString()));
					T_termitt_other=db.encode(db.convert_null(temp_result.getProperty("Type_Other").toString()));
					T_pesticide=db.encode(db.convert_null(temp_result.getProperty("NameofPesticide").toString()));
					T_terms=db.encode(db.convert_null(temp_result.getProperty("TermsandConditions").toString()));
					T_methodof=db.encode(db.convert_null(temp_result.getProperty("MethodofTreatment").toString()));
					T_comments=db.encode(db.convert_null(temp_result.getProperty("Comments").toString()));
					T_Treatlocation=db.encode(db.convert_null(temp_result.getProperty("Treatmentlocation").toString()));
					Cursor c =db.SelectTablefunction(db.treatment, " Where T_SRID='"+cf.selectedhomeid+"'");
					if(c.getCount()>0)
					{
						db.wdo_db.execSQL(" UPDATE "+db.treatment+" SET T_Enable='"+T_Enable+"',T_observed='"+T_observed+"',T_noticeofinspection='"+T_notice+"',T_notice_other='"+T_notice_other+"',T_typet='"+T_termitt+"',T_typet_other='"+T_termitt_other+"',T_companyhastreated='"+T_company+"'," +
						"T_organism='"+T_organism+"',T_organism_other='"+T_organism_other+"',T_nameofpesticide='"+T_pesticide+"',T_termsandcon='"+T_terms+"',T_methodoftreat='"+T_methodof+"',T_comments='"+T_comments+"'," +
						"T_Treatlocation='"+T_Treatlocation+"' Where  T_SRID='"+cf.selectedhomeid+"'");
					}
					else
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.treatment+" (T_InspectorId,T_SRID,T_Enable,T_observed,T_noticeofinspection,T_notice_other,T_typet,T_typet_other,T_companyhastreated,T_organism,T_organism_other," +
								"T_nameofpesticide,T_termsandcon,T_methodoftreat,T_comments,T_Treatlocation) VALUES " +
								"('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+T_Enable+"','"+T_observed+"','"+T_notice+"','"+T_notice_other+"','"+T_termitt+"','"+T_termitt_other+"','"+T_company+"','"+T_organism+"','"+T_organism_other+"'," +
								"'"+T_pesticide+"','"+T_terms+"','"+T_methodof+"','"+T_comments+"','"+T_Treatlocation+"')");
					}
					if(c!=null)
						c.close();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}

					
				
			}
		}
	 private void GetNoAccessArea() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException 
	 {
			SoapObject request = new SoapObject(wb.NAMESPACE,"ImportNoAccess");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportNoAccess",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			
		//	System.out.println("the findin info result is "+temp_result);
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					db.wdo_db.execSQL(" DELETE FROM "+db.no_access+" WHERE  N_SRID='"+cf.selectedhomeid+"'");
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
					
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						String N_Area=db.encode(db.convert_null(na.getProperty("InAccessibleArea").toString()));
						String N_Area_other=db.encode(db.convert_null(na.getProperty("InAccessibleArea_Other").toString()));
						String N_SArea=db.encode(db.convert_null(na.getProperty("SpecificArea").toString()));
						String N_SArea_other=db.encode(db.convert_null(na.getProperty("SpecificArea_Other").toString()));
						String N_Reason=db.encode(db.convert_null(na.getProperty("Reason").toString()));
						Cursor c=db.SelectTablefunction(db.no_access, " WHERE  N_SRID='"+cf.selectedhomeid+"' and N_Area='"+N_Area+"' ");
						if(c.getCount()>0)
						{
							db.wdo_db.execSQL(" UPDATE "+db.no_access+" SET N_Area='"+N_Area+"',N_Area_other='"+N_Area_other+"',N_SArea='"+N_SArea+"',N_SArea_other='"+N_SArea_other+"',N_Reason='"+N_Reason+"' WHERE N_SRID='"+cf.selectedhomeid+"' AND  N_Area='"+N_Area+"'");
						}
						else
						{
							db.wdo_db.execSQL(" INSERT INTO  "+db.no_access+" (N_InspectorId,N_SRID,N_Area,N_Area_other,N_SArea,N_SArea_other,N_Reason) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+N_Area+"','"+N_Area_other+"','"+N_SArea+"','"+N_SArea_other+"','"+N_Reason+"')");
						}
						if(c!=null)
							c.close();
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues is "+e.getMessage());
				
				}
			}
	 }
	 private void Getcommentsandfina() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
			
			
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"ImportCommentsFinancial");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportCommentsFinancial",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					
					String comments;
					
					comments=db.encode(db.convert_null(temp_result.getProperty("Comments").toString()));
					
					Cursor c =db.SelectTablefunction(db.CommentsandFinancials, " WHERE CF_SRID='"+cf.selectedhomeid+"'");
					if(c.getCount()>0)
					{
						db.wdo_db.execSQL(" UPDATE "+db.CommentsandFinancials+" SET CF_Commnents='"+comments+"' WHERE CF_SRID='"+cf.selectedhomeid+"'");
						
					}
					else
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.CommentsandFinancials+" (CF_InspectorId,CF_SRID,CF_Commnents) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+comments+"')");
					}
					if(c!=null)
						c.close();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}

					
				
			}
		}
	 private void GetQA_question()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException  {
			// TODO Auto-generated method stub
			
			
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"ImportQAquestion");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			//System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportQAquestion",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			
		//	System.out.println("the findin info result is "+temp_result);
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					
					
					
					
					String kitchen,living_R,Formal_R,Dining_R,Master_BR,Master_BaR,Bedroom1,Bedroom2,Bedroom3,Bedroom4,Bedroom5,Bedroom6,Hallways
					,Closets,Hall_BaR,Half_BaR,Florida_R,Garage,Storage,Outbuildings,PoolHouse;
					kitchen=db.convert_null(temp_result.getProperty("Kitchen").toString());
					living_R=db.convert_null(temp_result.getProperty("Living").toString());
					Formal_R=db.convert_null(temp_result.getProperty("Formal").toString());
					Dining_R=db.convert_null(temp_result.getProperty("Dining").toString());
					Master_BR=db.convert_null(temp_result.getProperty("Master").toString());
					Master_BaR=db.convert_null(temp_result.getProperty("Master_Bar").toString());
					Bedroom1=db.convert_null(temp_result.getProperty("Bedroom1").toString());
					Bedroom2=db.convert_null(temp_result.getProperty("Bedroom2").toString());
					Bedroom3=db.convert_null(temp_result.getProperty("Bedroom3").toString());
					Bedroom4=db.convert_null(temp_result.getProperty("Bedroom4").toString());
					Bedroom5=db.convert_null(temp_result.getProperty("Bedroom5").toString());
					Bedroom6=db.convert_null(temp_result.getProperty("Bedroom6").toString());
					Hallways=db.convert_null(temp_result.getProperty("Hallways").toString());
					Closets=db.convert_null(temp_result.getProperty("Closets").toString());
					Hall_BaR=db.convert_null(temp_result.getProperty("Hall_Bar").toString());
					Half_BaR=db.convert_null(temp_result.getProperty("Half_Bar").toString());
					Florida_R=db.convert_null(temp_result.getProperty("Florida").toString());
					Garage=db.convert_null(temp_result.getProperty("Garage").toString());
					Storage=db.convert_null(temp_result.getProperty("Storage").toString());
					Outbuildings=db.convert_null(temp_result.getProperty("Outbuildings").toString());
					PoolHouse=db.convert_null(temp_result.getProperty("PoolHouse").toString());
					Cursor c=db.SelectTablefunction(db.QA_question, " WHERE QA_SRID='"+cf.selectedhomeid+"'");
					if(c.getCount()>0)
					{
						db.wdo_db.execSQL(" UPDATE "+db.QA_question+" SET kitchen='"+kitchen+"',living_R='"+living_R+"',Formal_R='"+Formal_R+"',Dining_R='"+Dining_R+
						"',Master_BR='"+Master_BR+"',Master_BaR='"+Master_BaR+"',Bedroom1='"+Bedroom1+"',Bedroom2='"+Bedroom2+"',Bedroom3='"+Bedroom3+"',Bedroom4='"+
						Bedroom4+"',Bedroom5='"+Bedroom5+"',Bedroom6='"+Bedroom6+"',Hallways='"+Hallways+"',Closets='"+Closets+"',Hall_BaR='"+Hall_BaR+"',Half_BaR='"+Half_BaR+"',Florida_R='"+Florida_R+"'," +"Garage='"+
						Garage+"',Storage='"+Storage+"',Outbuildings='"+Outbuildings+"',PoolHouse='"+PoolHouse+"' WHERE QA_SRID='"+cf.selectedhomeid+"'");
					}
					else
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.QA_question+" (QA_InspectorId,QA_SRID,kitchen,living_R,Formal_R,Dining_R,Master_BR,Master_BaR,Bedroom1,Bedroom2,Bedroom3,Bedroom4,Bedroom5,Bedroom6,Hallways" +
						",Closets,Hall_BaR,Half_BaR,Florida_R,Garage,Storage,Outbuildings,PoolHouse) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"'," +
						"'"+kitchen+"','"+living_R+"','"+Formal_R+"','"+Dining_R+"','"+Master_BR+"','"+Master_BaR+"','"+Bedroom1+"','"+Bedroom2+"','"+Bedroom3+"','"+Bedroom4+"','"+Bedroom5+"','"+Bedroom6+"','"+Hallways+"','"+Closets+"','"+Hall_BaR+"','"+Half_BaR+"','"+Florida_R+"','"+Garage+"','"+Storage+"','"+Outbuildings+"','"+PoolHouse+"')");
					}
					if(c!=null)
						c.close();
					dynamic_qa();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}

					
				
			}
		}
	 private void dynamic_qa()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException   {

		// TODO Auto-generated method stub
			SoapObject request = new SoapObject(wb.NAMESPACE,"ImportQAquestionOther");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			//System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportQAquestionOther",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			
		//	System.out.println("the findin info result is "+temp_result);
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					db.wdo_db.execSQL(" DELETE FROM "+db.QA_question_dynamic+" WHERE QA_D_SRID='"+cf.selectedhomeid+"'");
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
						
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						String title=db.encode(db.convert_null(na.getProperty("Title").toString()));
						String QuestionOption=db.encode(db.convert_null(na.getProperty("QuestionOption").toString()));
						
						Cursor c =db.SelectTablefunction(db.QA_question_dynamic, " WHERE QA_D_SRID='"+cf.selectedhomeid+"' and QA_D_title='"+title+"'");
						if(c.getCount()>0)
						{
							db.wdo_db.execSQL(" UPDATE "+db.QA_question_dynamic+" SET QA_D_option='"+QuestionOption+"' WHERE QA_D_SRID='"+cf.selectedhomeid+"' and QA_D_title='"+title+"'");
						}
						else
						{
							db.wdo_db.execSQL(" INSERT INTO "+db.QA_question_dynamic+" (QA_D_InspectorId,QA_D_SRID,QA_D_title,QA_D_option) VALUES" +
									" ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+title+"','"+QuestionOption+"')");
							
							/*ed_tit.setText("");
							RG[19].clearCheck();
							((RadioButton) (RG[19].findViewWithTag("No"))).setChecked(true);*/
							
						}
						if(c!=null)
							c.close();
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues is "+e.getMessage());
				
				}
			}
	}
	 public void Getimage()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException  
	 {
			SoapObject request = new SoapObject(wb.NAMESPACE,"ImportElevationImg");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			//System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportElevationImg",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					File f=new File(Environment.getExternalStorageDirectory()+"/WDO Inspection");
					if(!f.exists())
					{
						f.mkdir();
						(new File(f,"/imported_image")).mkdir();
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
						
					}
					else if(!(new File(f,"/imported_image")).exists())
					{
						(new File(f,"/imported_image")).mkdir();
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
					}
					else if(!(new File(f,"/imported_image/"+cf.selectedhomeid)).exists())
					{
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
					}
					f=new File(f,"/imported_image/"+cf.selectedhomeid);
					Double count_p=25.00/Double.parseDouble(temp_result.getPropertyCount()+"");
					
					db.wdo_db.execSQL(" DELETE FROM "+db.ImageTable+" WHERE IM_SRID='"+cf.selectedhomeid+"'");
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
						progress_text=" Image "+(i+1)+" of "+temp_result.getPropertyCount()+" images.";
					
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						
						int elevation_no=0;
						String Elevation=db.convert_null(na.getProperty("Elevation").toString());
						
						for(int m=0;m<cf.elev.length;m++)
						{
							if(Elevation.trim().equals(cf.elev[m])) // convert the elevation text to no
							{
								elevation_no=m;
								break;
							}
						}
						
						String Path=db.convert_null(na.getProperty("Path").toString());
						String Description=db.encode(db.convert_null(na.getProperty("Description").toString()));
						String ImageOrder=db.encode(db.convert_null(na.getProperty("ImageOrder").toString()));
						//String CreatedOn=db.encode(db.convert_null(na.getProperty("CreatedOn").toString()));
						
						URL ulrn = new URL(Path.replace(" ", "%20"));
						HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						InputStream is = con.getInputStream();
					    
						Bitmap bmp = BitmapFactory.decodeStream(is);
					    ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
					    byte[] b = baos.toByteArray();
					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
					    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);
					    String FILENAME= cf.selectedhomeid+Elevation+ImageOrder+Path.substring(Path.lastIndexOf("."));
					    try
						{
							 
							
							
							File f1=new File(f,FILENAME);
							if(f1.exists())
							{
								f1.delete();
							}
							
							FileOutputStream fos = new FileOutputStream(f1);
							fos.write(decode3);
							fos.close();	
						}
						catch (IOException e){
							System.out.println("we found isseus in the webservice "+e.getMessage());
						}
						//Cursor c=db.SelectTablefunction(db.ImageTable, " WHERE  IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+Elevation+"' and IM_ImageOrder='"+ImageOrder+"'  ");
						//db.wdo_db.execSQL("DELETE FROM  "+db.ImageTable+ " WHERE  IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+elevation_no+"' and IM_ImageOrder='"+ImageOrder+"'" );
							db.wdo_db.execSQL("INSERT INTO "
									+ db.ImageTable
									+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
									+ " VALUES ('"+db.Insp_id+"','" + cf.selectedhomeid + "','"+elevation_no+ "','"+ db.encode(Environment.getExternalStorageDirectory()+"/WDO Inspection/imported_image/"+cf.selectedhomeid+"/"+FILENAME) + "','"
									+ Description+ "','" + ImageOrder + "')");
							total+=count_p;
						
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues is "+e.getMessage());
				
				}
			}
		 
	 }
	 protected void Getfeedback()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
	
	        SoapObject request = new SoapObject(wb.NAMESPACE,"ImportFeedbackInformaion");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportFeedbackInformaion",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
			
			
			
			
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					String CustomerServiceEvaluation=db.encode(db.convert_null(temp_result.getProperty("CustomerServiceEvaluation").toString()));
					String WhoPresent=db.encode(db.convert_null(temp_result.getProperty("WhoPresent").toString()));
					String WhoPresent_Other=db.encode(db.convert_null(temp_result.getProperty("WhoPresent_Other").toString()));
					String CustomerSatisfied=db.encode(db.convert_null(temp_result.getProperty("CustomerSatisfied").toString()));
					String comments=db.encode(db.convert_null(temp_result.getProperty("Comments").toString()));
					
					Cursor c1=db.SelectTablefunction(db.feedback_infomation, " WHERE FD_SRID='"+cf.selectedhomeid+"'");
					if(c1.getCount()>0)
					{
						db.wdo_db.execSQL("UPDATE "+db.feedback_infomation+" SET FD_CSC='"+CustomerServiceEvaluation+"',FD_whowas='"+WhoPresent+"',FD_whowas_other='"+WhoPresent_Other+"',FD_issatisfied='"+CustomerSatisfied+"',FD_comments='"+comments+"' WHERE FD_SRID='"+cf.selectedhomeid+"'");
					}
					else
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.feedback_infomation+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_issatisfied,FD_comments) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+CustomerServiceEvaluation+"','"+WhoPresent+"','"+WhoPresent_Other+"','"+CustomerSatisfied+"','"+comments+"')");
					}
					if(c1!=null)
						c1.close();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}
				
					total=80;
					Getfeedbackdocument();
				
			}
		}
	 private void Getfeedbackdocument() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException{
		// TODO Auto-generated method stub
		 SoapObject request = new SoapObject(wb.NAMESPACE,"ImportFeedbackDocument");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			request.addProperty("InspectorId",db.Insp_id);
			//System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportFeedbackDocument",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();
		System.out.println(" pdf result "+temp_result);
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					File f=new File(Environment.getExternalStorageDirectory()+"/WDO Inspection");
					db.wdo_db.execSQL("DELETE FROM  "+db.feedback_document+ " WHERE  FD_D_SRID='"+cf.selectedhomeid+"' " );
					if(!f.exists())
					{
						f.mkdir();
						(new File(f,"/imported_image")).mkdir();
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
						
					}
					else if(!(new File(f,"/imported_image")).exists())
					{
						(new File(f,"/imported_image")).mkdir();
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
					}
					else if(!(new File(f,"/imported_image/"+cf.selectedhomeid)).exists())
					{
						(new File(f,"/imported_image/"+cf.selectedhomeid)).mkdir();
					}
					f=new File(f,"/imported_image/"+cf.selectedhomeid);
					Double count_p=20.00/Double.parseDouble(temp_result.getPropertyCount()+"");
				
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
						progress_text=" Feedback Documents "+(i+1)+" of "+temp_result.getPropertyCount()+" Documents.";
					
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						
						int elevation_no=0;
						 
						String title=db.convert_null(na.getProperty("DocumentName").toString());
						String title_other=db.encode(db.convert_null(na.getProperty("DocumentName_other").toString()));
						String ImageOrder=db.encode(db.convert_null(na.getProperty("ImageOrder").toString()));
						//String CreatedOn=db.encode(db.convert_null(na.getProperty("Description").toString()));
						String Path=db.convert_null(na.getProperty("Path").toString());
						String type=db.encode(db.convert_null(na.getProperty("Type").toString()));
						type=(type.equals("true"))? "1":"0";
						URL ulrn = new URL(Path.replace(" ", "%20"));
						System.out.println(" the path "+Path);
						HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						InputStream is = con.getInputStream();
						/*Bitmap bmp = BitmapFactory.decodeStream(is);
					    ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
					    byte[] b = baos.toByteArray();
					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
					    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);*/
					    String FILENAME= cf.selectedhomeid+"feedback_"+title+ImageOrder+Path.substring(Path.lastIndexOf("."));
					   
					    try
						{
					    	
							 
					    	File f1=new File(f,FILENAME);
					    	if(f1.exists())
							{
								f1.delete();
							}
					    	FileOutputStream fos = new FileOutputStream(f1);
						    byte[] buffer = new byte[4096];
				            int length;
				            while ((length = is.read(buffer))>0){
				            	fos.write(buffer, 0, length);
				            }
				            fos.flush();
				            fos.close();
				            con.disconnect();
				            
				            
							
							
							
							
						}
						catch (IOException e){
							System.out.println("we found isseus in the webservice "+e.getMessage());
						}
						//Cursor c=db.SelectTablefunction(db.ImageTable, " WHERE  IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+Elevation+"' and IM_ImageOrder='"+ImageOrder+"'  ");
						
						db.wdo_db.execSQL(" INSERT INTO "+db.feedback_document+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
								"('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+title+"','"+title_other+"','"+db.encode(Environment.getExternalStorageDirectory()+"/WDO Inspection/imported_image/"+cf.selectedhomeid+"/"+FILENAME)+"','"+type+"') ");
							total+=count_p;
						
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues is "+e.getMessage());
				
				}
			}
	}

	 
	 @Override
		protected Dialog onCreateDialog(int id) {
					switch (id) {
					case 1:
						progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
						progDialog.setMax(100);	
						progDialog.setMessage(Html.fromHtml("Please be patient as we import your data and don't lock your screen it may affect your import.<br> Current importing Status : "+progress_text));	
						progDialog.setCancelable(false);	
						progThread = new ProgressThread(handler1);	
						progThread.start();	
						return progDialog;
					default:
						return null;
					}
				}
	 
		class ProgressThread extends Thread {


		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			
			while (mState == RUNNING) {
				try {
					
					// Control speed of update (but precision of delay not
					// guaranteed)
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					//Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", (int)total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			mState = state;
		}

	}
		/** Upload policy holder information Starts **/
	
	

		 	final Handler handler1 = new Handler() {
				public void handleMessage(Message msg) {
					// Get the current value of the variable total from the message data
					// and update the progress bar.
					
					int total = msg.getData().getInt("total");
					progDialog.setProgress(total);
					progDialog.setMessage(Html.fromHtml("To edit your inspection, we are importing your data back into your system. An internet connection is required. Please do not lock your screen. Current importing Status : "+progress_text));
					if (total == 100) {
						progDialog.setCancelable(true);
						dismissDialog(1);
						progThread.setState(ProgressThread.DONE);
				 	}

				}
			};
			 final Handler handler = new Handler() {

					public void handleMessage(Message msg) {
						 if(usercheck==1)
						 {
							 usercheck = 0;
							 cf.show_toast("Internet connection not available.",0);
						//	 startActivity(new Intent(Import.this, Import.class));
							 finish();
						 }
						 else if(usercheck==2)
						 {
							 usercheck = 0;
							 cf.show_toast("There is a problem on your network, so please try again later with better network.",0);
							 //startActivity(new Intent(Import.this, Import.class));
							 finish();
						 }
						 else if(usercheck==3)
						 {
							 usercheck = 0;
							 cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",0);
							// startActivity(new Intent(Import.this, Import.class));
							 finish();
						 }
						 else if(usercheck==4)
						 {
							 usercheck=0;
							 progDialog.dismiss();
							 final Dialog dialog1 = new Dialog(Reterive_data.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alert);
								dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
								LinearLayout li=(LinearLayout)dialog1.findViewById(R.id.help_alert);
								li.setVisibility(View.VISIBLE);
								TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
								txttitle.setText("Import");
								TextView txt = (TextView) li.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml("Retrieving data successfully completed"));
								ImageView btn_helpclose = (ImageView) li.findViewById(R.id.helpclose);
								btn_helpclose.setOnClickListener(new OnClickListener()
								{
								public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										finish();
									}
									
								});
								Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
								btn_ok.setVisibility(View.VISIBLE);
								btn_ok.setOnClickListener(new OnClickListener()
								{
								public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										Intent in =new Intent(Reterive_data.this,policyholderInformation.class);
										in.putExtra("SRID", cf.selectedhomeid);
										startActivity(in);
									}
									
								});
								
								dialog1.setCancelable(false);
								dialog1.show();
						
							// startActivity(new Intent(Import.this, Import.class));
						 }
						if(wl!=null)
							{
							wl.release();
							wl=null;
							}
					}

				};
				



}
