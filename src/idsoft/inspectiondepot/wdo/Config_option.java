package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.AddComments.del_clickerlistenr;
import idsoft.inspectiondepot.wdo.AddComments.edit_clicker;
import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class Config_option extends Activity {
Spinner sp1,sp2;
LinearLayout li_dy;
Button add_opt;
String module ="0";
String sp_val1[]={"--Select--","Inspection Finding","Treatment","No Access Area"},
sp_val11[]={"--Select--","Name of Organism"},
sp_val21[]={"--Select--","Notice of inspection has been affixed to the structure at","Type of termites","Organism"},
sp_val31[]={"--Select--","Not Accessible Area","Specific Areas"},
sp_dummy[]={"--Select--"};
DataBaseHelper db;
CommonFunction cf;
String d_type="radio";
int change=0;
int button[];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.config_option);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		declaration();
	}
	private void declaration() {
		// TODO Auto-generated method stub
	
		
		sp1=(Spinner) findViewById(R.id.config_option_SPmoduletype);
		sp2=(Spinner) findViewById(R.id.config_option_SPquestype);
		
		sp2.setEnabled(false);
		li_dy=(LinearLayout) findViewById(R.id.config_option_lidy);
		add_opt=(Button) findViewById(R.id.add_option);
		
        ((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(Config_option.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});

		ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,sp_val1);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp1.setAdapter(ad);
		ArrayAdapter ad1 =new ArrayAdapter(this, android.R.layout.simple_spinner_item,sp_dummy);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp2.setAdapter(ad1);
		sp1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp1.getSelectedItemPosition()==0)
				{
					sp2.setSelection(0);
					sp2.setEnabled(false);
				}
				else
				{
					sp2.setEnabled(true);
					if(sp1.getSelectedItemPosition()==1)
					{
						ArrayAdapter ad1 =new ArrayAdapter(Config_option.this, android.R.layout.simple_spinner_item,sp_val11);
						ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp2.setAdapter(ad1);
					}
					else if(sp1.getSelectedItemPosition()==2)
					{
						ArrayAdapter ad1 =new ArrayAdapter(Config_option.this, android.R.layout.simple_spinner_item,sp_val21);
						ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp2.setAdapter(ad1);
					}
					else if(sp1.getSelectedItemPosition()==3)
					{
						ArrayAdapter ad1 =new ArrayAdapter(Config_option.this, android.R.layout.simple_spinner_item,sp_val31);
						ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp2.setAdapter(ad1);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp2.getSelectedItemPosition()!=0)
				{
					if(sp2.getSelectedItem().toString().trim().equals(sp_val31[1]))
					{
						add_opt.setVisibility(View.INVISIBLE);
					}
					else
					{
						add_opt.setVisibility(View.VISIBLE);
					}
					
					show_option(sp1.getSelectedItemPosition(),sp2.getSelectedItemPosition());
					
				}
				else
				{
					add_opt.setVisibility(View.INVISIBLE);
					li_dy.removeAllViews();
					
				}
			}

			

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	private void show_option(int mtype,int qtype) {
		// TODO Auto-generated method stub
		
		if(mtype==1)
		{
			module="22";
		}
		else if(mtype==2)
		{
			module="23";
		}
		else if(mtype==3)
		{
			module="24";
		}
		Cursor c=db.SelectTablefunction(db.config_option, " WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' AND CP_Question='"+qtype+"' ORDER by CP_Order");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			
			View v;
			li_dy.removeAllViews();
			LinearLayout li_tit=new LinearLayout(this);
			li_tit.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
			li_tit.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithsubtabbackgroundcolor));
			TextView tv_no=new TextView(this,null,R.attr.textview_200);
			tv_no.setText("No");
			li_tit.addView(tv_no,50,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			li_tit.addView(v,2,LayoutParams.FILL_PARENT);
			
			TextView tv_opt=new TextView(this,null,R.attr.textview_200);
			tv_opt.setText("Option List");
			li_tit.addView(tv_opt,350,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			li_tit.addView(v,2,LayoutParams.FILL_PARENT);
			
			TextView tv_default=new TextView(this,null,R.attr.textview_200);
			tv_default.setText("Default");
			li_tit.addView(tv_default,100,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			li_tit.addView(v,2,LayoutParams.FILL_PARENT);
			
			TextView tv_edit=new TextView(this,null,R.attr.textview_200);
			tv_edit.setText("Edit/Delete");
			li_tit.addView(tv_edit,200,LayoutParams.WRAP_CONTENT);
			li_dy.addView(li_tit,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			button=new int[c.getCount()];
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				
				String option_n,edit,defa;
				int id=c.getInt(c.getColumnIndex("CP_Id"));
				option_n=db.decode(c.getString(c.getColumnIndex("CP_Option")));
				d_type=c.getString(c.getColumnIndex("CP_optiontype"));
				edit=c.getString(c.getColumnIndex("CP_editable"));
				defa=c.getString(c.getColumnIndex("CP_Default_op"));
			/*	v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				li_tit.addView(v,LayoutParams.FILL_PARENT,2);*/
				
				LinearLayout li_opt=new LinearLayout(this);
				
				TextView tv_no1=new TextView(this,null,R.attr.textview_200_inner);
				tv_no1.setText((i+1)+"");
				li_opt.addView(tv_no1,50,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				li_opt.addView(v,2,LayoutParams.FILL_PARENT);
				
				TextView tv_opt1=new TextView(this,null,R.attr.textview_200_inner);
				tv_opt1.setText(option_n);
				li_opt.addView(tv_opt1,350,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				li_opt.addView(v,2,LayoutParams.FILL_PARENT);
				LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(75,LayoutParams.WRAP_CONTENT);
				lp.setMargins(25, 0, 0, 0);
				if(d_type.equals("radio"))
				{
					RadioButton option_de=new RadioButton(this);
					
					button[i]=1000+i;
					option_de.setId(button[i]);
					if(defa.equals("1"))
					{
						option_de.setChecked(true);
					}
					li_opt.addView(option_de,lp);
					option_de.setOnCheckedChangeListener(new default_rdclicker(id,i));
					
				}
				else if(d_type.equals("chk"))
				{
					CheckBox option_de=new CheckBox(this);
					button[i]=1000+i;
					option_de.setId(button[i]);
					if(defa.equals("1"))
					{
						option_de.setChecked(true);
					}
					li_opt.addView(option_de,lp);
					option_de.setOnClickListener(new default_clicker(id,d_type));
				}
				
			
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				li_opt.addView(v,2,LayoutParams.FILL_PARENT);
				
				LinearLayout li=new LinearLayout(this);
				ImageView im =new ImageView(this);
				im.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofedit));
				ImageView im1 =new ImageView(this);
				im1.setBackgroundDrawable(getResources().getDrawable(R.drawable.close));
				li.addView(im,43,43);
				li.addView(im1,50,50);
				im.setOnClickListener(new edit_clicker(id,option_n));
				im1.setOnClickListener(new del_clickerlistenr(id));
				li.setGravity(Gravity.CENTER);
				
				li_opt.addView(li, 200,LayoutParams.WRAP_CONTENT);
				li.setPadding(5, 10, 5, 10);
				li_opt.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithoutcurve));
				if(edit.equals("0"))
				{
					li.setVisibility(View.INVISIBLE);
				}
				li_dy.addView(li_opt);
				
			}
		}
		if(c!=null)
			c.close();
	}
	class default_rdclicker implements OnCheckedChangeListener
	{
		int id; int i;
		public default_rdclicker(int id, int i) {
			// TODO Auto-generated constructor stub
			this.id=id;
			this.i=i;
		}
		@Override
		public void onCheckedChanged(final CompoundButton v,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
			if(isChecked ||(!isChecked && change==0) )
			{
			AlertDialog.Builder b =new AlertDialog.Builder(Config_option.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to change the default selection for selected Option?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Default_op='0' WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' AND CP_Question='"+sp2.getSelectedItemPosition()+"'");
							for(int i=0;i<button.length;i++)
							{
								
								if(button[i]!=v.getId())
								{
									change=1;
									((RadioButton)li_dy.findViewById(button[i])).setChecked(false);
									
								}
								else
								{
									change=1;
									((RadioButton)li_dy.findViewById(button[i])).setChecked(true);
									
									db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Default_op='1' WHERE CP_Id='"+id+"' ");
								}
								
							}
							change=0;
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					change=1;
					if(((RadioButton)v).isChecked())
					{
						((RadioButton)v).setChecked(false);
						
						
					}
					else
					{
						
						((RadioButton)v).setChecked(true);
					}
					change=0;
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show();
			}
			
		}
	}
	class default_clicker implements OnClickListener
	{
		int id; String type;
		public default_clicker(int id, String type) {
			// TODO Auto-generated constructor stub
			this.id=id;
			this.type=type;
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			AlertDialog.Builder b =new AlertDialog.Builder(Config_option.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to change the default selection for selected Option?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						
							if(((CheckBox)v).isChecked())
							{
								
								db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Default_op='1' WHERE CP_Id='"+id+"' ");
							}
							else
							{
								
								db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Default_op='0' WHERE CP_Id='"+id+"' ");
							}
					
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
						if(((CheckBox)v).isChecked())
						{
							((CheckBox)v).setChecked(false);
							
						}
						else
						{
							
							((CheckBox)v).setChecked(true);
						}
					}
					
				
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
			
		}

		
	}
	class del_clickerlistenr implements OnClickListener
	{
		int id;
		
		
		public del_clickerlistenr(int id) {
			// TODO Auto-generated constructor stub
			this.id=id;
		
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			
			AlertDialog.Builder b =new AlertDialog.Builder(Config_option.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to delete the selected Option?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wdo_db.execSQL(" DELETE FROM  "+db.config_option+"  WHERE CP_Id='"+id+"'");
						cf.show_toast("Selected Option deleted successfully", 1);
						show_option(sp1.getSelectedItemPosition(),sp2.getSelectedItemPosition());
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
		}
		
	}
	class edit_clicker implements OnClickListener
	{
		int id;
		String comments;
		edit_clicker(int id,String comment)
		{
			this.id=id;
			comments=comment;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final Dialog dialog1 = new Dialog(Config_option.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
			ed.setText(comments);
			Button save=((Button)dialog1.findViewById(R.id.save));
			Button clear=((Button)dialog1.findViewById(R.id.clear));
			ImageView close=((ImageView)dialog1.findViewById(R.id.helpclose));
			((TextView) dialog1.findViewById(R.id.txthead)).setText("Edit Option");
			((TextView) dialog1.findViewById(R.id.txtquestio)).setText("Please enter the option");
			close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
				}
			});
			clear.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ed.setText("");
				}
			});
			save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(!ed.getText().toString().trim().equals(""))
					{
						if(add_more_option(ed.getText().toString().trim(),id))
						{
						dialog1.dismiss();
						show_option(sp1.getSelectedItemPosition(),sp2.getSelectedItemPosition());
						}
					}
					else
					{
						cf.show_toast("Please enter Option ", 0);
					}
				}
			});
			dialog1.show();
		}
		
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.add_option:
				final Dialog dialog1 = new Dialog(Config_option.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alert);
				final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
				
				Button save=((Button)dialog1.findViewById(R.id.save));
				Button clear=((Button)dialog1.findViewById(R.id.clear));
				ImageView close=((ImageView)dialog1.findViewById(R.id.helpclose));
				((TextView) dialog1.findViewById(R.id.txthead)).setText("Add Option");
				((TextView) dialog1.findViewById(R.id.txtquestio)).setVisibility(View.GONE);
				close.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
					}
				});
				clear.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ed.setText("");
					}
				});
				save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(!ed.getText().toString().trim().equals(""))
						{
							if(add_more_option(ed.getText().toString().trim(),0))
							{
							dialog1.dismiss();
							show_option(sp1.getSelectedItemPosition(),sp2.getSelectedItemPosition());
							}
						}
						else
						{
							cf.show_toast("Please enter Option ", 0);
						}
					}
				});
				dialog1.show();
			break;
			case R.id.clear_all:
				AlertDialog.Builder b =new AlertDialog.Builder(Config_option.this);
				b.setTitle("Confirmation");
				b.setMessage("Do you want to clear all the default selection?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						try
						{
							db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Default_op='0' WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' AND CP_Question='"+sp2.getSelectedItemPosition()+"'");
							show_option(sp1.getSelectedItemPosition(),sp2.getSelectedItemPosition());
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("the exeption in delete"+e.getMessage());
						}
						
						
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
			break;
		}	
	}
	protected boolean add_more_option(String opt,int id) {
		// TODO Auto-generated method stub
		if(id==0)
		{
			Cursor c =db.SelectTablefunction(db.config_option, " WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' AND CP_Question='"+sp2.getSelectedItemPosition()+"' AND CP_Option='"+db.encode(opt)+"' ");
			if(c.getCount()>0)
			{
				cf.show_toast("Entered Caption already available", 0);
				if(c!=null)
					c.close();
				return false;
			}
			else
			{
				c =db.wdo_db.rawQuery(" SELECT max(CP_Order) as CP_Order from "+db.config_option+" WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' and CP_Option<>'"+db.encode("Other")+"'",null);
				if(c.getCount()>0)
				{
					c.moveToFirst();
					int img_order=c.getInt(c.getColumnIndex("CP_Order"));
					db.wdo_db.execSQL("INSERT INTO "+db.config_option+" (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Order,CP_optiontype) VALUES " +
							"('"+db.Insp_id+"','"+module+"','"+sp2.getSelectedItemPosition()+"','"+db.encode(opt)+"','"+img_order+"','"+d_type+"')");
					return true;
						
				}
				if(c!=null)
					c.close();
				return false;
			}
			
		}
		else
		{
			Cursor c =db.SelectTablefunction(db.config_option, " WHERE CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='"+module+"' AND CP_Question='"+sp2.getSelectedItemPosition()+"' AND CP_Option='"+db.encode(opt)+"' and CP_Id<>'"+id+"' ");
			
			if(c.getCount()>0)
			{
				cf.show_toast("Entered Caption already available", 0);
				if(c!=null)
					c.close();
				return false;
			}
			else
			{
				
				db.wdo_db.execSQL(" UPDATE "+db.config_option+" SET CP_Option='"+db.encode(opt)+"' WHERE CP_Id=='"+id+"'");
				if(c!=null)
					c.close();
				return true;
				
			}
			
		}
	}
	
}
    