package idsoft.inspectiondepot.wdo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.w3c.dom.Text;


import idsoft.inspectiondepot.wdo.Feedback.my_dyclicker;
import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.phone_nowatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class Addendum extends Activity {
CommonFunction cf;
public Uri CapturedImageURI;
Spinner sp_elev;
TextView no_uploaded;
LinearLayout upload_img;
DataBaseHelper db;
Button save;
EditText ed;
String saved_val[];
private int maximumindb,maximumallowed=8;
private boolean load_comment=true;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addendum);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Addendum",0,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,6,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		ed=(EditText) findViewById(R.id.addendum_comment);
		ed.addTextChangedListener(new TextWatchLimit(ed, 5000, ((TextView) findViewById(R.id.addendum_TVcomment))));
		Cursor c =db.SelectTablefunction(db.Addendum, " WHERE AD_D_SRID='"+cf.selectedhomeid+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			ed.setText(db.decode(c.getString(c.getColumnIndex("AD_comments"))));
			c.close();
		}
		
		
		cf.setTouchListener(findViewById(R.id.content));
	}
	public void clicker(View v)
	{
		if(R.id.save== v.getId())
		{
			db.CreateTable(26);
			if(!ed.getText().toString().trim().equals(""))
			{
				try
				{
					Cursor c=	db.SelectTablefunction(db.Addendum, " WHERE AD_D_SRID='"+cf.selectedhomeid+"'");
					if(c.getCount()>0)
					{
						db.wdo_db.execSQL("UPDATE "+db.Addendum+" SET AD_comments='"+db.encode(ed.getText().toString().trim())+"' WHERE AD_D_SRID='"+cf.selectedhomeid+"'");
					}
					else
					{
						db.wdo_db.execSQL("INSERT INTO "+db.Addendum+" (AD_D_InspectorId,AD_D_SRID,AD_comments) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+db.encode(ed.getText().toString().trim())+"')");
					}
					if(c!=null)
						c.close();
					cf.show_toast("Addendum saved successfully", 0);
					Intent in = new Intent(Addendum.this,QA_question.class);
					in.putExtra("SRID", cf.selectedhomeid);
					startActivity(in);
					if(c!=null)
						c.close();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			else
			{
				cf.show_toast("Please enter Addendum", 0);
			}
		}
		else if(R.id.hme==v.getId())
		{
			cf.go_home();
		}
		else if(R.id.clear==v.getId())
		{
			ed.setText("");
		}
		else if(R.id.addendum_loadcomments==v.getId())
		{
		int len=ed.getText().toString().length();
		
		if(load_comment )
		{
			load_comment=false;
			int loc[] = new int[2];
			v.getLocationOnScreen(loc);
			Intent in =new Intent(this,Load_comments.class);
			in.putExtra("id", R.id.addendum_loadcomments);
			in.putExtra("srid", cf.selectedhomeid);
			in.putExtra("question",9);
			in.putExtra("max_length", 5000);
			in.putExtra("cur_length", len);
			in.putExtra("xfrom", loc[0]+10);
			in.putExtra("yfrom", loc[1]+10);
			startActivityForResult(in, cf.loadcomment_code);
		}
		}
		
		}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==cf.loadcomment_code)
		{
				load_comment=true;
				if(resultCode==RESULT_OK)
				{
					ed.setText((ed.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
					
				}
				/*else if(resultCode==RESULT_CANCELED)
				{
					cf.show_toast("You have canceled  the comments selction ",0);
				}*/
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_back(CommentsAndFinancial.class);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
	
	}
