package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class QA_question extends Activity {
CommonFunction cf;
RadioGroup RG[]=new RadioGroup[21];
int Dynamicrooms[]=null;
String  Dynamicrooms_opt[]=null;

TableLayout tbl;
Validation va;
DataBaseHelper db;
EditText ed_tit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qa_question);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"QA_Question",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,3,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		declaration();
		va=new Validation(cf);
		show_saved_values();
		cf.setTouchListener(findViewById(R.id.content));
	}
	private void show_saved_values() {
		// TODO Auto-generated method stub
		db.CreateTable(11);
		Cursor c=db.SelectTablefunction(db.QA_question, " WHERE QA_SRID='"+cf.selectedhomeid+"'");
		String kitchen,living_R,Formal_R,Dining_R,Master_BR,Master_BaR,Bedroom1,Bedroom2,Bedroom3,Bedroom4,Bedroom5,Bedroom6,Hallways
		,Closets,Hall_BaR,Half_BaR,Florida_R,Garage,Storage,Outbuildings,PoolHouse;
		if(c.getCount()>0)
		{
			c.moveToFirst();
			kitchen=db.decode(c.getString(c.getColumnIndex("kitchen"))); 
		    living_R=db.decode(c.getString(c.getColumnIndex("living_R")));
		    Formal_R=db.decode(c.getString(c.getColumnIndex("Formal_R")));
		    Dining_R=db.decode(c.getString(c.getColumnIndex("Dining_R")));
		    Master_BR=db.decode(c.getString(c.getColumnIndex("Master_BR")));
		    Master_BaR=db.decode(c.getString(c.getColumnIndex("Master_BaR")));
		    Bedroom1=db.decode(c.getString(c.getColumnIndex("Bedroom1")));
		    Bedroom2=db.decode(c.getString(c.getColumnIndex("Bedroom2")));
		    Bedroom3=db.decode(c.getString(c.getColumnIndex("Bedroom3")));
		    Bedroom4=db.decode(c.getString(c.getColumnIndex("Bedroom4")));
		    Bedroom5=db.decode(c.getString(c.getColumnIndex("Bedroom5")));
		    Bedroom6=db.decode(c.getString(c.getColumnIndex("Bedroom6")));
		    Hallways=db.decode(c.getString(c.getColumnIndex("Hallways")));
		    Closets=db.decode(c.getString(c.getColumnIndex("Closets")));
		    Hall_BaR=db.decode(c.getString(c.getColumnIndex("Hall_BaR")));
		    Half_BaR=db.decode(c.getString(c.getColumnIndex("Half_BaR")));
		    Florida_R=db.decode(c.getString(c.getColumnIndex("Florida_R")));
		    Garage=db.decode(c.getString(c.getColumnIndex("Garage")));
		    Storage=db.decode(c.getString(c.getColumnIndex("Storage")));
		    Outbuildings=db.decode(c.getString(c.getColumnIndex("Outbuildings")));
		    PoolHouse=db.decode(c.getString(c.getColumnIndex("PoolHouse")));
		    cf.setvaluerd(RG[0], kitchen);
		    cf.setvaluerd(RG[1], living_R);
		    cf.setvaluerd(RG[2], Formal_R);
		    cf.setvaluerd(RG[3], Dining_R);
		    cf.setvaluerd(RG[4], Master_BR);
		    cf.setvaluerd(RG[5], Master_BaR);
		    cf.setvaluerd(RG[6], Bedroom1);
		    cf.setvaluerd(RG[7], Bedroom2);
		    cf.setvaluerd(RG[8], Bedroom3);
		    cf.setvaluerd(RG[9], Bedroom4);
		    cf.setvaluerd(RG[10], Bedroom5);
		    cf.setvaluerd(RG[11], Bedroom6);
		    cf.setvaluerd(RG[12], Hallways);
		    cf.setvaluerd(RG[13], Closets);
		    cf.setvaluerd(RG[14], Hall_BaR);
		    cf.setvaluerd(RG[15], Half_BaR);
		    cf.setvaluerd(RG[16], Florida_R);
		    cf.setvaluerd(RG[17], Garage);
		    cf.setvaluerd(RG[18], Storage);
		    cf.setvaluerd(RG[19], Outbuildings);
		    cf.setvaluerd(RG[20], PoolHouse);
		    
		}
		if(c!=null)
			c.close();
		show_dy_rooms();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		RG[0]=(RadioGroup) findViewById(R.id.qa_RG1);
		RG[1]=(RadioGroup) findViewById(R.id.qa_RG2);
		RG[2]=(RadioGroup) findViewById(R.id.qa_RG3);
		RG[3]=(RadioGroup) findViewById(R.id.qa_RG4);
		RG[4]=(RadioGroup) findViewById(R.id.qa_RG5);
		RG[5]=(RadioGroup) findViewById(R.id.qa_RG6);
		RG[6]=(RadioGroup) findViewById(R.id.qa_RG7);
		RG[7]=(RadioGroup) findViewById(R.id.qa_RG8);
		RG[8]=(RadioGroup) findViewById(R.id.qa_RG9);
		RG[9]=(RadioGroup) findViewById(R.id.qa_RG10);
		RG[10]=(RadioGroup) findViewById(R.id.qa_RG11);
		RG[11]=(RadioGroup) findViewById(R.id.qa_RG12);
		RG[12]=(RadioGroup) findViewById(R.id.qa_RG13);
		RG[13]=(RadioGroup) findViewById(R.id.qa_RG14);
		RG[14]=(RadioGroup) findViewById(R.id.qa_RG15);
		RG[15]=(RadioGroup) findViewById(R.id.qa_RG16);
		RG[16]=(RadioGroup) findViewById(R.id.qa_RG17);
		RG[17]=(RadioGroup) findViewById(R.id.qa_RG18);
		RG[18]=(RadioGroup) findViewById(R.id.qa_RG19);
		RG[19]=(RadioGroup) findViewById(R.id.qa_RG20);
		RG[20]=(RadioGroup) findViewById(R.id.qa_RG21);
		//RG[19]=(RadioGroup) findViewById(R.id.dynamic_rg);
		//ed_tit=(EditText) findViewById(R.id.dynamic_title);
		
		tbl=(TableLayout) findViewById(R.id.qa_Dy_tblmain);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_back(ScopeofInspection.class);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.clear:
				clear_all();
			break;
			case R.id.save:
				//save_all();
				if(validate())
				{
					save_data();
					
				}
			break;
			case R.id.hme:
				cf.go_home();
				//save_all();
			break;
			case R.id.qa_addmoreroom:
				final Dialog dialog1 = new Dialog(QA_question.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alert);
				final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
				Button save=((Button)dialog1.findViewById(R.id.save));
				Button clear=((Button)dialog1.findViewById(R.id.clear));
				ImageView close=((ImageView)dialog1.findViewById(R.id.helpclose));
				close.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
					}
				});
				clear.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ed.setText("");
					}
				});
				save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(!ed.getText().toString().trim().equals(""))
						{
							add_more_room(ed.getText().toString().trim());
							dialog1.dismiss();
						}
						else
						{
							cf.show_toast("Please enter title ", 0);
						}
					}
				});
				dialog1.show();
			break;
		default:
			break;
		}
	}
	private void save_data() {
		// TODO Auto-generated method stub
		String kitchen,living_R,Formal_R,Dining_R,Master_BR,Master_BaR,Bedroom1,Bedroom2,Bedroom3,Bedroom4,Bedroom5,Bedroom6,Hallways
		,Closets,Hall_BaR,Half_BaR,Florida_R,Garage,Storage,Outbuildings,PoolHouse;
		kitchen=cf.getvaluefromRG(RG[0]);
		living_R=cf.getvaluefromRG(RG[1]);
		Formal_R=cf.getvaluefromRG(RG[2]);
		Dining_R=cf.getvaluefromRG(RG[3]);
		Master_BR=cf.getvaluefromRG(RG[4]);
		Master_BaR=cf.getvaluefromRG(RG[5]);
		Bedroom1=cf.getvaluefromRG(RG[6]);
		Bedroom2=cf.getvaluefromRG(RG[7]);
		Bedroom3=cf.getvaluefromRG(RG[8]);
		Bedroom4=cf.getvaluefromRG(RG[9]);
		Bedroom5=cf.getvaluefromRG(RG[10]);
		Bedroom6=cf.getvaluefromRG(RG[11]);
		Hallways=cf.getvaluefromRG(RG[12]);
		Closets=cf.getvaluefromRG(RG[13]);
		Hall_BaR=cf.getvaluefromRG(RG[14]);
		Half_BaR=cf.getvaluefromRG(RG[15]);
		Florida_R=cf.getvaluefromRG(RG[16]);
		Garage=cf.getvaluefromRG(RG[17]);
		Storage=cf.getvaluefromRG(RG[18]);
		Outbuildings=cf.getvaluefromRG(RG[19]);
		PoolHouse=cf.getvaluefromRG(RG[20]);
		Cursor c=db.SelectTablefunction(db.QA_question, " WHERE QA_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			db.wdo_db.execSQL(" UPDATE "+db.QA_question+" SET kitchen='"+kitchen+"',living_R='"+living_R+"',Formal_R='"+Formal_R+"',Dining_R='"+Dining_R+
			"',Master_BR='"+Master_BR+"',Master_BaR='"+Master_BaR+"',Bedroom1='"+Bedroom1+"',Bedroom2='"+Bedroom2+"',Bedroom3='"+Bedroom3+"',Bedroom4='"+
			Bedroom4+"',Bedroom5='"+Bedroom5+"',Bedroom6='"+Bedroom6+"',Hallways='"+Hallways+"',Closets='"+Closets+"',Hall_BaR='"+Hall_BaR+"',Half_BaR='"+Half_BaR+"',Florida_R='"+Florida_R+"'," +"Garage='"+
			Garage+"',Storage='"+Storage+"',Outbuildings='"+Outbuildings+"',PoolHouse='"+PoolHouse+"' WHERE QA_SRID='"+cf.selectedhomeid+"'");
		}
		else
		{
			db.wdo_db.execSQL(" INSERT INTO "+db.QA_question+" (QA_InspectorId,QA_SRID,kitchen,living_R,Formal_R,Dining_R,Master_BR,Master_BaR,Bedroom1,Bedroom2,Bedroom3,Bedroom4,Bedroom5,Bedroom6,Hallways" +
			",Closets,Hall_BaR,Half_BaR,Florida_R,Garage,Storage,Outbuildings,PoolHouse) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"'," +
			"'"+kitchen+"','"+living_R+"','"+Formal_R+"','"+Dining_R+"','"+Master_BR+"','"+Master_BaR+"','"+Bedroom1+"','"+Bedroom2+"','"+Bedroom3+"','"+Bedroom4+"','"+Bedroom5+"','"+Bedroom6+"','"+Hallways+"','"+Closets+"','"+Hall_BaR+"','"+Half_BaR+"','"+Florida_R+"','"+Garage+"','"+Storage+"','"+Outbuildings+"','"+PoolHouse+"')");
		}
		if(c!=null)
			c.close();
		if(Dynamicrooms!=null)
		{
			for(int i=0;i<Dynamicrooms.length;i++)
			{
				String value=cf.getvaluefromRG(((RadioGroup)(findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup"))));
				db.wdo_db.execSQL(" UPDATE "+db.QA_question_dynamic+" SET QA_D_option='"+db.encode(value)+"' WHERE QA_D_SRID='"+cf.selectedhomeid+"' and QA_D_Id='"+Dynamicrooms[i]+"'");
			}
		}
		cf.go_back(Photos.class);
		cf.show_toast("QA Question saved successfully", 0);
	}
	private boolean validate() {
		// TODO Auto-generated method stub
		if(va.validate(RG[0], "Kitchen"))
		{
			if(va.validate(RG[1], "Living Room"))
			{
				if(va.validate(RG[2], "Formal Room"))
				{
					if(va.validate(RG[3], "Dining Room"))
					{
						if(va.validate(RG[4], "Master Bedroom"))
						{
							if(va.validate(RG[5], "Master Bathroom"))
							{
								if(va.validate(RG[6], "Bedroom 1"))
								{
									if(va.validate(RG[7], "Bedroom 2"))
									{
										if(va.validate(RG[8], "Bedroom 3"))
										{
											if(va.validate(RG[9], "Bedroom 4"))
											{
												if(va.validate(RG[10], "Bedroom 5"))
												{
													if(va.validate(RG[11], "Bedroom 6"))
													{
														if(va.validate(RG[12], "Hallways"))
														{
															if(va.validate(RG[13], "Closets"))
															{
																if(va.validate(RG[14], "Hall Bathroom"))
																{
																	if(va.validate(RG[15], "Half Bathroom"))
																	{
																		if(va.validate(RG[16], "Florida Room"))
																		{
																			if(va.validate(RG[17], "Garage"))
																			{
																				if(va.validate(RG[18], "Storage"))
																				{
																					if(va.validate(RG[19], "Outbuildings"))
																					{
																						if(va.validate(RG[20], "Pool House"))
																						{
																							if(check_dynamic_validation())
																							{
																								return true;
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	private boolean check_dynamic_validation() {
		// TODO Auto-generated method stub
		if(Dynamicrooms!=null)
		{
			for(int i=0;i<Dynamicrooms.length;i++)
			{
				RadioGroup rg=((RadioGroup)(findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup")));
				/*System.out.println("comes correctly1="+i+rg);
				System.out.println("comes correctly1.0="+i+((TextView)(findViewById(Dynamicrooms[i]).findViewWithTag("title"))).getText().toString());*/
				if(!va.validate(rg,((TextView)(findViewById(Dynamicrooms[i]).findViewWithTag("title"))).getText().toString()))
				{
					return false;
				}
				System.out.println("comes correctly2="+i);
			}
		}
		return true;
	}
	private void add_more_room(String s) {
		// TODO Auto-generated method stub
		/*if(!ed_tit.getText().toString().trim().equals(""))
		{
			if(va.validate(RG[19], ed_tit.getText().toString().trim()))
			{*/
				String value ="No";//cf.getvaluefromRG(RG[19]);
				Cursor c =db.SelectTablefunction(db.QA_question_dynamic, " WHERE QA_D_SRID='"+cf.selectedhomeid+"' and QA_D_title='"+db.encode(s)+"'");
				if(c.getCount()>0)
				{
					cf.show_toast(s+" condition type already available please try some other", 0);
				}
				else
				{
					db.wdo_db.execSQL(" INSERT INTO "+db.QA_question_dynamic+" (QA_D_InspectorId,QA_D_SRID,QA_D_title,QA_D_option) VALUES" +
							" ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+db.encode(s)+"','"+db.encode(value)+"')");
					
					if(Dynamicrooms!=null)
					{
						System.out.println("the dynamic value"+Dynamicrooms.length);
						
						Dynamicrooms_opt=null;
						Dynamicrooms_opt=new String[Dynamicrooms.length+1];
						System.out.println("the dynamic opt value"+Dynamicrooms_opt.length);
						for (int i =0;i<Dynamicrooms.length;i++)
						{
							System.out.println(" the values"+i+"="+cf.getvaluefromRG(((RadioGroup)(findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup")))));
							Dynamicrooms_opt[i]=cf.getvaluefromRG(((RadioGroup)(findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup"))));
						}
						Dynamicrooms_opt[Dynamicrooms.length]="No";
					}
					show_dy_rooms();
					/*ed_tit.setText("");
					RG[19].clearCheck();
					((RadioButton) (RG[19].findViewWithTag("No"))).setChecked(true);*/
					
				}
				
			/*	
			}
		}
		else
		{
			cf.show_toast("Please enter value for title ", 0);
		}*/
		
				if(c!=null)
					c.close();
	}
	private void show_dy_rooms() {
		// TODO Auto-generated method stub
		tbl.removeAllViews();
		Cursor c =db.SelectTablefunction(db.QA_question_dynamic, " WHERE QA_D_SRID='"+cf.selectedhomeid+"'");
		
		if(c.getCount()>0)
		{
			Dynamicrooms=new int[c.getCount()];
		
			 
			
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				Dynamicrooms[i]=c.getInt(c.getColumnIndex("QA_D_Id"));
				TableRow tbl_r=new TableRow(this);
				tbl_r.setId(Dynamicrooms[i]);
				tbl_r.setGravity(Gravity.CENTER_VERTICAL);
				TextView tv_str=new TextView(this,null,R.attr.star);
				tbl_r.addView(tv_str);
				TextView ed_tit=new TextView(this,null,R.attr.edittext_200_dy);
				ed_tit.setText(db.decode(c.getString(c.getColumnIndex("QA_D_title"))));
				ed_tit.setTag("title");
				ed_tit.setWidth(0);
				tbl_r.addView(ed_tit,200,LayoutParams.WRAP_CONTENT);
				TextView tv_colon=new TextView(this,null,R.attr.colon);
				TableRow.LayoutParams lp_c=new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				lp_c.setMargins(5, 0, 5, 0);
				tbl_r.addView(tv_colon,lp_c);
				
				RadioGroup rg=new RadioGroup(this);
				
				rg.setOrientation(LinearLayout.HORIZONTAL);
				rg.setTag("Radiogroup");
				RadioButton rd1=new RadioButton(this,null,R.attr.textview_rd);
				rd1.setText("Yes");
				rd1.setTag("Yes");
				rg.addView(rd1,100,LayoutParams.WRAP_CONTENT);
				RadioButton rd2=new RadioButton(this,null,R.attr.textview_rd);
				rd2.setText("No");
				rd2.setTag("No");
				rg.addView(rd2,100,LayoutParams.WRAP_CONTENT);
				RadioButton rd3=new RadioButton(this,null,R.attr.textview_rd);
				rd3.setText("Not Determined");
				rd3.setTag("Not Determined");
				rg.addView(rd3);
				RadioButton rd4=new RadioButton(this,null,R.attr.textview_rd);
				rd4.setText("Not Applicable");
				rd4.setTag("Not Applicable");
				rg.addView(rd4);
				
				tbl_r.addView(rg);
				ImageView im =new ImageView(this);
				im.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofdelete));
				im.setTag(c.getInt(c.getColumnIndex("QA_D_Id")));
				
				im.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						AlertDialog.Builder b =new AlertDialog.Builder(QA_question.this);
						b.setTitle("Confirmation");
						b.setMessage("Do you want to delete the selected QA_Question?");
						b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								try
								{
									db.wdo_db.execSQL(" DELETE FROM "+db.QA_question_dynamic+" WHERE QA_D_Id='"+v.getTag().toString()+"'");
									tbl.removeView((View)v.getParent());
									if(Dynamicrooms!=null)
									{
										Dynamicrooms_opt=null;
										Dynamicrooms_opt=new String[Dynamicrooms.length-1];
										for (int i =0,j=0;i<Dynamicrooms.length;i++)
										{
											if(((View)v.getParent()).getId()!=Dynamicrooms[i])
											{
												
												Dynamicrooms_opt[j]=cf.getvaluefromRG(((RadioGroup)(findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup"))));
												j++;
											}
										}
										//Dynamicrooms_opt[Dynamicrooms.length]="No";
									}
									show_dy_rooms();
								}catch (Exception e) {
									// TODO: handle exception
									System.out.println("the exeption in delete"+e.getMessage());
								}
								
								cf.show_toast("The selected QA_Question has been deleted successfully.", 1);
								
							}
						});
						b.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
							}
						});   
						AlertDialog al=b.create();
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show(); 
					
					}
				});
				tbl_r.addView(im);
				tbl.addView(tbl_r);
				
				if(Dynamicrooms_opt!=null)
				{
					if(Dynamicrooms_opt.length>i)
					{
						cf.setvaluerd(rg,Dynamicrooms_opt[i]);
					}
					else
					{
						cf.setvaluerd(rg,db.decode(c.getString(c.getColumnIndex("QA_D_option"))));
						
					}
				}
				else
				{
					cf.setvaluerd(rg,db.decode(c.getString(c.getColumnIndex("QA_D_option"))));
					
				}
			}
		}
		else
		{
			Dynamicrooms=null;
		}
	}
	private void clear_all() {
		// TODO Auto-generated method stub
		for(int i=0;i<RG.length;i++)
		{
			RG[i].clearCheck();
			((RadioButton)RG[i].findViewWithTag("No")).setChecked(true);
		}
		if(Dynamicrooms!=null)
		{
			for(int i=0;i<Dynamicrooms.length;i++)
			{
				
				((RadioGroup)findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup")).clearCheck();
				((RadioButton)((RadioGroup)findViewById(Dynamicrooms[i]).findViewWithTag("Radiogroup")).findViewWithTag("No")).setChecked(true);
				
			}
			//Dynamicrooms=null;
		}
	}
	}
