package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper1;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.R.integer;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class OrderInspection_backup extends Activity {
	Button placeorder, home;
	String redcolor = "<font color=red> * </font>", ImageName;
	CheckBox cb[], checkbox[];
	EditText et[];
	ImageView img[];
	int i, j, Cnt, n, width, height,noofbuild,ii;
	boolean rgcheck = false, rgcheck2 = false, call_county2 = true, oncreate,countysetselection=false;
	SoapObject chkaddservice;
	String insptypeid = "741", statevalue = "false",zipidentifier,state,county,city,stateid,countyid;
	String strinspectionbasesq_feet = "0", strinspectionbase_fee = "0",
			strinspectionadditionalsq_feet = "0",
			strinspectionaddtional_fee = "0", strbuildingbasesq_feet = "0",
			strbuildingbase_fee = "0", strbuildingaddsq_feet = "0",
			strbuildingadd_fee = "0", strsquarefootagevalue = "0",
			strtotalvalue = "0", stradditionalinspectionvalue = "0",
			stradditionalbuildingvalue = "0", strdiscountvalue = "0";
	float inspectionbasesq_feet, inspectionbase_fee,
			inspectionadditionalsq_feet, inspectionaddtional_fee,
			buildingbasesq_feet, buildingbase_fee, buildingaddsq_feet,
			buildingadd_fee, squarefootagevalue, totalvalue,
			additionalinspectionvalue, additionalbuildingvalue = 0,
			discountvalue,inspectionbase_fee2,discountvalue2,value=0;
	// lastnametxt.setText(Html.fromHtml(cf.redcolor+" "+"Last Name "));
	Spinner spinnerstate, spinnercounty,  spinnerstate2,
			spinneraddi, spinnercounty2, spinneryear, spinnerbuildintype,
			spinneragency, spinneragent, spinnerrealestatecompany,
			spinnerrealtor;
	RadioGroup rgtime,rgworkingwith;
	LinearLayout llgeneralinfo, llcreateanaccount, llspinner,
			llcalculatefees, llotheryear,
			llnoofbuildings, llotherbuildingtype,llagent,llrealtor,llagency,llrealestateagency;
	ImageView plus1, plus2, minus1, minus2, descriptionimage;
	String[] arrayid, array_Insp_ID,
			array_InspName, array_Type_ID, arraystateid, arraystatename,
			arraycountyid, arraycountyname, arraycompanyid, arraycompanyname,
			arraystateid2, arr_bType, arraystatename2, arraycountyid2, arraycountyname2,
			arrayagencyname,arrayagencyid,arrayagentname,arrayagentid,arrayrealestatecompanyname,
			arrayrealestatecompanyid,arrayrealtorname,arrayrealtorid;
	String yearbuilt[] = { "--Select--", "Other", "2013", "2012", "2011",
			"2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003",
			"2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995",
			"1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987",
			"1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979",
			"1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971",
			"1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963",
			"1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955",
			"1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947",
			"1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939",
			"1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931",
			"1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923",
			"1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915",
			"1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907",
			"1906", "1905", "1904", "1903", "1902", "1901", "1900" };
	TextView description,tvtotla;
	CommonFunction cf;
	int show_handler;
	String 	strpreferredtime, strinspaddress1, strinspaddress2, strstate,
			strcounty, strcity = "", strzip = "", strmailaddress1 = "",
			strmailaddress2 = "", strstate2, strcounty2, strcity2 = "",
			strzip2 = "", strselect = "--Select--", strcouponcode = "",
			strcompanyname,stryear, strtime = "", strcountyid,
			strcountyid2, strstateid, strstateid2, strcompanyid = "0",
			strbuildingname,strinspectioncategoryname, strinspectioncategoryid = "0",
			strinsptypename, str_building_edittext_value = "",
			strpolicyno = "", strnoofstories, strnoofbuildings, strdate,
			strfirstname, strlastname, strmail, strphone, currentdate,stragencyname,stragencyid="0",stragentname,stragentid="0",
			strrealestatecompanyname,strrealestatecompanyid="0",strrealtorname,strrealtorid="0",strworkingwith;
	CheckBox cbaddresscheck;
	EditText etinspaddress1, etinspaddress2, etcity, etzip, etmailaddress1,
			etmailaddress2, etcity2, etzip2, ettotalfees, etotherbuildingtype,
			etinspectionfees, etdiscount, etsquarefootage,
			etcouponcode, etdate, etnoofstories, etnoofbuildings, etfirstname,
			etlastname, etmail, etphone, etpolicyno, etotheryear,ettime,etinspfees,etinspdiscount,spinnerimc;
	EditText[] dynamic_etsquarefootage,dynamic_etnoofstories;
	ArrayAdapter<String> stateadapter, countyadapter, countyadapter2,
			agencyadapter, agentadapter, realestatecompanyadapter,
			realtoradapter;
	String inspector_id,inspectorname,companyid,companyname;
	SoapObject order_result;
	DatePickerDialog datePicker;
	int keyDel;
	int id_len;
	DataBaseHelper1 dbh1;
	DataBaseHelper dbh;
	DataBaseHelper db;
	Webservice_Function wb;
	static final int TIME_DIALOG_ID = 999;
	TimePickerDialog timepicker;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.orderinspection);
		cf = new CommonFunction(this);
		db=dbh=new DataBaseHelper(this);
		wb=new Webservice_Function(this);
		cf.setTouchListener(findViewById(R.id.widget54));

		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		Cursor cur=dbh.wdo_db.rawQuery("select * from "+dbh.inspectorlogin, null);
		int count=cur.getCount();
		if(count>0)
		{
			cur.moveToFirst();
			companyid=dbh.decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyId")));
			companyname=dbh.decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyName")));
			System.out.println("comepany id "+companyid);
		}
		inspectorname=dbh.Insp_firstname+" "+dbh.Insp_lastname;
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(OrderInspection_backup.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", dbh.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		
		// home = (Button) findViewById(R.id.orderinspection_home);
		placeorder = (Button) findViewById(R.id.orderinsp_btnfreeinspectionquotes);
		spinnerstate = (Spinner) findViewById(R.id.orderinspection_spinnerstate);
		spinnercounty = (Spinner) findViewById(R.id.orderinspection_spinnercounty);
		spinnerstate2 = (Spinner) findViewById(R.id.orderinspection_spinnerstate2);
		spinnercounty2 = (Spinner) findViewById(R.id.orderinspection_spinnercounty2);
		spinnerimc = (EditText) findViewById(R.id.orderinspection_preferredimc);
		spinneryear = (Spinner) findViewById(R.id.orderinspection_spinneryear);
		spinnerbuildintype = (Spinner) findViewById(R.id.orderinspection_buildingtype);
		spinneragency = (Spinner) findViewById(R.id.orderinspection_spinneragency);
		spinneragent = (Spinner) findViewById(R.id.orderinspection_spinneragent);
		spinnerrealestatecompany = (Spinner) findViewById(R.id.orderinspection_spinnerrealestatecompany);
		spinnerrealtor = (Spinner) findViewById(R.id.orderinspection_spinnerrealtor);
		//rgtime = (RadioGroup) findViewById(R.id.orderinsp_radiogrouptime);
		rgworkingwith = (RadioGroup) findViewById(R.id.orderinsp_radiogroupworkingwith);
		llagent = (LinearLayout) findViewById(R.id.orderinspection_llagent);
		llrealtor = (LinearLayout) findViewById(R.id.orderinspection_llrealtor);
		llgeneralinfo = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutgeneralinfoborder);
		llcreateanaccount = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutcreateanaccountborder);
		llspinner = (LinearLayout) findViewById(R.id.llspinner);
		llcalculatefees = (LinearLayout) findViewById(R.id.orderinsp_llcalculatefees);
		llotherbuildingtype = (LinearLayout) findViewById(R.id.orderinspection_llothetbuildingtype);
		llnoofbuildings = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutnoofbuildings);
		llotheryear = (LinearLayout) findViewById(R.id.orderinspection_llotheryear);
		llagency = (LinearLayout) findViewById(R.id.orderinspection_llagency);
		llrealestateagency = (LinearLayout) findViewById(R.id.orderinspection_llrealestateagency);
		plus1 = (ImageView) findViewById(R.id.orderinspection_plus1);
		plus2 = (ImageView) findViewById(R.id.orderinspection_plus2);
		minus1 = (ImageView) findViewById(R.id.orderinspection_minus1);
		minus2 = (ImageView) findViewById(R.id.orderinspection_minus2);
		descriptionimage = (ImageView) findViewById(R.id.orderinspection_descriptionimage);
		description = (TextView) findViewById(R.id.orderinspection_descriptiontext);
		cbaddresscheck = (CheckBox) findViewById(R.id.orderinspection_addresscheck);
		etinspaddress1 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress1);
		etinspaddress2 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress2);
		etcity = (EditText) findViewById(R.id.orderinspection_etcity);
		etzip = (EditText) findViewById(R.id.orderinspection_etzip);
		etmailaddress1 = (EditText) findViewById(R.id.orderinspection_etmailingaddress1);
		etmailaddress2 = (EditText) findViewById(R.id.orderinspection_etmailingaddress2);
		etcity2 = (EditText) findViewById(R.id.orderinspection_etcity2);
		etzip2 = (EditText) findViewById(R.id.orderinspection_etzip2);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		etinspectionfees = (EditText) findViewById(R.id.orderinsp_etinspectionfees);
		etdiscount = (EditText) findViewById(R.id.orderinsp_etdiscount);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		etsquarefootage = (EditText) findViewById(R.id.orderinsp_etsquarefootage);
		etcouponcode = (EditText) findViewById(R.id.orderinspection_couponcode);
		etdate = (EditText) findViewById(R.id.orderinsp_etinspdate);
		etnoofstories = (EditText) findViewById(R.id.orderinspection_etnoofstories);
		etnoofbuildings = (EditText) findViewById(R.id.orderinspection_noofbuildings);
		etotherbuildingtype = (EditText) findViewById(R.id.orderinspection_etotherbuildingtype);
		etfirstname = (EditText) findViewById(R.id.orderinspection_etfirstanme);
		etlastname = (EditText) findViewById(R.id.orderinspection_etlastname);
		etmail = (EditText) findViewById(R.id.orderinspection_etmail);
		etphone = (EditText) findViewById(R.id.orderinspection_etphone);
		etpolicyno = (EditText) findViewById(R.id.orderinspection_policynumber);
		etotheryear = (EditText) findViewById(R.id.orderinspection_etotheryear);
		ettime = (EditText) findViewById(R.id.orderinsp_etinsptime);
		etinspfees=(EditText) findViewById(R.id.orderinsp_etinspfee);
		etinspdiscount=(EditText) findViewById(R.id.orderinsp_etinspdiscount);
		tvtotla=(TextView) findViewById(R.id.orderinsp_tvtotalfee);
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		width = displayMetrics.widthPixels;
		height = displayMetrics.heightPixels;

	//	rgtime.setOnCheckedChangeListener(new check(4));
		rgworkingwith.setOnCheckedChangeListener(new check(5));
		
		LoadAgency();
		
		spinneragency.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stragencyname=spinneragency.getSelectedItem().toString();
				
				if(stragencyname.equals("--Select--"))
				{
					spinneragent.setEnabled(false);
					stragencyid="0";
					stragentid="0";
					
					arrayagentname = new String[0];
					arrayagentid = new String[0];
					agentadapter = new ArrayAdapter<String>(
							OrderInspection_backup.this,
							android.R.layout.simple_spinner_item,
							arrayagentname);
					agentadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinneragent.setAdapter(agentadapter);
					
				}
				else
				{
					spinneragent.setEnabled(true);
					int id=spinneragency.getSelectedItemPosition();
					stragencyid=arrayagencyid[id];
					LoadAgent(stragencyid);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		spinneragent.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stragentname=spinneragent.getSelectedItem().toString();
				int id=spinneragent.getSelectedItemPosition();
				stragentid=arrayagentid[id];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		LoadRealEstateCompany();
		
		spinnerrealestatecompany.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strrealestatecompanyname=spinnerrealestatecompany.getSelectedItem().toString();
				
				if(strrealestatecompanyname.equals("--Select--"))
				{
					spinnerrealtor.setEnabled(false);
					strrealestatecompanyid="0";
					strrealtorid="0";
					
					arrayrealtorname = new String[0];
					arrayrealtorid = new String[0];
					realtoradapter = new ArrayAdapter<String>(
							OrderInspection_backup.this,
							android.R.layout.simple_spinner_item,
							arrayrealtorname);
					realtoradapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnerrealtor.setAdapter(realtoradapter);
					
				}
				else
				{
					spinnerrealtor.setEnabled(true);
					int id=spinnerrealestatecompany.getSelectedItemPosition();
					strrealestatecompanyid=arrayrealestatecompanyid[id];
					LoadRealtor(strrealestatecompanyid);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		spinnerrealtor.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strrealtorname=spinnerrealtor.getSelectedItem().toString();
				int id=spinnerrealtor.getSelectedItemPosition();
				strrealtorid=arrayrealtorid[id];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(OrderInspection_backup.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);

			stateadapter = new ArrayAdapter<String>(OrderInspection_backup.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate2.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							OrderInspection_backup.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnerstate2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2 = true;
				return false;
			}
		});

		spinnerstate2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate2 = spinnerstate2.getSelectedItem().toString();
				int stateid = spinnerstate2.getSelectedItemPosition();
				strstateid2 = arraystateid[stateid];
				if (call_county2 == true) {
					if (!strstate2.equals("--Select--")) {
						LoadCounty2(strstateid2);
						spinnercounty2.setEnabled(true);

					} else {
						// spinnercounty2.setAdapter(null);
						spinnercounty2.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(
								OrderInspection_backup.this,
								android.R.layout.simple_spinner_item,
								arraycountyname2);
						countyadapter2
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spinnercounty2.setAdapter(countyadapter2);
					}
				} else {
					spinnercounty2.setEnabled(true);
					spinnercounty2.setSelection(spinnercounty
							.getSelectedItemPosition());
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int countyid = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty2 = spinnercounty2.getSelectedItem().toString();
				int countyid = spinnercounty2.getSelectedItemPosition();
				strcountyid2 = arraycountyid2[countyid];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		etinspfees.addTextChangedListener(new total_feecal());
		etinspdiscount.addTextChangedListener(new total_feecal());
		
		
		arraycompanyname=new String[1];
		arraycompanyid=new String[1];
		arraycompanyname[0]=companyname;
		arraycompanyid[0]=companyid;
		spinnerimc.setText(companyname);
		
		/*ArrayAdapter<String> imcadapter = new ArrayAdapter<String>(
				OrderInspection.this,
				android.R.layout.simple_spinner_item,
				arraycompanyname);
		imcadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		*/
		strcompanyid =companyid;
		LoadInspectionTypeDescription(insptypeid, companyid);
		strcompanyname=companyname;
		/*spinnerimc.setAdapter(imcadapter);
		spinnerimc.setEnabled(false);

		spinnerimc.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcompanyname = spinnerimc.getSelectedItem().toString();
				int companyid = spinnerimc.getSelectedItemPosition();
				strcompanyid = arraycompanyid[companyid];
				
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		*/
		LoadBuildingType("4");//category id

		spinnerbuildintype
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strbuildingname = spinnerbuildintype.getSelectedItem()
								.toString();
						if (strbuildingname.equals("Other")) {
							llotherbuildingtype.setVisibility(View.VISIBLE);
						} else {
							llotherbuildingtype.setVisibility(View.GONE);
						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		spinneryear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stryear = spinneryear.getSelectedItem().toString();
				if (stryear.equals("Other")) {
					llotheryear.setVisibility(View.VISIBLE);
				} else {
					llotheryear.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		ArrayAdapter<String> yearadapter = new ArrayAdapter<String>(
				OrderInspection_backup.this, android.R.layout.simple_spinner_item,
				yearbuilt);
		yearadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinneryear.setAdapter(yearadapter);

		cbaddresscheck
				.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
		etcouponcode
				.addTextChangedListener(new CustomTextWatcher(etcouponcode));
		etpolicyno.addTextChangedListener(new CustomTextWatcher(etpolicyno));
		etnoofstories.addTextChangedListener(new CustomTextWatcher(
				etnoofstories));
		etfirstname.addTextChangedListener(new CustomTextWatcher(etfirstname));
		etlastname.addTextChangedListener(new CustomTextWatcher(etlastname));
		etmail.addTextChangedListener(new CustomTextWatcher(etmail));
		etinspaddress1.addTextChangedListener(new CustomTextWatcher(
				etinspaddress1));
		etinspaddress2.addTextChangedListener(new CustomTextWatcher(
				etinspaddress2));
		etcity.addTextChangedListener(new CustomTextWatcher(etcity));
		etmailaddress1.addTextChangedListener(new CustomTextWatcher(
				etmailaddress1));
		etmailaddress2.addTextChangedListener(new CustomTextWatcher(
				etmailaddress2));
		etcity2.addTextChangedListener(new CustomTextWatcher(etcity2));
		etotherbuildingtype.addTextChangedListener(new CustomTextWatcher(
				etotherbuildingtype));

		etotheryear.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (etotheryear.getText().toString().trim().matches("^0")) {
					etotheryear.setText("");
				}
				if (!etotheryear.getText().toString().trim().equals("")) {
					if (Integer.parseInt((etotheryear.getText().toString()
							.trim())) > year1) {
						etotheryear.setText("");
						cf.show_toast("Please enter a valid year", 0);
						
					}
				}
				if (Arrays.asList(yearbuilt).contains(
						etotheryear.getText().toString().trim())) {
					etotheryear.setText("");
					cf.show_toast("Please enter a year that is not in the year list", 0);
					
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});
		
		etphone.setLongClickable(false);

		etphone.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etphone.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etphone.setText("");
		        }
				if (etphone.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etphone.setText("");
	            }

				etphone.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							System.out.println("Inside delete");
							
							String str = etphone.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==5)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}
				});

				if (keyDel == 0) {
					String a = "";
					String str = etphone.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					System.out.println("The length is "+id_len);
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					etphone.removeTextChangedListener(this);
					etphone.setText(a);
					if (before > 0)
						etphone.setSelection(start);
					else
						etphone.setSelection(a.length());
					etphone.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etsquarefootage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etsquarefootage.getText().toString().startsWith(" ")) {
					// Not allowed
					etsquarefootage.setText("");
				}
				if (etsquarefootage.getText().toString().trim().matches("^0")) {
					// Not allowed
					etsquarefootage.setText("");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etotheryear.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etotheryear.getText().toString().trim().length() >= 1) {
						if (etotheryear.getText().toString().trim().length() < 4) {
							etotheryear.setText("");
							cf.show_toast("Please enter a valid year", 0);
							
						}
					}
				}
			}
		});

		etmail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etmail.getText().toString().trim().length() >= 1) {
						if (!cf.eMailValidation(etmail.getText().toString()
								.trim())) {
							etmail.setText("");
							cf.show_toast("Please enter a valid Email", 0);
						}
					}
				}
			}
		});
		
		etcouponcode.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etcouponcode.getText().toString().trim().length() >= 1) {
						Check_Couponcode();
					}
				}
			}
		});
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					zipidentifier="zip1";
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etzip2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etzip2.getText().toString().trim().length() == 5) {
					spinnerstate2.setSelection(0);
					zipidentifier = "zip2";
					Load_State_County_City(etzip2);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		if(cur!=null)
			cur.close();
	}
	class total_feecal implements TextWatcher
{

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		if(!etinspfees.getText().toString().trim().equals(""))
		{
			int fee=0,disc=0;
			fee=Integer.parseInt(etinspfees.getText().toString());
			if(!etinspdiscount.getText().toString().trim().equals(""))
			{
				disc=Integer.parseInt(etinspdiscount.getText().toString());
			}
			if(fee>disc)
			{
				tvtotla.setText((fee-disc)+"");
			}
			else
			{
				tvtotla.setText("0");
			}
			
				
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}
	
}
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.orderinspection_plus1:
			System.out.println("plus1");
			llgeneralinfo.setVisibility(View.VISIBLE);
			plus1.setVisibility(View.GONE);
			minus1.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus1:
			System.out.println("minus1");
			llgeneralinfo.setVisibility(View.GONE);
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_plus2:
			System.out.println("plus2");
			llcreateanaccount.setVisibility(View.VISIBLE);
			plus2.setVisibility(View.GONE);
			minus2.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus2:
			System.out.println("minus2");
			llcreateanaccount.setVisibility(View.GONE);
			plus2.setVisibility(View.VISIBLE);
			minus2.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_home:
			Intent inthome = new Intent(OrderInspection_backup.this, HomeScreen.class);
			startActivity(inthome);
			finish();
			break;

		case R.id.orderinsp_btncalculatefees:
					inspectionbasesq_feet = 0;
					inspectionbase_fee = 0;
					inspectionadditionalsq_feet = 0;
					inspectionaddtional_fee = 0;
					buildingbasesq_feet = 0;
					buildingbase_fee = 0;
					buildingaddsq_feet = 0;
					buildingadd_fee = 0;
					discountvalue2 = 0;
					Calculate_Fees();

			break;

		case R.id.orderinsp_btnfreeinspectionquotes:
				Check_Validation();
			break;

		case R.id.orderinsp_getdate:
			showDialogDate(etdate);
			break;
		case R.id.orderinsp_gettime:
			showDialog(TIME_DIALOG_ID);
			break;
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
	switch (id) {
	case TIME_DIALOG_ID:
		final Calendar c1 = Calendar.getInstance();
		int hour = c1.get(Calendar.HOUR_OF_DAY);
		int minute = c1.get(Calendar.MINUTE);
		timepicker= new TimePickerDialog(this, timePickerListener, hour, minute,
				false);
		return timepicker;
	
	
	}
	return null;
	}
	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			int hour = selectedHour;
			int minute = selectedMinute;
			StringBuilder sb = new StringBuilder();
			if (hour >= 12) {
				sb.append(hour - 12).append(":").append(minute).append(" PM");
			} else {
				sb.append(hour).append(":").append(minute).append(" AM");
			}
			ettime.setText(sb);
			strtime = ettime.getText().toString();
			
			//updating time picker to current time
			final Calendar c = Calendar.getInstance();
			timepicker.updateTime(c.get(Calendar.HOUR_OF_DAY), 
					 c.get(Calendar.MINUTE));
		}
	};
	private void Check_Validation() {
		boolean mailvalidation, phonevalidation;

		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString().trim();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		System.out.println("The year is " + stryear);

		if (etmail.getText().toString().trim().equals("")) {
			mailvalidation = true;
		} else {
			if (cf.eMailValidation(etmail.getText().toString().trim())) {
				mailvalidation = true;
			} else {
				mailvalidation = false;
			}
		}

		if (etphone.getText().toString().trim().equals("")) {
			phonevalidation = true;
		} else {
			if (etphone.getText().toString().trim().length() < 13) {
				phonevalidation = false;
			} else {
				phonevalidation = true;
			}
		}
		
		boolean boolnoofbuildings = true;
		if (etnoofbuildings.getText().toString().trim().equals("")) {
			boolnoofbuildings = false;
		} else {
			boolnoofbuildings = true;
		}		
		
		boolean boolnoofstories = true;
		if (etnoofstories.getText().toString().trim().equals("")) {
			boolnoofstories = false;
		} else {
			boolnoofstories = true;
		}		
		
		boolean boolsquarefootage=true;
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				boolsquarefootage=false;
			}
			else
			{
				int val=Integer.parseInt(etsquarefootage.getText().toString().trim());
				if(val<399||val>100000)
				{
					boolsquarefootage=false;
				}
				else
				{
					boolsquarefootage=true;
				}
			}
		
			strtime=ettime.getText().toString().trim();
		if (boolsquarefootage) {
			if (!spinneryear.getSelectedItem().toString().equals("--Select--")) {
				if (!stryear.equals("")) {
					if (boolnoofstories) {
						if(boolnoofbuildings)
							{
							if (!strcompanyname.equals("--Select--")) {
								if (!etdate.getText().toString().trim()
										.equals("")) {
									if (!strtime.equals("")) {
										if (fees_calculation_validate()) {
											if (!etfirstname.getText()
													.toString().trim()
													.equals("")) {
												if (!etlastname.getText()
														.toString().trim()
														.equals("")) {
													if (mailvalidation) {
														if (phonevalidation) {
															if (!etinspaddress1
																	.getText()
																	.toString()
																	.trim()
																	.equals("")) {
																
																		
																			if (!etzip
																					.getText()
																					.toString()
																					.trim()
																					.equals("")) {
																				if ((etzip
																						.getText()
																						.toString()
																						.length() == 5)) {
																					if ((!etzip
																							.getText()
																							.toString()
																							.contains(
																									"00000"))) {
																						if (!etcity
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							if (!strstate
																									.equals("--Select--")) {
																								if (!strcounty
																										.equals("--Select--")) {
																						if (!etmailaddress1
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							
																									
																										if (!etzip2
																												.getText()
																												.toString()
																												.trim()
																												.equals("")) {
																											if ((etzip2
																													.getText()
																													.toString()
																													.length() == 5)) {
																												if ((!etzip2
																														.getText()
																														.toString()
																														.contains(
																																"00000"))) {
																													if (!etcity2
																															.getText()
																															.toString()
																															.trim()
																															.equals("")) {
																														if (!strstate2
																																.equals("--Select--")) {
																															if (!strcounty2
																																	.equals("--Select--")) {
																													Place_Order();
																													
																															} else {
																																cf.show_toast("Please select County under Mailing Address", 0);
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																														} else {
																															cf.show_toast("Please select State under Mailing Address", 0);
																															llcreateanaccount
																																	.setVisibility(View.VISIBLE);
																															plus2.setVisibility(View.GONE);
																															minus2.setVisibility(View.VISIBLE);

																														}
																															} else {
																																cf.show_toast("Please Enter City under Mailing Address", 0);
																																etcity2.requestFocus();
																																etcity2.setText("");
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																												} else {
																													cf.show_toast("Please enter a valid Zip under Mailing Address", 0);
																													etzip2.requestFocus();
																													etzip2.setText("");
																													llcreateanaccount
																															.setVisibility(View.VISIBLE);
																													plus2.setVisibility(View.GONE);
																													minus2.setVisibility(View.VISIBLE);
																												}
																											} else {
																												cf.show_toast("Zip should be 5 characters under Mailing Address", 0);
																												etzip2.requestFocus();
																												etzip2.setText("");
																												llcreateanaccount
																														.setVisibility(View.VISIBLE);
																												plus2.setVisibility(View.GONE);
																												minus2.setVisibility(View.VISIBLE);
																											}
																										} else {
																											cf.show_toast("Please enter Zip under Mailing Address", 0);
																											etzip2.requestFocus();
																											etzip2.setText("");
																											llcreateanaccount
																													.setVisibility(View.VISIBLE);
																											plus2.setVisibility(View.GONE);
																											minus2.setVisibility(View.VISIBLE);
																										}
																									
																								
																						} else {
																							cf.show_toast("Please enter Mailing Address #1", 0);
																							etmailaddress1
																									.requestFocus();
																							etmailaddress1
																									.setText("");
																							llcreateanaccount
																									.setVisibility(View.VISIBLE);
																							plus2.setVisibility(View.GONE);
																							minus2.setVisibility(View.VISIBLE);
																						}
																						// }
																								} else {
																									cf.show_toast("Please select County under Inspection Address", 0);
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																							} else {
																								cf.show_toast("Please select State under Inspection Address", 0);
																								llcreateanaccount
																										.setVisibility(View.VISIBLE);
																								plus2.setVisibility(View.GONE);
																								minus2.setVisibility(View.VISIBLE);
																							}
																								} else {
																									cf.show_toast("Please enter City under Inspection Address", 0);
																									etcity.requestFocus();
																									etcity.setText("");
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																					} else {
																						cf.show_toast("Please enter a valid Zip under Inspection Address", 0);
																						etzip.requestFocus();
																						etzip.setText("");
																						llcreateanaccount
																								.setVisibility(View.VISIBLE);
																						plus2.setVisibility(View.GONE);
																						minus2.setVisibility(View.VISIBLE);
																					}
																				} else {
																					cf.show_toast("Zip should be 5 characters under Inspection Address", 0);
																					etzip.requestFocus();
																					etzip.setText("");
																					llcreateanaccount
																							.setVisibility(View.VISIBLE);
																					plus2.setVisibility(View.GONE);
																					minus2.setVisibility(View.VISIBLE);
																				}
																			} else {
																				cf.show_toast("Please enter Zip under Inspection Address", 0);
																				etzip.requestFocus();
																				etzip.setText("");
																				llcreateanaccount
																						.setVisibility(View.VISIBLE);
																				plus2.setVisibility(View.GONE);
																				minus2.setVisibility(View.VISIBLE);
																			}
																		
																	
															} else {
																cf.show_toast("Please enter Inspection Address #1", 0);
																etinspaddress1
																		.requestFocus();
																etinspaddress1
																		.setText("");
																llcreateanaccount
																		.setVisibility(View.VISIBLE);
																plus2.setVisibility(View.GONE);
																minus2.setVisibility(View.VISIBLE);
															}
														} else {
															cf.show_toast("Please enter valid Phone", 0);
															etphone.requestFocus();
															etphone.setText("");
															llcreateanaccount
																	.setVisibility(View.VISIBLE);
															plus2.setVisibility(View.GONE);
															minus2.setVisibility(View.VISIBLE);
														}
													} else {
														cf.show_toast("Please enter valid Email", 0);
														etmail.requestFocus();
														etmail.setText("");
														llcreateanaccount
																.setVisibility(View.VISIBLE);
														plus2.setVisibility(View.GONE);
														minus2.setVisibility(View.VISIBLE);
													}
												} else {
													cf.show_toast("Please enter Lastname", 0);
													etlastname.requestFocus();
													etlastname.setText("");
													llcreateanaccount
															.setVisibility(View.VISIBLE);
													plus2.setVisibility(View.GONE);
													minus2.setVisibility(View.VISIBLE);
												}
											} else {
												cf.show_toast("Please enter Firstname", 0);
												etfirstname.requestFocus();
												etfirstname.setText("");
												llcreateanaccount
														.setVisibility(View.VISIBLE);
												plus2.setVisibility(View.GONE);
												minus2.setVisibility(View.VISIBLE);
											}
										} else {
											//cf.show_toast("Please Calculate Fees", 0);
											/*etinspectionfees.requestFocus();
											etinspectionfees.setText("");
											llgeneralinfo
													.setVisibility(View.VISIBLE);
											*/
											
											plus1.setVisibility(View.GONE);
											minus1.setVisibility(View.VISIBLE);
										}
									} else {
										cf.show_toast("Please select Preferred Time", 0);
										llgeneralinfo
												.setVisibility(View.VISIBLE);
										plus1.setVisibility(View.GONE);
										minus1.setVisibility(View.VISIBLE);
									}
								} else {
									cf.show_toast("Please enter Preferred Date", 0);
									llgeneralinfo.setVisibility(View.VISIBLE);
									plus1.setVisibility(View.GONE);
									minus1.setVisibility(View.VISIBLE);
									etdate.requestFocus();
									etdate.setText("");
								}
							} else {
								cf.show_toast("Please select Preferred IMC", 0);
								llgeneralinfo.setVisibility(View.VISIBLE);
								plus1.setVisibility(View.GONE);
								minus1.setVisibility(View.VISIBLE);
							}
						} else {
							cf.show_toast("Please enter No Of Buildings", 0);
							etnoofbuildings.requestFocus();
							etnoofbuildings.setText("");
							llgeneralinfo.setVisibility(View.VISIBLE);
							plus1.setVisibility(View.GONE);
							minus1.setVisibility(View.VISIBLE);
						}
					} else {
						cf.show_toast("Please enter No Of Stories", 0);
						etnoofstories.requestFocus();
						etnoofstories.setText("");
						llgeneralinfo.setVisibility(View.VISIBLE);
						plus1.setVisibility(View.GONE);
						minus1.setVisibility(View.VISIBLE);
					}
				} else {
					cf.show_toast("Please select the Year of Construction", 0);
					etotheryear.requestFocus();
					etotheryear.setText("");
					llgeneralinfo.setVisibility(View.VISIBLE);
					plus1.setVisibility(View.GONE);
					minus1.setVisibility(View.VISIBLE);
				}
			} else {
				cf.show_toast("Please select Year of Construction", 0);
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
		} else {
			
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				cf.show_toast("Please enter Square Footage", 0);
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			else
			{
				cf.show_toast("Please enter the Square Footage greater than 399 and less than 1,00,000", 0);
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			
		}
	}

	private boolean fees_calculation_validate() {
		// TODO Auto-generated method stub
		if(!etinspfees.getText().toString().trim().equals(""))
		{
			if(!etinspdiscount.getText().toString().trim().equals(""))
			{
				if(!etinspfees.getText().toString().trim().equals("0"))
				{
					if(!tvtotla.getText().toString().trim().equals("0"))
					{
						return true;
					}
					else
					{
						cf.show_toast("Please enter Inspection Fee greater than  Discount.", 0);
					}
				}
				else
				{
					etinspfees.requestFocus();
					cf.show_toast("Please enter the valild Inspection Fee", 0);
				}
			}
			else
			{
				etinspdiscount.requestFocus();
				cf.show_toast("Please enter the Discount", 0);
			}
		}
		else
		{
			etinspfees.requestFocus();
			cf.show_toast("Please enter the Inspection Fee", 0);
		}
		return false;
	}
	private void Place_Order() {
		if (cbaddresscheck.isChecked()) {
			strstateid2 = strstateid;
			strcountyid2 = strcountyid;
			strcounty2 = strcounty;
		}
		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		strcouponcode = etcouponcode.getText().toString();
		strsquarefootagevalue = etsquarefootage.getText().toString();
		strpolicyno = etpolicyno.getText().toString();
		strnoofstories = etnoofstories.getText().toString();
		strnoofbuildings = etnoofbuildings.getText().toString();
		strdate = etdate.getText().toString();
		strfirstname = etfirstname.getText().toString();
		strlastname = etlastname.getText().toString();
		strmail = etmail.getText().toString();
		strphone = etphone.getText().toString();
		strcity = etcity.getText().toString();
		strzip = etzip.getText().toString();
		strcity2 = etcity2.getText().toString();
		strzip2 = etzip2.getText().toString();
		strinspaddress1 = etinspaddress1.getText().toString();
		strinspaddress2 = etinspaddress2.getText().toString();
		strmailaddress1 = etmailaddress1.getText().toString();
		strmailaddress2 = etmailaddress2.getText().toString();

		if (wb.isInternetOn() == true) {
			cf.show_ProgressDialog("Ordering Inspection...");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(wb.NAMESPACE,
								"OrderInspection");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("CouponID", etcouponcode.getText()
								.toString());
						request.addProperty("InspectorId", dbh.Insp_id);//Integer.parseInt(AgentId)
						request.addProperty("InspectorName", inspectorname);//AgentName
						request.addProperty("Mailing_Address1", strmailaddress1);
						request.addProperty("Mailing_Address2", strmailaddress2);
						request.addProperty("Mailing_StateId",
								Integer.parseInt(strstateid2));
						request.addProperty("Mailing_County", strcounty2);
						request.addProperty("Mailing_Zip", strzip2);
						request.addProperty("Mailing_City", strcity2);
						request.addProperty("Insp_Address1", strinspaddress1);
						request.addProperty("CategoryID", 4);
						request.addProperty("Insp_Address2", strinspaddress2);
						request.addProperty("Insp_StateId",
								Integer.parseInt(strstateid));
						request.addProperty("Insp_County", strcounty);
						request.addProperty("Insp_Zip", strzip);
						request.addProperty("Insp_City", strcity);
						request.addProperty("Firstname", strfirstname);
						request.addProperty("Lastname", strlastname);
						request.addProperty("Email", strmail);
						request.addProperty("Phone", strphone);
						request.addProperty("MainInspTypeID",
								Integer.parseInt(insptypeid));
						request.addProperty("AdditionalInspTypes","");//stradditionalinspectionid
						request.addProperty("AdditionalBuildingTypes","");//stradditionalbuildingid
						request.addProperty("AddBuildindSqFootage",	"");//str_building_edittext_value
						request.addProperty("NoOfBuildings",
								Integer.parseInt(strnoofbuildings));
						request.addProperty("PolicyNumber", strpolicyno);
						request.addProperty("CarrierID", 0);//Integer.parseInt(strcarrierid)
						request.addProperty("AgentID",Integer.parseInt(stragentid));//Integer.parseInt(Agent_Id2)
						request.addProperty("AgencyID", Integer.parseInt(stragencyid));//Integer.parseInt(AgencyId)
						request.addProperty("Re_AgentID",Integer.parseInt(strrealtorid));//Integer.parseInt(Agent_Id2)
						request.addProperty("Re_AgencyID", Integer.parseInt(strrealestatecompanyid));//Integer.parseInt(AgencyId)
						request.addProperty("sqFootage",
								Long.parseLong(strsquarefootagevalue));
						request.addProperty("YearConstruction",
								Integer.parseInt(stryear));
						request.addProperty("NoOfStories",
								Integer.parseInt(strnoofstories));
//						request.addProperty("PreInspID", "");//Integer.parseInt(strinspectorid)
//						request.addProperty("PreCompanyID",
//								Integer.parseInt(strcompanyid));
						request.addProperty("PreDateInsp", strdate);
						request.addProperty("PreTime", strtime);
						request.addProperty("Fees", etinspfees.getText().toString().trim());
						
						request.addProperty("Discount", etinspdiscount.getText()
								.toString());

						envelope.setOutputSoapObject(request);
						System.out.println("OrderInspection request is "
								+ request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								wb.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(wb.NAMESPACE
								+ "OrderInspection", envelope);
						order_result = (SoapObject) envelope.getResponse();
						System.out.println("OrderInspection result is"
								+ order_result);
						if(order_result!=null)
						{
							System.out.println(" the order result "+order_result);
							
							String Status = String.valueOf(order_result.getProperty("Status"));
							System.out.println(" the status"+Status);
							String SRID = String.valueOf(order_result.getProperty("SRID"));
							if(Status.trim().equals("1"))
							{
								Start_import(SRID);
							}
							else
							{
								show_handler = 6;
								handler.sendEmptyMessage(0);
							}
						}
						else
						{
							show_handler = 6;
							handler.sendEmptyMessage(0);
						}
						
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}
				protected void Start_import(String sRID) {
					// TODO Auto-generated method stub
					try {
						cf.selectedhomeid=sRID;
						 getpolicyholder_info();
						 GetAgentinformation();
						 GetRealtorinformation();
						show_handler = 5;
						handler.sendEmptyMessage(0);
					}
					catch (SocketTimeoutException s) {
						System.out.println("s "+s.getMessage());
						show_handler = 5;
						handler.sendEmptyMessage(0);
							
					 } catch (NetworkErrorException n) {
						 show_handler = 5;
							handler.sendEmptyMessage(0);
						   
					 } catch (IOException io) {
						 System.out.println("io "+io.getMessage());
						 show_handler = 5;
							handler.sendEmptyMessage(0);
						    
					 } catch (XmlPullParserException x) {
						 show_handler = 5;
							handler.sendEmptyMessage(0);
						    
					 }
						 catch (Exception e) {
							 show_handler = 5;
								handler.sendEmptyMessage(0);
						}
						 
				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						}
						else if (show_handler == 6) {
							show_handler = 0;
							cf.show_toast("Sorry we can not order your inspection. Please contact Paperless administrator.", 0);

						}else if (show_handler == 5) {
							show_handler = 0;

							
								cf.show_toast("Order Placed Successfully", 0);
								
								Intent intent = new Intent(
										OrderInspection_backup.this, policyholderInformation.class);
								intent.putExtra("SRID", cf.selectedhomeid);
								startActivity(intent);
								finish();

							/*} else {
								cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);
							}*/

						}
					}
				};
			}.start();
		} else {
			cf.show_toast("Internet connection not available", 0);
		}
	}



	private void getpolicyholder_info()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException {
		// TODO Auto-generated method stub
	 SoapObject request = new SoapObject(wb.NAMESPACE,"UpdateMobileDB_SRID");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID",cf.selectedhomeid);
		envelope.setOutputSoapObject(request);
		System.out.println(" the saersdd "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+"UpdateMobileDB_SRID",envelope);
		SoapObject objInsert = (SoapObject) envelope.getResponse();
		 String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
	     PH_Fname,PH_Lname,PH_Mname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees = "",PH_CONTPERSON,CompID,YearBuilt,
	     PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,PH_Noofstories,IsInspected,BuildingSize,
	     PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0",PH_InspectionName,insp_name="",latitude="",longtitude="",PH_MainInspectionTypeId="",
	    		  MailingAddress="",MailingAddress2="",CommercialFlag="",Mailingcity="",MailingState="",MailingCounty="",Mailingzip="";
		 int Cnt;
		 db.CreateTable(2);
		 if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
		 {
			//	Cnt=0;total = 15;
		 }
		 else
		 {
			Cnt = objInsert.getPropertyCount();			
			
			for (int i = 0; i < Cnt; i++) 
			{
				SoapObject obj = (SoapObject) objInsert.getProperty(i);  
				try 
				{
					System.out.println( "property "+obj.toString());
					 HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
					
					 PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
					 PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
					 PH_Mname = (String.valueOf(obj.getProperty("MiddleName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("NA"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("N/A"))?"":String.valueOf(obj.getProperty("MiddleName"));
					 PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
					 PH_Address2 = (String.valueOf(obj.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address2")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address2")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address2"));
					 PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
					 PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
					 PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
					 PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
					 PH_HPhone = (String.valueOf(obj.getProperty("HomePhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("HomePhone"));
					 PH_CPhone = (String.valueOf(obj.getProperty("CellPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("CellPhone"));
					 PH_WPhone = (String.valueOf(obj.getProperty("WorkPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("WorkPhone"));
					 PH_Email = (String.valueOf(obj.getProperty("Email")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Email")).equals("NA"))?"":(String.valueOf(obj.getProperty("Email")).equals("N/A"))?"":String.valueOf(obj.getProperty("Email"));
					 PH_CONTPERSON = (String.valueOf(obj.getProperty("ContactPerson")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("NA"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("N/A"))?"":String.valueOf(obj.getProperty("ContactPerson"));
					 PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
					 PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
					 PH_SubStatus =(String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
					 CompID = (String.valueOf(obj.getProperty("CompanyId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("NA"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("N/A"))?"":String.valueOf(obj.getProperty("CompanyId"));
					 InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
					 ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
					 YearBuilt = (String.valueOf(obj.getProperty("YearBuilt")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("NA"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("N/A"))?"":String.valueOf(obj.getProperty("YearBuilt"));
					 PH_Noofstories = (String.valueOf(obj.getProperty("Nstories")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("NA"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("N/A"))?"":String.valueOf(obj.getProperty("Nstories"));
					 ScheduledCreatedDate = (String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduleCreatedDate"));
					 AssignedDate = (String.valueOf(obj.getProperty("AssignedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("AssignedDate"));
					 InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
					 InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
					 Schedulecomments = (String.valueOf(obj.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionComment"));
					 PH_IsInspected= (String.valueOf(obj.getProperty("IsInspected")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("NA"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("N/A"))?"":String.valueOf(obj.getProperty("IsInspected"));
					 PH_InsuranceCompany = (String.valueOf(obj.getProperty("InsuranceCompany")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuranceCompany"));
					 BuildingSize = (String.valueOf(obj.getProperty("BuildingSize")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingSize"));
					 latitude = (String.valueOf(obj.getProperty("Latitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Latitude"));
					 longtitude = (String.valueOf(obj.getProperty("Longitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Longitude"));				
					// PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
					 MailingAddress= (String.valueOf(obj.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress"));
					 MailingAddress2 = (String.valueOf(obj.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress2"));
					 Mailingcity = (String.valueOf(obj.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingcity"));
					 MailingState = (String.valueOf(obj.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingState"));
					 MailingCounty = (String.valueOf(obj.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingCounty"));				
					 Mailingzip = (String.valueOf(obj.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingzip"));
					 Cursor c1 = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.encode(db.Insp_id) + "' and PH_SRID='"+db.encode(HomeId)+"'");
					if(c1.getCount() >= 1)
					 {
						 c1.moveToFirst();
								 
									 String Sql="UPDATE "
												+ db.policyholder
												+ " SET  PH_FirstName ='"+ db.encode(PH_Fname)+"'," +
												       " PH_LastName  ='"+ db.encode(PH_Lname)+"'," +
												       " PH_Address1 ='"+ db.encode(PH_Address1)+"'," +
												       " PH_Address2 ='"+ db.encode(PH_Address2)+"'," +
												       " PH_City ='"+ db.encode(PH_City)+"'," +
												       " PH_Zip ='"+ db.encode(PH_Zip)+"'," +
												       " PH_State ='"+ db.encode(PH_State)+"', "+
												       " PH_County ='"+ db.encode(PH_County)+"',"+
												       " PH_Policyno ='"+ db.encode(PH_Policyno)+"',"+
												       " PH_Inspectionfees ='"+ db.encode(PH_Inspectionfees)+"',"+
												       " PH_InsuranceCompany ='"+ db.encode(PH_InsuranceCompany)+"',"+
												       " PH_HomePhone ='"+ db.encode(PH_HPhone)+"',"+
												       " PH_WorkPhone ='"+ db.encode(PH_WPhone)+"',"+
												       " PH_CellPhone ='"+ db.encode(PH_CPhone)+"', "+
												       " PH_NOOFSTORIES ='"+db.encode(PH_Noofstories)+"',"+
												       " PH_WEBSITE  ='',"+
												       " PH_Email ='"+ db.encode(PH_Email)+"', "+										       
												       " PH_EmailChkbx ='"+ db.encode(PH_EmailChk)+"', "+
												       " PH_IsInspected ='0',"+
												       " PH_IsUploaded ='0',"+
												       " PH_Status='"+ db.encode(PH_Status)+"',"+
												       " PH_SubStatus='"+ db.encode(PH_SubStatus)+"'," +
												       " Schedule_ScheduledDate ='"+ db.encode(ScheduledDate)+"'," +
												       " Schedule_InspectionStartTime='"+db.encode(InspectionStartTime)+"'," +
												       " Schedule_InspectionEndTime ='"+db.encode(InspectionEndTime)+"'," +
												       " Schedule_Comments ='"+db.encode(Schedulecomments)+"',"+
												       " Schedule_ScheduleCreatedDate ='"+db.encode(ScheduledCreatedDate)+"',"+
												       " Schedule_AssignedDate ='"+db.encode(AssignedDate)+"',"+
												       " ScheduleFlag =0 ," +
												       " YearBuilt ='"+YearBuilt+"',"+
												       " ContactPerson ='"+db.encode(PH_CONTPERSON)+"',BuidingSize='"+db.encode(BuildingSize)+"',fld_latitude='"+latitude+"',fld_longitude='"+longtitude+"' "+
												       " where PH_SRID='" + db.encode(HomeId)+ "' and PH_InspectorId='"+ db.encode(InspectorId)+"'";
									
									 db.wdo_db.execSQL(Sql);
									 
								 }
						 else
						 {
							 /*INSERTING THE TABLE - POLICYHOLDER*/
							
							 db.wdo_db.execSQL("INSERT INTO "
										+ db.policyholder
										+ " (PH_InspectorId,PH_SRID,PH_FirstName,PH_LastName,PH_Address1,PH_Address2," +
										" PH_City,PH_Zip,PH_State,PH_County,PH_Policyno,PH_Inspectionfees,PH_InsuranceCompany," +
										" PH_HomePhone,PH_WorkPhone,PH_CellPhone,PH_Email,PH_EmailChkbx," +
										" PH_IsInspected,PH_IsUploaded,PH_Status,PH_SubStatus,Schedule_ScheduledDate,Schedule_InspectionStartTime,"+
									  " Schedule_InspectionEndTime,Schedule_Comments,Schedule_ScheduleCreatedDate,Schedule_AssignedDate,ScheduleFlag,YearBuilt,ContactPerson,BuidingSize,PH_NOOFSTORIES,PH_WEBSITE,fld_latitude,fld_longitude,fld_homeownersign,fld_homewonercaption,fld_paperworksign,fld_paperworkcaption)"
										+ " VALUES ('"+ db.encode(InspectorId)+"','"
										+ db.encode(HomeId)+"','"
										+ db.encode(PH_Fname)+"','"
										+ db.encode(PH_Lname)+"','"
										+ db.encode(PH_Address1)+"','"
										+ db.encode(PH_Address2)+"','"
										+ db.encode(PH_City)+"','"
										+ db.encode(PH_Zip)+"','"
										+ db.encode(PH_State)+"','"
										+ db.encode(PH_County)+"','"
										+ db.encode(PH_Policyno)+"','"
										+ db.encode(PH_Inspectionfees)+"','"
										+ db.encode(PH_InsuranceCompany)+"','"
										+ db.encode(PH_HPhone)+"','"
										+ db.encode(PH_WPhone)+"','"
										+ db.encode(PH_CPhone)+"','"
										+ db.encode(PH_Email)+"','"
										+ db.encode(PH_EmailChk)+"','0','0','"
										+ db.encode(PH_Status)+"','"
										+ db.encode(PH_SubStatus)+"','"
										+ db.encode(ScheduledDate)+"','"+db.encode(InspectionStartTime)+"','"+db.encode(InspectionEndTime)+"','"
										+ db.encode(Schedulecomments)+"','"+db.encode(ScheduledCreatedDate)+"','"+db.encode(AssignedDate)+"','0','"+YearBuilt+"','"+db.encode(PH_CONTPERSON)+"','"+db.encode(BuildingSize)+"','"+db.encode(PH_Noofstories)+"','','"+latitude+"','"+longtitude+"','','','','')");
						 }
					if(c1!=null)
						c1.close();
					 db.CreateTable(3);
				//	 System.out.println("the mail address"+MailingAddress);
					        Cursor c21 = db.SelectTablefunction(db.MailingPolicyHolder," where ML_PH_InspectorId='" + db.encode(db.Insp_id) + "' and ML_PH_SRID='"+db.encode(HomeId)+"'");
							int rws = c21.getCount();
							if(rws >= 1)
							{
								db.wdo_db.execSQL("UPDATE " + db.MailingPolicyHolder
										+ " SET ML_PH_Address1='" + db.encode(MailingAddress)
										+ "',ML_PH_Address2='"
										+ db.encode(MailingAddress2)
										+ "',ML_PH_City='"
										+ db.encode(Mailingcity)
										+ "',ML_PH_Zip='"
										+ db.encode(Mailingzip) + "',ML_PH_State='"
										+ db.encode(MailingState) + "',ML_PH_County='"
										+ db.encode(MailingCounty) + "' WHERE ML_PH_SRID ='" + db.encode(HomeId)
										+ "'");
							}
							else
							{
								db.wdo_db.execSQL("INSERT INTO "
										+ db.MailingPolicyHolder
										+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
										+ "VALUES ('"+db.encode(HomeId)+"','"+db.encode(InspectorId)+"','0','"+db.encode(MailingAddress)+"','"
									    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
									    + db.encode(Mailingzip)+"','"+db.encode(MailingState)+"','"
									    + db.encode(MailingCounty)+"')");
							}
							if(c21!=null)
								c21.close();

				}
				catch(Exception e)
				{
					System.out.println("Exception"+e.getMessage());
				}
			}
			}
	}
 public void GetAgentinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
	
		 	int Cnt;
			String SRID,AgencyName,AgentName,AgentAddress,AgentAddress2,AgentCity,AgentCounty,AgentRole,AgentState,AgentZip,AgentOffPhone,AgentContactPhone,AgentFax,AgentEmail,AgentWebSite;
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadAgentInformation_SRID");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"LoadAgentInformation_SRID",envelope);
			SoapObject agentinfo = (SoapObject) envelope.getResponse();
			
			//System.out.println("the agent info result is "+agentinfo);
			Cnt=agentinfo.getPropertyCount();
			
			if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
			{
				//Cnt=0;total = 20;
			}
			else
			{
			 	
				db.CreateTable(5);
				
				for(int i=0;i<agentinfo.getPropertyCount();i++)
				{
					SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
					SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					AgencyName=(String.valueOf(temp_result.getProperty("AgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgencyName"));
					AgentName=(String.valueOf(temp_result.getProperty("AgentName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentName"));
					AgentAddress=(String.valueOf(temp_result.getProperty("AgentAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress"));
					AgentAddress2=(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress2"));
					AgentCity=(String.valueOf(temp_result.getProperty("AgentCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCity"));
					AgentCounty=(String.valueOf(temp_result.getProperty("AgentCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCounty"));
					AgentRole=(String.valueOf(temp_result.getProperty("AgentRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentRole"));
					AgentState=(String.valueOf(temp_result.getProperty("AgentState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentState"));
					AgentZip=(String.valueOf(temp_result.getProperty("AgentZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentZip"));
					AgentOffPhone=(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentOffPhone"));
					AgentContactPhone=(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentContactPhone"));
					AgentFax=(String.valueOf(temp_result.getProperty("AgentFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentFax"));
					AgentEmail=(String.valueOf(temp_result.getProperty("AgentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentEmail"));
					AgentWebSite=(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentWebSite"));
					
					try
					{
						Cursor c2=db.SelectTablefunction(db.Agent_tabble," where AI_SRID ='"+SRID+"'");
						if(c2.getCount()==0)
						{
							db.wdo_db.execSQL("Insert into "+db.Agent_tabble+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
									"('"+SRID+"','"+AgencyName+"','"+AgentName+"','"+AgentAddress+"','"+AgentAddress2+"','"+AgentCity+"','"+AgentCounty+"','"+AgentRole+"','"+AgentState+"','"+AgentZip+"','"+db.encode(AgentOffPhone)+"','"+db.encode(AgentContactPhone)+"','"+AgentFax+"','"+AgentEmail+"','"+AgentWebSite+"') ");
						}
						else if(c2.getCount()>=1)
						{
							db.wdo_db.execSQL("UPDATE "+db.Agent_tabble+" SET AI_SRID='"+SRID+"',AI_AgencyName='"+AgencyName+"',AI_AgentName='"+AgentName+"',AI_AgentAddress='"+AgentAddress+"',AI_AgentAddress2='"+AgentAddress2+"'," +
							"AI_AgentCity='"+AgentCity+"',AI_AgentCounty='"+AgentCounty+"',AI_AgentRole='"+AgentRole+"',AI_AgentState='"+AgentState+"',AI_AgentZip='"+AgentZip+"',AI_AgentOffPhone='"+db.decode(AgentOffPhone)+"'," +
							"AI_AgentContactPhone='"+db.decode(AgentContactPhone)+"',AI_AgentFax='"+AgentFax+"',AI_AgentEmail='"+AgentEmail+"',AI_AgentWebSite='"+AgentWebSite+"' WHERE AI_SRID='"+SRID+"'");
						}
						if(c2!=null)
							c2.close();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					}
					
				}
			 }
	}
 public void GetRealtorinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
	 
		 	int Cnt;
			String RealtorSRID,RealtorAgencyName,RealtorName,RealtorAddress,RealtorAddress2,RealtorCity,RealtorCounty,RealtorRole,RealtorState,RealtorZip,RealtorOffPhone,RealtorContactPhone,RealtorFax,RealtorEmail,RealtorWebSite;
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadRealtorInformation_SRID");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.selectedhomeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"LoadRealtorInformation_SRID",envelope);
			SoapObject agentinfo = (SoapObject) envelope.getResponse();
			
			//System.out.println("the realtor info result is "+agentinfo);
			
			
			if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
			{
				//Cnt=0;total = 25;
			}
			else
			{
			 	 
				db.CreateTable(25);
				
				for(int i=0;i<agentinfo.getPropertyCount();i++)
				{
					SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
					RealtorSRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					RealtorAgencyName=(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAgencyName"));
					RealtorName=(String.valueOf(temp_result.getProperty("RealtorName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorName"));
					RealtorAddress=(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress"));
					RealtorAddress2=(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress2"));
					RealtorCity=(String.valueOf(temp_result.getProperty("RealtorCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCity"));
					RealtorCounty=(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCounty"));
					RealtorRole=(String.valueOf(temp_result.getProperty("RealtorRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorRole"));
					RealtorState=(String.valueOf(temp_result.getProperty("RealtorState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorState"));
					RealtorZip=(String.valueOf(temp_result.getProperty("RealtorZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorZip"));
					RealtorOffPhone=(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorOffPhone"));
					RealtorContactPhone=(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorContactPhone"));
					RealtorFax=(String.valueOf(temp_result.getProperty("RealtorFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorFax"));
					RealtorEmail=(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorEmail"));
					RealtorWebSite=(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorWebSite"));
					
					try
					{
						Cursor c2=db.SelectTablefunction(db.Realtor_table," where AI_SRID ='"+RealtorSRID+"'");
						if(c2.getCount()==0)
						{
							db.wdo_db.execSQL("Insert into "+db.Realtor_table+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
									"('"+RealtorSRID+"','"+RealtorAgencyName+"','"+RealtorName+"','"+RealtorAddress+"','"+RealtorAddress2+"','"+RealtorCity+"','"+RealtorCounty+"','"+RealtorRole+"','"+RealtorState+"','"+RealtorZip+"','"+db.encode(RealtorOffPhone)+"','"+db.encode(RealtorContactPhone)+"','"+RealtorFax+"','"+RealtorEmail+"','"+RealtorWebSite+"') ");
						}
						else if(c2.getCount()>=1)
						{
							db.wdo_db.execSQL("UPDATE "+db.Realtor_table+" SET AI_SRID='"+RealtorSRID+"',AI_AgencyName='"+RealtorAgencyName+"',AI_AgentName='"+RealtorName+"',AI_AgentAddress='"+RealtorAddress+"',AI_AgentAddress2='"+RealtorAddress2+"'," +
							"AI_AgentCity='"+RealtorCity+"',AI_AgentCounty='"+RealtorCounty+"',AI_AgentRole='"+RealtorRole+"',AI_AgentState='"+RealtorState+"',AI_AgentZip='"+RealtorZip+"',AI_AgentOffPhone='"+RealtorOffPhone+"'," +
							"AI_AgentContactPhone='"+RealtorContactPhone+"',AI_AgentFax='"+RealtorFax+"',AI_AgentEmail='"+RealtorEmail+"',AI_AgentWebSite='"+RealtorWebSite+"' WHERE AI_SRID='"+RealtorSRID+"'");
						}
						if(c2!=null)
							c2.close();
					}
					catch(Exception e)
					{
						System.out.println("he issues is "+e.getMessage());
					
					}

					
				}
			 }
	}	 
 
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(OrderInspection_backup.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				/*System.out.println("current date " + date1);
				System.out.println("selected date " + date2);*/
			/*	if (date2.compareTo(date1) <= 0) {
					
					cf.show_toast("Please select Future Date", 0);
					v.setText("");
				} else {
					System.out.println("inside date else");
				}*/
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
	private void Calculate_Fees() {
		noofbuild = Integer.parseInt(etnoofbuildings.getText().toString());
		if (etsquarefootage.getText().toString().trim().equals("")) {
			cf.show_toast("Please enter Square Footage", 0);
			etsquarefootage.requestFocus();
		} else {

			llcalculatefees.setVisibility(View.VISIBLE);

			squarefootagevalue = Float.parseFloat(etsquarefootage.getText()
					.toString());
			inspectionbasesq_feet = Float.parseFloat(strinspectionbasesq_feet);
			inspectionbase_fee = Float.parseFloat(strinspectionbase_fee);
			inspectionadditionalsq_feet = Float
					.parseFloat(strinspectionadditionalsq_feet);
			inspectionaddtional_fee = Float
					.parseFloat(strinspectionaddtional_fee);
			if (squarefootagevalue <= inspectionbasesq_feet) {
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
				Calculate_discount();

			} else {
				if ((squarefootagevalue - inspectionbasesq_feet) <= inspectionadditionalsq_feet) {
					inspectionbase_fee = inspectionbase_fee
							+ inspectionaddtional_fee;

				} else if ((squarefootagevalue - inspectionbasesq_feet) > inspectionadditionalsq_feet) {
					inspectionbase_fee = inspectionbase_fee
							+ (((squarefootagevalue - inspectionbasesq_feet) / inspectionadditionalsq_feet) * inspectionaddtional_fee);

				}
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
				System.out.println("inspection base fees is "+inspectionbase_fee);
				Calculate_discount();
			}
		}
	}
	
	private void Display_Fees() {
		totalvalue = Float.parseFloat(etinspectionfees.getText().toString())
				- Float.parseFloat(etdiscount.getText().toString());
		System.out.println("The total value is " + totalvalue);
		 ettotalfees.setText(String.valueOf(totalvalue));
	}

	private void Calculate_discount() {
		System.out.println("Inside calculate discount");
		
		int companyid = 0;//spinnerimc.getSelectedItemPosition();
		String strcompanyid = arraycompanyid[companyid];

		strcouponcode = etcouponcode.getText().toString();

		System.out.println("The inspetion fees is "
				+ etinspectionfees.getText().toString());

		LoadDiscount(strcouponcode, etinspectionfees.getText().toString(),
				strcompanyid, insptypeid);
	}

	public void LoadDiscount(final String couponcode,
			final String inspectionfee, final String companyid,
			final String inspectionid) {
		System.out.println("Inside load discount");
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>"
					+ "Calculating Discount..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wb.Calling_WS_LoadInspectionType_Discount(
								couponcode, inspectionfee, companyid,
								inspectionid, "LoadInspectionType_Discount");
						System.out
								.println("response LoadInspectionType_Discount"
										+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("here is a problem on your application. Please contact Paperless administrator.", 0);
							
						} else if (show_handler == 5) {
							show_handler = 0;

							strdiscountvalue = String.valueOf(chklogin
									.getProperty("Discount"));
							discountvalue2 += Float.parseFloat(strdiscountvalue);
							etdiscount
							.setText(String.valueOf(strdiscountvalue));
							Display_Fees();

						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			
		}
	}

	class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub

			if (isChecked) {
				strinspaddress1 = etinspaddress1.getText().toString();
				strinspaddress2 = etinspaddress2.getText().toString();
				strcity = etcity.getText().toString();
				strzip = etzip.getText().toString();
				if (!strinspaddress1.equals("")) {
					if (!strstate.equals("--Select--")) {
						if (!strcounty.equals("--Select--")
								&& !strcounty.equals("")) {
							if (!strcity.equals("")) {
								if (!strzip.equals("")) {
									if ((strzip.length() == 5)) {

										call_county2 = false;

										etmailaddress1.setText(strinspaddress1);
										etmailaddress2.setText(strinspaddress2);
										etcity2.setText(strcity);
										etzip2.setText(strzip);

										arraystateid2 = arraystateid;
										arraystatename2 = arraystatename;
										arraycountyid2 = arraycountyid;
										arraycountyname2 = arraycountyname;

										spinnerstate2.setAdapter(stateadapter);
										spinnercounty2
												.setAdapter(countyadapter);
										spinnerstate2.setSelection(spinnerstate
												.getSelectedItemPosition());

									} else {
										cf.show_toast("Zip should be 5 characters", 0);
										etzip.requestFocus();
										cbaddresscheck.setChecked(false);
									}
								} else {
									cf.show_toast("Please enter Zip", 0);
									etzip.requestFocus();
									cbaddresscheck.setChecked(false);
								}
							} else {
								cf.show_toast("Please enter City", 0);
								etcity.requestFocus();
								cbaddresscheck.setChecked(false);
							}
						} else {
							cf.show_toast("Please select County", 0);
							cbaddresscheck.setChecked(false);
						}
					} else {
						cf.show_toast("Please select State", 0);
						cbaddresscheck.setChecked(false);
					}
				} else {
					cf.show_toast("Please enter Inspection Address1", 0);
					etinspaddress1.requestFocus();
					cbaddresscheck.setChecked(false);
				}
			} else {

				etmailaddress1.setText("");
				etmailaddress2.setText("");
				etcity2.setText("");
				etzip2.setText("");
				spinnerstate2.setSelection(stateadapter.getPosition(strselect));

				call_county2 = true;
			}

		}

	}


	public void LoadInspectionTypeDescription(final String id,
			final String companyid) {

		System.out.println("Inside Load Inspection Types");
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wb.Calling_WS_LoadInspectionTypeDescription(
								id, strcompanyid, "LoadInspectionTypeDetails");
						System.out
								.println("response LoadInspectionTypedescriptionDetails"
										+ chklogin);
						// LoadInspectionTypeDescription(chklogin);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);
							
						} else if (show_handler == 5) {
							show_handler = 0;

							if (chklogin.getPropertyCount() < 1) {
								System.out.println("Inside if");
								cf.show_toast("Company not in list", 0);
							} else {
								System.out.println("Inside else");
								LoadInspectionTypeDescription(chklogin);
							}
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			
		}
	}

	public void LoadInspectionTypeDescription(SoapObject chklogin) {

		SoapObject objInsert = (SoapObject) chklogin.getProperty(0);

		String Description = String.valueOf(objInsert
				.getProperty("Description"));
		ImageName = String.valueOf(objInsert.getProperty("ImageName"));
		strinspectionbasesq_feet = String.valueOf(objInsert
				.getProperty("Base_Sqft"));
		strinspectionbase_fee = String.valueOf(objInsert
				.getProperty("Base_Fee"));
		System.out.println("inspectionbase_fee is " + strinspectionbase_fee);
		strinspectionadditionalsq_feet = String.valueOf(objInsert
				.getProperty("Additional_Sqft"));
		strinspectionaddtional_fee = String.valueOf(objInsert
				.getProperty("Additional_Fee"));

		description.setText(Description);
		System.out.println("The Image Name is " + ImageName);

		// Drawable drawable = LoadImageFromWebOperations(ImageName);
		// descriptionimage.setImageDrawable(drawable);

		new DownloadImageTask(descriptionimage).execute(ImageName);

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog mDialog;
		private ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected void onPreExecute() {

			// mDialog =
			// ProgressDialog.show(ChartActivity.this,"Please wait...",
			// "Retrieving data ...", true);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			System.out.println("the image "+urldisplay);
			Bitmap mIcon11 = null;
			try {
				URL ulrn = new URL(urldisplay.replace(" ", "%20"));
				HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
				InputStream is = con.getInputStream();
				System.out.println("comes correctly1 image");
				mIcon11 = BitmapFactory.decodeStream(is);
				//InputStream in = new java.net.URL(urldisplay).openStream();
				System.out.println("comes correctly2 image");
				//mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", "image download error");
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			// set image of your imageview
			bmImage.setImageBitmap(result);
			// close
			// mDialog.dismiss();
		}
	}
	public String LoadState() {
		try {
			System.out.println("Start load state");
			dbh1 = new DataBaseHelper1(OrderInspection_backup.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = dbh.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = dbh.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();
			System.out.println("end load state");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(OrderInspection_backup.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + dbh.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = dbh.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = dbh.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(OrderInspection_backup.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}

	public void LoadCounty2(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(OrderInspection_backup.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + dbh.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = dbh.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = dbh.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData2();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData2() {
		countyadapter2 = new ArrayAdapter<String>(OrderInspection_backup.this,
				android.R.layout.simple_spinner_item, arraycountyname2);
		countyadapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty2.setAdapter(countyadapter2);
		
		if(countysetselection)
		{
			spinnercounty2.setSelection(countyadapter2.getPosition(county));
			countysetselection=false;
		}
		
	}

	
	private void Check_Couponcode()
	{
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				String chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = wb
								.Calling_WS_CheckCouponcode(etcouponcode.getText().toString().trim(),"CHECKCOUPONID");
						System.out.println("response CHECKCOUPONID" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							if(chklogin1.toLowerCase().equals("false"))
							{
								etcouponcode.setText("");
								etcouponcode.requestFocus();
								cf.show_toast("Coupon code is not valid", 0);
							}
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);

		}
	}
	
	private void Load_State_County_City(final EditText et)
	{

		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = wb
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);
							
						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								cf.show_toast("Please enter a valid Zip", 0);
								
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
	{
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			if(zipidentifier.equals("zip1"))
			{
				spinnerstate.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity.setText(city);
			}
			else if(zipidentifier.equals("zip2"))
			{
				spinnerstate2.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity2.setText(city);
			}
			
	}
	
	public void LoadBuildingType(final String id) {

		System.out.println("Inside Load building Types");
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = wb.Calling_WS_LoadBuildingTypes(
								id, "FillBuildingType");
						LoadBuildingType(chklogin);

						System.out.println("response FillBuildingType"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> btypeadapter = new ArrayAdapter<String>(
									OrderInspection_backup.this,
									android.R.layout.simple_spinner_item,
									arr_bType);
							btypeadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerbuildintype.setAdapter(btypeadapter);

						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
		}
	}

	public void LoadBuildingType(SoapObject objInsert) {
		dbh.CreateTable(18);
		dbh.wdo_db.execSQL("delete from " + dbh.LoadbuildingType);
		int n = objInsert.getPropertyCount();
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String buildingtype = String.valueOf(obj.getProperty("Type"));
				dbh.wdo_db.execSQL("insert into " + dbh.LoadbuildingType
						+ " (id,buildingtype) values('1','"
						+ dbh.encode(buildingtype) + "');");

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("catch" + e.getMessage());
			}
		}
		LoadBuildingTypeData();
	}

	public void LoadBuildingTypeData() {
		Cursor cur = dbh.wdo_db.rawQuery("select * from " + dbh.LoadbuildingType,
				null);
		int rows = cur.getCount();
		arr_bType = new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {

				arr_bType[i] = dbh.decode(cur.getString(cur
						.getColumnIndex("buildingtype")));

				i++;
			} while (cur.moveToNext());
		}
	}
	
	public void LoadAgency() {
		System.out.println("Start load agency");
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = wb
								.Calling_WS_LoadAgency("GETAGENCYNAME");
						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						// InsertData(obj);
						LoadAgency(chklogin1);
						System.out.println("Ends load agency");
					System.out.println("response GETAGENCYNAME" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							agencyadapter = new ArrayAdapter<String>(
									OrderInspection_backup.this,
									android.R.layout.simple_spinner_item,
									arrayagencyname);
							agencyadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinneragency.setAdapter(agencyadapter);
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			

		}
	}

	public void LoadAgency(SoapObject objInsert) {
		dbh.CreateTable(19);
		dbh.wdo_db.execSQL("delete from " + dbh.LoadAgency);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadAgency property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("AgencyID"));
				String Name = String.valueOf(obj.getProperty("Agencyname"));
				dbh.wdo_db.execSQL("insert into " + dbh.LoadAgency
						+ " (id,Name) values('" + dbh.encode(id) + "','"
						+ dbh.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadAgencyData();

	}

	public void LoadAgencyData() {
		Cursor cur = dbh.wdo_db.rawQuery("select * from " + dbh.LoadAgency+" order by Name COLLATE NOCASE", null);
		int rows = cur.getCount();
		// arraycompanyid = new String[rows + 1];
		// arraycompanyname = new String[rows + 1];
		arrayagencyid = new String[rows+1];
		arrayagencyname = new String[rows+1];
		arrayagencyid[0] = "0";
		arrayagencyname[0] = "--Select--";
		System.out.println("LoadAgency count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = dbh.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = dbh.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayagencyid[i] = ID;
				arrayagencyname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}
	
	public void LoadAgent(final String agencyid) {
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = wb
								.Calling_WS_LoadAgent(agencyid,"GETAGENTNAME");
//						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						LoadAgent(chklogin1);
						System.out.println("response GETAGENTNAME" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							agentadapter = new ArrayAdapter<String>(
									OrderInspection_backup.this,
									android.R.layout.simple_spinner_item,
									arrayagentname);
							agentadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinneragent.setAdapter(agentadapter);
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			

		}
	}

	public void LoadAgent(SoapObject objInsert) {
		dbh.CreateTable(20);
		dbh.wdo_db.execSQL("delete from " + dbh.LoadAgent);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadAgent property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("AgentID"));
				String Name = String.valueOf(obj.getProperty("Agentname"));
				dbh.wdo_db.execSQL("insert into " + dbh.LoadAgent
						+ " (id,Name) values('" + dbh.encode(id) + "','"
						+ dbh.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadAgentData();

	}

	public void LoadAgentData() {
		Cursor cur = dbh.wdo_db.rawQuery("select * from " + dbh.LoadAgent+" order by Name COLLATE NOCASE", null);
		int rows = cur.getCount();
		arrayagentid = new String[rows+1];
		arrayagentname = new String[rows+1];
		arrayagentid[0] = "0";
		arrayagentname[0] = "--Select--";
		System.out.println("LoadAgent count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = dbh.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = dbh.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayagentid[i] = ID;
				arrayagentname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}
	
	public void LoadRealEstateCompany() {
		System.out.println("Start load real estate company");
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = wb
								.Calling_WS_LoadRealEstateCompany("GETREALTORCOMPANYDETAILS");
						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						LoadRealEstateCompany(chklogin1);
						
						System.out.println("response GETREALTORCOMPANYDETAILS" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							realestatecompanyadapter = new ArrayAdapter<String>(
									OrderInspection_backup.this,
									android.R.layout.simple_spinner_item,
									arrayrealestatecompanyname);
							realestatecompanyadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerrealestatecompany.setAdapter(realestatecompanyadapter);
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			

		}
	}

	public void LoadRealEstateCompany(SoapObject objInsert) {
		dbh.CreateTable(21);
		dbh.wdo_db.execSQL("delete from " + dbh.LoadRealEstateCompany);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadRealEstateCompany property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("RealtorcompID"));
				String Name = String.valueOf(obj.getProperty("Realtorcompname"));
				dbh.wdo_db.execSQL("insert into " + dbh.LoadRealEstateCompany
						+ " (id,Name) values('" + dbh.encode(id) + "','"
						+ dbh.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadRealEstateCompanyData();

	}

	public void LoadRealEstateCompanyData() {
		Cursor cur = dbh.wdo_db.rawQuery("select * from " + dbh.LoadRealEstateCompany+" order by Name COLLATE NOCASE", null);
		int rows = cur.getCount();
		arrayrealestatecompanyid = new String[rows+1];
		arrayrealestatecompanyname = new String[rows+1];
		arrayrealestatecompanyid[0] = "0";
		arrayrealestatecompanyname[0] = "--Select--";
		System.out.println("LoadRealEstateCompany count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = dbh.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = dbh.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayrealestatecompanyid[i] = ID;
				arrayrealestatecompanyname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}
	
	public void LoadRealtor(final String realtorcompanyid) {
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection_backup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = wb
								.Calling_WS_LoadRealtor(realtorcompanyid,"GETREALTORNAME");
//						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						LoadRealtor(chklogin1);
						System.out.println("response GETREALTORNAME" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.", 0);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							realtoradapter = new ArrayAdapter<String>(
									OrderInspection_backup.this,
									android.R.layout.simple_spinner_item,
									arrayrealtorname);
							realtoradapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerrealtor.setAdapter(realtoradapter);
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available", 0);
			

		}
	}

	public void LoadRealtor(SoapObject objInsert) {
		dbh.CreateTable(22);
		dbh.wdo_db.execSQL("delete from " + dbh.LoadRealtor);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadRealtor property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("RealtorID"));
				String Name = String.valueOf(obj.getProperty("RealtorName"));
				dbh.wdo_db.execSQL("insert into " + dbh.LoadRealtor
						+ " (id,Name) values('" + dbh.encode(id) + "','"
						+ dbh.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadRealtorData();

	}

	public void LoadRealtorData() {
		Cursor cur = dbh.wdo_db.rawQuery("select * from " + dbh.LoadRealtor+" order by Name COLLATE NOCASE", null);
		int rows = cur.getCount();
		arrayrealtorid = new String[rows+1];
		arrayrealtorname = new String[rows+1];
		arrayrealtorid[0] = "0";
		arrayrealtorname[0] = "--Select--";
		System.out.println("LoadRealtor count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = dbh.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = dbh.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayrealtorid[i] = ID;
				arrayrealtorname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}

	class check implements OnCheckedChangeListener {
		int i;

		public check(int i) {
			// TODO Auto-generated constructor stub
			this.i = i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			RadioButton checkedRadioButton = (RadioButton) group
					.findViewById(checkedId);
			boolean isChecked = checkedRadioButton.isChecked();
			if (isChecked) {
				switch (i) {
				case 4:
					strtime = checkedRadioButton.getText().toString().trim();
					break;
					
				case 5:
					strworkingwith = checkedRadioButton.getText().toString().trim();
					if(strworkingwith.equals("Agency"))
					{
						llagency.setVisibility(View.VISIBLE);
						llrealestateagency.setVisibility(View.GONE);
					}
					else if(strworkingwith.equals("Real Estate Company"))
					{
						llagency.setVisibility(View.GONE);
						llrealestateagency.setVisibility(View.VISIBLE);
					}
					else
					{
						llagency.setVisibility(View.GONE);
						llrealestateagency.setVisibility(View.GONE);
					}
					break;

				}
			}
		}
	}

	private Drawable LoadImageFromWebOperations(String url) {
		// TODO Auto-generated method stub
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			System.out.println("Exc=" + e);
			return null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent inthome = new Intent(OrderInspection_backup.this, HomeScreen.class);
		startActivity(inthome);
		finish();
	}
	
}
