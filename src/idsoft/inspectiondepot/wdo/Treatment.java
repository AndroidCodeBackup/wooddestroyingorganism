package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.MultiSpinner;
import idsoft.inspectiondepot.wdo.supportclass.Option_list;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

public class Treatment extends Activity{
CommonFunction cf;
DataBaseHelper db;
RadioButton companyhas[]=new RadioButton[2],termsandcondtion[]=new RadioButton[4],method[]=new RadioButton[2],evidence[]=new RadioButton[2];
EditText observed,organism_other,typet_other,notic_other,pesticide,methodscomment,specific_location,treatmentcomments;
TableLayout companyyes;
Validation va;
Spinner sp_notice;
MultiSpinner ms_typet,ms_organism;
TextView tv_organism,tv_typet;
Option_list op1,op2,op3;
private boolean load_comment=true;
RadioGroup rdg, rdgcomp, rdgmethod;

		@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.treatment);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		db=new DataBaseHelper(this);
		
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Inspection Information=>Treatment",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,3,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		Decleration();
		show_savedvalue();
		cf.setTouchListener(findViewById(R.id.content));
		va=new Validation(cf);
	}
	private void show_savedvalue() {
			// TODO Auto-generated method stub
		
		db.CreateTable(8);
		Cursor c=db.SelectTablefunction(db.treatment, " WHERE T_SRID='"+cf.selectedhomeid+"'");
			if(c.getCount()>0)
			{
				c.moveToFirst();
				if(db.decode(c.getString(c.getColumnIndex("T_Enable"))).equals("true"))
				{
					evidence[0].setChecked(true);
					
				}
				else
				{
					evidence[1].setChecked(true);
				}
				
					observed.setText(db.decode(c.getString(c.getColumnIndex("T_observed"))));
					op1.set_selectionRD(db.decode(c.getString(c.getColumnIndex("T_noticeofinspection"))));
					//notice.setText(db.decode(c.getString(c.getColumnIndex("T_noticeofinspection"))));
					notic_other.setText(db.decode(c.getString(c.getColumnIndex("T_notice_other"))));
					if(!db.decode(c.getString(c.getColumnIndex("T_typet"))).trim().equals("N/A"))
					{
						op2.set_selectionCHk(db.decode(c.getString(c.getColumnIndex("T_typet"))));
					}
					
					typet_other.setText(db.decode(c.getString(c.getColumnIndex("T_typet_other"))));
					if(db.decode(c.getString(c.getColumnIndex("T_companyhastreated"))).equals("true"))
					{
						companyhas[0].setChecked(true);
						companyyes.setVisibility(View.VISIBLE);
						op3.set_selectionCHk(db.decode(c.getString(c.getColumnIndex("T_organism"))));
						organism_other.setText(db.decode(c.getString(c.getColumnIndex("T_organism_other"))));
						pesticide.setText(db.decode(c.getString(c.getColumnIndex("T_nameofpesticide"))));
						cf.setValuetoRadio(termsandcondtion,db.decode(c.getString(c.getColumnIndex("T_termsandcon"))));
						cf.setValuetoRadio(method,db.decode(c.getString(c.getColumnIndex("T_methodoftreat"))));
						methodscomment.setText(db.decode(c.getString(c.getColumnIndex("T_comments"))));
						specific_location.setText(db.decode(c.getString(c.getColumnIndex("T_Treatlocation"))));
						
					}
					else
					{
						companyhas[1].setChecked(true);
					}
				
			}
			if(c!=null)
				c.close();
		}
	private void Decleration() {
		// TODO Auto-generated method stub
		rdg = (RadioGroup)findViewById(R.id.treatment_RGevidence);
		rdgcomp = (RadioGroup)findViewById(R.id.treatment_RGcompanyhas);
		rdgmethod = (RadioGroup)findViewById(R.id.methodrd);
		evidence[0]=(RadioButton) findViewById(R.id.treatment_RDyesevidence);
		evidence[1]=(RadioButton) findViewById(R.id.treatment_RDnoevidence);
		ms_organism=(MultiSpinner) findViewById(R.id.treatment_msorganism);
		ms_typet=(MultiSpinner) findViewById(R.id.treatment_mstermites);
		sp_notice=(Spinner) findViewById(R.id.treatment_spnotice);
		tv_organism=(TextView) findViewById(R.id.treatment_tvorganism);
		tv_typet=(TextView) findViewById(R.id.treatment_tvtermites);
		typet_other=(EditText) findViewById(R.id.treatment_edtermites_other);
		notic_other=(EditText) findViewById(R.id.treatment_ednotice_other);
		companyhas[0]=(RadioButton) findViewById(R.id.treatment_RDyescompanyhas);
		companyhas[1]=(RadioButton) findViewById(R.id.treatment_RDnocompanyhas);
		termsandcondtion[0]=(RadioButton) findViewById(R.id.treatment_RDterms1);
		termsandcondtion[1]=(RadioButton) findViewById(R.id.treatment_RDterms2);
		termsandcondtion[2]=(RadioButton) findViewById(R.id.treatment_RDterms3);
		termsandcondtion[3]=(RadioButton) findViewById(R.id.treatment_RDterms4);
		method[0]=(RadioButton) findViewById(R.id.treatment_RDmethod1);
		method[1]=(RadioButton) findViewById(R.id.treatment_RDmethod2);
		
		observed =(EditText) findViewById(R.id.treatment_edobserved);
		treatmentcomments =(EditText) findViewById(R.id.treatment_EDmethodcomments);
	//	notice =(EditText) findViewById(R.id.treatment_ednotice);
		pesticide =(EditText) findViewById(R.id.treatment_EDpesticide);
		organism_other =(EditText) findViewById(R.id.treatment_EDorganism_other);
		methodscomment =(EditText) findViewById(R.id.treatment_EDmethodcomments);
		specific_location =(EditText) findViewById(R.id.treatment_EDspecifylocation);
		companyyes=(TableLayout) findViewById(R.id.treatment_tblcompanyhas);
		
		
		
		
		companyhas[0].setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(companyhas[0].isChecked())
				{
					companyyes.setVisibility(View.VISIBLE);
				}
			}
		});
		companyhas[1].setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(companyhas[1].isChecked())
				{
					clearcompny();
					companyyes.setVisibility(View.GONE);
				}
			}
		});
		
		observed.addTextChangedListener(new TextWatchLimit(observed, 150,((TextView) findViewById(R.id.treatment_observed_limit)),cf.addendm_txt));
		treatmentcomments.addTextChangedListener(new TextWatchLimit(treatmentcomments, 150,((TextView) findViewById(R.id.treatment_comments_limit)),cf.addendm_txt));
		op1=new Option_list(this, sp_notice, 23, 1, notic_other);
		op2=new Option_list(this, ms_typet, 23, 2, typet_other,tv_typet,"");
		op3=new Option_list(this, ms_organism, 23, 3, organism_other,tv_organism,"");
		
	}
	public void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.treatment_RDyesevidence:
	            if (checked)
	            evidence[0].setEnabled(true);
				observed.setEnabled(true);
				evidence[0].setChecked(true);
				observed.setText("");
				ms_typet.clearchk();
				tv_typet.setText("");
				ms_typet.setEnabled(true);
				findViewById(R.id.load_comments_treatment).setEnabled(true);
				/*observed.setFocusableInTouchMode(true);
				observed.setFocusable(true);*/
	            break;
	        case R.id.treatment_RDnoevidence:
	            if (checked)
	            	evidence[1].setEnabled(true); evidence[1].setChecked(true);
					
					op2.clear_selectionCHK();
					observed.setText("N/A");
					observed.setEnabled(false);
					findViewById(R.id.load_comments_treatment).setEnabled(false);
					/*observed.setFocusableInTouchMode(false);
					observed.setFocusable(false);*/
					
					tv_typet.setText("You have selected N/A");
					tv_typet.setVisibility(View.VISIBLE);
					ms_typet.setEnabled(false);
	            break;
	    }
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.clear:
				clear_all();
			break;
			case R.id.help_treatment:
				cf.show_help("If Yes, the structure exhibits evidence of previous treatment. List what was observed.");	
			break;

			case R.id.save:
				if(validate())
				{
					System.out.println("tets valdiate true");
					if(save_data())
					{
						cf.go_back(NoAccess.class);
					}
				}
				
			break;
			case R.id.load_comments_treatment:
				int len=observed.getText().toString().length();
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments_treatment);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 4);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
			break;
			case R.id.load_treatmentcomments:
				int len1=observed.getText().toString().length();
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_treatmentcomments);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 5);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len1);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
			break;
		}
			
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;System.out.println("resultCode="+resultCode);
					if(resultCode==RESULT_OK)
					{System.out.println("id="+data.getExtras().getInt("id"));
						if(data.getExtras().getInt("id")==R.id.load_comments_treatment)
						{
							observed.setText((observed.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						}
						else if(data.getExtras().getInt("id")==R.id.load_treatmentcomments)
						{
							treatmentcomments.setText((treatmentcomments.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						}
						
					}
					
					/*else if(resultCode==RESULT_CANCELED)
					{
						cf.show_toast("You have canceled  the comments selction ",0);
					}*/
			}		 		
	 		

	 	}
	private boolean save_data() {
		// TODO Auto-generated method stub
		
		String T_Enable = "",T_observed,T_notice,T_notice_other,T_termitt,T_termitt_other,T_company,T_organism,T_organism_other,T_pesticide,T_terms,T_methodof,T_comments,T_Treatlocation;
		if(evidence[0].isChecked())
			T_Enable="true";
		else if(evidence[1].isChecked())
			T_Enable="false";
		T_observed=db.encode(observed.getText().toString().trim());
		T_notice=db.encode(sp_notice.getSelectedItem().toString().trim());
		T_notice_other=db.encode(notic_other.getText().toString().trim());
		if(companyhas[0].isChecked())
			T_company="true";
		else if(companyhas[1].isChecked())
			T_company="false";
		else
			T_company="";
		
		T_organism=db.encode(op3.get_selected());
		T_organism_other=db.encode(organism_other.getText().toString().trim());
		T_termitt=db.encode(op2.get_selected());
		T_termitt_other=db.encode(typet_other.getText().toString().trim());
		T_pesticide=db.encode(pesticide.getText().toString().trim());
		T_terms=db.encode(cf.getvaluefromchk(termsandcondtion));
		T_methodof=db.encode(cf.getvaluefromchk(method));
		T_comments=db.encode(methodscomment.getText().toString().trim());
		T_Treatlocation=db.encode(specific_location.getText().toString().trim());
		Cursor c =db.SelectTablefunction(db.treatment, " Where T_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			db.wdo_db.execSQL(" UPDATE "+db.treatment+" SET T_Enable='"+T_Enable+"',T_observed='"+T_observed+"',T_noticeofinspection='"+T_notice+"',T_notice_other='"+T_notice_other+"',T_typet='"+T_termitt+"',T_typet_other='"+T_termitt_other+"',T_companyhastreated='"+T_company+"'," +
			"T_organism='"+T_organism+"',T_organism_other='"+T_organism_other+"',T_nameofpesticide='"+T_pesticide+"',T_termsandcon='"+T_terms+"',T_methodoftreat='"+T_methodof+"',T_comments='"+T_comments+"'," +
			"T_Treatlocation='"+T_Treatlocation+"' Where  T_SRID='"+cf.selectedhomeid+"'");
		}
		else
		{
			db.wdo_db.execSQL(" INSERT INTO "+db.treatment+" (T_InspectorId,T_SRID,T_Enable,T_observed,T_noticeofinspection,T_notice_other,T_typet,T_typet_other,T_companyhastreated,T_organism,T_organism_other," +
					"T_nameofpesticide,T_termsandcon,T_methodoftreat,T_comments,T_Treatlocation) VALUES " +
					"('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+T_Enable+"','"+T_observed+"','"+T_notice+"','"+T_notice_other+"','"+T_termitt+"','"+T_termitt_other+"','"+T_company+"','"+T_organism+"','"+T_organism_other+"'," +
					"'"+T_pesticide+"','"+T_terms+"','"+T_methodof+"','"+T_comments+"','"+T_Treatlocation+"')");
		}
		if(c!=null)
			c.close();
		cf.show_toast("Treatment saved successfully", 0);
		return true;
	}
	private boolean validate() {
		// TODO Auto-generated method stub
		db.getpolicyholderstatus(cf.selectedhomeid);
		
		if(va.validate(evidence, "Evidence of previous treatment observed"))
		{
			if(va.validate(observed, "Observed"))
			{
				if(va.validate(sp_notice, "A notice of inspection has been affixed to the structure at","-Select-"))
				{
					if(va.validate_other(notic_other,"A notice of inspection has been affixed to the structure at","Other"))
					{
						if(!db.state.equals("Florida"))
						{
							if(va.validate(tv_typet,"Types of termites","You have selected") || !ms_typet.isEnabled())
							{
								if(va.validate_other(typet_other,"Types of termites","Other"))
								{
									if(va.validate(companyhas, "This company has treated the structure(s) at the time of inspection"))
									{
										if(companyhas[0].isChecked())
										{
											if(va.validate(tv_organism,"Common name of organism treated","You have selected"))
											{
												if(va.validate_other(organism_other,"Common name of organism treated","Other"))
												{
													if(va.validate(pesticide,"Name of pesticide used"))
													{
														if(va.validate(method,"Method of treatment"))
															{
																if(va.validate(specific_location,"Specify treatment notice location"))
																{
																	if(va.validate(termsandcondtion,"Terms and conditions of treatment"))
																	{
																	return true;
																	}
																}
															}
														
													}
												}
											}
										}
										else
										{
											return true;
										}
									}
								}
							}
						}
						else
						{
							if(va.validate_other(typet_other,"Types of termites","Other"))
							{
								if(va.validate(companyhas, "This company has treated the structure(s) at the time of inspection"))
								{
									if(companyhas[0].isChecked())
									{
										if(va.validate(tv_organism,"Common name of organism treated","You have selected"))
										{
											if(va.validate_other(organism_other,"Common name of organism treated","Other"))
											{
												if(va.validate(pesticide,"Name of pesticide used"))
												{
													if(va.validate(method,"Method of treatment"))
														{
															if(va.validate(specific_location,"Specify treatment notice location"))
															{
																if(va.validate(termsandcondtion,"Terms and conditions of treatment"))
																{
																return true;
																}
															}
														}
													
												}
											}
										}
									}
									else
									{
										return true;
									}
								}
							}				
						}
					}
				}
			}
		}
		return false;
	}
	private void clear_all() {
		// TODO Auto-generated method stub
		op1.clear_selectionRD();
		op2.clear_selectionCHK();
		op3.clear_selectionCHK();
		rdg.clearCheck();
		
		//evidence[0].setChecked(false);
		//evidence[1].setChecked(false);
		evidence[0].setEnabled(true);
		evidence[1].setEnabled(true);
		
		rdgcomp.clearCheck();
		//companyhas[0].setChecked(false);
		//companyhas[1].setChecked(false);
		
		termsandcondtion[0].setChecked(false);
		termsandcondtion[1].setChecked(false);
		termsandcondtion[2].setChecked(false);
		rdgmethod.clearCheck();
		//method[0].setChecked(false);
		//method[1].setChecked(false);
		observed.setText("");
		//notice.setText("");
		pesticide.setText("");
		organism_other.setText("");
		methodscomment.setText("");
		specific_location.setText("");
		organism_other.setVisibility(View.GONE);
		companyyes.setVisibility(View.GONE);
	}
	public void clearcompny()
	{
		op3.clear_selectionCHK();
		
		termsandcondtion[0].setChecked(false);
		termsandcondtion[1].setChecked(false);
		termsandcondtion[2].setChecked(false);
		method[0].setChecked(false);
		method[1].setChecked(false);
		pesticide.setText("");
		organism_other.setText("");
		methodscomment.setText("");
		specific_location.setText("");
		organism_other.setVisibility(View.GONE);
		companyyes.setVisibility(View.GONE);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_back(InspectionsFindings.class);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
