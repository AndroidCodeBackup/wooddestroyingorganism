package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Edit_photos extends Activity {

	private int currnet_rotated;
	Button rl,rr,z;
	ImageView ph_im,close_im;
	EditText ed;
	Bundle b;
	String path,caption,saved_val[];
	private Bitmap rotated_b;
	CommonFunction cf;
	String delete,getelevation="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_photos);
		cf=new CommonFunction(this);
		b=getIntent().getExtras();
		if(b!=null)
		{
			path=b.getString("Path");
			saved_val=b.getStringArray("saved_val");
			caption=b.getString("Caption");
			delete=b.getString("delete");
			getelevation=b.getString("elevation");
			
			if(caption==null)
			{
				ed.setVisibility(View.GONE);
				((Button) findViewById(R.id.clear)).setVisibility(View.GONE);
				caption="";
			}
		}
		cf.setTouchListener(findViewById(R.id.main_head));
		declaration();
	}
	
	private void declaration() {
		// TODO Auto-generated method stub
		ed=(EditText) findViewById(R.id.ph_caption);
		rl=(Button) findViewById(R.id.rotateleft);
		rr=(Button) findViewById(R.id.rotateright);
		z=(Button) findViewById(R.id.zoom);
		ph_im=(ImageView) findViewById(R.id.ph_img);
		if(delete!=null)
		{
			if(delete.equals("false"))
			{
				((Button) findViewById(R.id.delete)).setVisibility(View.GONE);
			}
		}
		set_image();
		
	}

	private void set_image() {
		// TODO Auto-generated method stub
		rotated_b=cf.ShrinkBitmap(path, 400, 400);
		
		if(rotated_b ==null)
		{
			ph_im.setImageDrawable(getResources().getDrawable(R.drawable.noimage));
		}
		ph_im.setImageBitmap(rotated_b);
		ed.setText(caption);
		System.out.println("path="+path);
	}

	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.clear:
			ed.setText("");
		break;
		case R.id.rotateleft:
			if(rotated_b!=null)
			{
			System.gc();
			currnet_rotated-=90;
			if(currnet_rotated<0)
			{
				currnet_rotated=270;
			}

			
			Bitmap myImg;
			try {
				System.out.println("pathlef-"+path);
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
				        matrix, true);
				 
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
		break;

		case R.id.rotateright:
			if(rotated_b!=null)
			{
			currnet_rotated+=90;
			if(currnet_rotated>=360)
			{
				currnet_rotated=0;
			}
			
			Bitmap myImg1;
			try {
				
					myImg1 = BitmapFactory.decodeStream(new FileInputStream(path));
				
				
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg1, 0, 0,  myImg1.getWidth(),myImg1.getHeight(),
				        matrix, true);
				 System.gc();
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
			break;
		case R.id.zoom:
			if(rotated_b!=null)
			{
			Intent reptoit1 = new Intent(Edit_photos.this,ImageZoom.class);
			
			reptoit1.putExtra("Path", path);
			startActivity(reptoit1);
			}
		break;
		case R.id.ph_img:
			Intent reptoit = new Intent(Edit_photos.this,Select_phots.class);
			    reptoit.putExtra("elevation", getelevation);
				reptoit.putExtra("Selectedvalue", saved_val); 
				reptoit.putExtra("Maximumcount", 0);
				reptoit.putExtra("Total_Maximumcount", 1); 
				//reptoit.setClassName("com.idinspection","idsoft.inspectiondepot.wdo");
				startActivityForResult(reptoit,122); 
		break;
		case R.id.close:
			Intent in = getIntent();
			in.putExtras(b);
			setResult(RESULT_CANCELED, in);
			finish();
		break;
		case R.id.delete:
			final AlertDialog.Builder b =new AlertDialog.Builder(Edit_photos.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to delete the selected image?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						
						Intent in2 = getIntent();
						Bundle b =in2.getExtras();
						b.putBoolean("Delete_data", true);
						in2.putExtras(b);
						setResult(RESULT_OK, in2);
						finish();	
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					cf.show_toast("The selected image has been deleted successfully.", 1);
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show();
			
		break;
		case R.id.update:
			if(!ed.getText().toString().trim().equals("") || ed.getVisibility()==View.GONE)
			{
				if(currnet_rotated>0)
				{ 

					try
					{
						/**Create the new image with the rotation **/
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
						  ContentValues values = new ContentValues();
						  values.put(MediaStore.Images.Media.ORIENTATION, 0);
						  Edit_photos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=cf.getPath(Uri.parse(current));
						
							
							File fout = new File(this.path);
							fout.delete();
							/** delete the selected image **/
							
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(this.path));
						}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
					
			
			
			}
				Intent in2 = getIntent();
				Bundle b1 =in2.getExtras();
				b1.putBoolean("Delete_data", false);
				b1.putString("Caption", ed.getText().toString().trim());
				b1.putString("Path", path);
				in2.putExtras(b1);
				setResult(RESULT_OK, in2);
				finish();	
			}
			else
			{
				cf.show_toast(" Please enter the caption", 0);
			}
		break;
		default:
		break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK)
		{
			if(requestCode==122)
			{
				String[] value=	data.getExtras().getStringArray("Selected_array");
				path=value[0];
				set_image();
			}
		}
	}
	
}
