package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

public class PolicyholdeInfoHead extends Activity implements AnimationListener {
String homeid,insp_id,table_name;
CommonFunction cf;
DataBaseHelper db;
boolean b=false;
public Animation myFadeInAnimation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		setContentView(R.layout.policyholder_info_head);
		String type="";
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		if(b!=null)
		{
			homeid=b.getString("homeid");
			type=b.getString("Type");
			insp_id=db.Insp_id;
			
			if(type==null)
			{
				finish();
			}
		}
		System.out.println(" the policy holde"+type);
		if(type.equals("Policyholder"))
		{
			
			try
			{
			Show_ph_info();
			}
			catch (Exception e) {
				// TODO: handle exception
			
				finish();
			}
		}
		else if(type.equals("Inspector"))
		{     
			try
			{
				Show_insp_info();
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(" the issues"+e.getMessage());
				finish();
			}
		}
		
		cf.getInspectorId();
	    TranslateAnimation trans=new TranslateAnimation(310,0,0,0); 
	       trans.setDuration(500);
	       trans.setFillAfter(true);
	       ((ScrollView) findViewById(R.id.anim_con)).setAnimation(trans);
	       trans.start();
	    
	}
	private void Show_ph_info() {
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.ph_info_tbl)).setVisibility(View.VISIBLE);
		
		Cursor c=db.wdo_db.rawQuery(" Select PH_FirstName||' '||PH_LastName as name,PH_Address1||' '|| PH_Address2 as address,PH_City,PH_County,PH_State,PH_HomePhone,PH_Policyno FROM "+db.policyholder+" Where PH_SRID='"+homeid+"' and PH_InspectorId='"+insp_id+"' limit 1 ", null);
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.ph_name)).setText(db.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.ph_address)).setText(db.decode(c.getString(c.getColumnIndex("address"))).trim());
			if(c.getString(c.getColumnIndex("PH_City"))!=null)
				((TextView) findViewById(R.id.ph_city)).setText(db.decode(c.getString(c.getColumnIndex("PH_City"))).trim());
			if(c.getString(c.getColumnIndex("PH_County"))!=null)
				((TextView) findViewById(R.id.ph_county)).setText(db.decode(c.getString(c.getColumnIndex("PH_County"))).trim());
			if(c.getString(c.getColumnIndex("PH_State"))!=null)
				((TextView) findViewById(R.id.ph_state)).setText(db.decode(c.getString(c.getColumnIndex("PH_State"))).trim());
			if(c.getString(c.getColumnIndex("PH_HomePhone"))!=null)
				((TextView) findViewById(R.id.ph_ph_no)).setText(db.decode(c.getString(c.getColumnIndex("PH_HomePhone"))).trim());
			if(c.getString(c.getColumnIndex("PH_Policyno"))!=null)
				((TextView) findViewById(R.id.ph_p_no)).setText(db.decode(c.getString(c.getColumnIndex("PH_Policyno"))).trim());
			
		}
		if(c!=null)
			c.close();	
	}
	private void Show_insp_info() {     
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.insp_info_tbl)).setVisibility(View.VISIBLE);
		
		
		Cursor c=db.wdo_db.rawQuery(" Select Fld_InspectorFirstName||'  '||Fld_InspectorLastName as name,Fld_InspectorAddress as address,Fld_InspectorPhotoExtn,Fld_InspectorEmail," +
				"Company_name,insp_idcard_no,insp_lic_no,insp_lic_isue_date,insp_lic_exp_date,insp_lic_path  FROM "+db.inspectorlogin+" " +
				"LEFT JOIN  "+db.Companyinformation +" ON Ins_Id=Fld_InspectorId "+
				" Where Fld_InspectorId='"+insp_id+"' limit 1 ", null);
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.insp_name)).setText(db.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.insp_address)).setText(db.decode(c.getString(c.getColumnIndex("address"))).trim());
			
			if(c.getString(c.getColumnIndex("Company_name"))!=null)
				((TextView) findViewById(R.id.insp_company)).setText(db.decode(c.getString(c.getColumnIndex("Company_name"))).trim());
			if(c.getString(c.getColumnIndex("Fld_InspectorEmail"))!=null)
				((TextView) findViewById(R.id.insp_email)).setText(db.decode(c.getString(c.getColumnIndex("Fld_InspectorEmail"))).trim());
			if(c.getString(c.getColumnIndex("insp_idcard_no"))!=null)
				((TextView) findViewById(R.id.insp_id_no)).setText(db.decode(c.getString(c.getColumnIndex("insp_idcard_no"))).trim());
			if(c.getString(c.getColumnIndex("insp_lic_no"))!=null)
				((TextView) findViewById(R.id.insp_lic_no)).setText(db.decode(c.getString(c.getColumnIndex("insp_lic_no"))).trim());
			if(c.getString(c.getColumnIndex("insp_lic_isue_date"))!=null)
				((TextView) findViewById(R.id.insp_lic_iss_date)).setText(db.decode(c.getString(c.getColumnIndex("insp_lic_isue_date"))).trim());
			if(c.getString(c.getColumnIndex("insp_lic_exp_date"))!=null)
				((TextView) findViewById(R.id.insp_lic_exp_date)).setText(db.decode(c.getString(c.getColumnIndex("insp_lic_exp_date"))).trim());
			Bitmap bm=cf.ShrinkBitmap(this.getFilesDir()+"/"+db.decode(c.getString(c.getColumnIndex("insp_lic_path"))), 200, 200);
		
			if(bm!=null)
				((ImageView) findViewById(R.id.insp_lic_img)).setImageBitmap(bm);
			
			/*if(c.getString(c.getColumnIndex("Fld_InspectorCompanyId"))!=null)
				((TextView) findViewById(R.id.insp_company_id)).setText(" "+db.decode(c.getString(c.getColumnIndex("Fld_InspectorCompanyId"))).trim());*/
			String Ext=c.getString(c.getColumnIndex("Fld_InspectorPhotoExtn"));
			try {
				File outputFile = new File(this.getFilesDir()+"/"+db.Insp_id.toString()+Ext);
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				((ImageView) findViewById(R.id.insp_photo)).setImageDrawable(bmd);
				
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the value in the s=  ffds  ff"+e.getMessage());
			}
			if(c!=null)
				c.close();
			//File f =new File(+insp_id+Ext);
			
		}
	}
	
	public void clicker(View v)

	{
		/*TranslateAnimation trans1=new TranslateAnimation(0,310,0,0); 
	       trans1.setDuration(1500);
	       trans1.setFillAfter(true);
	       ((ScrollView) findViewById(R.id.anim_con)).setAnimation(trans1);
	       trans1.start();
	       trans1.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			System.out.println("Animation starts");	
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
	       finish();
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
			//((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
			//((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	
			  
}
