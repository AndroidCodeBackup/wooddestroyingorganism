package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
public class Login_page extends Activity {
	CommonFunction cf;
	Button clear_pass;
	int show_handler;
	CheckBox rememberpass;
	DataBaseHelper db;
	Webservice_Function wb;
	private AutoCompleteTextView et_username;
	private EditText et_password;
	
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf = new CommonFunction(this);
        db=new DataBaseHelper(this);
        wb=new Webservice_Function(this);
        db.CreateTable(1);
        cf.setTouchListener(findViewById(R.id.relativeLayout1));
        try {
            Cursor c_chkinsp = db.SelectTablefunction(db.inspectorlogin,
					"where Fld_InspectorFlag='1'");
			int r_chkinsp = c_chkinsp.getCount();
			System.out.println("get count "+r_chkinsp);
			if (r_chkinsp == 1) {
				Call_NextLayout();
			//	finish();
				

			} else if (r_chkinsp > 1) {
				/** More than 0ne user logged in, so we log out the users **/
				db.wdo_db.execSQL("update " + db.inspectorlogin
						+ " set Fld_InspectorFlag=0 where Fld_InspectorFlag=1");
			}
			if(c_chkinsp!=null)
				c_chkinsp.close();
		} catch (Exception e) {
		//	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ getApplicationContext()+" "+" in the stage of Checking Rows in Inspector Login table at "+"  "+"in apk"+" ");
		}

        setContentView(R.layout.login_page);
       
        /*INSPECTOR LOGIN CREATION TABLE*/
                 
        /*DECLARATION OF EDIT TEXTBOX FOR USERNAME AND PASSWORD*/
      // public AutoCompleteTextView r; 
        et_username = (AutoCompleteTextView) this.findViewById(R.id.eusername);
		et_password = (EditText) this.findViewById(R.id.epwd);
		
//		et_username.setText("");
//		et_password.setText("");
		
		String Version = "0";
		try {
			PackageInfo	pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			Version =  pInfo.versionName;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName.toString();
		} catch (NameNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(wb.URL.contains("idmaws.paperless-inspectors.com"))
			((TextView)findViewById(R.id.version_show)).setText("LV:"+Version);
		else
			((TextView)findViewById(R.id.version_show)).setText("UV:"+Version);
		et_username.addTextChangedListener(new AF_watcher());
        /*DECLARATION OF LOGIN BUTTION AND ITS CLICK EVENT*/
		clear_pass =(Button) findViewById(R.id.clear_pass);
		
		clear_pass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				db.wdo_db.execSQL("UPDATE "
						+ db.inspectorlogin
						+ " SET Ins_rememberpwd=0"
						+ " WHERE Fld_InspectorUserName ='"
						+ db.encode(et_username.getText().toString())+ "'");
				clear_pass.setVisibility(View.GONE);
				et_password.setText("");
			}
		});
		rememberpass=(CheckBox)findViewById(R.id.chkrememberpwd);
        Button btn_login = (Button)findViewById(R.id.login);
        
        btn_login.setOnClickListener(new OnClickListener()
        {
        	 @Override
            public void onClick(View v) 
            {
        			if (!"".equals(et_username.getText().toString().trim())
    						&& !"".equals(et_password.getText().toString().trim())) {/*CHECK FOR USERNAME AND PASSWORD NOT BE EMPTY*/
    					
    								try{
    									
    									if(wb.isInternetOn()==true)//CHECK FOR INTERNET CONNECTION
    									{
    										//GETTING VALUES FROM WEBSERVICE
    									cf.show_ProgressDialog("Processing ");
    									
    									new Thread() {
    										
    										public void run() {
    											Looper.prepare();
    											try {
    												SoapObject request = new SoapObject(wb.NAMESPACE,"CheckUserAuthentication");
    												SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    												envelope.dotNet = true;
    												request.addProperty("UserName",et_username.getText().toString());
    												request.addProperty("Password",et_password.getText().toString());
    												envelope.setOutputSoapObject(request);;
    												HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL_sink);
    												androidHttpTransport.call(wb.NAMESPACE+"CheckUserAuthentication",envelope);
    												SoapObject chklogin =(SoapObject) envelope.getResponse(); 
    												SoapObject obj = (SoapObject) chklogin.getProperty(0);
    												String chkauth = String.valueOf(obj.getProperty("userAuthentication"));
    												if(chkauth.equals("true")) //USERAUTHENTICATION IS TRUE
    												{
    													InsertData(obj);
    												}
    												else// USERAUTHENTICATION IS FALSE   
    												{
    													show_handler=1;
    													handler.sendEmptyMessage(0);
    													     
    													
    												}
    											} catch (SocketException e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=3;
    												handler.sendEmptyMessage(0);
    												
    											} catch (NetworkErrorException e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=3;
    												handler.sendEmptyMessage(0);
    												
    											} catch (IOException e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=3;
    												handler.sendEmptyMessage(0);
    												
    											} catch (TimeoutException e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=3;
    												handler.sendEmptyMessage(0);
    												
    											} catch (XmlPullParserException e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=3;
    												handler.sendEmptyMessage(0);
    												
    											}catch (Exception e) {
    												// TODO Auto-generated catch block
    												e.printStackTrace();
    												show_handler=4;
    												handler.sendEmptyMessage(0);
    												
    											}
    											
    										
											
										}
    										private void InsertData(SoapObject chklogin) throws NetworkErrorException,SocketTimeoutException, IOException, XmlPullParserException,Exception{
    											// TODO Auto-generated method stub
    											System.out.println("comes correctly top");
    									    	wb.status = String.valueOf(chklogin.getProperty("AndroidStatus"));
    											if (wb.status.equals("true")) {
    												final String id = String.valueOf(chklogin.getProperty("userId"));
    												Cursor cur1 = db.SelectTablefunction(db.inspectorlogin," where Fld_InspectorId = '" + id.toString()+ "'");
    												
    												System.out.println("comes correctly top0");
    												if(cur1.getCount()<=0)
    												{
    													System.out.println("comes correctly top1");
    													cf.Device_Information();
    													System.out.println("comes correctly top2");
    													SoapObject add_property = wb.export_header("ExportDeviceInformation");
    													add_property.addProperty("Deviceid", cf.deviceId);
    													add_property.addProperty("ModelNumber", cf.model);
    													add_property.addProperty("Manufacturer", cf.manuf);
    													add_property.addProperty("OSVersion", cf.devversion);
    													add_property.addProperty("APILevel", cf.apiLevel);
    													add_property.addProperty("IPAddress", cf.ipAddress);
    													add_property.addProperty("InspectorLd", id.toString());
    													add_property.addProperty("Date", cf.datewithtime);
    													System.out.println("comes correctly top3"+add_property);
    													String result_export_deviceinfo=wb.export_footer(wb.envelope,add_property,"ExportDeviceInformation");
    													System.out.println("the result was "+result_export_deviceinfo);
    												}
    												if(cur1!=null)
    													cur1.close();
    												try{
    													Cursor cur = db.SelectTablefunction(db.inspectorlogin,
    														" where Id = '" + id.toString()
    																+ "'");
    												int rws = cur.getCount();
    												if(cur!=null)
    													cur.close();
    												if (rws == 0) {
    													LoginInsert(id,0);
    												} else {
    													LoginInsert(id,1);
    												}
    	                                            cf.pd.dismiss();
    	                                            handler.sendEmptyMessage(0);
    	                                         
    												}
    												catch(Exception e)
    												{
    													
    												}
    											}
    											else
    											{
    												show_handler=2;handler.sendEmptyMessage(0);
    											}
    											
    										  }
 									private Handler handler = new Handler() {
											@Override
											public void handleMessage(Message msg) {
												cf.pd.dismiss();
												if(show_handler==1){
													show_handler=0;
												cf.show_toast("Invalid UserName or Password.",1);
												et_username.setText("");
												et_password.setText("");
												et_username.requestFocus();	
												}
												else if(show_handler==2)
												{
													show_handler=0;
													cf.show_toast("You are not eligible to login.",1);
													
												}
												else if(show_handler==3)
												{
													show_handler=0;
													cf.show_toast("There is a problem on your Network. Please try again later with better Network.",2);
													
												}
												else if(show_handler==4)
												{
													show_handler=0;
													cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",2);
													
												}
												else if(show_handler==12)
												{
													Cursor c=db.SelectTablefunction(db.Companyinformation, " WHERE Ins_Id='"+db.Insp_id+"'");
													if(c.getCount()>0)
													{
														c.moveToFirst();
														if(c.getString(c.getColumnIndex("WDO_status")).equals("true"))
														{
															show_handler=0;
															Call_RememberPass();
														}
														else
														{
														final AlertDialog alertDialog = new AlertDialog.Builder(Login_page.this)
												        .setTitle("Can't Access")
													    .setMessage(Html.fromHtml("To access WDO application, you must be a WDO licensed inspector.<br>To become a WDO licensed inspector, please visit http://www.paperlessinspectors.com."))
													    .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
																
	
																public void onClick(DialogInterface dialog,
																		int which) {
																	/*
																	Intent intent=new Intent(getApplicationContext(),LogOut.class);
																	startActivity(intent);*/
																}
															}).create();
														alertDialog.show();
													
														}
													}
												}
												
											}
										};	
										private void LoginInsert(String id,int update) {
											// TODO Auto-generated method stub
											System.out.println("comes correctly");
											try {
												SoapObject request = new SoapObject(wb.NAMESPACE,"InspectorDetail");
												SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
												envelope.dotNet = true;
												request.addProperty("inspectorid",id);
												System.out.println(" the user id="+id);
												envelope.setOutputSoapObject(request);;
												HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL_sink);
												androidHttpTransport.call(wb.NAMESPACE+"InspectorDetail",envelope);
												SoapObject chklogin = (SoapObject) envelope.getResponse(); //cf.Calling_WS1(id,"InspectorDetail");
												db.Insp_id = String.valueOf(chklogin.getProperty("Inspectorid"));
												db.Insp_firstname = (String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorfirstname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorfirstname"));
												String Insp_middlename =(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectormiddlename")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectormiddlename"));
												db.Insp_lastname = (String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorlastname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorlastname"));
												db.Insp_address = (String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectoraddress")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectoraddress"));
												db.Insp_companyname = (String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorcompanyname")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorcompanyname"));
												String Insp_companyId = (String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorcompanyId")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorcompanyId"));
												String Insp_username = (String.valueOf(chklogin.getProperty("Inspectorusername")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorusername")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorusername")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorusername"));
												String Insp_password = (String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Inspectorpassword")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Inspectorpassword"));
												String Insp_Photo = (String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorPhoto")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorPhoto"));
												String Insp_PhotoExtn = (String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorPhotoExt")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorPhotoExt"));
												String Insp_Status = (String.valueOf(chklogin.getProperty("AndroidStatus")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("AndroidStatus")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("AndroidStatus")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("AndroidStatus"));
												db.Insp_email = (String.valueOf(chklogin.getProperty("InspectorEmail")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorEmail")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorEmail")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorEmail"));
												String Insp_sign = (String.valueOf(chklogin.getProperty("InspectorSign")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("InspectorSign")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("InspectorSign")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("InspectorSign"));
												String Insp_PrimaryLicenseType = (String.valueOf(chklogin.getProperty("PrimaryLicenseType")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("PrimaryLicenseType")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("PrimaryLicenseType")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("PrimaryLicenseType"));
												String Insp_PrimaryLicenseNumber = (String.valueOf(chklogin.getProperty("PrimaryLicenseNumber")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("PrimaryLicenseNumber")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("PrimaryLicenseNumber")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("PrimaryLicenseNumber"));
												String Insp_flag1 = "0";
												
												//byte[] decode = Base64.decode(cf.Insp_Photo.toString(), 0);
												byte[] decode1 = Base64.decode(Insp_sign.toString(), 0);
												String headshot="";
												try
												{
												 headshot=(chklogin.getProperty("Headshot")==null)?"":(String.valueOf(chklogin.getProperty("Headshot")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("Headshot")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("Headshot")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("Headshot"));
												}
												catch (Exception e) {
													// TODO: handle exception
												}
												System.out.println("the heda shot "+headshot);
												if(!headshot.equals(""))
												{
												try
												{
													
												//	headshot=headshot.replace("\", "/");
													URL ulrn = new URL(headshot.replace(" ", "%20"));
													HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
													InputStream is = con.getInputStream();
												    System.out.println("worked correctlu"+is);
												    Bitmap bmp=null;
												    if(bmp!=null)
													{
														bmp.recycle();
													}
													
													 bmp = BitmapFactory.decodeStream(is);
													
												    ByteArrayOutputStream baos = new ByteArrayOutputStream();
												    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
												    byte[] b = baos.toByteArray();
												    
												    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
												    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);
												    try
													{
														String FILENAME = db.Insp_id +Insp_PhotoExtn;
														File f=new File(getFilesDir(),FILENAME);
														if(f.exists())
														{
															f.delete();
														}
														FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
														fos.write(decode3);
														fos.close();	
													}
													catch (IOException e){
														System.out.println("we found isseus in the webservice "+e.getMessage());
													}
												    
												}
												catch (Exception e1){
												
													System.out.println("Imagge exception = "+e1.getMessage());
												}
												}
												try
												{
													String FILENAME = "Signature"+db.Insp_id +".jpg";
													File f=new File(getFilesDir(),FILENAME);
													if(f.exists())
													{
														f.delete();
													}
													FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
													fos.write(decode1);
													fos.close();	
												}
												catch (IOException e){
													System.out.println("Imagge exception = "+e.getMessage());
												}
												System.out
														.println("comes befor insert");
												try {
//													Cursor c =db.SelectTablefunction(db.inspectorlogin, " WHERE Fld_InspectorId='"+db.Insp_id+"'");
//													if(c.getCount()<=0)
//													{
														db.wdo_db.execSQL("delete from "+ db.inspectorlogin);
														db.wdo_db.execSQL("INSERT INTO "
																+ db.inspectorlogin
																+ " (Fld_InspectorId ,Fld_InspectorFirstName ,Fld_InspectorMiddleName ,Fld_InspectorLastName ,Fld_InspectorAddress ," +
																"Fld_InspectorCompanyName ,Fld_InspectorCompanyId ,Fld_InspectorUserName ,Fld_InspectorPassword ,Ins_rememberpwd , " +
																"Fld_InspectorPhotoExtn ,Android_status ,Fld_InspectorFlag,Fld_InspectorEmail ,Fld_InspectorPEmail ,Fld_signature  " +
																",Fld_licenceno ,Fld_licencetype)"
																+ " VALUES ('" + db.Insp_id + "','"
																+ db.encode(db.Insp_firstname) + "','"
																+ db.encode(Insp_middlename) + "','"
																+ db.encode(db.Insp_lastname) + "','"
																+ db.encode(db.Insp_address) + "','"
																+ db.encode(db.Insp_companyname) + "','"
																+ db.encode(Insp_companyId) + "','"
																+ db.encode(Insp_username.toLowerCase())
																+ "','" + db.encode(Insp_password) + "','0','"+Insp_PhotoExtn+"','" + Insp_Status + "','"
																+ Insp_flag1 + "','"+db.encode(db.Insp_email)+"','','"+db.encode("Signature"+db.Insp_id +".jpg")+"'," +
																"'"+db.encode(Insp_PrimaryLicenseNumber)+"','"+db.encode(Insp_PrimaryLicenseType)+"')");
														
														/*db.wdo_db.execSQL(" INSERT INTO "+cf.Additional_inspecinfo+" (Ins_A_Id,Insp_sign_img,Insp_PrimaryLicenseType,Insp_PrimaryLicenseNumber) VALUES ('"+cf.Insp_id+"','"+db.encode("Signature"+cf.Insp_id +".jpg")+"','"+db.encode(cf.Insp_PrimaryLicenseType)+"','"+db.encode(cf.Insp_PrimaryLicenseNumber)+"')");*/
//													}
//													else
//													{
//														String sql="UPDATE "+db.inspectorlogin+" SET Fld_InspectorFirstName='"+db.encode(db.Insp_firstname)+"',Fld_InspectorMiddleName='"+ db.encode(Insp_middlename)+"',"+
//																"Fld_InspectorLastName='"+db.encode(db.Insp_lastname)+"',Fld_InspectorAddress='"+ db.encode(db.Insp_address)+"',Fld_InspectorCompanyName='"+db.encode(db.Insp_companyname)+"', "+
//																"Fld_InspectorCompanyId='"+db.encode(Insp_companyId)+"',Fld_InspectorPassword='"+ db.encode(Insp_password)+"',Android_status='"+db.encode(Insp_Status)+"', "+
//																"Fld_InspectorPhotoExtn='"+Insp_PhotoExtn+"',Fld_InspectorEmail='"+ db.encode(db.Insp_email)+"',Fld_licencetype='"+db.encode(Insp_PrimaryLicenseType)+"',Fld_licenceno='"+db.encode(Insp_PrimaryLicenseNumber)+"' WHERE Id='"+db.Insp_id+"' ";
//														db.wdo_db.execSQL(sql);
//														
//														/*db.wdo_db.execSQL(" UPDATE  "+cf.Additional_inspecinfo+" SET Insp_PrimaryLicenseType='"+db.encode(cf.Insp_PrimaryLicenseType)+"',Insp_PrimaryLicenseNumber='"+db.encode(cf.Insp_PrimaryLicenseNumber)+"' WHERE Ins_A_Id='"+cf.Insp_id+"'");*/
//
//													}
													db.wdo_db.execSQL("UPDATE " + db.inspectorlogin
															+ " SET Fld_InspectorFlag='1'"
															+ " WHERE Fld_InspectorId ='" + db.Insp_id + "'");
													
												} catch (Exception e) {
													
													System.out.println("there is isseues in the insert"+e.getMessage());
												}
												/***Start wdo functionality **/
												try{
													db.CreateTable(6);
													Cursor c=db.SelectTablefunction(db.Companyinformation, " WHERE Ins_Id='"+db.Insp_id+"'");
													String wdo_status= (String.valueOf(chklogin.getProperty("WDOLicense")).equals("anyType{}"))?"":(String.valueOf(chklogin.getProperty("WDOLicense")).equals("NA"))?"":(String.valueOf(chklogin.getProperty("WDOLicense")).equals("N/A"))?"":String.valueOf(chklogin.getProperty("WDOLicense"));
													System.out
															.println(" status"+wdo_status);
													if(wdo_status.equals("true"))
													{
														
														get_wdo_inspector_info(db.Insp_id,c);
													}else{
														if(c.getCount()>0)
														{
															db.wdo_db.execSQL(" UPDATE "+db.Companyinformation+" SET WDO_status='false' WHERE Ins_Id='"+db.Insp_id+"'");
														}
														else
														{
															db.wdo_db.execSQL(" INSERT INTO "+db.Companyinformation+" (Ins_Id,WDO_status) Values ('"+db.Insp_id+"','false')");
															
														}
														
													}
												}
												catch (Exception e) {
													// TODO: handle exception
													System.out
															.println("the issues "+e.getMessage());
												}
												show_handler=12;
												handler.sendEmptyMessage(0);
												/***Ends wdo functionality **/
											} catch (SocketException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
											}
											catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											}catch (XmlPullParserException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=3;
												handler.sendEmptyMessage(0);
												
											}catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												show_handler=4;
												handler.sendEmptyMessage(0);
												
											} 
											
										}

    									}.start();
    										}
    										else
    										{  
    											cf.show_toast("Internet connection is not available.",1);
    											
    										}
    								
    									
    										
    										/*if(et_username.getText().toString().trim().equals("test_user") && et_password.getText().toString().trim().equals("password"))
    										{
    											Cursor cur1 = db.SelectTablefunction(db.inspectorlogin," ");
    											if(cur1.getCount()<=0)
    											{
    												
    												db.wdo_db.execSQL("INSERT INTO "+db.inspectorlogin+" (Fld_InspectorId,Fld_InspectorUserName,Fld_InspectorPassword,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorPhotoExtn,Fld_InspectorFlag ,Fld_InspectorEmail) VALUES " +
    														"('2826','"+db.encode(et_username.getText().toString().trim())+"','"+db.encode(et_password.getText().toString().trim())+"','"+db.encode("Test inspector")+"','','"+db.encode("Test inspector")+"','"+db.encode("Test address")+"','"+db.encode("Test company name")+"','"+db.encode("456432")+"','"+db.encode(".jpg")+"','"+db.encode("1")+"','"+db.encode("test@gmail.com")+"')");
    														

    												db.getInspectorId();
    											}
    											else
    											{
    												db.wdo_db.execSQL("update " + db.inspectorlogin	+ " set Fld_InspectorFlag=1 where Fld_InspectorUserName='"+db.encode(et_username.getText().toString().trim())+"'");
    											}
    											Call_RememberPass();
    											
    										}*/
    									}
    									catch(Exception e)
    									{
    									System.out.println(" the isseus "+e.getMessage());
    									  cf.show_toast("Invalid UserName or Password.",0);
    										et_username.setText("");
    										et_password.setText("");
    										et_username.requestFocus();
    									}
    									
    								
    							}
    							else
    							{
    								if(et_password.getText().toString().trim().equals(""))
    								{
    								cf.show_toast("Please enter Password.",0);
    								et_password.requestFocus();
    								}
    								else if(et_username.getText().toString().trim().equals(""))
    								{
    									cf.show_toast("Please enter Username.",0);
    									et_username.requestFocus();
    								}
    							}


            }
        });
        
        /*DECLARATION OF CANCEL BUTTON AND ITS CLICK EVENT*/        
        Button btn_cancellogin = (Button)findViewById(R.id.cancellogin);
        btn_cancellogin.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            	Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
        
            }
        });
  ((TextView)findViewById(R.id.contact_us)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(Login_page.this,Contact_us.class), 25);
				
			}
		});
  
        cf.setTouchListener(findViewById(R.id.widget54));
    }
  
    protected void Call_NextLayout() {
		// TODO Auto-generated method stub
    	Intent iInspectionList = new Intent(this,HomeScreen.class);
		startActivity(iInspectionList);
	}

    public void get_wdo_inspector_info(String insp_id,Cursor c) throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
    	SoapObject request = new SoapObject(wb.NAMESPACE,"INSPECTORWDOLICENSEDETAILS");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorId",insp_id);
		envelope.setOutputSoapObject(request);
		System.out.println(" the request "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+"INSPECTORWDOLICENSEDETAILS",envelope);
		
		SoapObject chklogin = (SoapObject) envelope.getResponse(); //cf.Calling_WS1(id,"InspectorDetail");
		System.out.println(" the result "+chklogin);
		SoapObject obj=(SoapObject) chklogin.getProperty(0);
		String WDO_status="true" ,sign_with_wdo ,Company_name,Company_address,Company_city,Company_state,Company_county,Company_zipcode ,business_lic_no,PH_no,Issue_date ,Expiry_date ,License_path,comments,insp_idcard_no,insp_lic_no ,insp_lic_path ,insp_lic_isue_date ,insp_lic_exp_date;
		
		sign_with_wdo="";//cf.getval(obj.getProperty(""));
		Company_name=getval(obj.getProperty("CompanyName"));
		Company_address=getval(obj.getProperty("Address"));
		Company_city=getval(obj.getProperty("City"));
		Company_state=getval(obj.getProperty("Statename"));
		Company_county=getval(obj.getProperty("Countyname"));
		Company_zipcode=getval(obj.getProperty("ZipCode"));
		business_lic_no=getval(obj.getProperty("Company_Business_LicenseNo"));
		PH_no=getval(obj.getProperty("Phone_Number"));
		Issue_date =getval(obj.getProperty("Company_License_IssueDate"));
		Expiry_date=getval(obj.getProperty("Company_License_ExpiryDate"));
		License_path=db.decode(getval(obj.getProperty("Company_License_Upload")));
		comments=getval(obj.getProperty("Inspector_WDOlic_Comments"));
		insp_idcard_no=getval(obj.getProperty("IDCard_Number"));
		insp_lic_no=getval(obj.getProperty("Inspector_License_Number"));
		insp_lic_path=db.decode(getval(obj.getProperty("Inspector_License_Upload")));
		insp_lic_isue_date =getval(obj.getProperty("Inspector_License_IssueDate"));
		insp_lic_exp_date =getval(obj.getProperty("Inspector_License_ExpiryDate"));
		String path_com="WDO_CMP_LIC_"+insp_id +License_path.substring(License_path.lastIndexOf("."));
		String path_insp="WDO_INSP_LIC_"+insp_id +insp_lic_path.substring(insp_lic_path.lastIndexOf("."));
		if(!insp_lic_path.equals(""))
		{
			try
			{	
				
				System.gc();
				URL ulrn = new URL(insp_lic_path.replace(" ", "%20"));
				HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
				InputStream is = con.getInputStream();
				String FILENAME =path_insp;
				File f=new File(getFilesDir(),FILENAME);
				if(f.exists())
				{
					f.delete();
				}
				FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
				
			    byte[] buffer = new byte[4096];
	            int length;
	            while ((length = is.read(buffer))>0){
	            	fos.write(buffer, 0, length);
	            }
	            fos.flush();
	            fos.close();
	            con.disconnect();
				
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				System.out.println("out of memory exception = "+e.getMessage());
			}
			catch (Exception e1){
			
				System.out.println("Imagge exception = "+e1.getMessage());
			}
		}
		if(!License_path.equals(""))
		{
			try
			{
				System.gc();
				URL ulrn = new URL(License_path.replace(" ", "%20"));
				HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
				InputStream is = con.getInputStream();
				String FILENAME =path_com;
				File f=new File(getFilesDir(),FILENAME);
				if(f.exists())
				{
					f.delete();
				}
				FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
				byte[] buffer = new byte[4096];
	            int length;
	            while ((length = is.read(buffer))>0){
	            	fos.write(buffer, 0, length);
	            }
	            fos.flush();
	            fos.close();
	            con.disconnect();
				
			    
			}
			catch (Exception e1){
			
				System.out.println("Imagge exception = "+e1.getMessage());
			}
		}
		 c=db.SelectTablefunction(db.Companyinformation, " WHERE Ins_Id='"+db.Insp_id+"'");
		if(c.getCount()>0)
		{
			db.wdo_db.execSQL(" UPDATE "+db.Companyinformation+" SET WDO_status='"+WDO_status+"', sign_with_wdo='"+sign_with_wdo+"',Company_name='"+Company_name+"',Company_address='"+Company_address+"'," +
					"Company_city='"+Company_city+"',Company_state='"+Company_state+"',Company_county='"+Company_county+"',Company_zipcode ='"+Company_zipcode+"',business_lic_no='"+business_lic_no+"'," +
							"PH_no='"+PH_no+"',Issue_date ='"+Issue_date+"',Expiry_date ='"+Expiry_date+"',License_path='"+db.encode(path_com)+"',comments='"+comments+"',insp_idcard_no='"+insp_idcard_no+"'," +
									"insp_lic_no ='"+insp_lic_no+"',insp_lic_path ='"+db.encode(path_insp)+"',insp_lic_isue_date ='"+insp_lic_isue_date+"',insp_lic_exp_date='"+insp_lic_exp_date+"'" +
					" WHERE Ins_Id='"+db.Insp_id+"'");
		}
		else
		{
		/*	System.out.println(" INSERT INTO "+db.Companyinformation+" (Ins_Id,WDO_status,sign_with_wdo ,Company_name,Company_address,Company_city,Company_state,Company_county,Company_zipcode ,business_lic_no,PH_no,Issue_date ,Expiry_date ,License_path,comments,insp_idcard_no,insp_lic_no ,insp_lic_path ,insp_lic_isue_date ,insp_lic_exp_date) Values " +
					"('"+Ins_Id+"','"+WDO_status+"','"+sign_with_wdo +"','"+Company_name+"','"+Company_address+"','"+Company_city+"','"+Company_state+"','"+Company_county+"','"+Company_zipcode +"','"+business_lic_no+"','"+PH_no+"','"+Issue_date +"','"+Expiry_date +"','"+db.encode(License_path)+"','"+comments+"','"+insp_idcard_no+"','"+insp_lic_no +"','"+db.encode(insp_lic_path) +"','"+insp_lic_isue_date +"','"+insp_lic_exp_date+"')");*/
			db.wdo_db.execSQL(" INSERT INTO "+db.Companyinformation+" (Ins_Id,WDO_status,sign_with_wdo ,Company_name,Company_address,Company_city,Company_state,Company_county,Company_zipcode ,business_lic_no,PH_no,Issue_date ,Expiry_date ,License_path,comments,insp_idcard_no,insp_lic_no ,insp_lic_path ,insp_lic_isue_date ,insp_lic_exp_date) Values " +
					"('"+insp_id+"','"+WDO_status+"','"+sign_with_wdo +"','"+Company_name+"','"+Company_address+"','"+Company_city+"','"+Company_state+"','"+Company_county+"','"+Company_zipcode +"','"+business_lic_no+"','"+PH_no+"','"+Issue_date +"','"+Expiry_date +"','"+db.encode(path_com)+"','"+comments+"','"+insp_idcard_no+"','"+insp_lic_no +"','"+db.encode(path_insp) +"','"+insp_lic_isue_date +"','"+insp_lic_exp_date+"')");
			
		}
		
	}

    /*CLICK EVENT OF BACK KEY*/

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
	public void Call_RememberPass() {
		// TODO Auto-generated method stub
	//	try{
		System.out.println("comes correctly"+rememberpass.isChecked());
		if(rememberpass.isChecked())
		{
			db.wdo_db.execSQL("UPDATE "
					+ db.inspectorlogin
					+ " SET Ins_rememberpwd=1"
					+ " WHERE Fld_InspectorUserName='"+ db.encode(et_username.getText().toString()).toLowerCase()+ "'");
			
		}
		else
		{
			db.wdo_db.execSQL("UPDATE "
					+ db.inspectorlogin
					+ " SET Ins_rememberpwd=0"
					+ " WHERE Fld_InspectorUserName='"+ db.encode(et_username.getText().toString()).toLowerCase()+ "'");
			
		}
		cf.show_toast("Logged in successfully", 0);
		Call_NextLayout();
		/*Cursor	sel_logcur = cf.SelectTablefunction(db.inspectorlogin,
				"where Fld_InspectorUserName='"+ db.encode(et_username.getText().toString()).toLowerCase()+ "'");
		if(sel_logcur!=null)
		{
			if(sel_logcur.getCount()>=0)
			{
				sel_logcur.moveToFirst();
				if(sel_logcur.getString(sel_logcur.getColumnIndex("Ins_rememberpwd")).equals("0"))
				{
					Show_alert(sel_logcur);
				}
				else
				{
					Call_NextLayout();
				}
			}
			else
			{
				Call_NextLayout();
			}
		}
		else
		{
			Call_NextLayout();
		}*/
		
	}

	
	 class AF_watcher implements TextWatcher
	 {

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			try {
				Cursor cur1 = db.wdo_db.rawQuery("select * from "+ db.inspectorlogin + " where Fld_InspectorUserName like '" + db.encode(et_username.getText().toString().toLowerCase()) + "%'",null);
			String[] autousername = new String[cur1.getCount()];
				cur1.moveToFirst();
				if(cur1.getCount()!=0)
				{
					if (cur1 != null) {
						int i = 0;
						do {
							autousername[i] = db.decode(cur1.getString(cur1.getColumnIndex("Fld_InspectorUserName")));
							
							if (autousername[i].contains("null")) {
								autousername[i] = autousername[i].replace("null", "");
							}
							 
							i++;
						} while (cur1.moveToNext());
					}
					cur1.close();
				}
				 ArrayAdapter<String> adapter = new ArrayAdapter<String>(Login_page.this,R.layout.loginnamelist,autousername);
				 et_username.setThreshold(1);
				 et_username.setAdapter(adapter);
				 Cursor cur2 = db.wdo_db.rawQuery("select * from "+ db.inspectorlogin + " where Fld_InspectorUserName = '" + db.encode(et_username.getText().toString().toLowerCase()) + "' and Ins_rememberpwd='1'",null);
				 /** Set the password for the remember option enable**/
				 cur2.moveToFirst();
				 if(cur2.getCount()>0)
				 {
					if(db.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorUserName"))).equals(et_username.getText().toString().toLowerCase()) && cur2.getString(cur2.getColumnIndex("Ins_rememberpwd")).equals("1"))
					{
						et_password.setText(db.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorPassword"))));
						rememberpass.setChecked(true);
					}
					else
					{
						et_password.setText("");
						rememberpass.setChecked(false);
						//clear_pass.setVisibility(View.GONE);
					}
					/** Set the password for the remember option enable Ends here **/
				 }
				 else
					{
						et_password.setText("");
						rememberpass.setChecked(false);
						///clear_pass.setVisibility(View.GONE);
					}
				 if(cur2!=null)
						cur2.close();
			}
			catch(Exception e)
			{
				//cf.Error_LogFile_Creation("Error in the autofill of the login information query ");
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		 
	 }
	
	 public String getval(Object property) {
			// TODO Auto-generated method stub
			if(property!=null)
			{
				String s= property.toString();
				if(s.equals("anyType{}") ||s.equals("NA") ||s.equals("N/A"))
				{
					return "";
				}
				else
				{
					return db.encode(s);
				}
			}
			else
			{
				return "";
			}
			
		}
}