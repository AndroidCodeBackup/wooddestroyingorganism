package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdditionalContacts extends Activity {
CommonFunction cf;
DataBaseHelper db;
TextView tv[]=new TextView[9];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.additional_contact);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Policyholder",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,6,cf));
		declaration();
		db.CreateTable(4);
		try
		{
		Cursor c=db.SelectTablefunction(db.Additional_table, " Where AD_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			showsaved_data(c);
		}
		else
		{
			
			/*db.wdo_db.execSQL("INSERT INTO "+db.Additional_table+" (AD_SRID,AD_INSPID,AD_IsRecord,AD_UserTypeName,AD_ContactEmail,AD_PhoneNumber,AD_MobileNumber,AD_BestTimetoCallYou,AD_Bestdaycall,AD_FirstChoice,AD_SecondChoice,AD_ThirdChoice,SF_AD_FeedbackComments) VALUES" +
					" ('"+cf.selectedhomeid+"','"+db.Insp_id+"','true','test usertype name','test@gmail.com','(962)910-8196','(962)910-8197','9:30 AM','Monday','7:30 am','7:30 pm','10:30 pm','test comments') ");
			Cursor c1=db.SelectTablefunction(db.Additional_table, " Where AD_SRID='"+cf.selectedhomeid+"'");
			showsaved_data(c1);*/
		}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("tthe issuys "+e.getMessage());
		}
	}
	private void declaration() {
		// TODO Auto-generated method stub
		
		tv[0]=(TextView) findViewById(R.id.efirstname);
		tv[1]=(TextView) findViewById(R.id.econtactperson);
		tv[2]=(TextView) findViewById(R.id.txtaddr1);
		tv[3]=(TextView) findViewById(R.id.txtaddr2);
		tv[4]=(TextView) findViewById(R.id.txtdaycal);
		tv[5]=(TextView) findViewById(R.id.txtcity);
		tv[6]=(TextView) findViewById(R.id.txtstate);
		tv[7]=(TextView) findViewById(R.id.txtcountry);
		tv[8]=(TextView) findViewById(R.id.txtzip);
		
	}
	private void showsaved_data(Cursor c) {
		// TODO Auto-generated method stub
		c.moveToFirst();
		tv[0].setText(db.convert_null(c.getString(c.getColumnIndex("AD_ContactEmail"))));
		tv[1].setText(db.convert_null(c.getString(c.getColumnIndex("AD_PhoneNumber"))));
		tv[2].setText(db.convert_null(c.getString(c.getColumnIndex("AD_MobileNumber"))));
		tv[3].setText(db.convert_null(c.getString(c.getColumnIndex("AD_BestTimetoCallYou"))));
		tv[4].setText(db.convert_null(c.getString(c.getColumnIndex("AD_Bestdaycall"))));
		tv[5].setText(db.convert_null(c.getString(c.getColumnIndex("AD_FirstChoice"))));
		tv[6].setText(db.convert_null(c.getString(c.getColumnIndex("AD_SecondChoice"))));
		tv[7].setText(db.convert_null(c.getString(c.getColumnIndex("AD_ThirdChoice"))));
		tv[8].setText(db.convert_null(c.getString(c.getColumnIndex("SF_AD_FeedbackComments"))));
		
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goback(12);
			cf.go_back(Schedule.class);
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		db.wdo_db.close();
		super.onDestroy();
	}
}
