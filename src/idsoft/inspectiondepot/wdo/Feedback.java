package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.TouchFoucs;
import idsoft.inspectiondepot.wdo.supportclass.Validation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
   
public class Feedback extends Activity {
	CommonFunction cf;
	Spinner sp[]=new Spinner[3];
	EditText ed_others[]=new EditText[3],comments;
	LinearLayout sup_l,off_l;
	String[] array_who={"--Select--","Owner","Representative","Agent","Other"},
	array_doc={"--Select--","Acknowledgement Form","CSE Form","OIR 1802 Form","Paper Signup Sheet","Other Information","Roof Permit","Sketch","Building Permit","Property Appraisal Information","Field Inspection Report","Field Notes"};
    DataBaseHelper db;
    RadioGroup rg[] = new RadioGroup[2]; 
    Validation va;
    private boolean load_comment=true;
    String whowas="",who_other="";
    public CheckBox cbwhowas[] = new CheckBox[4];
    public int[] cbwhowasid = {R.id.feedback_owner,R.id.feedback_rep,R.id.feedback_agent,R.id.feedback_Other};

	    
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Feedback Information",0,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,5,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-20;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		 va =new Validation(cf);
		declaration();
		
	}
	private void declaration() {
		// TODO Auto-generated method stub
		sp[0]=(Spinner) findViewById(R.id.feedback_SPwhowas);
		sp[1]=(Spinner) findViewById(R.id.feedback_SPsupplement);
		sp[2]=(Spinner) findViewById(R.id.feedback_SPoffice);
		ed_others[0]=(EditText) findViewById(R.id.feedback_EDwhowas_other);
		ed_others[1]=(EditText) findViewById(R.id.feedback_EDsupplement_other);
		ed_others[2]=(EditText) findViewById(R.id.feedback_EDoffice_other);
		comments=(EditText) findViewById(R.id.feedback_EDcomment);
		 for(int i=0;i<4;i++)
    	 {
    		try{
    			cbwhowas[i] = (CheckBox)findViewById(cbwhowasid[i]);
    		}
    		catch (Exception e) {
				// TODO: handle exception
			}
    	 }
		ArrayAdapter ad =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_who);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter ad1 =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_doc);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp[0].setAdapter(ad);
		sp[1].setAdapter(ad1);
		sp[2].setAdapter(ad1);
		sp[0].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[0].getSelectedItem().toString().equals("Other"))
				{
					ed_others[0].setVisibility(View.VISIBLE);
					ed_others[0].setTag("required");
				}
				else
				{
					ed_others[0].setText("");
					ed_others[0].setVisibility(View.GONE);
					ed_others[0].setTag("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp[1].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[1].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[1].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[1].setText("");
					ed_others[1].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp[2].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[2].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[2].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[2].setText("");
					ed_others[2].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		comments.setOnTouchListener(new TouchFoucs(comments));
		comments.addTextChangedListener(new TextWatchLimit(comments, 500, ((TextView) findViewById(R.id.feedback_TVcommentslimit))));
		cf.setTouchListener(findViewById(R.id.content)); 
		sup_l=(LinearLayout)findViewById(R.id.feedback_LLsupplement_dy_list);
		off_l=(LinearLayout)findViewById(R.id.feedback_LLoffice_dy_list);
		rg[0]=(RadioGroup) findViewById(R.id.feedback_RGowner);
		rg[1]=(RadioGroup) findViewById(R.id.feedback_RGcustomerservice);
		db.CreateTable(13);
		Cursor c1=db.SelectTablefunction(db.feedback_infomation, " WHERE FD_SRID='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String csc,who,who_other,issat,comments;
			csc=db.decode(c1.getString(c1.getColumnIndex("FD_CSC")));
			who=db.decode(c1.getString(c1.getColumnIndex("FD_whowas")));
			who_other=db.decode(c1.getString(c1.getColumnIndex("FD_whowas_other")));
			if(!who_other.trim().equals(""))
			{
				ed_others[0].setVisibility(View.VISIBLE);
			}
			else
			{
				ed_others[0].setVisibility(View.GONE);
			}
			issat=db.decode(c1.getString(c1.getColumnIndex("FD_issatisfied")));
			comments=db.decode(c1.getString(c1.getColumnIndex("FD_comments")));
			cf.setvaluerd(rg[0], csc);
			cf.setvaluerd(rg[1], issat);
			setvaluechk1(who,cbwhowas);
			//sp[0].setSelection(ad.getPosition(who));
			ed_others[0].setText(who_other);
			this.comments.setText(comments);
			
		}
		if(c1!=null)
			c1.close();
		show_saved_data();
	}
	
	public void setvaluechk1(String typechk_val, CheckBox[] typechk) {
		// TODO Auto-generated method stub
		if(!typechk_val.equals(""))
		{
			
			String temp[]=typechk_val.split(",");
			
			int j=0;
			do{
				for(int i=0;i<typechk.length;i++)
				{
					if(temp[j].trim().equals(typechk[i].getText().toString().trim()))
					{
						typechk[i].setChecked(true);
						j++;
						if(j==temp.length)
						{
							return;
						}
					}
					
				}
				
			}while(j<temp.length);
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.feedback_Other:
			if(cbwhowas[3].isChecked())
			{
				ed_others[0].setVisibility(v.VISIBLE);
			}
			else
			{
				ed_others[0].setText("");who_other="";
				ed_others[0].setVisibility(v.GONE);
			}
			break;
		case R.id.feedback_BTsupplement_browse:
			if(sp[1].getSelectedItemPosition()!=0)
			{
				if(!sp[1].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[1].getText().toString().trim().equals(""))
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
					reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
					reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
					reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
					startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
				}
				else
				{
					cf.show_toast("Please enter the Other information for the supplemental document", 1);
				}
				
			}
			else
			{
				cf.show_toast("Please select supplemental document title", 1);
			}
		break;
		case R.id.feedback_BToffice_browse:
			if(sp[2].getSelectedItemPosition()!=0)
			{
				if(!sp[2].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[2].getText().toString().trim().equals(""))
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
					reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
					reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
					reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
					startActivityForResult(reptoit1,122); /** Call the Select image page in the idma application  image ***/
				}
				else
				{
					cf.show_toast("Please enter the Other information for the office document", 1);
				}
				
			}
			else
			{
				cf.show_toast("Please select office document title", 1);
			}
		break;
		case R.id.hme:
			cf.go_home();
		break;
		case R.id.clear:
			sp[0].setSelection(0);
			rg[0].clearCheck();
			rg[1].clearCheck();
			for(int i=0;i<cbwhowas.length;i++)
			{
				cbwhowas[i].setChecked(false);
				 if(cbwhowas[i].getText().toString().contains("Other"))
				 {
					 ed_others[0].setText("");
					 ed_others[0].setVisibility(v.GONE);
				 }
			}
			comments.setText("");
		break;
		case R.id.save:
			save_values();
		break;
		case R.id.feedback_loadcomments:
			int len=comments.getText().toString().length();
			
			if(load_comment )
			{
				load_comment=false;
				int loc[] = new int[2];
				v.getLocationOnScreen(loc);
				Intent in =new Intent(this,Load_comments.class);
				in.putExtra("id", R.id.feedback_loadcomments);
				in.putExtra("srid", cf.selectedhomeid);
				in.putExtra("question", 7);
				in.putExtra("max_length", 500);
				in.putExtra("cur_length", len);
				in.putExtra("xfrom", loc[0]+10);
				in.putExtra("yfrom", loc[1]+10);
				startActivityForResult(in, cf.loadcomment_code);
			}
			break;
		default:
			break;
		}
		
	}
	public String getselected_chk(CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		String s = "";
		for(int i=0;i<CB.length;i++)
		{
			if(CB[i].isChecked())
			{
				 s+=CB[i].getText().toString()+",";
			}
		} 
		/*if(s.length()>=2)
		{
			s=s.substring(0, s.length());
		}*/
		return s;
	}
	private void save_values() {
		// TODO Auto-generated method stub
		
		/*if(va.validate(sp[0], "Who was present at inspection?",array_who[0]))
		{
			if(va.validate_other(ed_others[0], "Who was present at inspection?", "Other"))
			{*/
		 whowas= getselected_chk(cbwhowas);
		// String chkother=(cbwhowas[cbwhowas.length-1].isChecked())? ","+ed_others[0].getText().toString():""; // append the other text value in to the selected option
		// whowas+=chkother;
    	
		 System.out.println("whowas"+whowas);
		 if(whowas.equals(""))
		 {
			 cf.show_toast("Please select the option for who was present at inspection?", 1);
		 }
		 else
		 {
			 if(whowas.contains("Other"))
			 {
				 if(ed_others[0].getText().toString().trim().equals(""))
				 {
					 cf.show_toast("Please enter the Other text for who was present at inspection", 1);
					 ed_others[0].requestFocus();
				 }
				 else
				 {
					 continuevalidation();
				 }
			 }
			 else
			 {
				 continuevalidation();
			 }
		 }
		/*	}
		}*/
	}
	private void continuevalidation() {
		// TODO Auto-generated method stub
		if(va.validate(rg[0], "Owner/Representative satisfied with inspection"))
		{
			if(va.validate(rg[1], "Customer service evaluation form completed and signed by owner / representative"))
			{
				/*if(va.validate(comments, "Inspector Feedback Comments - For Office Use Only "))
				{*/
					/*Cursor c=db.SelectTablefunction(db.feedback_document, " WHERE FD_D_SRID='"+cf.selectedhomeid+"' and FD_D_type='0'");
					if(c.getCount()>0)
					{*/
						Cursor c1=db.SelectTablefunction(db.feedback_infomation, " WHERE FD_SRID='"+cf.selectedhomeid+"'");
						String who,csc,issat,comments;
						//who=db.encode(sp[0].getSelectedItem().toString().trim());
						who_other=db.encode(ed_others[0].getText().toString().trim());
						csc=db.encode(((RadioButton) rg[0].findViewById((rg[0].getCheckedRadioButtonId()))).getText().toString().trim());
						issat=db.encode(((RadioButton) rg[1].findViewById((rg[1].getCheckedRadioButtonId()))).getText().toString().trim());
						comments=db.encode(this.comments.getText().toString().trim());
						if(c1.getCount()>0)
						{
							db.wdo_db.execSQL("UPDATE "+db.feedback_infomation+" SET FD_CSC='"+csc+"',FD_whowas='"+whowas+"',FD_whowas_other='"+who_other+"',FD_issatisfied='"+issat+"',FD_comments='"+comments+"' WHERE FD_SRID='"+cf.selectedhomeid+"'");
						}
						else
						{
							db.wdo_db.execSQL(" INSERT INTO "+db.feedback_infomation+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_issatisfied,FD_comments) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+csc+"','"+whowas+"','"+who_other+"','"+issat+"','"+comments+"')");
						}
						cf.go_back(Submit.class);
						cf.show_toast("Feedback information saved successfully ", 1);
					
					/*}
					else
					{
						cf.show_toast("Please add feedback document for Supplemental Documents ", 0);
						
					}*/
					
				//}
			}	
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK && requestCode!=cf.loadcomment_code)
		{ int type=0; 
			String title="";
			String title_other="";
			if(requestCode==121)
			{
				type=0;
				title=db.encode(sp[1].getSelectedItem().toString().trim());
				title_other=db.encode(ed_others[1].getText().toString().trim());
				
				sp[1].setSelection(0);
			}
			else if(requestCode==122)
			{
				type=1;
				title=db.encode(sp[2].getSelectedItem().toString().trim());
				title_other=db.encode(ed_others[2].getText().toString().trim());
				sp[2].setSelection(0);
			}
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
				for(int i=0;i<value.length;i++ )
				{
					
					/*System.out.println(" INSERT INTO "+db.feedback_document+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+title+"','"+title_other+"','"+db.encode(value[i])+"','"+type+"'");*/
					db.wdo_db.execSQL(" INSERT INTO "+db.feedback_document+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+title+"','"+title_other+"','"+db.encode(value[i])+"','"+type+"')");
					
				}
				show_saved_data();
		}
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
						comments.setText((comments.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						
					}
					/*else if(resultCode==RESULT_CANCELED)
					{
						cf.show_toast("You have canceled  the comments selction ",0);
					}*/
			}		
		
	}
	private void show_saved_data() {
		// TODO Auto-generated method stub
	
		Cursor c =db.SelectTablefunction(db.feedback_document, " WHERE FD_D_SRID='"+cf.selectedhomeid+"'");
		
		sup_l.removeAllViews();
		off_l.removeAllViews();
		findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.GONE);
		findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.GONE);
		if(c.getCount()>0)
		{
			
			c.moveToFirst();
			do{
				String path,doc_tit,doc_tit_other;
				int id,type;
				path=db.decode(c.getString(c.getColumnIndex("FD_D_path")));
				doc_tit=db.decode(c.getString(c.getColumnIndex("FD_D_doctit")));
				doc_tit_other=db.decode(c.getString(c.getColumnIndex("FD_D_doctit_other")));
				if(doc_tit.equals("Other Information"))
				{
					doc_tit+="("+doc_tit_other+")";
				}
				id=c.getInt(c.getColumnIndex("FD_D_Id"));
				type=c.getInt(c.getColumnIndex("FD_D_type"));
				LinearLayout li=new LinearLayout(this);
				ImageView im =new ImageView(this);
				im.setImageDrawable(getResources().getDrawable(R.drawable.allfilesicon));
				li.addView(im);
				TextView tv =new TextView(this);
				tv.setTag(id);
				tv.setText(doc_tit);
				li.addView(tv,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				li.setPadding(0, 5, 10, 5);
				li.setOnClickListener(new my_dyclicker(id,path,doc_tit));
				li.setGravity(Gravity.CENTER_VERTICAL);
				if(type==0)
				{
					findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.VISIBLE);
						sup_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
						View v=new View(this);
						v.setBackgroundColor(getResources().getColor(R.color.main_menu));
						sup_l.addView(v,LayoutParams.FILL_PARENT,2);
					
				}
				else if(type==1)
				{
					findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.VISIBLE);
					off_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					View v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.main_menu));
					off_l.addView(v,LayoutParams.FILL_PARENT,2);
					
					
				}
				else
				{
					cf.show_toast("Comes in the wrong place", 0);
					
				}
			}while (c.moveToNext());
		}
		if(c!=null)
			c.close();	
	}
	class my_dyclicker implements OnClickListener
	{
		int doc_id;
		String path;
		String title;
		 int currnet_rotated;
		 Bitmap rotated_b;
		 View v2;

		my_dyclicker(int id,String path,String title)
		{
			this.doc_id=id;
			this.path=path;
			this.title=title;
			
			
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			 final Dialog dialog = new Dialog(Feedback.this);
	            dialog.getWindow().setContentView(R.layout.maindialog);
	            dialog.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+title+"</font>"));//.pri
	            
	            dialog.setCancelable(true);
	            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
//	            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
//	            ed1.setVisibility(v2.GONE);
	            img.setVisibility(v2.GONE);
	            
	            Button button_close = (Button) dialog.findViewById(R.id.Button01);
	    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
	    		
	    		
	            Button button_d = (Button) dialog.findViewById(R.id.Button03);
	    		button_d.setText("Delete");
	    		button_d.setVisibility(v2.VISIBLE);
	    		
	    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
	    		button_view.setText("View");
	    		button_view.setVisibility(v2.VISIBLE);
	    		
	    		
	    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
	    		button_saveimage.setVisibility(v2.GONE);
	    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
	    		linrotimage.setVisibility(v2.GONE);
	    		
	    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
	    		rotateleft.setVisibility(v2.GONE);
	    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
	    		rotateright.setVisibility(v2.GONE);
	     		
	    		button_d.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            
		            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
		   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
		   				.setTitle("Confirmation")
		   				.setIcon(R.drawable.alertmsg)
	   			       .setCancelable(false)
	   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			        	 db.wdo_db.execSQL("Delete From " +  db.feedback_document + "  WHERE FD_D_Id ='" + doc_id + "'");
	   			        	cf.show_toast("Document has been deleted sucessfully.",1);
	   					   show_saved_data();
	   			           }
	   			       })
	   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			                dialog.cancel();
	   			           }
	   			       });
	   			 builder.show();
	            
	   			 dialog.cancel(); 
            }
		           	
		  	 });
	    		
	    		
	    		
	    		button_view.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            currnet_rotated=0;
		            	if(path.endsWith(".pdf"))
					     {
		            		img.setVisibility(v2.GONE);
		            		String tempstr ;
		            		if(path.contains("file://"))
		    		    	{
		    		    		 tempstr = path.replace("file://","");		    		
		    		    	}
		            		else
		            		{
		            			tempstr = path;
		            		}
		            		 File file = new File(tempstr);
						 
			                 if (file.exists()) {
			                	Uri path = Uri.fromFile(file);
			                    Intent intent = new Intent(Intent.ACTION_VIEW);
			                    intent.setDataAndType(path, "application/pdf");
			                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			  
			                    try {
			                        startActivity(intent);
			                    } 
			                    catch (ActivityNotFoundException e) {
			                    	cf.show_toast("No application available to view PDF.",1);
			                       
			                    }
			               
			                }
					     }
		            	else  
		            	{
		            		Bitmap bitmap2=cf.ShrinkBitmap(path,250,250);
		            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
	    					img.setImageDrawable(bmd2);
	    					img.setVisibility(v2.VISIBLE);
	    					linrotimage.setVisibility(v2.VISIBLE);
	    					rotateleft.setVisibility(v2.VISIBLE);
	    					rotateright.setVisibility(v2.VISIBLE);
				            button_view.setVisibility(v2.GONE);
				            button_saveimage.setVisibility(v2.VISIBLE);
				           
		            		
		            	}
		            }
	    		});
	    		rotateleft.setOnClickListener(new OnClickListener() {  			
	    			
					public void onClick(View v) {

	    				// TODO Auto-generated method stub
	    			
	    				System.gc();
	    				currnet_rotated-=90;
	    				if(currnet_rotated<0)
	    				{
	    					currnet_rotated=270;
	    				}

	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);

	    				} catch (FileNotFoundException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch (Exception e) {
	    					
	    				}
	    				catch (OutOfMemoryError e) {
	    					
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.show_toast("You cannot rotate this image. Image size is too large.",1);
	    					}
	    				}

	    			
	    			}
	    		});
	    		rotateright.setOnClickListener(new OnClickListener() {
	    		    public void onClick(View v) {
	    		    	
	    		    	currnet_rotated+=90;
	    				if(currnet_rotated>=360)
	    				{
	    					currnet_rotated=0;
	    				}
	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);  

	    				} catch (FileNotFoundException e) {
	    					//.println("FileNotFoundException "+e.getMessage()); 
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch(Exception e){}
	    				catch (OutOfMemoryError e) {
	    					//.println("comes in to out ot mem exception");
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.show_toast("You cannot rotate this image. Image size is too large.",1);
	    					}
	    				}

	    		    }
	    		});
	    		button_saveimage.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						
						if(currnet_rotated>0)
						{ 

							try
							{
								/**Create the new image with the rotation **/
						
								String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
								 ContentValues values = new ContentValues();
								  values.put(MediaStore.Images.Media.ORIENTATION, 0);
								  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
								
								if(current!=null)
								{
								String path_new=cf.getPath(Uri.parse(current));
								File fout = new File(path);
								fout.delete();
								/** delete the selected image **/
								File fin = new File(path_new);
								/** move the newly created image in the slected image pathe ***/
								fin.renameTo(new File(path));
								cf.show_toast("Saved successfully.",1);dialog.cancel();
								show_saved_data();
								
								
							}
							} catch(Exception e)
							{
								//.println("Error occure while rotate the image "+e.getMessage());
							}
							
						}
						else
						{
							cf.show_toast("Saved successfully.",1);
							dialog.cancel();
							show_saved_data();
						}
						
					}
				});
	    		button_close.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	dialog.cancel();
		            }
	    		});
	    		dialog.show();
			
		}
		
	}
	
}
