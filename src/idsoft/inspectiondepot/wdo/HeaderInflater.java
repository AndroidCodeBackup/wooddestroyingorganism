package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HeaderInflater extends LinearLayout {
Context con;
int camera=0;
String srid;
ImageView camer_icon,policyholder,inspector;
	public HeaderInflater(Context context,String breadcum,int camera,String srid) {
		super(context);
		this.con=context;
		this.camera=camera;
		this.srid=srid;
		inflat_layout();
		// TODO Auto-generated constructor stub
	}
	
	public HeaderInflater(Context context,AttributeSet attrs,String breadcum,int camera,String srid) {
		super(context);
		this.con=context;
		this.camera=camera;
		this.srid=srid;
		inflat_layout();
		// TODO Auto-generated constructor stub
	}
	private void inflat_layout() {
		// TODO Auto-generated method stub
		LayoutInflater layoutInflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.header, this, true);
		
		camer_icon=(ImageView) findViewById(R.id.head_take_image);
		policyholder=(ImageView) findViewById(R.id.head_policy_info);
		inspector=(ImageView) findViewById(R.id.head_insp_info);
		if(camera==1)
		{
			
			camer_icon.setVisibility(View.VISIBLE);
			policyholder.setVisibility(View.VISIBLE);
			inspector.setVisibility(View.VISIBLE);
			
		}
		else
		{
			camer_icon.setVisibility(View.GONE);
			policyholder.setVisibility(View.VISIBLE);
			inspector.setVisibility(View.VISIBLE);
		}
		
		
		/*findViewById(R.id.head_insp_info).setVisibility(View.INVISIBLE);*/
		camer_icon.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(con,Show_tackenimage.class);
				insp_info.putExtra("homeid", srid);
				//insp_info.putExtra("Type", "Inspector");
				((Activity)con).startActivityForResult(insp_info,77);
			}
		});
		inspector.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(con,PolicyholdeInfoHead.class);
				insp_info.putExtra("homeid", srid);
				insp_info.putExtra("Type", "Inspector");
				((Activity)con).startActivityForResult(insp_info,77);
			}
		});
		policyholder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println(" the policy holdecvdfsvdsg");
				Intent insp_info = new Intent(con,PolicyholdeInfoHead.class);
				insp_info.putExtra("homeid", srid);
				insp_info.putExtra("Type", "Policyholder");
				((Activity)con).startActivityForResult(insp_info,77);
			}
		});
	}
}
