package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EmailReport extends Activity {
	AutoCompleteTextView actsearch;
	Button home, search, clear;
	CommonFunction cf;
	DataBaseHelper db;
	LinearLayout lldynamic;
	RelativeLayout rlheader;
	String[] data, datasend;
	static String mailid, policyno, name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_report);
		cf = new CommonFunction(this);
		db=new DataBaseHelper(this);
	((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(EmailReport.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		ImageView iv = (ImageView) findViewById(R.id.ImageView02);
		actsearch = (AutoCompleteTextView) findViewById(R.id.emailreport_actsearch);
		home = (Button) findViewById(R.id.emailreport_home);
		home = (Button) findViewById(R.id.emailreport_home);
		search = (Button) findViewById(R.id.emailreport_search);
		clear = (Button) findViewById(R.id.emailreport_cancel);
		lldynamic = (LinearLayout) findViewById(R.id.emailreport_lldynamic);
		rlheader = (RelativeLayout) findViewById(R.id.emailreport_rlheader);
		
//		cf.hidekeyboard((EditText) actsearch);
		
		db.CreateTable(2);
		Cursor cur = db.wdo_db.rawQuery("select * from " + db.policyholder,
				null);
		display_list(cur);
		if(cur!=null)
			cur.close();
		TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,
					int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,
					int i2) {
				if (actsearch.getText().toString().startsWith(" ")) {
					// Not allowed
					actsearch.setText("");
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {
				// here, after we introduced something in the EditText we get
				// the string from it
				try {
					db.CreateTable(2);
					Cursor cur1 = db.wdo_db.rawQuery("select * from "
							+ db.policyholder + " where PH_Policyno like '"
							+ db.encode(actsearch.getText().toString()) + "%'",
							null);
					String[] autopolicyno = new String[cur1.getCount()];
					cur1.moveToFirst();
					if (cur1.getCount() != 0) {
						if (cur1 != null) {
							int i = 0;
							do {
								autopolicyno[i] = db.decode(cur1.getString(cur1
										.getColumnIndex("PH_Policyno")));

								if (autopolicyno[i].contains("null")) {
									autopolicyno[i] = autopolicyno[i].replace(
											"null", "");
								}

								i++;
							} while (cur1.moveToNext());
						}
						cur1.close();
					}

					Cursor cur2 = db.wdo_db.rawQuery("select * from "
							+ db.policyholder + " where PH_FirstName like '"
							+ db.encode(actsearch.getText().toString()) + "%'",
							null);
					String[] autoownersname = new String[cur2.getCount()];
					cur2.moveToFirst();
					if (cur2.getCount() != 0) {
						if (cur2 != null) {
							int i = 0;
							do {
								autoownersname[i] = db
										.decode(cur2.getString(cur2
												.getColumnIndex("PH_FirstName")));

								if (autoownersname[i].contains("null")) {
									autoownersname[i] = autoownersname[i]
											.replace("null", "");
								}

								i++;
							} while (cur2.moveToNext());
						}
						cur2.close();
					}

					Cursor cur3 = db.wdo_db.rawQuery("select * from "
							+ db.policyholder + " where PH_LastName like '"
							+ db.encode(actsearch.getText().toString()) + "%'",
							null);
					String[] lastname = new String[cur3.getCount()];
					cur3.moveToFirst();
					if (cur3.getCount() != 0) {
						if (cur3 != null) {
							int i = 0;
							do {
								lastname[i] = db.decode(cur3.getString(cur3
										.getColumnIndex("PH_LastName")));

								if (lastname[i].contains("null")) {
									lastname[i] = lastname[i].replace("null",
											"");
								}

								i++;
							} while (cur3.moveToNext());
						}
						cur3.close();
					}

					List<String> images = new ArrayList<String>();
					images.addAll(Arrays.asList(autopolicyno));
					images.addAll(Arrays.asList(autoownersname));
					images.addAll(Arrays.asList(lastname));

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							EmailReport.this, R.layout.autocompletelist, images);
					actsearch.setThreshold(1);
					actsearch.setAdapter(adapter);

				} catch (Exception e) {

				}

			}
		};

		// third, we must add the textWatcher to our EditText
		actsearch.addTextChangedListener(textWatcher);

	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.emailreport_home:
			Intent intent_emailreport = new Intent(EmailReport.this,
					HomeScreen.class);
			startActivity(intent_emailreport);
			finish();
			break;

		case R.id.emailreport_search:
			Search();
			cf.hidekeyboard((EditText) actsearch);
			break;

		case R.id.emailreport_cancel:
			actsearch.setText("");
			db.CreateTable(2);
			Cursor cur = db.wdo_db.rawQuery("select * from " + db.policyholder,
					null);
			display_list(cur);
			cur.close();
			cf.hidekeyboard((EditText) actsearch);
			break;
		}
	}

	private void Search() {
		if (actsearch.getText().toString().trim().equals("")) {
			cf.show_toast("Please enter the Name or Policy Number to search.",
					0);
			actsearch.requestFocus();
			actsearch.setText("");
		} else {
			db.CreateTable(2);
			Cursor cur = db.wdo_db.rawQuery(
					"select * from " + db.policyholder
							+ " where PH_FirstName like '"
							+ db.encode(actsearch.getText().toString().trim())
							+ "%' or PH_LastName like '"
							+ db.encode(actsearch.getText().toString().trim())
							+ "%' or PH_Policyno like '"
							+ db.encode(actsearch.getText().toString().trim())
							+ "%'", null);

		/*	System.out.println("select * from " + db.policyholder
					+ " where PH_FirstName like '"
					+ db.encode(actsearch.getText().toString().trim())
					+ "%' or PH_LastName like '"
					+ db.encode(actsearch.getText().toString().trim())
					+ "%' or PH_Policyno like '"
					+ db.encode(actsearch.getText().toString().trim()) + "%'");

			System.out
					.println("policyholder search count is " + cur.getCount());
*/
			display_list(cur);
			if(cur!=null)
				cur.close();
		}
	}

	private void display_list(Cursor cur) {
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			lldynamic.removeAllViews();
			rlheader.setVisibility(View.VISIBLE);
			int cnt = cur.getCount();
			data = new String[cnt];
			datasend = new String[cnt];
			String[] ownersName = new String[cnt];

			TextView[] tv = new TextView[cnt];
			Button[] sendmail = new Button[cnt];
			LinearLayout.LayoutParams textparams, btnparams;

			int i = 0;
			do {
				String firstname = db.decode(cur.getString(cur
						.getColumnIndex("PH_FirstName")));
				data[i] = firstname + " ";

				String lastname = db.decode(cur.getString(cur
						.getColumnIndex("PH_LastName")));
				data[i] += lastname + " | ";
				ownersName[i] = firstname + " " + lastname;

				String policynumber = db.decode(cur.getString(cur
						.getColumnIndex("PH_Policyno")));
				data[i] += policynumber + " | ";
				String email = db.decode(cur.getString(cur
						.getColumnIndex("PH_Email")));
				data[i] += email;

				// For sending policy number and status to email report3
				String pn = db.decode(cur.getString(cur
						.getColumnIndex("PH_Policyno")));
				datasend[i] = pn + "&#40";
				String status = db.decode(String.valueOf(cur.getInt(cur
						.getColumnIndex("PH_Status"))));
				datasend[i] += status + "&#40" + ownersName[i];
				//datasend[i] +=  ":" +  db.decode(cur.getString(cur.getColumnIndex("PH_SRID")));
				// End of For sending policy number and status to email report3

				LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams.setMargins(0, 2, 0, 0);

				LinearLayout ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				lldynamic.addView(ll);

				textparams = new LinearLayout.LayoutParams(775,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				textparams.setMargins(20, 20, 20, 20);
				// textparams.gravity = Gravity.CENTER_VERTICAL;

				btnparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				btnparams.setMargins(20, 0, 20, 0);
				btnparams.gravity = Gravity.CENTER_VERTICAL;

				// LinearLayout lltext = new LinearLayout(this);
				// ll.addView(lltext);

				tv[i] = new TextView(this, null, R.attr.textview_fw);
				tv[i].setLayoutParams(textparams);
				tv[i].setText(data[i]);
				tv[i].setTextSize(14);
				ll.addView(tv[i]);

				// LinearLayout llbtn = new LinearLayout(this);
				// llbtn.setGravity(Gravity.CENTER_VERTICAL);
				// ll.addView(llbtn);

				sendmail[i] = new Button(this, null, R.attr.button);
				sendmail[i].setLayoutParams(btnparams);
				sendmail[i].setText("Send Mail");
//				sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
//				sendmail[i].setTextColor(0xffffffff);
				sendmail[i].setTextSize(14);
				sendmail[i].setTypeface(null, Typeface.BOLD);
				sendmail[i].setTag(email + "&#40" + policynumber + "&#40"
						+ ownersName[i] + "&#40"+i+"&#40" + db.decode(cur.getString(cur.getColumnIndex("PH_SRID"))));
				ll.addView(sendmail[i]);

				sendmail[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String value = v.getTag().toString();
						String[] splitvalue = value.split("&#40");
						mailid = splitvalue[0];
						policyno = splitvalue[1];
						name = splitvalue[2];

						String ivalue = splitvalue[3];
						int ival = Integer.parseInt(ivalue);
						String ivalsplit = datasend[ival];
						String[] arrayivalsplit = ivalsplit.split("&#40");
						String pn = arrayivalsplit[0];
						String status = arrayivalsplit[1];
						String ownersname = arrayivalsplit[2];

						System.out.println("Mail id is :" + mailid);
						System.out.println("Policy no is :" + policyno);

						Intent intent = new Intent(EmailReport.this,
								EmailReport2.class);
						intent.putExtra("policynumber", pn);
						intent.putExtra("SRID",splitvalue[4] );
						intent.putExtra("policynumber", pn);
						intent.putExtra("status", status);
						intent.putExtra("mailid", mailid);
						intent.putExtra("classidentifier", "EmailReport");
						intent.putExtra("ownersname", ownersname);
						startActivity(intent);
						finish();

					}
				});

				if (i % 2 == 0) {
					ll.setBackgroundColor(Color.parseColor("#6E81A1"));
				} else {
					ll.setBackgroundColor(Color.parseColor("#64789A"));
				}
				i++;
			} while (cur.moveToNext());
		} else {
			rlheader.setVisibility(View.GONE);
			lldynamic.removeAllViews();
			cf.show_toast("Sorry, No results available", 0);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent_emailreport = new Intent(EmailReport.this,
				HomeScreen.class);
		startActivity(intent_emailreport);
		finish();
	}

}
