package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Contact_us extends Activity {
EditText ed[]=new EditText[5];
int show_handler;
ProgressDialog pd;
Webservice_Function wf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_us);
		wf=new Webservice_Function(this);
		ed[0]=(EditText) findViewById(R.id.F_name);
		ed[1]=(EditText) findViewById(R.id.L_name);
		ed[2]=(EditText) findViewById(R.id.email);
		ed[3]=(EditText) findViewById(R.id.phone);
		ed[4]=(EditText) findViewById(R.id.comment);
		ed[3].addTextChangedListener(new phone_nowatcher(ed[3]));
	}
public void clicker(View v)
{
	
	switch (v.getId())
	{
	case R.id.Cancel:
	case R.id.close:
		
		setResult(RESULT_CANCELED);
		finish();
	break;
	case R.id.Clear:
		for(int j=0;j<ed.length;j++)
		{
			ed[j].setText("");
		}
	break;
	case R.id.submit:
		Toast t = new Toast(Contact_us.this);
		if(!ed[0].getText().toString().trim().equals(""))
		{
			if(!ed[2].getText().toString().trim().equals(""))
			{
				if(eMailValidation(ed[2].getText().toString().trim()))
				{
					if(!ed[3].getText().toString().trim().equals(""))
					{
						if(ed[3].getText().toString().trim().length()==13)
						{
							if(wf.isInternetOn())
							{
							String source = "<b><font color=#00FF33> Sending Mail...</font></b>";
							pd = ProgressDialog.show(Contact_us.this, "", Html.fromHtml(source), true);
							new Thread() {
								public void run() {
									Looper.prepare();
									try {
										SoapObject request = new SoapObject(wf.NAMESPACE, "Contactusmail");
										SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
												SoapEnvelope.VER11);
										envelope.dotNet = true;
										/* <Firstname>string</Firstname>
									      <Lastname>string</Lastname>
									      <Email>string</Email>
									      <Phone>string</Phone>
									      <Comments>string</Comments>
									      <Inspectionname>string</Inspectionname>*/
										request.addProperty("Firstname", ed[0].getText().toString().trim());
										request.addProperty("Lastname", ed[1].getText().toString().trim());
										request.addProperty("Email", ed[2].getText().toString().trim());
										request.addProperty("Phone", ed[3].getText().toString().trim());
										request.addProperty("Comments", ed[4].getText().toString().trim());
										request.addProperty("Inspectionname", "Wood Destroying Organism");
										
										/*request.addProperty("ApplicationVersion",versionname);
										request.addProperty("DeviceName",model);
										request.addProperty("APILevel",apiLevel);*/
										System.out.println("Request is " + request);
										envelope.setOutputSoapObject(request);

										HttpTransportSE androidHttpTransport = new HttpTransportSE(wf.URL_sink);
										androidHttpTransport.call(wf.NAMESPACE + "Contactusmail", envelope);
										String result = envelope.getResponse().toString();
										if(result.equals("true"))
											show_handler=1;
										else
											show_handler=2;
										handler.sendEmptyMessage(0);
									} catch (SocketException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler=3;
										handler.sendEmptyMessage(0);
										
									}  catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler=3;
										handler.sendEmptyMessage(0);
										
									} catch (XmlPullParserException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler=3;
										handler.sendEmptyMessage(0);
										
									}catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler=4;
										handler.sendEmptyMessage(0);
										
									}
								}
									private Handler handler = new Handler() {
										

										@Override
										public void handleMessage(Message msg) {
											pd.dismiss();
											
											if(show_handler==1){
												show_handler=0;
												//setResult(RESULT_OK);
												//Toast.makeText(getApplicationContext(),"Mail send successfully  " , Toast.LENGTH_LONG).show();
												//cf.ShowToast("  ", 0);
												//finish();
												setResult(RESULT_OK);
												Toast.makeText(getApplicationContext(),"Mail has been sent successfully  " , Toast.LENGTH_LONG).show();
												//cf.ShowToast(" Mail has been sent successfully ", 0);
												finish();
											
											}
											else if(show_handler==3)
											{
												setResult(RESULT_CANCELED);
												Toast.makeText(getApplicationContext(),"Sorry please try again some other time, you have problem sending mail  " , Toast.LENGTH_LONG).show();
												//cf.ShowToast(" Sorry please try again some other time, you have problem sending mail", 0);
												finish();
												
											}
											else if(show_handler==4)
											{
												setResult(RESULT_CANCELED);
												Toast.makeText(getApplicationContext(),"Sorry please try again some other time, you have problem sending mail  " , Toast.LENGTH_LONG).show();
												//cf.ShowToast(" Sorry please try again some other time, you have problem sending mail", 0);
												finish();
												
											}
											
											
										}
									};
								}.start();
							}
							else
							{
								Toast.makeText(getApplicationContext()," Please enable internet connection and try again  " , Toast.LENGTH_LONG).show();
								//cf.ShowToast(" Please enable internet connection and try again", 0);
							}
					
						}
						else
						{
							Toast.makeText(getApplicationContext(),"Please enter valid Phone no " , 
									   Toast.LENGTH_LONG).show();

							
						}
					}
					else
					{
						Toast.makeText(getApplicationContext(),"Please enter Phone no  " , Toast.LENGTH_LONG).show();
						//t.setText("");t.show();
						
					}
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Please enter valid Email " , Toast.LENGTH_LONG).show();
					
					
				}
			}
			else
			{
				Toast.makeText(getApplicationContext(),"Please enter Email " , Toast.LENGTH_LONG).show();
				
				
			}
		}
		else
		{
			Toast.makeText(getApplicationContext(),"Please enter First Name " , Toast.LENGTH_LONG).show();
			
			
		}
	break;
	
	}
}
public boolean eMailValidation(CharSequence stringemailaddress1) {
	// TODO Auto-generated method stub
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	Pattern pattern = Pattern.compile(EMAIL_PATTERN);
	Matcher matcher = pattern.matcher(stringemailaddress1);

	return matcher.matches();

}
class phone_nowatcher implements TextWatcher
{
	EditText ed;
	public phone_nowatcher(EditText ed)
	{
		this.ed=ed;
		//System.out.println("comes correctly1");
	}
	int keyDel=0;
	int keydown=0;

	   @Override
	   public void onTextChanged(CharSequence s, int start, int before,
	     int count) {
		  // System.out.println("comes correctly2");
		   if (ed.getText().toString().startsWith(" "))
	          {
			   ed.setText("");
	          }
	    
	    if (ed.getText().toString().trim().matches("^0") )
	             {
	           			ed.setText("");
	             }
	

	   }

	   @Override
	   public void beforeTextChanged(CharSequence s, int start, int count,
	     int after) {
	    // TODO Auto-generated method stub

	   }

	   @Override
	   public void afterTextChanged(Editable s) {
	    // TODO Auto-generated method stub

		   
	    // TODO Auto-generated method stub
	  
	    if(keydown==0 )
	    {
	    	String ed_val=ed.getText().toString().trim();
	    	ed_val=ed_val.replace("(", "");
	    	ed_val=ed_val.replace(")", "");
	    	ed_val=ed_val.replace("-", "");
	    	ed_val=ed_val.trim();
	    	if(ed_val.length()>6)
	    			{
	    				keydown=1;
	    					ed.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,6)+"-"+ed_val.substring(6,ed_val.length()));
	    					//ed.setSelection(ed.getText().toString().length());
	    					
	    			}
	    			else if(ed_val.length()==6)
	    			{
	    				keydown=1;
    					ed.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,ed_val.length()));
    					//ed.setSelection(ed.getText().toString().length());
    					
	    			}
	    			else if(ed_val.length()>3)
	    			{
	    				keydown=1;
	    					ed.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,ed_val.length()));
	    				//	ed.setSelection(ed.getText().toString().length());
	    					
	    			}
	    			else if(ed_val.length()==3)
	    			{
	    				keydown=1;
	    				ed.setText("("+ed_val.substring(0,ed_val.length()));
	    				//ed.setSelection(ed.getText().toString().length());
	    				
	    			}
	    			else if(ed_val.length()==1) 
	    			{
	    				keydown=1;
	    				ed.setText("("+ed_val);
	    			//	ed.setSelection(ed.getText().toString().length());
	    				
	    			}
	    			else if(ed_val.equals(""))
	    			{
	    				keydown=1;
	    				ed.setText("");
	    				//ed.setSelection(ed.getText().toString().length());
	    				
	    			}
	    	ed.setSelection(ed.getText().toString().length());		
	    }
	    else
	    {
	    	keydown=0;
	    }
	    	
	    	
	    }

	   
	   
	
}
}
