package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.text.AttributedCharacterIterator.Attribute;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MenuInflater extends LinearLayout {
Context con;
int menu_no;
Button menus[]=new Button[7];
CommonFunction cf;

	public MenuInflater(Context context,int menu,CommonFunction cf) {
		super(context);
		con=context;
		menu_no=menu;
		this.cf=cf;
		create_menu();
		// TODO Auto-generated constructor stub
	}
	public MenuInflater(Context context,Attribute attr,int menu,CommonFunction cf) {
		super(context);
		con=context;
		menu_no=menu;
		this.cf=cf;
		create_menu();
		// TODO Auto-generated constructor stub
	}
	private void create_menu() {
		// TODO Auto-generated method stub
		LayoutInflater ly=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ly.inflate(R.layout.general_menus, this, true);
		
		menus[0]=(Button) findViewById(R.id.menu_general);
		menus[1]=(Button) findViewById(R.id.menu_insp);
		menus[2]=(Button) findViewById(R.id.menu_QA_question);
		menus[3]=(Button) findViewById(R.id.menu_photos);
		menus[4]=(Button) findViewById(R.id.menu_feedback);
		menus[5]=(Button) findViewById(R.id.menu_map);
		menus[6]=(Button) findViewById(R.id.menu_sub);
		switch (menu_no) {
		case 1:
			
		menus[0].setBackgroundDrawable(getResources().getDrawable(R.drawable.general));	
		break;
		case 2:
			
			menus[1].setBackgroundDrawable(getResources().getDrawable(R.drawable.inspectioninfo));	
		break;
		case 3:
			menus[2].setBackgroundDrawable(getResources().getDrawable(R.drawable.qa_question));	
		break;
		case 4:
			menus[3].setBackgroundDrawable(getResources().getDrawable(R.drawable.photos));	
		break;
		case 5:
			menus[4].setBackgroundDrawable(getResources().getDrawable(R.drawable.feedback));	
		break;
		case 6:
			menus[5].setBackgroundDrawable(getResources().getDrawable(R.drawable.map));	
		break;
		case 7:
			menus[6].setBackgroundDrawable(getResources().getDrawable(R.drawable.submit));	
		break;    
		default:
			System.out.println("comes wrong");
			break;
		}
		

		menus[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,policyholderInformation.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,InspectionsFindings.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[2].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,QA_question.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[3].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,Photos.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[4].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,Feedback.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[5].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,Map.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		menus[6].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent(con,Submit.class);
				in.putExtra("SRID", cf.selectedhomeid);
				con.startActivity(in);
			}
		});
		
		

	}

}
