package idsoft.inspectiondepot.wdo;



import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class Photos extends Activity {
CommonFunction cf;
public Uri CapturedImageURI;
Spinner sp_elev;
TextView no_uploaded;
LinearLayout upload_img;
DataBaseHelper db;
Button save;

String saved_val[];
private int maximumindb,maximumallowed=8;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photos);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Photos",0,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,4,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		save=(Button)findViewById(R.id.save);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				db.CreateTable(12);
				Cursor c = db.SelectTablefunction(db.ImageTable, " WHERE IM_SRID='"+cf.selectedhomeid+"'");
				if(c.getCount()>=1)
				{
					cf.show_toast("Photos saved successfully", 0);
					Intent in = new Intent(Photos.this,Feedback.class);
					in.putExtra("SRID", cf.selectedhomeid);
					startActivity(in);
					
				}
				else
				{
					cf.show_toast("Please add atleast one image", 0);
				}
				if(c!=null)
					c.close();
			}
		});
		
		declaration();
		show_savedvalue();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		sp_elev=(Spinner) findViewById(R.id.ph_elev_sp);
		ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,cf.elev);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_elev.setAdapter(ad);
		sp_elev.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				show_savedvalue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		no_uploaded=(TextView) findViewById(R.id.photo_noofuploads);
		upload_img=(LinearLayout) findViewById(R.id.uploaded_img);
	}
	public void clicker(View v)
	{
		if(v.getId()==R.id.hme)
		{
			cf.go_home();
		}
		else if(sp_elev.getSelectedItemPosition()!=0)
		{
			switch(v.getId())
			{
				
				case R.id.photos_galary:
					Cursor c1 = db.SelectTablefunction(db.ImageTable, " WHERE IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
					maximumindb =c1.getCount();
					if(maximumindb<8)
					{
		 				Intent reptoit1 = new Intent(this,Select_phots.class);
		 				//Bundle b=new Bundle();
		 				System.out.println("saved_val="+saved_val);
		 				System.out.println("gallery="+sp_elev.getSelectedItem().toString());
		 				reptoit1.putExtra("elevation", sp_elev.getSelectedItem().toString());
		 				reptoit1.putExtra("Selectedvalue", saved_val); /**Send the already selected image **/
		 				reptoit1.putExtra("Maximumcount", maximumindb);/**Total count of image in the database **/
		 				reptoit1.putExtra("Total_Maximumcount", 8); /***Total count of image we need to accept**/
		 				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
		 				startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
					}else
					{
						cf.show_toast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString(),0);
						
					}
					if(c1!=null)
						c1.close();
				break;
				case R.id.photos_camera:
					db.CreateTable(12);
					Cursor c = db.SelectTablefunction(db.ImageTable, " WHERE IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
					maximumindb =c.getCount();
					if(maximumindb<8)
					{
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						CapturedImageURI = getContentResolver().insert(
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
						startActivityForResult(intent, 111);
					}
					else
					{
						cf.show_toast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString(),0);
					}
					if(c!=null)
						c.close();
				break;
				
			}
		}
		else
		{
			cf.show_toast("Please select elevation type ", 0);
		}
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
		if(resultCode == RESULT_OK)
		{
			if (requestCode == 111) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(CapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor
						.getString(column_index_data);
				String selectedImagePath = capturedImageFilePath;
				display_taken_image(selectedImagePath);
				/*if(cursor!=null)
					cursor.close();*/
	
			}
			else if(requestCode == 121)
			{
				/** we get the result from the Idma page for this elevation **/
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
			  System.out.println("spin"+sp_elev.getSelectedItem().toString());
				for(int i=0;i<value.length;i++ )
				{
					
		 					db.wdo_db.execSQL("INSERT INTO "
	 							+ db.ImageTable
	 							+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
	 							+ " VALUES ('"+db.Insp_id+"','" + cf.selectedhomeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value[i]) + "','"
	 							+ db.encode(sp_elev.getSelectedItem().toString() + " "+(maximumindb+i+1))+ "','" + (maximumindb+1+i) + "')");
		 					
				}
				cf.show_toast(sp_elev.getSelectedItem().toString()+" image saved successfully", 1);
				show_savedvalue();
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 112)
			{
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  
			 
				  
			  
							db.wdo_db.execSQL("INSERT INTO "
	 							+ db.ImageTable
	 							+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
	 							+ " VALUES ('"+db.Insp_id+"','" + cf.selectedhomeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value) + "','"
	 							+ db.encode(Caption)+ "','" + (maximumindb+1) + "')");
		 					
		
				cf.show_toast(sp_elev.getSelectedItem().toString()+" image saved successfully", 1);
				show_savedvalue();
			  
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 122)
			{
			
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  String  id=	data.getExtras().getString("id"); 
			  boolean dele=data.getExtras().getBoolean("Delete_data");
			  if(!dele)
			  {
							db.wdo_db.execSQL(" UPDATE  "
	 							+ db.ImageTable
	 							+ " SET IM_path='"+db.encode(value)+"',IM_Description='"+db.encode(Caption)+"' "
	 							+ "  WHERE IM_Id='"+id+"'");
		 					
		
				cf.show_toast("Image saved successfully", 1);
				
				/** we get the result from the Idma page for this elevation Ends **/	
			  }
			  else
			  {
				 
				  try
				  {
				  Cursor c =db.SelectTablefunction(db.ImageTable, " WHERE IM_Elevation='"+sp_elev.getSelectedItemPosition()+ "' and IM_SRID='"+cf.selectedhomeid+"' AND IM_ImageOrder>(SELECT IM_ImageOrder FROM "+db.ImageTable+" WHERE IM_Id='"+id+"')" );
				  //System.out.println(" the query WHERE IM_Elevation='"+sp_elev.getSelectedItemPosition()+ "' and IM_SRID='"+cf.selectedhomeid+"' AND IM_ImageOrder>(SELECT IM_ImageOrder FROM "+db.ImageTable+" WHERE IM_Id='"+id+"')");
				  c.moveToFirst();
				  for(int i =0;i<c.getCount();i++,c.moveToNext())
				  {
					  db.wdo_db.execSQL(" UPDATE  "
							+ db.ImageTable
							+ " SET IM_ImageOrder=((SELECT IM_ImageOrder FROM "+db.ImageTable+" WHERE IM_Id='"+c.getInt(c.getColumnIndex("IM_Id"))+"')-1) "
							+ "  WHERE IM_Id='"+c.getInt(c.getColumnIndex("IM_Id"))+"'");
					  
				  }
				  db.wdo_db.execSQL(" DELETE FROM "+db.ImageTable+" WHERE IM_Id='"+id+"'");
				  if(c!=null)
						c.close();
				  }catch (Exception e) {
					// TODO: handle exception
					  System.out.println("the erro "+e.getMessage());
				}
			  }
			  show_savedvalue();
			}
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void display_taken_image(String selectedImagePath) {
		// TODO Auto-generated method stub
		Intent in = new  Intent(this,Edit_photos.class);
		in.putExtra("Path",selectedImagePath);
		in.putExtra("elevation", sp_elev.getSelectedItem().toString());
		in.putExtra("Caption",sp_elev.getSelectedItem().toString()+" "+(maximumindb+1));
		in.putExtra("saved_val",saved_val);
		in.putExtra("delete","false");
		startActivityForResult(in, 112);
	}
	private void show_savedvalue() {
		// TODO Auto-generated method stub
		db.CreateTable(12);
		Cursor c = db.SelectTablefunction(db.ImageTable, " WHERE IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
		System.out.println("sp_elev.getSelectedItemPosition()"+sp_elev.getSelectedItemPosition());
		maximumindb =c.getCount();
		no_uploaded.setText("No of image uploaded/Remaining image to be upload :"+maximumindb+"/"+(maximumallowed-maximumindb));
		if(c.getCount()>0)
		{
			c.moveToFirst();
			saved_val=new String[c.getCount()];
			upload_img.removeAllViews();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String path,img_order,description;
				path=db.decode(c.getString(c.getColumnIndex("IM_path")));
				saved_val[i]=db.decode(c.getString(c.getColumnIndex("IM_path")));
				description=db.decode(c.getString(c.getColumnIndex("IM_Description")));
				img_order=db.decode(c.getString(c.getColumnIndex("IM_ImageOrder")));
				
				LinearLayout li=new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				TextView tv= new TextView(this,null,R.attr.textview_200_inner);
				tv.setText(description);
				li.addView(tv,200,LayoutParams.WRAP_CONTENT);
				ImageView im =new ImageView(this,null,R.attr.photos_imageview);
				im.setTag(c.getString(c.getColumnIndex("IM_Id")));
				File f =new File(path);
				if(f.exists())
				{
					Bitmap b =cf.ShrinkBitmap(path, 100, 100);
					if(b!=null)
					{
						im.setImageBitmap(b);
					}
				}
				im.setOnClickListener(new im_clicker(path,c.getString(c.getColumnIndex("IM_Id"))));
				li.addView(im,100,100);
				
				Spinner sp =new Spinner(this);
				Cursor cap=db.SelectTablefunction(db.ImageCaption, " WHERE IM_C_InspectorId='"+db.Insp_id+"' AND IM_C_Elevation='"+sp_elev.getSelectedItemPosition()+"' and IM_C_ImageOrder='"+img_order+"'");
				String caption[];
				if(cap.getCount()>0)
				{
					cap.moveToFirst();
					caption=new String[cap.getCount()+2];
					cap.moveToFirst();
					caption[0]="--Select--";
					caption[1]="Add Caption";
					for(int j=2;j<cap.getCount()+2;j++,cap.moveToNext())
					{
						caption[j]=db.decode(cap.getString(cap.getColumnIndex("IM_C_caption")));
					}
					
				}
				else
				{
					caption=new String[2];
					caption[0]="--Select--";
					caption[1]="Add Caption";
				}
				if(cap!=null)
					cap.close();
				ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item, caption);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);
				sp.setOnItemSelectedListener(new sp_onclicker(c.getString(c.getColumnIndex("IM_Id")),img_order,sp,tv));
				sp.setPadding(20, 0, 0, 0);
				li.addView(sp,220,LayoutParams.WRAP_CONTENT);
				upload_img.addView(li,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				LinearLayout.LayoutParams  lp=(LinearLayout.LayoutParams)li.getLayoutParams();
				lp.setMargins(0, 5, 0, 5);
				li.setLayoutParams(lp);
			}
		}
		else
		{
			upload_img.removeAllViews();
			TextView tv= new TextView(this);
			tv.setText("No image found");
			upload_img.addView(tv,150,LayoutParams.WRAP_CONTENT);
			saved_val=null;
		}
		if(c!=null)
			c.close();	
	}
	class im_clicker implements OnClickListener
	{
		String path,id;
			public im_clicker(String path, String string) {
			// TODO Auto-generated constructor stub
				this.path=path;
				this.id=string;
		}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor c =db.SelectTablefunction(db.ImageTable," WHERE IM_Id='"+id+"'");
				c.moveToFirst();
				Intent in = new  Intent(Photos.this,Edit_photos.class);
				in.putExtra("elevation", sp_elev.getSelectedItem().toString());
				in.putExtra("Path",path);
				in.putExtra("Caption",db.decode(c.getString(c.getColumnIndex("IM_Description"))));
				in.putExtra("saved_val",saved_val);
				in.putExtra("id", id);
				startActivityForResult(in, 122);
				if(c!=null)
					c.close();
 				
			}
		}
	class sp_onclicker implements OnItemSelectedListener
	{
		String id,img_order;
		Spinner sp;
		TextView tv;
			public sp_onclicker(String id,String  img_order,Spinner sp,TextView tv) {
			// TODO Auto-generated constructor stub
				this.img_order=img_order;
				this.id=id;
				this.sp=sp;
				this.tv=tv;
		}

			

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				if(sp.getSelectedItem().toString().trim().equals("Add Caption"))
				{
					final Dialog dialog1 = new Dialog(Photos.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					ImageView close=((ImageView)dialog1.findViewById(R.id.helpclose));
					((TextView)dialog1.findViewById(R.id.txthead)).setText("Add caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							sp.setSelection(0);
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								db.wdo_db.execSQL(" INSERT INTO "+db.ImageCaption+" (IM_C_InspectorId,IM_C_Elevation,IM_C_caption,IM_C_ImageOrder) VALUES ('"+db.Insp_id+"','"+sp_elev.getSelectedItemPosition()+"','"+db.encode(ed.getText().toString().trim())+"','"+img_order+"')");
								//add_caption(ed.getText().toString().trim(),img_order);
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.show_toast("Please enter caption ", 0);
							}
						}
 
						
					});
					dialog1.show();				
					}
				else if(!sp.getSelectedItem().toString().trim().equals("--Select--"))
				{
					final Dialog dialog1 = new Dialog(Photos.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final LinearLayout li1=((LinearLayout)dialog1.findViewById(R.id.maintable));
					final LinearLayout li2=((LinearLayout)dialog1.findViewById(R.id.edit_Caption_li));
					li1.setVisibility(View.GONE);
					li2.setVisibility(View.VISIBLE);
					//final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button select=((Button)dialog1.findViewById(R.id.select));
					Button edit=((Button)dialog1.findViewById(R.id.edit));
					Button delete=((Button)dialog1.findViewById(R.id.delete));
					ImageView close1=((ImageView)dialog1.findViewById(R.id.helpclose1));
					((TextView)dialog1.findViewById(R.id.txtquestio1)).setText(sp.getSelectedItem().toString().toString());
					close1.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							//sp.setSelection(0);
							dialog1.dismiss();
						}
					});
					edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							li1.setVisibility(View.VISIBLE);
							li2.setVisibility(View.GONE);
						}
					});
					delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							AlertDialog.Builder b =new AlertDialog.Builder(Photos.this);
							b.setTitle("Confirmation");
							b.setMessage("Do you want to delete the selected Caption?");
							b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									try
									{
										
										db.wdo_db.execSQL(" DELETE  FROM "+db.ImageCaption+" WHERE IM_C_InspectorId='"+db.Insp_id+"' and IM_C_Elevation='"+sp_elev.getSelectedItemPosition()+"' and IM_C_ImageOrder='"+img_order+"' AND IM_C_caption='"+db.encode(sp.getSelectedItem().toString().trim())+"'");
										show_savedvalue();
										dialog1.dismiss();
										
									}catch (Exception e) {
										// TODO: handle exception
										System.out.println("the exeption in delete"+e.getMessage());
									}
									
									cf.show_toast("The selected caption has been deleted successfully.", 1);
									
								}
							});
							b.setNegativeButton("No", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
								}
							});   
							AlertDialog al=b.create();
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show(); 
						}
					});
					select.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
								db.wdo_db.execSQL(" UPDATE "+db.ImageTable+" SET IM_Description='"+db.encode(sp.getSelectedItem().toString().trim())+"' WHERE IM_Id='"+id+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								tv.setText(sp.getSelectedItem().toString().trim());
								//sp.setSelection(0);
								dialog1.dismiss();
						}

						
					});
					dialog1.show();
					
					/***For edit layout **/
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					ed.setText(sp.getSelectedItem().toString().trim());
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					ImageView close=((ImageView)dialog1.findViewById(R.id.helpclose));
					((TextView)dialog1.findViewById(R.id.txthead)).setText("Edit caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							sp.setSelection(0);
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								
								
								db.wdo_db.execSQL(" UPDATE "+db.ImageCaption+" SET IM_C_caption='"+db.encode(ed.getText().toString().trim())+"' WHERE  IM_C_InspectorId='"+db.Insp_id+"' and IM_C_Elevation='"+sp_elev.getSelectedItemPosition()+"' and IM_C_ImageOrder='"+img_order+"' AND IM_C_caption='"+db.encode(sp.getSelectedItem().toString().trim())+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.show_toast("Please enter caption ", 0);
							}
						}

						
					});
					dialog1.show();	
					
					/***For edit layout ends **/
				
				}
			}
			private void add_caption(String trim, String img_order) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		}

}
