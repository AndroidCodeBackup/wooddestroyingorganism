package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.phone_nowatcher;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditPhoneNumber extends Activity {
EditText ed;
int id;
String field,table,srid;
DataBaseHelper db;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_phone_number);
		 id= getIntent().getExtras().getInt("id");
		 String number= getIntent().getExtras().getString("number");
		 srid=getIntent().getExtras().getString("srid");
		 field=getIntent().getExtras().getString("field_name");
		 table=getIntent().getExtras().getString("table");
		 db=new DataBaseHelper(this);
	
		ed= (EditText)findViewById(R.id.editphone_edenter);
		ed.addTextChangedListener(new phone_nowatcher(ed));
		ed.setText(number);
	}

 public void clicker(View v)
 {
	 switch(v.getId())
	 {
	 	case R.id.close:
	 		Intent in = getIntent();
	 		in.putExtra("id", id);
	 		setResult(RESULT_CANCELED, in);
	 		finish();
	 	break;
	 	case R.id.clear:
	 		ed.setText("");
		break;
	 	case R.id.save:
	 		if(ed.getText().length()>=13)
	 		{
	 		Intent in1 = getIntent();
	 		in1.putExtra("id", id);
	 		in1.putExtra("number", ed.getText().toString());
	 		setResult(RESULT_OK, in1);
	 		update_number(ed.getText().toString().trim());
	 		finish();
	 		}
	 		else
	 		{
	 			final Toast toast = new Toast(this);
	 			 LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 					 View layout = inflater.inflate(R.layout.toast, null);
	 			
	 			TextView tv = (TextView) layout.findViewById(
	 					R.id.text);
	 			tv.setTextColor(Color.parseColor("#890200"));
	 			toast.setGravity(Gravity.CENTER, 0, 0);
	 			tv.setText("Please enter valid number");
	 			toast.setView(layout); 
	 			Thread t = new Thread() {
	 	            public void run() {
	 	                int count = 0;
	 	                try {
	 	                    while (true && count < 10) {
	 	                        toast.show();
	 	                        sleep(3);
	 	                        count++;

	 	                        // do some logic that breaks out of the while loop
	 	                    }
	 	                } catch (Exception e) {
	 	                   
	 	                }
	 	            }
	 	        };
	 	        t.start();
	 		}
		break;
	 }
 }

private void update_number(String no) {
	// TODO Auto-generated method stub
	if(table.equals(db.policyholder))
	{
		System.out.println(" the query="+"UPDATE "+db.policyholder+" SET "+field+"='"+db.encode(no)+"'  WHERE PH_SRID='"+srid+"'");
		db.wdo_db.execSQL("UPDATE "+db.policyholder+" SET "+field+"='"+db.encode(no)+"'  WHERE PH_SRID='"+srid+"'");
		
	}
	if(table.equals(db.Agent_tabble))
	{
		db.CreateTable(5);
		Cursor c=db.SelectTablefunction(db.Agent_tabble, " WHERE AI_SRID='"+srid+"'");
		if(c.getCount()>0)
			db.wdo_db.execSQL("UPDATE "+db.Agent_tabble+" SET "+field+"='"+no+"'  WHERE AI_SRID='"+srid+"'");
		else
		{
			db.wdo_db.execSQL("INSERT INTO "+db.Agent_tabble+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite)" +
					" VALUES ('"+srid+"','','','','','','','','','','','','','','') ");
			db.wdo_db.execSQL("UPDATE "+db.Agent_tabble+" SET "+field+"='"+no+"'  WHERE AI_SRID='"+srid+"'");
		}
	}
	if(table.equals(db.Additional_table))
	{
		db.CreateTable(4);
		Cursor c=db.SelectTablefunction(db.Additional_table, " WHERE AD_SRID='"+srid+"'");
		if(c.getCount()>0)
			db.wdo_db.execSQL("UPDATE "+db.Additional_table+" SET "+field+"='"+no+"'  WHERE AD_SRID='"+srid+"'");
		else
		{
			db.wdo_db.execSQL("INSERT INTO "+db.Additional_table+" (AD_SRID,AD_INSPID,AD_IsRecord,AD_UserTypeName,AD_ContactEmail,AD_PhoneNumber,AD_MobileNumber,AD_BestTimetoCallYou,AD_Bestdaycall,AD_FirstChoice,AD_SecondChoice,AD_ThirdChoice,SF_AD_FeedbackComments) VALUES" +
					" ('"+srid+"','"+db.Insp_id+"','','','','','','','','','','','') ");
			db.wdo_db.execSQL("UPDATE "+db.Additional_table+" SET "+field+"='"+no+"'  WHERE AD_SRID='"+srid+"'");
		}
	}
}
}

