package idsoft.inspectiondepot.wdo;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class View_pdf extends Activity {
	DataBaseHelper db;
	Webservice_Function wb;
    CommonFunction cf;
    RelativeLayout rlqainspection,rlexport;
    TextView tvnoofrecords,tvnorecord;
    LinearLayout llqainspectionlist,replin;
    private int show_handler;
	private int total;
	private String[] data;
	private String[] datasend;
	private String[] pdfpath;
	private String path;
	private String status;
	private String classidentifier;
	private int no_of_record=0;
	private int current=1;
	int rec_per_page=10;
	int numbeofpage=1;
	PowerManager.WakeLock wl=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.export);
		cf=new CommonFunction(this);
		db= new DataBaseHelper(this);
		wb=new Webservice_Function(this);
		
		rlqainspection=(RelativeLayout)findViewById(R.id.export_rlqainspection);
		rlexport=(RelativeLayout)findViewById(R.id.export_rlexport);
		tvnoofrecords=(TextView)findViewById(R.id.export_tvnoofrecords);
		tvnorecord=(TextView)findViewById(R.id.export_tvnorecord);
		llqainspectionlist=(LinearLayout)findViewById(R.id.export_llqainspectionlist);
		replin = (LinearLayout)findViewById(R.id.mylin);
		
		((TextView) findViewById(R.id.insp_type)).setText("Status : Reports Ready");
		findViewById(R.id.insp_type).setVisibility(View.VISIBLE);
		Bundle b =getIntent().getExtras();
		if(b!=null)
		{
		status=b.getString("type");
		classidentifier=b.getString("classidentifier");
		no_of_record=b.getInt("total_record");
		current=b.getInt("current");
		
			rlexport.setVisibility(View.GONE);
			rlqainspection.setVisibility(View.VISIBLE);
	//		QA_Inspection();
		
		}
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-20;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		rlexport.setVisibility(View.GONE);
		  findViewById(R.id.list_insp_search_ed).setVisibility(View.GONE);
		  findViewById(R.id.list_insp_clear_bt).setVisibility(View.GONE);
		  findViewById(R.id.list_insp_search_bt).setVisibility(View.GONE);
		rlqainspection.setVisibility(View.VISIBLE);
		System.out.println("classidentifier="+classidentifier);
		if(classidentifier.equals("DashBoard"))
		{
			replin.setVisibility(View.GONE);
		}
		else
		{
			replin.setVisibility(View.VISIBLE);
		}
		QA_Inspection();
		cf.setTouchListener(findViewById(R.id.content));
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(View_pdf.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});

//		/***pagination starts***/
//		if(no_of_record>rec_per_page)
//		{
//			((LinearLayout) findViewById(R.id.page_li)).setVisibility(View.VISIBLE);
//			
//			numbeofpage= ((no_of_record%rec_per_page) !=0)? (no_of_record/rec_per_page)+1:(no_of_record/rec_per_page);  
//			((TextView) findViewById(R.id.page_txt_no_fo_page)).setText("Current page/Total page:"+current+"/"+numbeofpage);
//			if(current==1)
//			{
//				((Button) findViewById(R.id.page_prev)).setVisibility(View.INVISIBLE);
//			}
//			if(current==numbeofpage)
//			{
//				((Button) findViewById(R.id.page_next)).setVisibility(View.INVISIBLE);
//			}
//		}
		
	}
	private void QA_Inspection()
	{
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing... Please wait..."
					+ " </font></b>";
			final ProgressDialog pd = ProgressDialog.show(View_pdf.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				

				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(wb.NAMESPACE,"ExportReportsready");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorId",Integer.parseInt(db.Insp_id));
						/*request.addProperty("Startrec",(rec_per_page*(current-1))+1);
						request.addProperty("Endrec",(rec_per_page*current));*/
						envelope.setOutputSoapObject(request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
						androidHttpTransport.call(wb.NAMESPACE+"ExportReportsready",envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						LoadExportReportsready(result);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",0);
							Intent in = new Intent(View_pdf.this,Dashboard.class);
							startActivity(in);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",0);
							Intent in = new Intent(View_pdf.this,Dashboard.class);
							startActivity(in);

						} else if (show_handler == 5) {
							show_handler = 0;
							
							//db.CreateTable(24);
							
							Cursor cur=db.wdo_db.rawQuery("select * from "+db.ReportsReady, null);//+"  order by Schedule_ScheduledDate desc"
							int count=cur.getCount();
							if(count>=1)
							{
								tvnoofrecords.setText("No of Records/Total : "+count+"/"+no_of_record);
								llqainspectionlist.setVisibility(View.VISIBLE);
								tvnorecord.setVisibility(View.INVISIBLE);
								
								((TextView) findViewById(R.id.export_list_note)).setText("FIRST NAME | LAST NAME | ADDRESS1 | ADDRESS2 | COUNTY | STATE | CITY | ZIP | EMAIL | WORK ORDER NO | ");
								
								DynamicList(((rec_per_page*(current-1))+1),(rec_per_page*current));
								
							}
							else
							{
								tvnoofrecords.setText("No of Records : 0");
								llqainspectionlist.setVisibility(View.GONE);
								tvnorecord.setVisibility(View.VISIBLE);
							}
							if(cur!=null)
								cur.close();
							
							
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available",0);
			Intent in = new Intent(this,Dashboard.class);
			startActivity(in);
			
		}
	}
	
	private void LoadExportReportsready(SoapObject result)
	{
		db.CreateTable(24);
		
	 	db.wdo_db.execSQL("delete from "+db.ReportsReady);
		int count;
		if(String.valueOf(result).equals("null") || String.valueOf(result).equals(null) || String.valueOf(result).equals("anytype{}"))
		 {
				count=0;
				total = 100;
		  }
		 else
		 {
			 count = result.getPropertyCount();
			System.out.println("the reulst"+result);
			 no_of_record=count;
			 int min=0,max=500;
			 int j=0;
			 do{
				 String sql="";
				 
			 for (int i = min; i < count && i<max;) {
				 
					SoapObject obj = (SoapObject) result.getProperty(i);
					
					String srid = String.valueOf(obj.getProperty("SRID"));
					String firstname = String.valueOf(obj.getProperty("FirstName"));
					String lastname = String.valueOf(obj.getProperty("LastName"));
					String address1 = String.valueOf(obj.getProperty("Address1"));
					String address2 = String.valueOf(obj.getProperty("Address2"));
					String state = String.valueOf(obj.getProperty("State"));
					String county = String.valueOf(obj.getProperty("Country"));
					String city = String.valueOf(obj.getProperty("City"));
					String zip = String.valueOf(obj.getProperty("Zip"));
					String email = String.valueOf(obj.getProperty("Email"));
					String status = String.valueOf(obj.getProperty("Status"));
					String policynumber = String.valueOf(obj.getProperty("OwnerPolicyNo"));
					String pdfpath = String.valueOf(obj.getProperty("Pdfpath"));
					String create_date =String.valueOf(obj.getProperty("AcceptedDate"));
					
					if(i==min)
					{
						sql="INSERT INTO "+db.ReportsReady+" SELECT ( SELECT max(id)+"+(i++)+" FROM "+db.ReportsReady+") as id,'"+ db.encode(srid) + "' as srid,'"+ db.encode(firstname) + "' as firstname,'"+ db.encode(lastname) + "' as lastname" +
						",'"+ db.encode(address1) + "' as address1,'"+ db.encode(address2) + "' as address2,'"+ db.encode(state) + "' as state,'"+ db.encode(county) + "' as county,'"+ db.encode(city) + "' as city,'"+ db.encode(zip) + "' as zip,'"+ db.encode(policynumber) + "' as policyno" +
						",'"+ db.encode(email) + "' as email,'"+ db.encode(status) + "' as status,'"+ db.encode(pdfpath) + "' as pdfpath,'"+create_date+"' as created_Date "; 
					}
					else
					{
						sql+=" UNION SELECT  ( SELECT max(id)+"+(i++)+" FROM "+db.ReportsReady+"),'"+ db.encode(srid) + "','"+ db.encode(firstname) + "','"+ db.encode(lastname) + "','"+ db.encode(address1) + "','"+ db.encode(address2) + "','"+ db.encode(state) + "'" +
								",'"+ db.encode(county) + "','"+ db.encode(city) + "','"+ db.encode(zip) + "','"+ db.encode(policynumber) + "','"+ db.encode(email) + "','"+ db.encode(status) + "','"+ db.encode(pdfpath) + "','"+create_date+"'";
					}
					
					if(max==(i) || count==(i))
					{
						try
						{
						
						db.wdo_db.execSQL(sql);
						min=i+1;
						}
						catch (Exception e) {
							// TODO: handle exception
							System.out.println("Some problem occres "+e.getMessage());
						}
					}
					/*"Insert into "+config_option+" SELECT   (Select max(CP_Id)+"+(i++)+" from "+config_option+") as CP_Id, '"+Insp_id+"' as CP_InspectorId,'22' as CP_Mtype,'1' as CP_Question,'"+encode("Subterranean Termites")+"' as CP_Option,'0' as CP_Default_op,'1' as CP_Order,'chk' as CP_optiontype,'0' as CP_editable,'' as CP_CreatedOn" +
					" UNION SELECT  (Select max(CP_Id)+"+(i++)+" from "+config_option+"),'"+Insp_id+"','22','1','"+encode("Powder Post Beetles")+"','0','2','chk','0',''"+
					
					db.wdo_db.execSQL("insert into "
							+ db.ReportsReady
							+ " (srid,firstname,lastname,address1,address2,state,county,city,zip,policyno,email,status,pdfpath) values('"
							+ db.encode(srid) + "','"
							+ db.encode(firstname) + "','"
							+ db.encode(lastname) + "','"
							+ db.encode(address1) + "','"
							+ db.encode(address2) + "','"
							+ db.encode(state) + "','"
							+ db.encode(county) + "','"
							+ db.encode(city) + "','"
							+ db.encode(zip) + "','"
							+ db.encode(policynumber) + "','"
							+ db.encode(email) + "','"
							+ db.encode(status) + "','" + db.encode(pdfpath)
							+ "');");
			*/		 j++;		
			 }
			
			 }while (j<count);
		 }
			
	}
	
	private void DynamicList(int start,int end) {
		//System.out.println(" comes the sql= ORDER BY created_Date DESC LIMIT "+(start-1)+","+(end-1));
		Cursor cur=db.wdo_db.rawQuery(" SELECT * from "+db.ReportsReady+" WHERE srid not in (Select PH_SRID From  "+db.policyholder+ " WHERE PH_Status='1') ORDER BY created_Date DESC LIMIT "+(start-1)+","+rec_per_page,null);
	System.out.println(" SELECT * from "+db.ReportsReady+" WHERE srid  not in (Select PH_SRID From  "+db.policyholder+ " WHERE PH_Status='1') ORDER BY created_Date DESC LIMIT "+(start-1)+","+rec_per_page);	
		llqainspectionlist.removeAllViews();
		ScrollView sv = new ScrollView(this);
		llqainspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		int rows = cur.getCount();
		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] pter = new Button[rows];
		
		//final Button[] sendmail = new Button[rows];
		LinearLayout[] rl = new LinearLayout[rows];
		final String[] ownersname = new String[rows];
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		l1.removeAllViews();
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {

				String SRID = db.decode(cur.getString(cur
						.getColumnIndex("srid")));
				String FirstName = db.decode(cur.getString(cur
						.getColumnIndex("firstname")));
				data[i] = " " + FirstName + " | ";
				ownersname[i] = FirstName + " ";
				String LastName = db.decode(cur.getString(cur
						.getColumnIndex("lastname")));
				data[i] += LastName + " | ";
				ownersname[i] += " " + FirstName;
				String Address1 = db.decode(cur.getString(cur
						.getColumnIndex("address1")));
				data[i] += Address1 + " | ";
				String Address2 = db.decode(cur.getString(cur
						.getColumnIndex("address2")));
				data[i] += Address2 + " | ";
				String City = db.decode(cur.getString(cur
						.getColumnIndex("city")));
				data[i] += City + " | ";
				String State = db.decode(cur.getString(cur
						.getColumnIndex("state")));
				data[i] += State + " | ";
				String Country = db.decode(cur.getString(cur
						.getColumnIndex("county")));
				data[i] += Country + " | ";
				String Zip = db
						.decode(cur.getString(cur.getColumnIndex("zip")));
				data[i] += Zip + " | ";
				String Email = db.decode(cur.getString(cur
						.getColumnIndex("email")));
				data[i] += Email + " | ";
				String OwnerPolicyNo = db.decode(cur.getString(cur
						.getColumnIndex("policyno")));
				data[i] += OwnerPolicyNo + " | ";
				datasend[i] = OwnerPolicyNo + "&#40";
				String Status = db.decode(cur.getString(cur
						.getColumnIndex("status")));
				datasend[i] += Status;
				String COMMpdf = db.decode(cur.getString(cur
						.getColumnIndex("pdfpath")));
				pdfpath[i] = COMMpdf;

//				System.out.println("The COMMpdf path is " + COMMpdf);
//
//				System.out.println("The Datas is " + data[i]);

				LinearLayout.LayoutParams llparams;
				LinearLayout.LayoutParams lltxtparams;

				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				llparams.setMargins(0, 2, 0, 0);

				rl[i] = new LinearLayout(this);
				l1.addView(rl[i], llparams);

				lltxtparams = new LinearLayout.LayoutParams(
						550,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(10, 20, 10, 20);

				tvstatus[i] = new TextView(this, null, R.attr.textview_fw);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setId(1);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextSize(14);
				tvstatus[i].setPadding(10, 10, 10, 10);
				rl[i].addView(tvstatus[i]);

				LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams1.setMargins(0, 0, 20, 0);
				downloadparams1.gravity = Gravity.CENTER_VERTICAL;

				LinearLayout.LayoutParams sendmailparams = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				sendmailparams.setMargins(110, 0, 20, 0);
				sendmailparams.gravity = Gravity.CENTER_VERTICAL;

				view[i] = new Button(this, null, R.attr.button);
				view[i].setLayoutParams(downloadparams1);
				view[i].setId(2);
				view[i].setText("View PDF");
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				view[i].setTag(i);
				rl[i].addView(view[i]);
				

				pter[i] = new Button(this, null, R.attr.button);
				pter[i].setLayoutParams(downloadparams1);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setTextSize(14);
				pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i + "&#40" + Email + "&#40" + ownersname[i] + "&#40" + i+"&#40"+SRID);
				rl[i].addView(pter[i]);
				
				Button edit = new Button(this, null, R.attr.button);
				edit.setLayoutParams(downloadparams1);
				//edit.setId(2);
				edit.setText("Edit");
				edit.setTextSize(14);
				edit.setTypeface(null, Typeface.BOLD);
				edit.setTag(SRID);
				rl[i].addView(edit);

				if (COMMpdf.equals("N/A")) {
				//	sendmail[i].setVisibility(View.VISIBLE);
					view[i].setVisibility(View.GONE);
					pter[i].setVisibility(View.GONE);
				} else {
				//	sendmail[i].setVisibility(View.GONE);
					view[i].setVisibility(View.VISIBLE);
					pter[i].setVisibility(View.VISIBLE);

				}

				view[i].setOnClickListener(new OnClickListener() {


					

					@Override
					public void onClick(View v) {

						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						File sdDir = new File(Environment
								.getExternalStorageDirectory().getPath());
						File file = new File(sdDir.getPath()
								+ "/DownloadedPdfFile/" + filename);

						if (file.exists()) {
							View_Pdf_File(filename,path);
						} else {
							if (wb.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... ");
								 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
								 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
								 wl.acquire();
								new Thread() {

									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {

										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",0);

											} else if (show_handler == 2) {
												show_handler = 0;

												View_Pdf_File(filename,path);

											}
											if(wl!=null)
											{
											wl.release();
											wl=null;
											}
										}
									};
								}.start();
							} else {
								cf.show_toast("Internet connection not available",0);

							}
						}
					}
				});

				
				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String buttonvalue = v.getTag().toString();
						String[] arrayvalue = buttonvalue.split("&#40");
						String value = arrayvalue[0];
						String emailaddress = arrayvalue[1];
						String name = arrayvalue[2];

						String ivalue = arrayvalue[3];
						int ival = Integer.parseInt(ivalue);
						String ivalsplit = datasend[ival];
						String[] arrayivalsplit = ivalsplit.split("&#40");
						String pn = arrayivalsplit[0];
						String status = arrayivalsplit[1];

						int s = Integer.parseInt(value);
						path = pdfpath[s];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];

						Intent intent = new Intent(View_pdf.this,
								EmailReport2.class);
						intent.putExtra("policynumber", pn);
						intent.putExtra("status", status);
						intent.putExtra("mailid", emailaddress);
						intent.putExtra("SRID",arrayvalue[4] );
						intent.putExtra("classidentifier", classidentifier);
						intent.putExtra("ownersname", ownersname[ival]);
						startActivity(intent);
						finish();

					}
				});
				edit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						AlertDialog.Builder ai= new AlertDialog.Builder(View_pdf.this)
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								//Start_import(v.getTag().toString());
								if(wb.isInternetOn())
								{
									Cursor c =db.SelectTablefunction(db.ReportsReady, " WHERE srid='"+v.getTag().toString()+"'");
									c.moveToFirst();
									
									path = db.decode(c.getString(c.getColumnIndex("pdfpath")));
									String[] filenamesplit = path.split("/");
									final String filename = filenamesplit[filenamesplit.length - 1];
									File sdDir = new File(Environment
											.getExternalStorageDirectory().getPath());
									File file = new File(sdDir.getPath()
											+ "/DownloadedPdfFile/" + filename);
								
									//cf.go_back(Import.class);
									if(file.exists())
									{
										file.delete();
									}
									Intent in = new Intent(View_pdf.this,Reterive_data.class);
									in.putExtra("SRID", v.getTag().toString());
									startActivityForResult(in, 22);
									if(c!=null)
										c.close();
								}
								else
								{
									cf.show_toast("Please enable internet connection then try import", 0);
								}
							}
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
								
							}
						})
						.setMessage(" Do you want to edit and re-generate the selected report? An internet connection is required.");
					    AlertDialog ad=ai.create();
					    ad.show();
					    
								
					}
				});
				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#6E81A1"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#64789A"));
				}
				i++;
			} while (cur.moveToNext());

		}
		/***pagination starts***/
		if(no_of_record>rec_per_page)
		{
			((LinearLayout) findViewById(R.id.page_li)).setVisibility(View.VISIBLE);
			
			numbeofpage= ((no_of_record%rec_per_page) !=0)? (no_of_record/rec_per_page)+1:(no_of_record/rec_per_page);  
			((TextView) findViewById(R.id.page_txt_no_fo_page)).setText("Current page/Total page:"+current+"/"+numbeofpage);
			if(current==1)
			{
				((Button) findViewById(R.id.page_prev)).setVisibility(View.INVISIBLE);
			}
			else 
			{
				((Button) findViewById(R.id.page_prev)).setVisibility(View.VISIBLE);	
			}
			if(current==numbeofpage)
			{
				((Button) findViewById(R.id.page_next)).setVisibility(View.INVISIBLE);
			}
			else
			{
				((Button) findViewById(R.id.page_next)).setVisibility(View.VISIBLE);
			}
			
		}
		else
		{
			((LinearLayout) findViewById(R.id.page_li)).setVisibility(View.INVISIBLE);
		}
		if(cur!=null)
			cur.close();
	}
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(View_pdf.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.go_home();
		break;
		case R.id.list_insp_clear_bt:
			((EditText) findViewById(R.id.list_insp_search_ed)).setText("");
			
		break;
		
		case R.id.page_next:
			/*Intent in =new Intent(this,View_pdf.class);
			in.putExtra("type", status);
			in.putExtra("classidentifier", classidentifier);
			in.putExtra("total_record", no_of_record);
			in.putExtra("current", current+1);
			startActivity(in);*/
			current++;
			DynamicList(((rec_per_page*(current-1))+1),(rec_per_page*current));
		break;
		case R.id.page_prev:
			/*Intent in_prev =new Intent(this,View_pdf.class);
			in_prev.putExtra("type", status);
			in_prev.putExtra("classidentifier", classidentifier);
			in_prev.putExtra("total_record", no_of_record);
			in_prev.putExtra("current", current-1);
			startActivity(in_prev);*/
			current--;
			DynamicList(((rec_per_page*(current-1))+1),(rec_per_page*current));
		break;
		case R.id.page_goto:
			String go=((EditText) findViewById(R.id.page_ed_go_to)).getText().toString();
			if(!go.equals(""))
			{
				if(Integer.parseInt(go)<=numbeofpage)
				{
					if(Integer.parseInt(go)<=current)
					{
						/*Intent in_goto =new Intent(this,View_pdf.class);
						in_goto.putExtra("type", status);
						in_goto.putExtra("classidentifier", classidentifier);
						in_goto.putExtra("total_record", no_of_record);
						in_goto.putExtra("current", Integer.parseInt(go));
						startActivity(in_goto);*/
						current=Integer.parseInt(go);
						DynamicList(((rec_per_page*(current-1))+1),(rec_per_page*current));
					}else
					{
						cf.show_toast("Request page and current page are same ", 0);
					}
				}
				else
				{
					cf.show_toast("Invalid page no", 0);
				}
			}else
			{
				cf.show_toast("Invalid page no", 0);
			}
					
		break;
		default:
			break;
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent in = new Intent(this,Dashboard.class);
			startActivity(in);
		}
		return super.onKeyDown(keyCode, event);
	}

}
