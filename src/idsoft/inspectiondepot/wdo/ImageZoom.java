package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.imagezoom.ImageViewTouch;
import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class ImageZoom extends Activity {
	ImageViewTouch mImage;
	Button mButton;
	String arrpath;
	CommonFunction cf;
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		requestWindowFeature( Window.FEATURE_NO_TITLE );
		cf= new CommonFunction(this);
		setContentView(R.layout.imagezoom);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			arrpath = extras.getString("Path");
			System.out.println("the path"+arrpath);
	 	}		
		mImage = (ImageViewTouch) findViewById( R.id.image );
		mButton = (Button) findViewById( R.id.button );		
		mButton.setOnClickListener( new OnClickListener() {			
			@Override
			public void onClick( View v ) {
				finish();
			}
		});
		Uri imageUri = null;
		try {
			imageUri = Uri.parse(URLEncoder.encode(arrpath, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Bitmap bitmap = BitmapFactory.decodeFile(arrpath);//DecodeUtils.decode( this, imageUri, 1024, 1024 );
		mImage.setImageBitmap( bitmap );
	}
}
  