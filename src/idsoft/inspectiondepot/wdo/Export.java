package idsoft.inspectiondepot.wdo;
import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.Policy;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Export extends Activity{
CommonFunction cf;
String status="",Where="";
TextView type_insp_txt,no_of_records;
EditText search_ed;
LinearLayout list_layout;
DataBaseHelper db;
Webservice_Function wb;
int mState;
int typeBar = 1;
double total; // Determines type progress bar: 0 = spinner, 1 = horizontal
int delay = 40; // Milliseconds of delay in the update loop
int maxBarValue = 0,show_handler; // Maximum value of horizontal progress bar
Dialog dialog1;
public ProgressThread progThread;
ProgressDialog progDialog;
int RUNNING = 1;
String Exportbartext="";
String classidentifier;
int handler_verif=0;
PowerManager.WakeLock wl=null;
int Current_insp=0;
private String  Error_tracker="";
private String feed_missingfiles,path;  
boolean ex_status[] ={false,false,false,false,false,false,true,false,false};
boolean ex_function[] ={false,false,false,false,false,false,true,false,false};

String[] data,datasend,pdfpath; 

RelativeLayout rlqainspection,rlexport;
TextView tvnoofrecords,tvnorecord;
LinearLayout llqainspectionlist,replin;

@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.export);
		cf=new CommonFunction(this);
		db= new DataBaseHelper(this);
		wb=new Webservice_Function(this);
		
		rlqainspection=(RelativeLayout)findViewById(R.id.export_rlqainspection);
		rlexport=(RelativeLayout)findViewById(R.id.export_rlexport);
		tvnoofrecords=(TextView)findViewById(R.id.export_tvnoofrecords);
		tvnorecord=(TextView)findViewById(R.id.export_tvnorecord);
		llqainspectionlist=(LinearLayout)findViewById(R.id.export_llqainspectionlist);
		replin = (LinearLayout)findViewById(R.id.mylin);
		
		Bundle b =getIntent().getExtras();
		
		
			cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-20;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		 
		if(b!=null)
		{
			status=b.getString("type");
			classidentifier=b.getString("classidentifier");
			if(classidentifier.equals(null))
			{
				classidentifier="Export";
			}
		
			/*if(status.equals("emailreport"))
			{
				rlexport.setVisibility(View.GONE);
				rlqainspection.setVisibility(View.VISIBLE);
				QA_Inspection();
			}
			else
			{
				Declaration();
			}*/
			
			Declaration();
		}
		else
		{
			
			Intent in = new Intent(this,Dashboard.class);
			startActivity(in);
		}
		cf.setTouchListener(findViewById(R.id.content));
//		findViewById(R.id.head_policy_info).setVisibility(View.INVISIBLE);
//		findViewById(R.id.head_take_image).setVisibility(View.INVISIBLE);
		
	
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(Export.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});

	}

	private void Declaration() {
		// TODO Auto-generated method stub
		
		rlexport.setVisibility(View.VISIBLE);
		rlqainspection.setVisibility(View.GONE);
		
		type_insp_txt=(TextView) findViewById(R.id.insp_type);
		findViewById(R.id.page_li).setVisibility(View.GONE);
		if(status.equals("export"))
		{
			type_insp_txt.setText("Status : Inspected");
			type_insp_txt.setVisibility(View.VISIBLE);
//			((Button) findViewById(R.id.reexport)).setText("Re-Export");
			((Button) findViewById(R.id.reexport)).setText("Export");
			replin.setVisibility(View.VISIBLE);
		}
		else
		{
			replin.setVisibility(View.GONE);
		}
//		else
//		{
//			type_insp_txt.setText("Re-Export Inspection");
//			((Button) findViewById(R.id.reexport)).setText("Export");
//		}
		no_of_records=(TextView) findViewById(R.id.list_insp_noofrecords); 
		search_ed =(EditText) findViewById(R.id.list_insp_search_ed);
		list_layout =(LinearLayout) findViewById(R.id.list_insp_inspeclist);
		((TextView) findViewById(R.id.list_note)).setText("NAME | WORK ORDER NO | ADDRESS | CITY - STATE | COUNTY | ZIP");
		Add_list_layout("");
		
	}

	private void Add_list_layout(String search) {
		// TODO Auto-generated method stub
		if(status.equals("export"))
		{
			Where=" Where PH_InspectorId='"+db.Insp_id+"' and PH_IsInspected='1' and PH_SubStatus<>'1' ";
		}
		else
		{
			Where=" Where PH_InspectorId='"+db.Insp_id+"' and PH_IsInspected='2' and PH_SubStatus<>'1' ";
		}
			
			if(!search.equals(""))
			{
				Where+=" AND (PH_FirstName like '%"+db.encode(search)+"%' or PH_LastName like '%"+db.encode(search)+"%' or PH_Policyno  like '%"+db.encode(search)+"%')";
			}
			
			//For displaying from newest to oldest
			
			Where +="  order by Schedule_ScheduledDate desc";
			
			//For displaying from newest to oldest
		
		Cursor c=db.SelectTablefunction(db.policyholder, Where);
		no_of_records.setText("No of Records : "+c.getCount());
		list_layout.removeAllViews();
		System.out.println(" WHERE "+c.getCount()+"/n"+db.policyholder+" "+Where);
		if(c.getCount()>0)
		{
			c.moveToFirst();
			findViewById(R.id.no_record).setVisibility(View.INVISIBLE);
			((TextView) findViewById(R.id.list_note)).setVisibility(View.VISIBLE);
			for(int i =0;i<c.getCount();i++,c.moveToNext())
			{
				String srid,f_name,l_name,po_no,address,city,state,county,zipcode,starttime,endtime,insp_date;
				srid=db.decode(c.getString(c.getColumnIndex("PH_SRID")));
				f_name=db.decode(c.getString(c.getColumnIndex("PH_FirstName")));
				l_name=db.decode(c.getString(c.getColumnIndex("PH_LastName")));
				po_no=db.decode(c.getString(c.getColumnIndex("PH_Policyno")));
				address=db.decode(c.getString(c.getColumnIndex("PH_Address1")));
				city=db.decode(c.getString(c.getColumnIndex("PH_City")));
				state=db.decode(c.getString(c.getColumnIndex("PH_State")));
				county=db.decode(c.getString(c.getColumnIndex("PH_County")));
				zipcode=db.decode(c.getString(c.getColumnIndex("PH_Zip")));
				starttime=db.decode(c.getString(c.getColumnIndex("Schedule_InspectionStartTime")));
				endtime=db.decode(c.getString(c.getColumnIndex("Schedule_InspectionEndTime")));
				insp_date=db.decode(c.getString(c.getColumnIndex("Schedule_ScheduledDate")));
				RelativeLayout[] rl=new RelativeLayout[c.getCount()];
				Button[] bt=new Button[c.getCount()];   
				TextView[] tv=new TextView[c.getCount()];
				rl[i]=new RelativeLayout(this);
				Button export=new Button(this,null,R.attr.button);
				tv[i] =new TextView(this,null,R.attr.textview_fw);
				tv[i].setText(f_name+" "+l_name+" | "+po_no+" | "+address+" | "+city+" - "+state+" | "+county+" | "+zipcode);
				tv[i].setPadding(10, 10, 10, 10);
						//+" | "+insp_date+" | "+starttime+" | "+endtime);
				/*bt.setTag("1");*/
				export.setText("Create My Report");
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						900,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lp.setMargins(10, 20, 10, 20);
				rl[i].addView(tv[i],lp);
				tv[i].setClickable(true);
				
				rl[i].addView(export);
				/***Align the text view and button ***/
				RelativeLayout.LayoutParams rl_lp=(android.widget.RelativeLayout.LayoutParams) export.getLayoutParams();
				rl_lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				rl_lp.setMargins(10, 0, 20, 0);
				rl_lp.addRule(RelativeLayout.CENTER_VERTICAL);
				export.setLayoutParams(rl_lp);
				LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				lp2.setMargins(0, 2, 0, 0);
				list_layout.addView(rl[i],lp2);
//				View v =new View(this);
//				v.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
//				list_layout.addView(v,LayoutParams.FILL_PARENT,2);
				
				export.setOnClickListener(new tv_clicker(srid));
				
				tv[i].setOnClickListener(new tv_clicker(srid));
				
				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#6E81A1"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#64789A"));
				}
				
			}
		}
		else
		{
			findViewById(R.id.no_record).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.list_note)).setVisibility(View.GONE);
		}
		/***Align the text view and button ends ***/
		if(c!=null)
			c.close();
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.go_home();
		break;
		case R.id.list_insp_search_bt:
			if(search_ed.getText().toString().trim().equals(""))
			{
				cf.show_toast("Please enter searche text.",0);
			}
			else
			{
				Add_list_layout(search_ed.getText().toString().trim());
			}
		break;
		case R.id.list_insp_clear_bt:
			search_ed.setText("");
			Add_list_layout("");
		break;
		case R.id.reexport:
//			if(!((Button) v).getText().toString().trim().equals("Export"))
//			{
				Intent submitint = new Intent(getApplicationContext(),Export.class);
//				submitint.putExtra("type", "Re-export");
				submitint.putExtra("type", "Export");
				submitint.putExtra("classidentifier", "Export");
				startActivity(submitint);
//			}
//			else
//			{
//				Intent submitint = new Intent(getApplicationContext(),Export.class);
//				submitint.putExtra("type", "export");
//				startActivity(submitint);
//			}
		break;
		
		case R.id.export_qainspection:
			/*rlexport.setVisibility(View.GONE);
			rlqainspection.setVisibility(View.VISIBLE);
			QA_Inspection();*/
			break;
		
		default:
			cf.show_toast("Button under construction", 1);
		break;
		}
		
	}
	
	

	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
				if(classidentifier.equals("DashBoard"))
				{
					Intent in = new Intent(this,Dashboard.class);
					startActivity(in);
				}
				else
				{
					Intent in = new Intent(this,HomeScreen.class);
					startActivity(in);
				}
		}
		return super.onKeyDown(keyCode, event);
	}
	class tv_clicker implements OnClickListener 
	{
		String srid;
		tv_clicker(String srid)
		{
			this.srid=srid;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 final Dialog dialog1 = new Dialog(Export.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alert);
				((TextView) dialog1.findViewById(R.id.alert_title)).setText("Confirmation");
				((TextView) dialog1.findViewById(R.id.alert_txt)).setText("Confirm you want to create your WDO report?");
				dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
				dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);
				
				Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
				Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
				ImageView close = (ImageView) dialog1.findViewById(R.id.alert_close);
				btn_yes.setOnClickListener(new OnClickListener()
				{
	           	@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
	           		dialog1.dismiss();
	           		
					startexpot(srid,ex_function);
					
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				close.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				dialog1.setCancelable(false);
				dialog1.show();
		}
	
		
	
			
		}
		
	public void startexpot(String srid2,final boolean[] function) {
		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {
			cf.selectedhomeid=srid2;
			 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
			 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
			 wl.acquire();
			 total=0.0;
             /**We need to delete the preivious progress bar then we start new one code starts herer**/
				try
				{
				removeDialog(1);
				}
				catch (Exception e)
				{
					
				}
				/**We need to delete the preivious progress bar then we start new one code Ends herer**/
              progDialog = new ProgressDialog(Export.this); // we creat dilog box 
				showDialog(1);
				new Thread() {

					private String elevvetiondes;
					private int usercheck;

					public void run() {
						Looper.prepare();

						try {
							
						Error_tracker="";	
						String	selectedItem = cf.selectedhomeid;
							total = 1.0;
							Exportbartext="Check the status of inspection ";
							total=5.0;
							String current=wb.IsCurrentInspector(db.Insp_id,cf.selectedhomeid, "IsCurrentInspector");
							System.out.println(" the inspector"+current);
							if(wb.check_result(current))
							{
								System.out.println(" the inspector1");
								SoapObject add_property =wb.export_header("GetInspectionStatus");
								System.out.println(" the inspector2");
								add_property.addProperty("InspectorID",db.Insp_id);
								add_property.addProperty("SRID",cf.selectedhomeid);
								System.out.println("property"+add_property);
								String result=wb.export_footer(wb.envelope, add_property, "GetInspectionStatus");
								System.out.println("he status"+result);
								if(result.trim().equals("40")|| result.trim().equals("30")||result.trim().equals("41") || result.trim().equals("2") )
								{
									Current_insp=1;
									total=10.0;
									Exportbartext="Policy holder information";
									if(!function[0])
									{
										ex_status[0]=true;
										if(!policyholder())
										{
											ex_status[0]=false;
											Error_tracker+=" You have problem exporting policy holder information @";
										}
										
										
									}
									else
									{
										ex_status[0]=true;
										
									}
									total=15.0;
									Current_insp=2;
									if(!function[1])
									{
										Exportbartext="Inspection Findings information";
										ex_status[1]=true;
										if(!finding_inspection())
										{
											ex_status[1]=false;
											Error_tracker+=" You have problem exporting "+Exportbartext+" @";
										}
										
									}
									else
									{
										ex_status[1]=true;
										
									}
									
									total=20.0;
									Current_insp=3;
									if(!function[2])
									{
										Exportbartext="Treatment information";
										ex_status[2]=true;
										if(!treatment())
										{
											ex_status[2]=false;
											Error_tracker+=" You have problem exporting "+Exportbartext+" @";
										}
									}
									else
									{
										ex_status[2]=true;
										
									}
									
									total=25.0;
									Current_insp=4;
									if(!function[3])
									{
										Exportbartext="No Access Area information";
										
										ex_status[3]=true;
										if(!No_Access())
										{
											ex_status[3]=false;
											Error_tracker+=" You have problem exporting "+Exportbartext+" @";
										}
									}
									else
									{
										ex_status[3]=true;
										
									}
									total=35.0;
									Current_insp=5;
									if(!function[4])
									{
										Exportbartext="Comments and financial information";
										ex_status[4]=true;
										if(!comments_financial())
										{
											ex_status[4]=false;
											Error_tracker+=" You have problem exporting "+Exportbartext+" @";
										}
										
									}
									else
									{
										ex_status[4]=true;
											
									}
									total=40.0;
									Current_insp=6;
									
									System.out.println("function[5]"+function[5]);
									if(!function[5])
									{System.out.println("cameinside5");
										Exportbartext="QA_Question information";
										ex_status[5]=true;
										if(!QA_Question())
										{
											ex_status[5]=false;
											Error_tracker+=" You have problem exporting "+Exportbartext+" @";
										}
										
									}
									else
									{
										ex_status[5]=true;
										
									}
									
									total=50.0;
									Current_insp=7;
									try
									{
										Cursor c =db.SelectTablefunction(db.ImageTable, " WHERE IM_SRID='"+cf.selectedhomeid+"' and IM_InspectorId='"+db.Insp_id+"'");
									    if(c.getCount()>0)
									    {
									    	function[6] = false;
									    }
									    else
									    {
									    	function[6] = true;
									    }
									}
									catch (Exception e) {
										// TODO: handle exception
									}
									System.out.println("function[6"+function[6]);
									if(!function[6])
									{System.out.println("cameinside6");
										Exportbartext="Photos ";
										ex_status[6]=Expot_photos();
									}
									else
									{
										ex_status[6]=true;	
									}
									//Expot_photos();
									total=70.0;
									Current_insp=8;
									if(!function[7])
									{
										Exportbartext="Feed back ";
										ex_status[7]=Expot_feedback();
									}
									else
									{
										ex_status[7]=true;	
									}
									total=90.0;
									Current_insp=9;
									
									if(!function[8])
									{
										Exportbartext="Update Inspection Status";
										ex_status[8]=Update_Inspection_Status();
									}
									else
									{
										ex_status[8]=true;	
									}
									total=95.0;
									
									/***/
									for(int i =0;i<ex_status.length;i++)
									{
										System.out.println(" the status"+ex_status[i]);
									}
									if(ex_status[0] && ex_status[1] && ex_status[2] && ex_status[3] && ex_status[4] && ex_status[5] && ex_status[6] && ex_status[7] && ex_status[8])
									{
										change_status();
										
										total=100.0;
										/***/
										total=100.0;
										handler_verif=0;
										handler_export.sendEmptyMessage(0);
										Current_insp=10;
										
									}
									else
									{
										handler_verif=3;
										total=100.00;
										handler_export.sendEmptyMessage(0);
										Error_tracker+=" You have problem exporting "+Exportbartext+" @";
									}
									
//									total=100.0;
//									/***/
//									total=100.0;
//									handler_export.sendEmptyMessage(0);
//									Current_insp=10;
									
								}
								else
								{
									handler_verif=2;
									total=100.0;// Inspection not in the schedule or assign
									handler_export.sendEmptyMessage(0);
								}
								
							}
							else
							{
								handler_verif=1;
								total=100.0;// Inspection allocated to some other inspector
								handler_export.sendEmptyMessage(0);
							}
						}
						catch(SocketTimeoutException e)
						{
						handler_verif=3;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(SocketException e)
						{
						handler_verif=3;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(IOException e)
						{
						handler_verif=3;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(NetworkErrorException e)
						{
						handler_verif=3;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(TimeoutException e)
						{
						handler_verif=3;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(Exception e)
							{
							handler_verif=3;
							total=100.00;
							handler_export.sendEmptyMessage(0);
							Error_tracker+=" You have problem exporting "+Exportbartext+" @";
							}
						
						}
					}.start();
			 }
			 else
			 {
				 cf.show_toast("Internet connection is not available.", 1);
			 }
	}
	protected boolean change_status() {
		// TODO Auto-generated method stub
		
		db.wdo_db.execSQL("UPDATE "
				+ db.policyholder
				+ " SET PH_IsInspected=2,PH_IsUploaded=1,PH_Status='2' WHERE PH_SRID ='"
				+ cf.selectedhomeid
				+ "'");
		File f=new File(Environment.getExternalStorageDirectory()+"/WDO Inspection/imported_image/"+cf.selectedhomeid);
		if(f.exists())
		{
			if(f.isDirectory())
			{
			   String[] children = f.list();
		        for (int i = 0; i < children.length; i++) {
		        	new File(f, children[i]).delete();
		        }
			}
			f.delete();
		}
		/*System.out.println("UPDATE "
				+ db.policyholder
				+ " SET PH_IsInspected=2,PH_IsUploaded=1,PH_SubStatus=41 WHERE PH_SRID ='"
				+ cf.selectedhomeid
				+ "'");*/
		
		return true;
	}

	protected boolean finding_inspection()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportInspectionFindings");
		wb.envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		wb.envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.inspection_finding, " where IF_SRID='" + cf.selectedhomeid + "' and IF_InspectorId='"+ db.Insp_id + "'");
		Cursor general= db.SelectTablefunction(db.policyholder,  " WHERE PH_InspectorId='"+db.Insp_id+"' and PH_SRID='"+cf.selectedhomeid+"'");
		if(data.getCount()==1)
		{
		
			data.moveToFirst();
			general.moveToFirst();
		
			  request.addProperty("InspectorId", db.Insp_id);
		      request.addProperty("SRID", cf.selectedhomeid);
		      request.addProperty("FI_Visiblesignofwdo", db.decode(data.getString(data.getColumnIndex("FI_Visiblesignofwdo"))));
		      boolean livewdo=(db.decode(data.getString(data.getColumnIndex("FI_wdo_present"))).equals("true"))?true:false;
		      request.addProperty("LiveWDO_present", livewdo);
		      request.addProperty("LiveWDO_organismother", db.decode(data.getString(data.getColumnIndex("FI_Livewdo_other"))));
		      request.addProperty("Livewdo_organism", db.decode(data.getString(data.getColumnIndex("FI_Livewdo"))));
		      request.addProperty("WDO_Location", db.decode(data.getString(data.getColumnIndex("FI_Location"))));
		      boolean evidence=(db.decode(data.getString(data.getColumnIndex("FI_Evidence_present"))).equals("true"))?true:false;
		      request.addProperty("Evidence_present", evidence);
		      request.addProperty("Evidence_organismother", db.decode(data.getString(data.getColumnIndex("FI_Evidence_other"))));
		      request.addProperty("Evidence_organism", db.decode(data.getString(data.getColumnIndex("FI_Evidence"))));
		      request.addProperty("Evidence_Location", db.decode(data.getString(data.getColumnIndex("FI_Evidence_Location"))));
		      boolean damage=(db.decode(data.getString(data.getColumnIndex("FI_Damage_present"))).equals("true"))?true:false;
		      request.addProperty("Damage_present", damage);
		      request.addProperty("Damage_organismother", db.decode(data.getString(data.getColumnIndex("FI_Damage_other"))));
		      request.addProperty("Damage_organism", db.decode(data.getString(data.getColumnIndex("FI_Damage"))));
		      request.addProperty("Damage_Location", db.decode(data.getString(data.getColumnIndex("FI_Damage_Location"))));
		      
		      
		      String ReportReqby=db.decode(general.getString(general.getColumnIndex("R_Requestedby"))),
		    		 Structureonpdf=db.decode(general.getString(general.getColumnIndex("structure"))),
		    		 Reportssendto=db.decode(general.getString(general.getColumnIndex("R_Requestedto")));
		      if(ReportReqby.contains("Other"))
		      {
		    	  ReportReqby+="("+db.decode(general.getString(general.getColumnIndex("R_Requestedby_other")))+")";
		      }
		      if(Structureonpdf.contains("Other"))
		      {
		    	  Structureonpdf+="("+db.decode(general.getString(general.getColumnIndex("structure_other")))+")";
		      }
		      if(Reportssendto.contains("Other"))
		      {
		    	  Reportssendto+="("+db.decode(general.getString(general.getColumnIndex("R_Requestedto_other")))+")";
		      }
		      request.addProperty("ReportReqby",ReportReqby );
		      request.addProperty("Structureonpdf", Structureonpdf);
		      request.addProperty("Reportssendto", Reportssendto);
		      request.addProperty("Handout", general.getString(general.getColumnIndex("hand_out")));
		      
		      db.CreateTable(26);
		      Cursor c=db.SelectTablefunction(db.Addendum, " WHERE AD_D_SRID='"+cf.selectedhomeid+"'");
		      if(c.getCount()>0)
		      {
		    	  c.moveToFirst();
		    	  request.addProperty("Addendumcomments", db.decode(c.getString(c.getColumnIndex("AD_comments"))));
		      }
		      else
		      {
		    	  request.addProperty("Addendumcomments", "");
		      }
		      
	      	 wb.envelope.setOutputSoapObject(request);
			 System.out.println("request="+request);
			
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			try {
				androidHttpTransport.call(wb.NAMESPACE+"ExportInspectionFindings",wb.envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String result =  wb.envelope.getResponse().toString();
			System.out.println("result="+result);
			return wb.check_result(result);
		}
		if(data!=null)
			data.close();
		if(general!=null)
			general.close();
		return true;
		
		
		
	}
	
	protected boolean Update_Inspection_Status() throws SocketException,
			IOException, FileNotFoundException, NetworkErrorException,
			TimeoutException, SocketTimeoutException {

		SoapObject request = new SoapObject(wb.NAMESPACE, "SETINSPECTIONSTATUS");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		request.addProperty("InspectorID", db.Insp_id);
		request.addProperty("SRID", cf.selectedhomeid);
		envelope.setOutputSoapObject(request);
		System.out.println("request=" + request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		try {
			androidHttpTransport.call(wb.NAMESPACE + "SETINSPECTIONSTATUS",
					envelope);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String result = envelope.getResponse().toString();
		System.out.println("result=" + result);
		return wb.check_result(result);
	}
	
	protected boolean comments_financial()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportCommentsFinancial");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.CommentsandFinancials, " where CF_SRID='" + cf.selectedhomeid + "' and CF_InspectorId='"+ db.Insp_id + "'");
		if(data.getCount()==1)
		{
			data.moveToFirst();
			
			  request.addProperty("CF_InspectorId", db.Insp_id);
		      request.addProperty("CF_SRID", cf.selectedhomeid);
		      request.addProperty("CF_Commnents", db.decode(data.getString(data.getColumnIndex("CF_Commnents"))));
		      envelope.setOutputSoapObject(request);
		      System.out.println("request="+request);
		      HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		      try {
				androidHttpTransport.call(wb.NAMESPACE+"ExportCommentsFinancial",envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      String result =  envelope.getResponse().toString();
		      System.out.println("result="+result);
		      return wb.check_result(result);
		}
		if(data!=null)
			data.close();
		return true;
		
		
	}
	protected boolean QA_Question()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportQAquestion");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.QA_question, " where QA_SRID='" + cf.selectedhomeid + "' and QA_InspectorId='"+ db.Insp_id + "'");
		if(data.getCount()==1)
		{
			data.moveToFirst();
			   
			
			  request.addProperty("InspectorId", db.Insp_id);
		      request.addProperty("SRID", cf.selectedhomeid);
		      request.addProperty("kitchen", db.decode(data.getString(data.getColumnIndex("kitchen"))));
		      request.addProperty("living_R", db.decode(data.getString(data.getColumnIndex("living_R"))));
		      request.addProperty("Formal_R", db.decode(data.getString(data.getColumnIndex("Formal_R"))));
		      request.addProperty("Dining_R", db.decode(data.getString(data.getColumnIndex("Dining_R"))));
		      request.addProperty("Master_BR", db.decode(data.getString(data.getColumnIndex("Master_BR"))));
		      request.addProperty("Master_BaR", db.decode(data.getString(data.getColumnIndex("Master_BaR"))));
		      request.addProperty("Bedroom1", db.decode(data.getString(data.getColumnIndex("Bedroom1"))));
		      request.addProperty("Bedroom2", db.decode(data.getString(data.getColumnIndex("Bedroom2"))));
		      request.addProperty("Bedroom3", db.decode(data.getString(data.getColumnIndex("Bedroom3"))));
		      request.addProperty("Bedroom4", db.decode(data.getString(data.getColumnIndex("Bedroom4"))));
		      request.addProperty("Bedroom5", db.decode(data.getString(data.getColumnIndex("Bedroom5"))));
		      request.addProperty("Bedroom6", db.decode(data.getString(data.getColumnIndex("Bedroom6"))));
		      request.addProperty("Hallways", db.decode(data.getString(data.getColumnIndex("Hallways"))));
		      request.addProperty("Closets", db.decode(data.getString(data.getColumnIndex("Closets"))));
		      request.addProperty("Hall_BaR", db.decode(data.getString(data.getColumnIndex("Hall_BaR"))));
		      request.addProperty("Half_BaR", db.decode(data.getString(data.getColumnIndex("Half_BaR"))));
		      
		      request.addProperty("Florida_R", db.decode(data.getString(data.getColumnIndex("Florida_R"))));
		      request.addProperty("Garage", db.decode(data.getString(data.getColumnIndex("Garage"))));
		      request.addProperty("Storage", db.decode(data.getString(data.getColumnIndex("Storage"))));
		      request.addProperty("Outbuildings", db.decode(data.getString(data.getColumnIndex("Outbuildings"))));
		      request.addProperty("PoolHouse", db.decode(data.getString(data.getColumnIndex("PoolHouse"))));
		      envelope.setOutputSoapObject(request);
		      System.out.println("request="+request);
		      HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		      try {
				androidHttpTransport.call(wb.NAMESPACE+"ExportQAquestion",envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      String result =  envelope.getResponse().toString();
		      System.out.println("result="+result);
		      if(wb.check_result(result))
		      {
		    	  QA_questiondy();
		      }
		}
		if(data!=null)
			data.close();
		return true;
		
		
	}
	protected boolean QA_questiondy()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportQAquestionOther");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
	
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.QA_question_dynamic, " where QA_D_SRID='" + cf.selectedhomeid + "' and QA_D_InspectorId='"+ db.Insp_id + "'");
		if(data.getCount()>0)
		{
			data.moveToFirst();
			for(int i=0;i<data.getCount();i++,data.moveToNext())
			{

			  request.addProperty("InspectorId", db.Insp_id);
		      request.addProperty("SRID", cf.selectedhomeid);
		      request.addProperty("QA_D_title", db.decode(data.getString(data.getColumnIndex("QA_D_title"))));
		      request.addProperty("QA_D_option", db.decode(data.getString(data.getColumnIndex("QA_D_option"))));
		      request.addProperty("AndroidID", i+1);
		      	envelope.setOutputSoapObject(request);
				System.out.println("request="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportQAquestionOther",envelope);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();
				request = new SoapObject(wb.NAMESPACE,"ExportQAquestionOther");
				System.out.println("result="+result);
				wb.check_result(result);
			}
		      
	      	
			
		}
		if(data!=null)
			data.close();
		return true;
		
		
	}
	protected boolean No_Access()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException {
		
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportNoAccess");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.no_access, " where N_SRID='" + cf.selectedhomeid + "' and N_InspectorId='"+ db.Insp_id + "'");
		if(data.getCount()>0)
		{ data.moveToFirst();
			for(int i=0;i<data.getCount();i++,data.moveToNext())
			{
			  request.addProperty("InspectorId", db.Insp_id);
		      request.addProperty("SRID", cf.selectedhomeid);
		      request.addProperty("N_Area", db.decode(data.getString(data.getColumnIndex("N_Area"))));
		      request.addProperty("N_Area_other", db.decode(data.getString(data.getColumnIndex("N_Area_other"))));
		      request.addProperty("N_SArea", db.decode(data.getString(data.getColumnIndex("N_SArea"))));
		      request.addProperty("N_SArea_other", db.decode(data.getString(data.getColumnIndex("N_SArea_other"))));
		      request.addProperty("N_Reason", db.decode(data.getString(data.getColumnIndex("N_Reason"))));
		      request.addProperty("AndroidID", i+1);
		      
		      	envelope.setOutputSoapObject(request);
				System.out.println("request="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportNoAccess",envelope);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();
				request = new SoapObject(wb.NAMESPACE,"ExportNoAccess");
				
				System.out.println("result="+result);
				wb.check_result(result);
			}
		      
	      	
			
		}
		if(data!=null)
			data.close();
		return true;
		
		
	}
	protected boolean treatment()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException{
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportTreatment");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor data= db.SelectTablefunction(db.treatment, " where T_SRID='" + cf.selectedhomeid + "' and T_InspectorId='"+ db.Insp_id + "'");
		if(data.getCount()==1)
		{
			data.moveToFirst();
			  request.addProperty("InspectorId", db.Insp_id);
		      request.addProperty("SRID", cf.selectedhomeid);
		      request.addProperty("T_Enable", db.decode(data.getString(data.getColumnIndex("T_Enable"))));
		      request.addProperty("T_observed", db.decode(data.getString(data.getColumnIndex("T_observed"))));
		      request.addProperty("T_noticeofinspection", db.decode(data.getString(data.getColumnIndex("T_noticeofinspection"))));
		      request.addProperty("T_notice_other", db.decode(data.getString(data.getColumnIndex("T_notice_other"))));
		      request.addProperty("T_typet", db.decode(data.getString(data.getColumnIndex("T_typet"))));
		      request.addProperty("T_typet_other", db.decode(data.getString(data.getColumnIndex("T_typet_other"))));
		      request.addProperty("T_companyhastreated", db.decode(data.getString(data.getColumnIndex("T_companyhastreated"))));
		      request.addProperty("T_organism", db.decode(data.getString(data.getColumnIndex("T_organism"))));
		      request.addProperty("T_organism_other", db.decode(data.getString(data.getColumnIndex("T_organism_other"))));
		      request.addProperty("T_nameofpesticide", db.decode(data.getString(data.getColumnIndex("T_nameofpesticide"))));
		      request.addProperty("T_termsandcon", db.decode(data.getString(data.getColumnIndex("T_termsandcon"))));
		      request.addProperty("T_methodoftreat", db.decode(data.getString(data.getColumnIndex("T_methodoftreat"))));
		      request.addProperty("T_comments", db.decode(data.getString(data.getColumnIndex("T_comments"))));
		      request.addProperty("T_Treatlocation", db.decode(data.getString(data.getColumnIndex("T_Treatlocation"))));
		      
	      	envelope.setOutputSoapObject(request);
			System.out.println("request="+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			try {
				androidHttpTransport.call(wb.NAMESPACE+"ExportTreatment",envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String result =  envelope.getResponse().toString();
			System.out.println("result="+result);
			return wb.check_result(result);
		}
		if(data!=null)
			data.close();
		return true;
		
		
	}
	protected boolean policyholder()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException  {
		// TODO Auto-generated method stub
//System.out.println("comes correctly1");
		SoapObject request = new SoapObject(wb.NAMESPACE,"ExportData");
		
	//	System.out.println("comes correctly1");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		/** set the value get from the policy holder table starts **/
		Cursor C_policy= db.SelectTablefunction(db.policyholder, " where PH_SRID='" + cf.selectedhomeid + "' and PH_InspectorId='"+ db.Insp_id + "'");
		//System.out.println("comes correctly1"+C_policy.getCount());
		if(C_policy.getCount()==1)
		{
			
			String[]policyinformation=setvalueToArray(C_policy);/** set the value to the array for the easy usage **/
			System.out.println("comes correctly21");
			request.addProperty("InspectorID",policyinformation[1]);
			request.addProperty("SRID",policyinformation[2]);
			request.addProperty("OwnerFirstName",policyinformation[3]);
			request.addProperty("OwnerLastName",policyinformation[4]);
			request.addProperty("Address",policyinformation[5]);
			request.addProperty("Address2",policyinformation[6]);
			request.addProperty("Contactperson",policyinformation[34]);
			request.addProperty("City",policyinformation[7]);
			request.addProperty("Zip",policyinformation[8]);
			request.addProperty("State",policyinformation[9]);
			request.addProperty("County",policyinformation[10]);
			request.addProperty("HomePhone",policyinformation[14]);
			request.addProperty("WorkPhone",policyinformation[15]);
			request.addProperty("CellPhone",policyinformation[16]);
			request.addProperty("Email",policyinformation[19]);		
			request.addProperty("PolicyNumber",policyinformation[11]);
			request.addProperty("YearofConstruction",policyinformation[33]);
			request.addProperty("InsuranceCarrier",policyinformation[13]);
			request.addProperty("NStorie",policyinformation[17]);
			request.addProperty("Website",policyinformation[18]);
			
			Cursor c =db.SelectTablefunction(db.MailingPolicyHolder, " WHERE ML_PH_SRID='"+cf.selectedhomeid+"'");
			String address1="",address2="",city="",zip="",state="",county="";
			c.moveToFirst();
			if(c.getCount()>0)
			{
				address1=remove_na(c.getString(c.getColumnIndex("ML_PH_Address1")));
				address2=remove_na(c.getString(c.getColumnIndex("ML_PH_Address2")));
				city=remove_na(c.getString(c.getColumnIndex("ML_PH_City")));
				zip=remove_na(c.getString(c.getColumnIndex("ML_PH_Zip")));
				state=remove_na(c.getString(c.getColumnIndex("ML_PH_State")));
				county=remove_na(c.getString(c.getColumnIndex("ML_PH_County")));
				
			}
			request.addProperty("Mailingaddress1",address1);
			request.addProperty("Mailingaddress2",address2);
			request.addProperty("Mailingcity",city);
			request.addProperty("Mailingzip",zip);
			request.addProperty("Mailingstate",state);
			request.addProperty("Mailingcounty",county);
			if(c!=null)
				c.close();
			
					/** set the value get from the policy holder mailing  table Ends **/
			envelope.setOutputSoapObject(request);
			System.out.println("request="+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			try {
				androidHttpTransport.call(wb.NAMESPACE+"ExportData",envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String result =  envelope.getResponse().toString();
			System.out.println("result="+result);
			return wb.check_result(result);
		}
		/** set the value get from the policy holder table Ends **/
		
		if(C_policy!=null)
			C_policy.close();
		
		return true;
		
		
	}
	private String remove_na(String string) {
		// TODO Auto-generated method stub
		if(string!=null)
		{
			string=db.decode(string);
			if(!string.equals("N/A") && !string.equals("anytype{}"))
			{
				return string;
			}
		}
		return "";
	}

	private boolean Expot_photos() throws SocketException,NetworkErrorException, TimeoutException, IOException {
	System.gc();
	
	
	 Bitmap bitmap=null;
	String sql = " Select * from " + db.ImageTable + " Where IM_SRID='"	+ cf.selectedhomeid + "' ";
	Cursor C_photos = db.wdo_db.rawQuery(sql, null);
	C_photos.moveToFirst();
	String result_val = "";
	if (C_photos.getCount() >= 1) {
		double sub_count=(double)(20.00/C_photos.getCount());
		System.out.println("the value"+sub_count);
		C_photos.moveToFirst();
		String  path = "", name = "";
		boolean b = false;
		for (int i = 0; i < C_photos.getCount(); i++, C_photos.moveToNext()) {
		
			
			b = false;
			try {
				bitmap.recycle();// to release the memory
			} catch (Exception e) {
				// TODO: handle exception
			}
	
			
			path = db.decode(C_photos.getString(C_photos.getColumnIndex("IM_path")));
			name = path.substring(path.lastIndexOf("/") + 1);
			File f = new File(path);
			if (f.exists()) {
				SoapObject request = wb.export_header("ExportElevationImg");
				try {
					
					bitmap = cf.ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
															// 800, 800);
					System.out.println("comes correc1");
	
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					bitmap.compress(CompressFormat.PNG, 100, out);
					byte[] raw = out.toByteArray();
					request.addProperty("imgByte", raw);
				} catch (Exception e) {
					// TODO: handle exception
	
					Error_tracker += "You have problem in exporting Image in the Elevation= "+ cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]	+ " ,image order = "+ C_photos.getString(C_photos.getColumnIndex("ARR_IM_ImageOrder"))+ " Path=" + path + " @";
				}
	
			
				Exportbartext="Photos in "+cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]+" Image "+C_photos.getString(C_photos.getColumnIndex("IM_ImageOrder"));
				request.addProperty("SRID", cf.selectedhomeid);// "RI12348");
				request.addProperty("InspectorId", db.Insp_id);
				request.addProperty("IM_Elevation",cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]);
				request.addProperty("ElevationOther", "");
				request.addProperty("IM_path", path);
				request.addProperty("ImageNameWithExtension", name);
				request.addProperty("IM_Description", db.decode(C_photos.getString(C_photos.getColumnIndex("IM_Description"))));
				request.addProperty("IM_ImageOrder", C_photos.getString(C_photos.getColumnIndex("IM_ImageOrder")));
				request.addProperty("IM_CreatedOn", C_photos.getString(C_photos.getColumnIndex("IM_CreatedOn")));
				request.addProperty("AndroidID", i+1);
				//request.addProperty("ROWID", C_photos.getString(C_photos.getColumnIndex("IM_ID")));
				wb.envelope.setOutputSoapObject(request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(wb.envelope);
				wb.envelope.setOutputSoapObject(request);
				System.out.println("Photos=" + request);
				b = true;
	
				if (b) {
					HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
					try {
						androidHttpTransport.call(wb.NAMESPACE+ "ExportElevationImg", wb.envelope);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String val= wb.envelope.getResponse().toString();
					result_val +=val+ "~";
					if(val.equals("false"))
					{
						Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
					}
					System.out.println("the photos reult =" + result_val);
				} else {
					result_val += "false~";
				}
				// EX_CHKGCH[3]=
				// cf.SoapResponse("ExportGCHSummaryHazardsConcerns",cf.request);
	
			} else {
				Error_tracker += "Please check the following Image is available in"	+ ", Elevation ="+ cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]
						+ ", image order = "
						+ C_photos.getString(C_photos
								.getColumnIndex("IM_ImageOrder"))
						+ " Path=" + path + " @";
				// photos_missingfiles+=db.decode(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Description")))+"&#94;";
				result_val += "false~";
				// System.out.println("File missing");
			}
	
			
			// }
			total+=sub_count;
		}
		
		if(C_photos!=null)
			C_photos.close();
		if (result_val.contains("false"))
			return false;
		else
			return true;
	} else {
		return false;
	}

}
	private boolean Expot_feedback() throws SocketException,NetworkErrorException, TimeoutException, IOException{
		SoapObject request;
		Bitmap bitmap=null;
		String sql = " Select * from " + db.feedback_infomation
				+ " Where FD_SRID='" + cf.selectedhomeid+ "' ";
		Cursor C_feed_i = db.wdo_db.rawQuery(sql, null);
		C_feed_i.moveToFirst();
		String result_val = "";
		
		if (C_feed_i.getCount() >= 1) {
			
			C_feed_i.moveToFirst();
			for (int i = 0; i < C_feed_i.getCount(); i++, C_feed_i.moveToNext()) {
		
				
				request = wb.export_header("ExportFeedbackInformaion");
				request.addProperty("InspectorId", db.Insp_id);
				request.addProperty("SRID", cf.selectedhomeid);
				request.addProperty("FD_issatisfied",db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_issatisfied"))));
				request.addProperty("FD_CSC", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CSC"))));
				request.addProperty("FD_whowas",db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_whowas"))));
				request.addProperty("FD_whowas_other", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_whowas_other"))));
				request.addProperty("FD_comments", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_comments"))));
				request.addProperty("FD_CreatedOn", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CreatedOn"))));
				System.out.println("feed info request val=" + request);
				result_val += wb.export_footer(wb.envelope, request, "ExportFeedbackInformaion")+ "~";
				total=75.00;
				System.out.println("Feed info rsult" + result_val);
			}
			
			if (result_val.contains("false")) {
					Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
				return false;
			} else {
		
				
				
				/** Start export feedback document ***/
				String sql1 = " Select * from " + db.feedback_document
						+ " Where FD_D_SRID='" + cf.selectedhomeid
						+ "' ";
		
				Cursor C_feed_d = db.wdo_db.rawQuery(sql1, null);
				if(C_feed_d.getCount()>0)
				{
					double subcount=(double) 20.00/C_feed_d.getCount();
					C_feed_d.moveToFirst();
					String result_val_d = "";
					String[] array_doc = new String[12];
					array_doc[0] = "--Select--";
					array_doc[1] = "Acknowledgement Form";
					array_doc[2] = "CSE Form";
					array_doc[3] = "OIR 1802 Form";
					array_doc[4] = "Paper Signup Sheet";
					array_doc[5] = "Other Information";
					array_doc[6] = "Roof Permit ";
					array_doc[7] = "Sketch";
					array_doc[8] = "Building Permit";
					array_doc[9] = "Property Appraisal Information";
					array_doc[10] = "Field Inspection Report";
					array_doc[11] = "Field Notes";
					boolean com = false;
					int feed_inc = 50 / C_feed_d.getCount();
				
		
				for (int i = 0; i < C_feed_d.getCount(); i++, C_feed_d
						.moveToNext()) {
				
					com = false;
					try {
						bitmap.recycle();// to release the memory
					} catch (Exception e) {
						// TODO: handle exception
					}
					String tit = db.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_doctit")));
					Boolean titb = false;
					for (int n = 0; n < array_doc.length; n++) {
						if (array_doc[n].equals(tit)) {
							titb = true;
						}
					}
					String path = db.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_path")));
					String name = path.substring(path.lastIndexOf("/") + 1);
					Boolean b = (C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_type"))
							.equals("1")) ? true : false;
					
					Exportbartext="Feed back Document, Title("+tit+"), Document "+(i+1);
						request =wb.export_header("ExportFeedbackDocument");
					request.addProperty("SRID", cf.selectedhomeid);
					request.addProperty("InspectorId", db.Insp_id);
					
					if (titb) {
						request.addProperty("FD_D_doctit", tit);
						request.addProperty("FD_D_doctit_other",  (i+1));
					}
					
				      
					request.addProperty("ImageNameWithExtension", name);
					request.addProperty("FD_D_type", b);
					request.addProperty("ImageOrder",C_feed_d.getString(C_feed_d.getColumnIndex("FD_D_Id")));
					request.addProperty("Description","");
					//request.addProperty("FD_D_CreatedOn",  db.decode(C_feed_d.getString(C_feed_d.getColumnIndex("FD_D_CreatedOn"))));
					request.addProperty("AndroidID", i+1);
					System.out.println("propery feed "+request);
					/*request.addProperty("ROWID", db.decode(C_feed_d
							.getString(C_feed_d
									.getColumnIndex("FI_DocumentId"))));
		*/
					if (name.endsWith(".pdf")) {
		
						File dir = Environment.getExternalStorageDirectory();
						if (path.startsWith("file:///")) {
							path = path.substring(11);
						}
		
						File assist = new File(path);
						if (assist.exists()) {
							try {
								String mypath = path;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1]
										.lastIndexOf(".");
								String tests = temppath[temppath.length - 1]
										.substring(ss);
								String namedocument;
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length
										&& (numRead = fis.read(bytes, offset,
												bytes.length - offset)) >= 0) {
									offset += numRead;
								}
								Object strBase64 = Base64.encode(bytes);
								request.addProperty("imgByte", strBase64);
		
								System.out.println("Feed info rsult"
										+ result_val);
								com = true;
							} catch (Exception e) {
								// TODO: handle exception.
								Error_tracker += "You have problem uploadin Feed back information @";
							}
							if (com) {
								String val= wb.export_footer(wb.envelope, request, "ExportFeedbackDocument");
								result_val +=val+ "~";
								if(val.equals("false"))
								{
									Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
								}
							} else {
								result_val += "false~";
							}
		
						} else {
							Error_tracker += "Please check the following Image/Document is available "
									+  " Path=" + path + " @";
							 feed_missingfiles+=tit+"&#94;"; 
							result_val += "false~";
							 System.out.println("file missing"); 
						}
		
					} else {
						bitmap = null;
						MarshalBase64 marshal = new MarshalBase64();
						File f = new File(path);
						if (f.exists()) {
							/***
							 * Check the file size compress the based on the
							 * size
							 **/
		
							/***
							 * Check the file size compress the based on the
							 * size ends
							 **/
							try {
								bitmap = cf.ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																		// 800,
																		// 800);
								ByteArrayOutputStream out = null;
								out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();
		
								request.addProperty("imgByte", raw);
								marshal.register(wb.envelope);
								com = true;
							} catch (Exception e) {
								// TODO: handle exception
								Error_tracker += "You have problem uploadin Feed back information @";
							}
							if (com) {
								String val= wb.export_footer(wb.envelope, request, "ExportFeedbackDocument");
								result_val +=val+ "~";
								if(val.equals("false"))
								{
									Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
								}
							} else {
								result_val += "false";
							}
							System.out.println("Feed info rsult" + result_val);
						} else {
							// feedback_fileexits=true;
							Error_tracker += "Please check the following Image/Document is available Path=" + path + " @";
							result_val += "false~";
		
						}
		
					}
		
					// System.out.println("feed info request val="+request);
		
				total+=subcount;
				}
				}
				else
				{
					
				}
		
				/** export feedback document ends ***/
				// }
				if(C_feed_d!=null)
					C_feed_d.close();
			}
			if(C_feed_i!=null)
				C_feed_i.close();
			if (result_val.contains("false"))
				return false;
			else if (result_val.equals(""))
				return false;
			else
				return true;
		
		}
		
		return false;
}

	private String[] setvalueToArray(Cursor tbl) {
		// TODO Auto-generated method stub
		String[] arr=null;
		if(tbl!=null && tbl.getCount()==1)
		{
			tbl.moveToFirst();
			arr=new String[tbl.getColumnCount()];
			for(int i=0;i<tbl.getColumnCount();i++)
			{
				
				if(tbl.getString(i)!=null)
				{
				
				if(db.decode(tbl.getString(i))==null)
				{
				
					arr[i]="";	
				}
				else
				{
					
					
					arr[i]=db.decode(tbl.getString(i));
				
				}
				
				}
				else
				{
					arr[i]="";	
				}
				
			}
		}
			return arr;
		
	}

	/** PROGRESS DILOG WITH LODING DATA INFORMATION **/		
	@Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:
				
				
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(maxBarValue);
				progDialog.setMessage(Html.fromHtml("Please be patient as we create your inspection report. Please do not lock your screen as it may affect your export.<br> Current Status : "+Exportbartext+"."));
				progDialog.setCancelable(false);
				progThread = new ProgressThread(handler1);
				progThread.start();
				return progDialog;
			default:
				return null;
			}
		}
	 
		class ProgressThread extends Thread {

    
		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			
			while (mState == RUNNING) {
				try {
					
					// Control speed of update (but precision of delay not
					// guaranteed)
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					//Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", (int)total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			mState = state;
		}

	}
		/** Upload policy holder information Starts **/
		final Handler handler1 = new Handler() {

			public void handleMessage(Message msg) {
				// Get the current value of the variable total from the message data
				// and update the progress bar.
				int total = msg.getData().getInt("total");
				progDialog.setProgress(total);
				progDialog.setMessage(Html.fromHtml("Please be patient as we create your inspection report. Please do not lock your screen as it may affect your export.<br> Current Status : "+Exportbartext+"."));
				//Exportbartext="";
				if (total == 100) {
					progDialog.setCancelable(true);
					dismissDialog(typeBar);
					progThread.setState(ProgressThread.DONE);
					

				}

			}
		};
		final Handler handler_export = new Handler() {

			public void handleMessage(Message msg) {
				
				total=100.0;
				if(handler_verif==1)
				{
					cf.show_toast("Sorry inspection re-allocate to some other inspector", 0);
				}
				else if(handler_verif==2)
					{
						cf.show_toast("You cannot export this inspection please check the satus in www.paperlessinspectors.com", 0);
					}
				
				else if(handler_verif==4)
				{
					cf.show_toast("Sorry you have problem in exporting data ",0);
				}
				else if(handler_verif==3)
				{
					re_export();
				}
				/*else if(handler_verif==8)
				{
					cf.show_toast("Please schedule your inspection and then export it",0);
				}*/
				else if(!ex_status[0]||!ex_status[1]||!ex_status[2]||!ex_status[3]||!ex_status[4]||!ex_status[5]||!ex_status[6]||!ex_status[7])
				{
					re_export();
				}
				else
				{
					cf.show_toast("You have exported successfully",0);
					progDialog.dismiss();
					Declaration();
					
					final Dialog dialog1 = new Dialog(Export.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					((TextView) dialog1.findViewById(R.id.alert_title)).setText("Exported successfully");
					((TextView) dialog1.findViewById(R.id.alert_txt)).setText("Your inspection report was successfully created. Please click on the reports ready status to view your report. You can also email your report direct from this application. ");
					dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
					dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);
						LinearLayout error_list=(LinearLayout) dialog1.findViewById(R.id.li_Erro_list);
						error_list.setVisibility(View.VISIBLE);
						dialog1.findViewById(R.id.tv_errorlist).setVisibility(View.GONE);
					
					Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
					Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
					ImageView close = (ImageView) dialog1.findViewById(R.id.alert_close);
					btn_yes.setText("Ok");
					btn_cancel.setText("Cancel");
					btn_yes.setOnClickListener(new OnClickListener()
					{
		           	@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
		           		Intent in = new Intent(Export.this,View_pdf.class);
		           		in.putExtra("current", 1);
		           		in.putExtra("classidentifier", "export");
						startActivity(in);
						}
						
					});
					btn_cancel.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							
						}
						
					});
					close.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							
						}
						
					});
					dialog1.setCancelable(false);
					dialog1.show();
				}
				if(wl!=null)
				{
				wl.release();
				wl=null;
				}
				System.out.println("export erro "+Error_tracker);
				
			
			}
		};
		protected void re_export() {
			// TODO Auto-generated method stub
			 final Dialog dialog1 = new Dialog(Export.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alert);
				((TextView) dialog1.findViewById(R.id.alert_title)).setText("Error in Export");
				((TextView) dialog1.findViewById(R.id.alert_txt)).setText("You have problem in your exporting function.");
				dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
				dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);
				if(Error_tracker.contains("@"))
				{
					LinearLayout error_list=(LinearLayout) dialog1.findViewById(R.id.li_Erro_list);
					dialog1.findViewById(R.id.tv_errorlist).setVisibility(View.VISIBLE);
					error_list.setVisibility(View.VISIBLE);
					String error[]=Error_tracker.split("@");
					for(int i=0;i<error.length;i++)
					{
						TextView tv=new TextView(this,null,R.attr.textview_fw);
						tv.setText((i+1)+error[i]);
						tv.setPadding(0, 5, 0, 5);
						tv.setTextColor(getResources().getColor(R.color.red));
						error_list.addView(tv);
						
					}
				}
				else
				{
					LinearLayout error_list=(LinearLayout) dialog1.findViewById(R.id.li_Erro_list);
					error_list.setVisibility(View.VISIBLE);
					dialog1.findViewById(R.id.tv_errorlist).setVisibility(View.GONE);
				}
				Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
				Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
				ImageView close = (ImageView) dialog1.findViewById(R.id.alert_close);
				btn_yes.setText("Re-Export");
				btn_cancel.setText("Cancel");
				btn_yes.setOnClickListener(new OnClickListener()
				{
	           	@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
	           		dialog1.dismiss();
	           		
					startexpot(cf.selectedhomeid,ex_status);
					
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				close.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				dialog1.setCancelable(false);
				dialog1.show();
		}

		}
