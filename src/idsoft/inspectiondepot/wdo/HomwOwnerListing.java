package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomwOwnerListing extends Activity {
	CommonFunction cf;
	String status = "", Where = "";
	TextView type_insp_txt, status_txt, no_of_records;
	EditText search_ed;
	LinearLayout list_layout;
	DataBaseHelper db;
	private int no_of_record = 0;
	private int current = 1;
	int rec_per_page = 10;
	int numbeofpage = 1;
	private int start;
	private int end;
	String inspection_type="WDO Re-Inspection";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homw_owner_list);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		Bundle b = getIntent().getExtras();
		cf.getDeviceDimensions();
		LayoutParams lp = ((LinearLayout) findViewById(R.id.content))
				.getLayoutParams();
		lp.width = cf.wd - 20;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);

		if (b != null) {

			status = b.getString("status");
			cf.selected_insp_type = b.getString("type_id");
			if (status == null || cf.selected_insp_type == null) {

				Intent in = new Intent(this, Dashboard.class);
				startActivity(in);

			} else {
				Declaration();
			}
		} else {

			Intent in = new Intent(this, Dashboard.class);
			startActivity(in);
		}
		cf.setTouchListener(findViewById(R.id.content));
		// findViewById(R.id.head_policy_info).setVisibility(View.INVISIBLE);
		// findViewById(R.id.head_take_image).setVisibility(View.INVISIBLE);

		/* findViewById(R.id.head_insp_info).setVisibility(View.INVISIBLE); */
		((ImageView) findViewById(R.id.head_insp_info))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent insp_info = new Intent(HomwOwnerListing.this,
								PolicyholdeInfoHead.class);
						// insp_info.putExtra("homeid", cf.selectedhomeid);
						insp_info.putExtra("Type", "Inspector");
						insp_info.putExtra("insp_id", db.Insp_id);
						startActivityForResult(insp_info, 77);
					}
				});

	}

	private void Declaration() {
		// TODO Auto-generated method stub
		type_insp_txt = (TextView) findViewById(R.id.list_insp_type);
		status_txt = (TextView) findViewById(R.id.list_insp_status);
		no_of_records = (TextView) findViewById(R.id.list_insp_noofrecords);
		search_ed = (EditText) findViewById(R.id.list_insp_search_ed);
		list_layout = (LinearLayout) findViewById(R.id.list_insp_inspeclist);

		/*
		 * if ("30".equals(status)) { ((TextView) findViewById(R.id.list_note))
		 * .
		 * setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP "
		 * ); status_txt.setText("Awaiting Scheduling"); } else if
		 * ("1".equals(status)) { ((TextView) findViewById(R.id.list_note))
		 * .setText
		 * ("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP ");
		 * status_txt.setText("Inspected"); } else if ("110".equals(status)) {
		 * ((TextView) findViewById(R.id.list_note))
		 * .setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP "
		 * ); status_txt.setText("UTS"); }
		 */if ("40".equals(status)) {
			((TextView) findViewById(R.id.list_note))
					.setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | INSPECTION START TIME  - END TIME ");
			status_txt.setText("Scheduled");
		} else	if("30".equals(status)) {
			((TextView) findViewById(R.id.list_note))
					.setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP | ASSIGN DATE ");
			status_txt.setText("Awaiting Schedule");
		}
		 else	if("1".equals(status)) {
		((TextView) findViewById(R.id.list_note))
				.setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP ");
		status_txt.setText("Inspected");
	}
		 else {
				((TextView) findViewById(R.id.list_note))
						.setText("NAME | WORK ORDER NO | ADDRESS | CITY | STATE | COUNTY | ZIP ");
				status_txt.setText("UTS");
			}
		if (!status.equals("ALL")) {
			if (cf.type_id_carr.equals(cf.selected_insp_type)) {
				type_insp_txt.setText("WDO carr.order.");

			} else {
				type_insp_txt.setText("WDO Re-Inspection");

			}
		} else {
			status_txt.setVisibility(View.GONE);
			((TextView) findViewById(R.id.dummy1)).setText("Delete Inspection");
			findViewById(R.id.list_insp_type).setVisibility(View.INVISIBLE);
			findViewById(R.id.dummy2).setVisibility(View.INVISIBLE);
			findViewById(R.id.dummy3).setVisibility(View.GONE);
			findViewById(R.id.dummy4).setVisibility(View.GONE);
		}
		db.CreateTable(2);
		db.CreateTable(3);
		start = 1;
		end = rec_per_page * current;
		Add_list_layout("");

	}

	private void Add_list_layout(String search) {
		// TODO Auto-generated method stub
		if (status.equals("110")) {
			Where = " Where PH_InspectorId='" + db.Insp_id
					+ "' and PH_Status='" + status
					+ "' ";
			if (!search.equals("")) {
				Where += " AND (PH_FirstName like '%" + db.encode(search)
						+ "%' or PH_LastName like '%" + db.encode(search)
						+ "%' or PH_Policyno  like '%" + db.encode(search)
						+ "%')";
			}
		} else if (status.equals("1")) {
			Where = " Where PH_InspectorId='" + db.Insp_id
					+ "' and PH_Status='" + status + "'";
			if (!search.equals("")) {
				Where += " AND (PH_FirstName like '%" + db.encode(search)
						+ "%' or PH_LastName like '%" + db.encode(search)
						+ "%' or PH_Policyno  like '%" + db.encode(search)
						+ "%')";
			}

		} else if (!status.equals("ALL")) {
			Where = " Where PH_InspectorId='" + db.Insp_id
					+ "' and PH_Status='" + status
					+ "' and (PH_SubStatus='' or PH_SubStatus='0') ";
			if (!search.equals("")) {
				Where += " AND (PH_FirstName like '%" + db.encode(search)
						+ "%' or PH_LastName like '%" + db.encode(search)
						+ "%' or PH_Policyno  like '%" + db.encode(search)
						+ "%')";
			}
		} else {
			Where = " Where PH_InspectorId='" + db.Insp_id + "' ";
			if (!search.equals("")) {
				Where += " AND (PH_FirstName like '%" + db.encode(search)
						+ "%' or PH_LastName like '%" + db.encode(search)
						+ "%' or PH_Policyno  like '%" + db.encode(search)
						+ "%')";
			}
		}

		if (status.equals("30")) {
			Where += "  order by Schedule_AssignedDate desc";
		} else if (status.equals("40")) {
			Where += "  order by Schedule_ScheduledDate desc";
		} else if (status.equals("1")) {
			Where += "  order by Schedule_ScheduledDate desc";
		} else if (status.equals("110")) {
			Where += "  order by Schedule_AssignedDate desc";
		}

		// Cursor c = db.SelectTablefunction(db.policyholder, Where);
		Cursor c = db.wdo_db.rawQuery(" SELECT (SELECT count() from "
				+ db.policyholder + " " + db.policyholder + " " + Where
				+ " ) as total,* FROM " + db.policyholder + " " + Where
				+ " LIMIT " + (start - 1) + "," + rec_per_page, null);

		list_layout.removeAllViews();
		/*
		 * System.out.println(" WHERE " + c.getCount() + "/n" + db.policyholder
		 * + " " + Where);
		 */
		if (c.getCount() > 0) {
			c.moveToFirst();
			no_of_record = c.getInt(c.getColumnIndex("total"));
			no_of_records.setText("Displayed Records/Total Records : "
					+ c.getCount() + "/" + no_of_record);
			findViewById(R.id.no_record).setVisibility(View.GONE);
			((TextView) findViewById(R.id.list_note))
					.setVisibility(View.VISIBLE);
			for (int i = 0; i < c.getCount(); i++, c.moveToNext()) {
				String srid, f_name, l_name, po_no, address, city, state, county, zipcode, starttime, endtime, insp_date;
				srid = db.decode(c.getString(c.getColumnIndex("PH_SRID")));
				f_name = db
						.decode(c.getString(c.getColumnIndex("PH_FirstName")));
				l_name = db
						.decode(c.getString(c.getColumnIndex("PH_LastName")));
				po_no = db.decode(c.getString(c.getColumnIndex("PH_Policyno")));
				po_no=(po_no.equals(""))? "N/A":po_no;
				address = db
						.decode(c.getString(c.getColumnIndex("PH_Address1")));
				city = db.decode(c.getString(c.getColumnIndex("PH_City")));
				state = db.decode(c.getString(c.getColumnIndex("PH_State")));
				county = db.decode(c.getString(c.getColumnIndex("PH_County")));
				zipcode = db.decode(c.getString(c.getColumnIndex("PH_Zip")));
				starttime = db.decode(c.getString(c
						.getColumnIndex("Schedule_InspectionStartTime")));
				endtime = db.decode(c.getString(c
						.getColumnIndex("Schedule_InspectionEndTime")));
				insp_date = db.decode(c.getString(c
						.getColumnIndex("Schedule_ScheduledDate")));
				String assign=db.decode(c.getString(c
						.getColumnIndex("Schedule_AssignedDate")));
				LinearLayout[] rl = new LinearLayout[c.getCount()];
				Button[] bt = new Button[c.getCount()];
				TextView[] tv = new TextView[c.getCount()];
				rl[i] = new LinearLayout(this);
				bt[i] = new Button(this, null, R.attr.button);
				tv[i] = new TextView(this, null, R.attr.textview_fw);
				if (status.equals("40")) {
					tv[i].setText(f_name + " " + l_name + " | " + po_no + " | "
							+ address + " | " + city + " | " + state + " | "
							+ county + " | " + zipcode + " | " + insp_date
							+ " | " + starttime + " | " + endtime);

				}
				else if(status.equals("30"))  {
					tv[i].setText(f_name + " " + l_name + " | " + po_no + " | "
							+ address + " | " + city + " | " + state + " | "
							+ county + " | " + zipcode+" | "+assign);
				}
				else {
					tv[i].setText(f_name + " " + l_name + " | " + po_no + " | "
							+ address + " | " + city + " | " + state + " | "
							+ county + " | " + zipcode);
				}
				bt[i].setTag("1");
				bt[i].setText("Delete");
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						650, ViewGroup.LayoutParams.WRAP_CONTENT);
				lp.setMargins(20, 30, 20, 30);
				rl[i].addView(tv[i], lp);
				tv[i].setClickable(true);

				rl[i].addView(bt[i]);
				/*** Align the text view and button ***/
				LinearLayout.LayoutParams rl_lp = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				rl_lp.setMargins(110, 0, 20, 0);
				rl_lp.gravity = Gravity.CENTER_VERTICAL;

				bt[i].setLayoutParams(rl_lp);
				LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				lp2.setMargins(0, 2, 0, 0);
				list_layout.addView(rl[i], lp2);
				// View v =new View(this);
				// v.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
				// list_layout.addView(v,LayoutParams.FILL_PARENT,2);

				bt[i].setOnClickListener(new delte_clicker(srid));

				System.out.println("status is " + status);

				if (!status.equals("ALL")) {
					tv[i].setOnClickListener(new tv_clicker(srid));
				}

				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#6E81A1"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#64789A"));
				}
				
			}
			if (c != null)
				c.close();
		} else {
			findViewById(R.id.no_record).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.list_note)).setVisibility(View.GONE);
			no_of_records.setVisibility(View.INVISIBLE);
		}
		/*** Align the text view and button ends ***/
		/*** pagination starts ***/
		if (no_of_record > rec_per_page) {
			((LinearLayout) findViewById(R.id.page_li))
					.setVisibility(View.VISIBLE);

			numbeofpage = ((no_of_record % rec_per_page) != 0) ? (no_of_record / rec_per_page) + 1
					: (no_of_record / rec_per_page);
			((TextView) findViewById(R.id.page_txt_no_fo_page))
					.setText("Current page/Total page:" + current + "/"
							+ numbeofpage);
			if (current == 1) {
				((Button) findViewById(R.id.page_prev))
						.setVisibility(View.INVISIBLE);
			} else {
				((Button) findViewById(R.id.page_prev))
						.setVisibility(View.VISIBLE);
			}
			if (current == numbeofpage) {
				((Button) findViewById(R.id.page_next))
						.setVisibility(View.INVISIBLE);
			} else {
				((Button) findViewById(R.id.page_next))
						.setVisibility(View.VISIBLE);
			}

		} else {
			((LinearLayout) findViewById(R.id.page_li))
					.setVisibility(View.INVISIBLE);
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.hme:
			cf.go_home();
			break;
		case R.id.list_insp_search_bt:
			if (search_ed.getText().toString().trim().equals("")) {
				cf.show_toast("Please enter search text.", 0);
			} else {
				Add_list_layout(search_ed.getText().toString().trim());
			}
			break;
		case R.id.list_insp_clear_bt:
			search_ed.setText("");
			Add_list_layout("");
			break;
		case R.id.list_insp_deleteall_bt:
			final Dialog dialog1 = new Dialog(HomwOwnerListing.this,
					android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			((TextView) dialog1.findViewById(R.id.alert_title))
					.setText("Confirmation");
			((TextView) dialog1.findViewById(R.id.alert_txt))
					.setText("Do you want to delete all the inspection?");
			dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
			dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);

			Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
			ImageView close = (ImageView) dialog1
					.findViewById(R.id.alert_close);
			btn_yes.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					String search=search_ed.getText().toString().trim();
					String Where="";
					if (status.equals("110")) {
						Where = " Where PH_InspectorId='" + db.Insp_id
								+ "' and PH_Status='" + status
								+ "' and (PH_SubStatus='110' or PH_SubStatus='111') ";
						if (!search.equals("")) {
							Where += " AND (PH_FirstName like '%" + db.encode(search)
									+ "%' or PH_LastName like '%" + db.encode(search)
									+ "%' or PH_Policyno  like '%" + db.encode(search)
									+ "%')";
						}
					} else if (status.equals("1")) {
						Where = " Where PH_InspectorId='" + db.Insp_id
								+ "' and PH_Status='" + status + "'";
						if (!search.equals("")) {
							Where += " AND (PH_FirstName like '%" + db.encode(search)
									+ "%' or PH_LastName like '%" + db.encode(search)
									+ "%' or PH_Policyno  like '%" + db.encode(search)
									+ "%')";
						}

					} else if (!status.equals("ALL")) {
						Where = " Where PH_InspectorId='" + db.Insp_id
								+ "' and PH_Status='" + status
								+ "' and (PH_SubStatus='' or PH_SubStatus='0') ";
						if (!search.equals("")) {
							Where += " AND (PH_FirstName like '%" + db.encode(search)
									+ "%' or PH_LastName like '%" + db.encode(search)
									+ "%' or PH_Policyno  like '%" + db.encode(search)
									+ "%')";
						}
					} else {
						Where = " Where PH_InspectorId='" + db.Insp_id + "' ";
						if (!search.equals("")) {
							Where += " AND (PH_FirstName like '%" + db.encode(search)
									+ "%' or PH_LastName like '%" + db.encode(search)
									+ "%' or PH_Policyno  like '%" + db.encode(search)
									+ "%')";
						}
					}
					db.wdo_db.execSQL("Delete FROM " + db.policyholder + " "
							+ Where);
					// db.wdo_db.execSQL("Delete FROM "+db.MailingPolicyHolder+" WHERE ML_PH_InspectorId='"+db.Insp_id+"'");
					Intent in = new Intent(HomwOwnerListing.this,
							HomeScreen.class);
					startActivity(in);

				}

			});
			btn_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();

				}

			});
			close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();

				}

			});
			dialog1.setCancelable(false);
			dialog1.show();
			break;
		case R.id.page_next:

			current++;
			start = ((rec_per_page * (current - 1)) + 1);
			end = (rec_per_page * current);
			Add_list_layout(search_ed.getText().toString().trim());

			break;
		case R.id.page_prev:
			current--;
			start = ((rec_per_page * (current - 1)) + 1);
			end = (rec_per_page * current);
			Add_list_layout(search_ed.getText().toString().trim());

			break;
		case R.id.page_goto:
			String go = ((EditText) findViewById(R.id.page_ed_go_to)).getText()
					.toString();
			if (!go.equals("")) {
				if (Integer.parseInt(go) <= numbeofpage) {
					if (Integer.parseInt(go) != current) {
						current = Integer.parseInt(go);
						start = ((rec_per_page * (current - 1)) + 1);
						end = (rec_per_page * current);
						Add_list_layout(search_ed.getText().toString().trim());

					} else {
						cf.show_toast(
								"Request page and current page are same ", 0);
					}
				} else {
					cf.show_toast("Invalid page no", 0);
				}
			} else {
				cf.show_toast("Invalid page no", 0);
			}

			break;
		default:
			// cf.show_toast("Button under construction", 1);
			break;
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (status.equals("ALL")) {
				Intent in = new Intent(this, HomeScreen.class);
				startActivity(in);
			} else {
				Intent in = new Intent(this, Dashboard.class);
				startActivity(in);
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	class delte_clicker implements OnClickListener {
		String srid;

		delte_clicker(String i) {
			srid = i;
			// cf.show_toast("under constr", 1);
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final Dialog dialog1 = new Dialog(HomwOwnerListing.this,
					android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			((TextView) dialog1.findViewById(R.id.alert_title))
					.setText("Confirmation");
			((TextView) dialog1.findViewById(R.id.alert_txt))
					.setText("Do you want to delete the selected inspection?");
			dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
			dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);

			Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
			ImageView close = (ImageView) dialog1
					.findViewById(R.id.alert_close);
			btn_yes.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					db.wdo_db.execSQL("Delete FROM " + db.policyholder
							+ " WHERE PH_SRID='" + srid + "'");
					db.wdo_db.execSQL("Delete FROM " + db.MailingPolicyHolder
							+ " WHERE ML_PH_SRID='" + srid + "'");
					File f = new File(Environment.getExternalStorageDirectory()
							+ "/WDO Inspection/imported_image/"
							+ cf.selectedhomeid);
					if (f.exists()) {
						if (f.isDirectory()) {
							String[] children = f.list();
							for (int i = 0; i < children.length; i++) {
								new File(f, children[i]).delete();
							}
						}
						f.delete();
					}
					Add_list_layout("");
				}

			});
			btn_cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();

				}

			});
			close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();

				}

			});
			dialog1.setCancelable(false);
			dialog1.show();
		}

	}

	class tv_clicker implements OnClickListener {
		String srid;

		tv_clicker(String srid) {
			this.srid = srid;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if ("30".equals(status)) {
				Intent in = new Intent(HomwOwnerListing.this, policyholderInformation.class);
				in.putExtra("SRID", srid);
				startActivity(in);
			} else if ("40".equals(status) || "1".equals(status)) {
				Intent in = new Intent(HomwOwnerListing.this,
						policyholderInformation.class);
				in.putExtra("SRID", srid);
				startActivity(in);
			}

		}

	}

}
