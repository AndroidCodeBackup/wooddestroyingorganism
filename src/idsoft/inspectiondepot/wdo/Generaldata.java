package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Generaldata extends Activity {
CommonFunction cf;
Validation va;
DataBaseHelper db;
TextView tv[]=new TextView[11];
ImageView im_lic;
String s[]={"Company Name","Business License No","Address","Phone Number","City","State","Zipcode"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		
		va= new Validation(cf);
		db=new DataBaseHelper(this);
		setContentView(R.layout.general_data);
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Inspection Company Information ",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,7,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		cf.setTouchListener(findViewById(R.id.content));
		declaration();
	
		db.CreateTable(6);
	
		Cursor c =db.SelectTablefunction(db.Companyinformation, " WHERE Ins_Id='"+db.Insp_id+"'");
		if(c.getCount()>0)
		{
			showsave_data(c);
		}
		if(c!=null)
			c.close();
	}
	private void showsave_data(Cursor c) {
		// TODO Auto-generated method stub
		c.moveToFirst();
		
		tv[0].setText(set_empty(c.getString(c.getColumnIndex("Company_name"))));
		tv[1].setText(set_empty(c.getString(c.getColumnIndex("Company_address"))));
		tv[2].setText(set_empty(c.getString(c.getColumnIndex("Company_city"))));
		tv[3].setText(set_empty(c.getString(c.getColumnIndex("Company_state"))));
		tv[4].setText(set_empty(c.getString(c.getColumnIndex("Company_county"))));
		tv[5].setText(set_empty(c.getString(c.getColumnIndex("Company_zipcode"))));
		tv[6].setText(set_empty(c.getString(c.getColumnIndex("business_lic_no"))));
		tv[7].setText(set_empty(c.getString(c.getColumnIndex("PH_no"))));
		tv[8].setText(set_empty(c.getString(c.getColumnIndex("Issue_date"))));
		tv[9].setText(set_empty(c.getString(c.getColumnIndex("Expiry_date"))));
		tv[10].setText(set_empty(c.getString(c.getColumnIndex("comments"))));
		Bitmap bm=cf.ShrinkBitmap(getFilesDir()+"/"+db.decode(c.getString(c.getColumnIndex("License_path"))), 300, 300);
		im_lic.setImageBitmap(bm);
		
		
	

	}
	
	private String set_empty(String string) {
		// TODO Auto-generated method stub
		
		if(string!=null)
		{
			string=db.decode(string);
		if(!string.equals("") && !string.equals("()-") && !string.equals("anytype{}"))
		{
			return string; 
		}
		}
		return "N/A";
	}
	private void declaration() {
		// TODO Auto-generated method stub
		tv[0]=(TextView) findViewById(R.id.com_name);
		tv[1]=(TextView) findViewById(R.id.com_add);
		tv[2]=(TextView) findViewById(R.id.com_city);
		tv[3]=(TextView) findViewById(R.id.com_state);
		tv[4]=(TextView) findViewById(R.id.comp_county);
		tv[5]=(TextView) findViewById(R.id.com_zip);
		tv[6]=(TextView) findViewById(R.id.com_lic_no);
		tv[7]=(TextView) findViewById(R.id.com_ph);
		tv[8]=(TextView) findViewById(R.id.com_lic_iss_date);
		tv[9]=(TextView) findViewById(R.id.com_lic_exp_date);
		tv[10]=(TextView) findViewById(R.id.com_comments);
		im_lic=(ImageView) findViewById(R.id.com_lic_img);
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.go_home();	
		break;
		/*case R.id.save:
			//cf.go_();
			validation_check();
		//	cf.show_toast("Tab under construction", 1);
		break;*/
		default:
			break;
		}
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goback(12);
			cf.go_back(Schedule.class);
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
