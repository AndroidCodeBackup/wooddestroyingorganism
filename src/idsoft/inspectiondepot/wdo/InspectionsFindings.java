package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.MultiSpinner;
import idsoft.inspectiondepot.wdo.supportclass.Option_list;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class InspectionsFindings extends Activity {
	CommonFunction cf;
	Validation va;
	DataBaseHelper db;
	String live_tremites[]={"-Select-","Subterranean temites","Powder Post Beetles","Wood Boring Beetles","Dry wood termites","Wood Decaying Fungus","Other"};
	ArrayAdapter ad;
	RadioGroup Rg1;
	RadioButton rd[]=new RadioButton[2];
	RadioGroup[] Rg=new RadioGroup[3];
	
	EditText ed[]=new EditText[3];
	EditText ed_other[]=new EditText[3];
	
	MultiSpinner organism[]=new MultiSpinner[3];
	TextView org_tv[]=new TextView[3];
	Option_list op[]=new Option_list[3];
	
	private boolean load_comment=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		db=new DataBaseHelper(this);
		va=new Validation(cf);
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		setContentView(R.layout.finding_inspection);
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Inspection Information=>Inspection Findings",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,2,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		declaration();
		show_savedvalues();
		cf.setTouchListener(findViewById(R.id.content));
	}
	private void show_savedvalues() {
		// TODO Auto-generated method stub
		db.CreateTable(7);
		String organism_v[]=new String[3];
		Cursor c=db.SelectTablefunction(db.inspection_finding, " WHERE IF_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			
			if(db.decode(c.getString(c.getColumnIndex("FI_Visiblesignofwdo"))).trim().equals("true"))
			{
				rd[1].setChecked(true);
				organism_v[0]=db.decode(c.getString(c.getColumnIndex("FI_Livewdo")));
				organism_v[1]=db.decode(c.getString(c.getColumnIndex("FI_Evidence")));
				organism_v[2]=db.decode(c.getString(c.getColumnIndex("FI_Damage")));
				op[0] = new Option_list(this, organism[0], 22, 1,ed_other[0],org_tv[0],organism_v[0]);
				op[1] = new Option_list(this, organism[1], 22, 1,ed_other[1],org_tv[1],organism_v[1]);
				op[2] = new Option_list(this, organism[2], 22, 1,ed_other[2],org_tv[2],organism_v[2]);
				
				ed_other[0].setText(db.decode(c.getString(c.getColumnIndex("FI_Livewdo_other"))).trim());
				ed_other[1].setText(db.decode(c.getString(c.getColumnIndex("FI_Evidence_other"))).trim());
				ed_other[2].setText(db.decode(c.getString(c.getColumnIndex("FI_Damage_other"))).trim());
				ed[0].setText(db.decode(c.getString(c.getColumnIndex("FI_Location"))).trim());
				ed[1].setText(db.decode(c.getString(c.getColumnIndex("FI_Evidence_Location"))).trim());
				ed[2].setText(db.decode(c.getString(c.getColumnIndex("FI_Damage_Location"))).trim());
				if(db.decode(c.getString(c.getColumnIndex("FI_wdo_present"))).trim().equals("true"))
				{
					((RadioButton)Rg[0].findViewWithTag("Yes")).setChecked(true);
				}
				else if(db.decode(c.getString(c.getColumnIndex("FI_wdo_present"))).trim().equals("false"))
				{
					((RadioButton)Rg[0].findViewWithTag("No")).setChecked(true);
				}
				if(db.decode(c.getString(c.getColumnIndex("FI_Evidence_present"))).trim().equals("true"))
				{
					((RadioButton)Rg[1].findViewWithTag("Yes")).setChecked(true);
				}
				else if(db.decode(c.getString(c.getColumnIndex("FI_Evidence_present"))).trim().equals("false"))
				{
					((RadioButton)Rg[1].findViewWithTag("No")).setChecked(true);
				}
				if(db.decode(c.getString(c.getColumnIndex("FI_Damage_present"))).trim().equals("true"))
				{
					((RadioButton)Rg[2].findViewWithTag("Yes")).setChecked(true);
				}
				else if(db.decode(c.getString(c.getColumnIndex("FI_Damage_present"))).trim().equals("false"))
				{
					((RadioButton)Rg[2].findViewWithTag("No")).setChecked(true);
				}
				//sp_wdo.setSelection(ad.getPosition(db.decode(c.getString(c.getColumnIndex("FI_Livewdo"))).trim()));
				
				
				findViewById(R.id.findings_tbl).setVisibility(View.VISIBLE);
				
			}
			else
			{
				rd[0].setChecked(true);
				organism_v[0]=organism_v[1]=organism_v[2]="";
				op[0] = new Option_list(this, organism[0], 22, 1,ed_other[0],org_tv[0],organism_v[0]);
				op[1] = new Option_list(this, organism[1], 22, 1,ed_other[1],org_tv[1],organism_v[1]);
				op[2] = new Option_list(this, organism[2], 22, 1,ed_other[2],org_tv[2],organism_v[2]);
			}
		}
		else
		{
			organism_v[0]=organism_v[1]=organism_v[2]="";
			op[0] = new Option_list(this, organism[0], 22, 1,ed_other[0],org_tv[0],organism_v[0]);
			op[1] = new Option_list(this, organism[1], 22, 1,ed_other[1],org_tv[1],organism_v[1]);
			op[2] = new Option_list(this, organism[2], 22, 1,ed_other[2],org_tv[2],organism_v[2]);
		}
		if(c!=null)
			c.close();
		
	}
	private void declaration() {
		// TODO Auto-generated method stub
		Rg1=(RadioGroup) findViewById(R.id.findings_evidentgroup);
		Rg[0]=(RadioGroup) findViewById(R.id.findings_wdo_RG);
		Rg[1]=(RadioGroup) findViewById(R.id.findings_evidence_RG);
		Rg[2]=(RadioGroup) findViewById(R.id.findings_damage_RG);
		
		rd[0]=(RadioButton) findViewById(R.id.findings_no_visible);
		rd[1]=(RadioButton) findViewById(R.id.findings_visible);
		
		ed[0]=(EditText) findViewById(R.id.findings_wdo_location);
		ed[1]=(EditText) findViewById(R.id.findings_evidence_location);
		ed[2]=(EditText) findViewById(R.id.findings_damage_location);
		
		ed_other[0]=(EditText) findViewById(R.id.findings_wdo_MS_other);
		ed_other[1]=(EditText) findViewById(R.id.findings_evidence_MS_Other);
		ed_other[2]=(EditText) findViewById(R.id.findings_damage_MS_other);
		
		
		organism[0]=(MultiSpinner) findViewById(R.id.findings_wdo_MS);
		organism[1]=(MultiSpinner) findViewById(R.id.findings_evidence_MS);
		organism[2]=(MultiSpinner) findViewById(R.id.findings_damage_MS);
		org_tv[0]=(TextView) findViewById(R.id.findings_wdo_MS_TV);
		org_tv[1]=(TextView) findViewById(R.id.findings_evidence_MS_TV);
		org_tv[2]=(TextView) findViewById(R.id.findings_damage_MS_TV);
		
		rd[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(rd[1].isChecked())
				{
					findViewById(R.id.findings_tbl).setVisibility(View.VISIBLE);
					
				}
			}
		});
		rd[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(rd[0].isChecked() )
				{
					if(findViewById(R.id.findings_tbl).getVisibility()==View.VISIBLE && Rg[0].getCheckedRadioButtonId()!=-1 && Rg[1].getCheckedRadioButtonId()!=-1 && Rg[2].getCheckedRadioButtonId()!=-1 )
					{
						AlertDialog.Builder b =new AlertDialog.Builder(InspectionsFindings.this);
						b.setTitle("Confirmation");
						b.setMessage("Do you want to clear the data?");
						b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								try
								{
									clear_visible();
								}catch (Exception e) {
									// TODO: handle exception
									System.out.println("the exeption in delete"+e.getMessage());
								}
								
							
								
							}
						});
						b.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								rd[0].setChecked(false);
								rd[1].setChecked(true);
							}
						});   
						AlertDialog al=b.create();
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show();  
					}
					else
					{
						clear_visible();
					}
					//findViewById(R.id.findings_tbl).setVisibility(View.GONE);
				}
				
			}

			
		});
		
		Rg[0].setOnCheckedChangeListener(new rd_change(0, ((LinearLayout) findViewById(R.id.findings_wdo_sub))));
		Rg[1].setOnCheckedChangeListener(new rd_change(1, ((LinearLayout) findViewById(R.id.findings_evidence_sub))));
		Rg[2].setOnCheckedChangeListener(new rd_change(2, ((LinearLayout) findViewById(R.id.findings_damage_sub))));
		
		
		ed[0].addTextChangedListener(new TextWatchLimit(ed[0], 150,((TextView) findViewById(R.id.findings_wdo_limit)),cf.addendm_txt));
        ed[1].addTextChangedListener(new TextWatchLimit(ed[1], 150,((TextView) findViewById(R.id.findings_evidence_limit)),cf.addendm_txt));
        ed[2].addTextChangedListener(new TextWatchLimit(ed[2], 300,((TextView) findViewById(R.id.findings_damage_limit)),cf.addendm_txt));
	}
	class rd_change implements OnCheckedChangeListener
	{
		int i;
		LinearLayout li;
		
		rd_change(int i,LinearLayout li)
		{
			this.i=i;
			this.li=li;
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if(((RadioButton)group.findViewWithTag("Yes")).isChecked())
			{
				li.setVisibility(View.VISIBLE);
			}
			else
			{
				li.setVisibility(View.GONE);
				ed[i].setText("");
				op[i].clear_selectionCHK();
				
			}
		}
	}
	public void clear_visible() {
		// TODO Auto-generated method stub
		op[0].clear_selectionCHK();
		op[1].clear_selectionCHK();
		op[2].clear_selectionCHK();
		ed[0].setText("");
		ed[1].setText("");
		ed[2].setText("");
		ed_other[0].setText("");
		ed_other[1].setText("");
		ed_other[2].setText("");
		Rg[0].clearCheck();
		Rg[1].clearCheck();
		Rg[2].clearCheck();
		
		findViewById(R.id.findings_tbl).setVisibility(View.GONE);
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.clr:
				Rg1.clearCheck();
				clear_visible();
				
			break;
			case R.id.save:
				if(check_validation())
				{
					if(save_Values())
					{
						cf.show_toast(" Saved successfully", 0);
						cf.go_back(Treatment.class);
					}
				}
				
			break;
			case R.id.load_comments_wdo:
				int len=ed[0].getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments_wdo);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 3);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
				break;
			case R.id.load_comments_evidence:
				int len2=ed[1].getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments_evidence);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 3);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len2);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
				break;
			case R.id.load_comments_damage:
				int len1=ed[2].getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments_damage);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 3);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len1);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
				break;
			case R.id.help_evidence:
			cf.show_help("Evidence of WDO(s) (dead wood- destroying insects, insect parts, frass, shelter tubes, exit holes or other evidence)");
			break;
			case R.id.help_damage:
				cf.show_help("Damage caused by WDO(s) was observed and noted.");
			break;
		}
	}
	
	private boolean save_Values() {
		// TODO Auto-generated method stub
		String visiblesign,wdo_pre,livewdo,wdo_other,wdo_location,
		eve_pre,eve,eve_other,eve_location,
		dam_pre,dam,dam_other,dam_location;
		wdo_pre=(Rg[0].getCheckedRadioButtonId()==-1)? "":((RadioButton)Rg[0].findViewWithTag("Yes")).isChecked()+"";
		eve_pre=(Rg[1].getCheckedRadioButtonId()==-1)? "":((RadioButton)Rg[1].findViewWithTag("Yes")).isChecked()+"";
		dam_pre=(Rg[2].getCheckedRadioButtonId()==-1)? "":((RadioButton)Rg[2].findViewWithTag("Yes")).isChecked()+"";
		if(rd[0].isChecked())
			visiblesign="false";
		else
			visiblesign="true";
		livewdo=db.encode(op[0].get_selected());
		eve=db.encode(op[1].get_selected());
		dam=db.encode(op[2].get_selected());
		
		wdo_other=db.encode(ed_other[0].getText().toString().trim());
		eve_other=db.encode(ed_other[1].getText().toString().trim());
		dam_other=db.encode(ed_other[2].getText().toString().trim());
		
		wdo_location=db.encode(ed[0].getText().toString().trim());
		eve_location=db.encode(ed[1].getText().toString().trim());
		dam_location=db.encode(ed[2].getText().toString().trim());
		System.out.println("comes correctly");
		try
		{
			
		Cursor c=db.SelectTablefunction(db.inspection_finding, " WHERE IF_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			/*(IF_Id INTEGER PRIMARY KEY AUTOINCREMENT,IF_InspectorId varchar(50) NOT NULL,IF_SRID varchar(50) NOT NULL,FI_Visiblesignofwdo varchar(5),FI_wdo_present varchar(5),FI_Livewdo varchar(50),FI_Livewdo_other varchar(100),FI_Location varchar(100)," +
					",FI_Evidence_present varchar(5),FI_Evidence varchar(50),FI_Evidence_other varchar(100),FI_Evidence_Location varchar(100)," +
					",FI_Damage_present varchar(5),FI_Damage varchar(50),FI_Damage_other varchar(100),FI_Damage_Location varchar(100));");*/
			
			db.wdo_db.execSQL(" UPDATE "+db.inspection_finding+" SET FI_Visiblesignofwdo='"+visiblesign+"',FI_wdo_present='"+wdo_pre+"',FI_Livewdo='"+livewdo+"',FI_Location='"+wdo_location+"',FI_Livewdo_other='"+wdo_other+"'" +
					",FI_Evidence_present='"+eve_pre+"',FI_Evidence='"+eve+"',FI_Evidence_Location='"+eve_location+"',FI_Evidence_other='"+eve_other+"'" +
					",FI_Damage_present='"+dam_pre+"',FI_Damage='"+dam+"',FI_Damage_Location='"+dam_location+"',FI_Damage_other='"+dam_other+"' " +
					"WHERE IF_SRID='"+cf.selectedhomeid+"'");
		}
		else
		{
			db.wdo_db.execSQL(" INSERT INTO "+db.inspection_finding+" (IF_SRID,IF_InspectorId,FI_Visiblesignofwdo,FI_wdo_present,FI_Livewdo,FI_Location,FI_Livewdo_other,FI_Evidence_present,FI_Evidence,FI_Evidence_Location,FI_Evidence_other,FI_Damage_present,FI_Damage,FI_Damage_Location,FI_Damage_other) VALUES " +
					"('"+cf.selectedhomeid+"','"+db.Insp_id+"','"+visiblesign+"','"+wdo_pre+"','"+livewdo+"','"+wdo_location+"','"+wdo_other+"','"+eve_pre+"','"+eve+"','"+eve_location+"','"+eve_other+"','"+dam_pre+"','"+dam+"','"+dam_location+"','"+dam_other+"')");
		}
		if(c!=null)
			c.close();
		}
		catch (Exception e) {
		System.out.println(" comes error "+e.getMessage());
			// TODO: handle exception
		}
		return true;
	}
	private boolean check_validation() {
		// TODO Auto-generated method stub
		if(va.validate(rd, "Visible sign/Visible evidence of WDO"))
		{
			if(rd[1].isChecked())
			{
				if(live_wdo_validata())
				{
					if(evidence_validata()){
						if(damage_validata()){
							return true;
						}	
					}
				}
			}
			else
			{
				return true;	
			}
		}
		
		return false;
	}
	
	private boolean live_wdo_validata() {
		// TODO Auto-generated method stub
		if(va.validate(Rg[0], "Live Wdo(s)"))
		{
			if(((RadioButton)Rg[0].findViewWithTag("Yes")).isChecked())
			{
				if(va.validate(op[0], "Live Wdo(s) Organism", "You have selected"))
				{
					if(va.validate_other(ed_other[0], "Live Wdo(s) Organism", "Other"))
					{
						if(va.validate(ed[0], "Live WDO(s) Location"))
						{
							return true;
						}
					}
				}
			}
			else
			{
				return true;
			}
			
		}
		return false;
	}
	private boolean damage_validata() {
		// TODO Auto-generated method stub
		if(va.validate(Rg[2], "DAMAGE caused by WDO(s)"))
		{
			if(((RadioButton)Rg[2].findViewWithTag("Yes")).isChecked())
			{
				if(va.validate(op[2], "DAMAGE caused by WDO(s) Organism", "You have selected"))
				{
					if(va.validate_other(ed_other[2], "DAMAGE caused by WDO(s) Organism", "Other"))
					{
						if(va.validate(ed[2], "DAMAGE caused by WDO(s) Location"))
						{
						 return true;	
						}
					}
				}
			}
			else
			{
				return true;
			}
	}
		return false;
	}
	private boolean evidence_validata() {
		// TODO Auto-generated method stub
		if(va.validate(Rg[1], "EVIDENCE of WDO(s)"))
		{
			if(((RadioButton)Rg[1].findViewWithTag("Yes")).isChecked())
			{
				if(va.validate(op[1], "EVIDENCE of WDO(s) Organism", "You have selected"))
				{
					if(va.validate_other(ed_other[1], "EVIDENCE of WDO(s) Organism", "Other"))
					{
						if(va.validate(ed[1], "EVIDENCE of WDO(s) Location"))
						{
						 return true;	
						}
					}
				}
			}
			else 
			{
				return true;
			}
	}
		return false;
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
						if(data.getExtras().getInt("id")==R.id.load_comments_wdo)
							ed[0].setText((ed[0].getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						else if(data.getExtras().getInt("id")==R.id.load_comments_evidence)
							ed[1].setText((ed[1].getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						else if(data.getExtras().getInt("id")==R.id.load_comments_damage)
							ed[2].setText((ed[2].getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						
					}
					
					/*else if(resultCode==RESULT_CANCELED)
					{
						cf.show_toast("You have canceled  the comments selction ",0);
					}*/
			}		 		
	 		

	 	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		//cf.goback(12);
		cf.go_back(ScopeofInspection.class);
		
		return true;
	}
	return super.onKeyDown(keyCode, event);
}
	
}
