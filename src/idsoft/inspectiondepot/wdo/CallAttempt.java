package idsoft.inspectiondepot.wdo;


import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;



import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;
import idsoft.inspectiondepot.wdo.supportclass.phone_nowatcher;
  
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.DownloadListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class CallAttempt extends Activity implements Runnable {
CommonFunction cf;
DataBaseHelper db;
Webservice_Function wb;
Spinner UTS,IR,result_call,title;
Validation va;
RadioButton phone_no_rd[]= new RadioButton[7],call_info_rd[]=new RadioButton[2];
TextView phone_no_tv[]= new TextView[7],ACI_tv[]=new  TextView[5],txtnolist;
EditText called_no,call_info_ed[]=new EditText[9];
TableLayout dy_val;
String tit[]={"--Select--","Policyholder","Agent","Agency CSR","Property Manager","Family Member","Realtor","Other"};
private boolean load_comment=true;
private int verify;
SoapObject	res ;
String assigndata="";
Thread t;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunction(this);
		wb=new Webservice_Function(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
			cf.selected_insp_type=b.getString("SRID");
		}
		db= new DataBaseHelper(this);
		setContentView(R.layout.call_attempt);
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Call Attempt",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,4,cf));
		declaration();
		cf.setTouchListener(findViewById(R.id.content));
		Setvalues();
		va=new Validation(cf);
		if(wb.isInternetOn())
		{
			 String source = "<font color=#FFFFFF>Downloading Call Attempt..."	+ "</font>";
			 cf.pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
			 t =new Thread(this);
			 t.start();
			 
		}
		else
		{
			cf.show_toast("Please enable internet connection then try downloading call attempt", 0);
		}
		db.getpolicyholderstatus(cf.selectedhomeid);
	}
	
	private void Setvalues() {
		// TODO Auto-generated method stub
		db.CreateTable(2);
		db.CreateTable(3);
		db.CreateTable(4);
		db.CreateTable(5);
		Cursor c=db.wdo_db.rawQuery(" SELECT  PH_Status,Schedule_AssignedDate,PH_HomePhone,PH_WorkPhone,PH_CellPhone,AI_AgentOffPhone,AI_AgentContactPhone,AD_PhoneNumber,AD_MobileNumber,AD_BestTimetoCallYou,AD_Bestdaycall,AD_FirstChoice,AD_SecondChoice,AD_ThirdChoice FROM "+db.policyholder+" LEFT JOIN "+db.Agent_tabble+" ON AI_SRID='"+cf.selectedhomeid+"'  LEFT JOIN "+db.Additional_table+" ON AD_SRID='"+cf.selectedhomeid+"' WHERE PH_SRID ='"+cf.selectedhomeid+"'", null);
		if(c.getCount()>0)
		{
			c.moveToFirst();
			phone_no_tv[0].setText(db.decode(c.getString(c.getColumnIndex("PH_HomePhone"))));
			phone_no_tv[1].setText(db.decode(c.getString(c.getColumnIndex("PH_WorkPhone"))));
			phone_no_tv[2].setText(db.decode(c.getString(c.getColumnIndex("PH_CellPhone"))));
			phone_no_tv[3].setText(db.decode(c.getString(c.getColumnIndex("AI_AgentOffPhone"))));
			phone_no_tv[4].setText(db.decode(c.getString(c.getColumnIndex("AI_AgentContactPhone"))));
			phone_no_tv[5].setText(db.decode(c.getString(c.getColumnIndex("AD_MobileNumber"))));
			phone_no_tv[6].setText(db.decode(c.getString(c.getColumnIndex("AD_PhoneNumber"))));
			ACI_tv[0].setText(db.convert_null(db.decode(c.getString(c.getColumnIndex("AD_Bestdaycall")))));
			ACI_tv[1].setText(db.convert_null(db.decode(c.getString(c.getColumnIndex("AD_BestTimetoCallYou")))));
			ACI_tv[2].setText(db.convert_null(db.decode(c.getString(c.getColumnIndex("AD_FirstChoice")))));
			ACI_tv[3].setText(db.convert_null(db.decode(c.getString(c.getColumnIndex("AD_SecondChoice")))));
			ACI_tv[4].setText(db.convert_null(db.decode(c.getString(c.getColumnIndex("AD_ThirdChoice")))));
			assigndata=db.convert_null(c.getString(c.getColumnIndex("Schedule_AssignedDate")));
			if(!db.decode(c.getString(c.getColumnIndex("PH_Status"))).equals("40") && !db.decode(c.getString(c.getColumnIndex("PH_Status"))).equals("30"))
			{
				//findViewById(R.id.submit).setEnabled(false);
				findViewById(R.id.cancl).setVisibility(View.GONE);
			}
			
		}
		if(c!=null)
			c.close();
	}

	private void declaration() {
		// TODO Auto-generated method stub
		phone_no_rd[0]=(RadioButton) findViewById(R.id.H_ph);
		phone_no_rd[1]=(RadioButton) findViewById(R.id.W_ph);
		phone_no_rd[2]=(RadioButton) findViewById(R.id.C_ph);
		phone_no_rd[3]=(RadioButton) findViewById(R.id.O_ph);
		phone_no_rd[4]=(RadioButton) findViewById(R.id.Con_ph);
		phone_no_rd[5]=(RadioButton) findViewById(R.id.m_ph);
		phone_no_rd[6]=(RadioButton) findViewById(R.id.P_ph);
		
		phone_no_tv[0]=(TextView) findViewById(R.id.Call_H_ph);
		phone_no_tv[1]=(TextView) findViewById(R.id.Call_W_ph);
		phone_no_tv[2]=(TextView) findViewById(R.id.Call_C_ph);
		phone_no_tv[3]=(TextView) findViewById(R.id.Call_A_ph);
		phone_no_tv[4]=(TextView) findViewById(R.id.Call_A_C_ph);
		phone_no_tv[5]=(TextView) findViewById(R.id.ACI_mbl_no);
		phone_no_tv[6]=(TextView) findViewById(R.id.ACI_ofc_no);
		called_no=(EditText) findViewById(R.id.numbercalled);
		result_call = (Spinner) findViewById(R.id.spresult);
		ACI_tv[0]=(TextView) findViewById(R.id.ACI_BDC);
		ACI_tv[1]=(TextView) findViewById(R.id.ACI_BTC);
		ACI_tv[2]=(TextView) findViewById(R.id.ACI_first);
		ACI_tv[3]=(TextView) findViewById(R.id.ACI_second);
		ACI_tv[4]=(TextView) findViewById(R.id.ACI_third);
		
		call_info_ed[0]=(EditText) findViewById(R.id.ed1);
		call_info_ed[1]=(EditText) findViewById(R.id.calldate);
		call_info_ed[2]=(EditText) findViewById(R.id.callday);
		call_info_ed[3]=(EditText) findViewById(R.id.calltime);
		call_info_ed[4]=called_no;
		call_info_ed[5]=(EditText) findViewById(R.id.resul_other);
		call_info_ed[6]=(EditText) findViewById(R.id.personanswered);
		call_info_ed[7]=(EditText) findViewById(R.id.othertitle);
		call_info_ed[8]=(EditText) findViewById(R.id.initialcomment);
		call_info_ed[0].addTextChangedListener(new TextWatchLimit(call_info_ed[0], 500, ((TextView) findViewById(R.id.gencomm_tv_type1))));
		call_info_ed[8].addTextChangedListener(new TextWatchLimit(call_info_ed[8], 500, ((TextView) findViewById(R.id.initialcomm_txt))));
		call_info_ed[4].addTextChangedListener(new phone_nowatcher(call_info_ed[4]));
		
		title=(Spinner) findViewById(R.id.sptitle);
		
		call_info_rd[0]=(RadioButton) findViewById(R.id.chk1);
		call_info_rd[1]=(RadioButton) findViewById(R.id.chk2);
		txtnolist=(TextView) findViewById(R.id.nolist);
		dy_val=(TableLayout) findViewById(R.id.dynamic_callAttempt);
		
		ArrayAdapter loResult = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loResult.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		loResult.add("--Select--");
		loResult.add("Answered");
		loResult.add("No Answer");
		loResult.add("Left Voicemail");
		loResult.add("Number Disconnected");
		loResult.add("Answered & Disconnected");
		loResult.add("Re Order Left voice mail");
		loResult.add("Other");
		result_call.setAdapter(loResult);
		UTS = (Spinner) findViewById(R.id.spunableschedule);
		ArrayAdapter loUnableschedule = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loUnableschedule
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		UTS.setAdapter(loUnableschedule);
		UTS.setEnabled(false);
		loUnableschedule.add("--Select--");
		loUnableschedule.add("Bad Contact Information");
		loUnableschedule.add("No Contact with Policyholder");
		loUnableschedule.add("Scheduled then Cancelled");
		loUnableschedule.add("Seasonal Resident");
		loUnableschedule.add("Policyholder on Military Deployment");
		loUnableschedule.add("Not Insured by Carrier Anymore");
		loUnableschedule.add("Other");
		
		IR = (Spinner) findViewById(R.id.spinspectionrefused);
		ArrayAdapter loInspectionrefused = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loInspectionrefused
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		IR.setAdapter(loInspectionrefused);
		IR.setEnabled(false);
		loInspectionrefused.add("--Select--");
		loInspectionrefused.add("Out of Coverage Area");
		loInspectionrefused.add("Unable to meet Cycle Times");
		loInspectionrefused.add("Conflict � Did Original Inspection");
		loInspectionrefused.add("Other");
		result_call.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				if(result_call.getSelectedItem().toString().trim().equals("Answered") ||result_call.getSelectedItem().toString().trim().equals("Re Order Left voice mail"))
				{
					
					 findViewById(R.id.txtpersonanswered).setVisibility(View.VISIBLE);
					 findViewById(R.id.txttitle).setVisibility(View.VISIBLE);
					 call_info_ed[6].setTag("required");
					 title.setTag("required");
					 call_info_ed[5].setText("");
					 call_info_ed[5].setVisibility(View.GONE);
				}
				else if(result_call.getSelectedItem().toString().trim().equals("Other"))
				{
					 call_info_ed[5].setVisibility(View.VISIBLE);
					findViewById(R.id.txtpersonanswered).setVisibility(View.GONE);
					 findViewById(R.id.txttitle).setVisibility(View.GONE);
					 call_info_ed[6].setTag("");
					 title.setTag("");
					 title.setSelection(0);
				}
				else
				{
					 call_info_ed[5].setText("");
					 call_info_ed[5].setVisibility(View.GONE);
					 findViewById(R.id.txtpersonanswered).setVisibility(View.GONE);
					 findViewById(R.id.txttitle).setVisibility(View.GONE);
					 call_info_ed[6].setTag("");
					 title.setTag("");
					 title.setSelection(0);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		ArrayAdapter tit_ad=  new ArrayAdapter(this,android.R.layout.simple_spinner_item, tit);
        tit_ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        title.setAdapter(tit_ad);
        title.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(title.getSelectedItem().toString().equals("Other"))
				{
					call_info_ed[7].setTag("required");
					call_info_ed[7].setVisibility(View.VISIBLE);
				}
				else
				{
					call_info_ed[7].setTag("");
					call_info_ed[7].setText("");
					call_info_ed[7].setVisibility(View.GONE);
				}
			
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        call_info_rd[0].setOnCheckedChangeListener(new checked_change());
		call_info_rd[1].setOnCheckedChangeListener(new checked_change());
		((TextView) findViewById(R.id.note)).setText(Html.fromHtml("<font color='#DE9618'><b>Note</b> :</font>INTERNET CONNECTION REQUIRED TO SUBMIT" +
				" CALL ATTEMPTS. PLEASE RECORD ANY PERTINANT COMMENTS RELATING TO YOUR CONVERSATIONS OR SCHEDULNG INFORMATION. " +
				"IF HOMEOWNER TAKING TIME OFF WORK FOR ACCESS TO HOME PLEASE MAKE SURE THIS IS RECORDED."));
		 
	}
	class checked_change implements OnCheckedChangeListener
	{

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			System.out.println("comes first");
			if(isChecked)
			{
				
				if(buttonView.getId()==call_info_rd[0].getId())
				{
					UTS.setEnabled(true);
					((LinearLayout) findViewById(R.id.gencmt)).setVisibility(View.VISIBLE);
					call_info_ed[0].setTag("required");
					UTS.setTag("required");
					
				}
				else
				{
					call_info_rd[0].setChecked(false);
					UTS.setEnabled(false);
					UTS.setTag("");
					UTS.setSelection(0);
				}
				if(buttonView.getId()==call_info_rd[1].getId())
				{
					IR.setEnabled(true);
					((LinearLayout) findViewById(R.id.gencmt)).setVisibility(View.VISIBLE);
					call_info_ed[0].setTag("required");
					IR.setTag("required");
					
					
				}
				else
				{
					call_info_rd[1].setChecked(false);
					IR.setEnabled(false);
					IR.setTag("");
					IR.setSelection(0);
				}
			}
			
		}
		
	}

	public void clicker(View v)
	{
		Intent in;
		switch (v.getId()) {
		
		case R.id.submit:
			if(!db.status.equals("1") && !db.substatus.equals("1") )
			{
			if(validation_submit())
			{
				upload_call_attempt(R.id.submit);
				//cf.show_toast("Saved successfully", 0);
			}
			}
			else
			{
				cf.show_toast("Sorry already inspected record cannot make the callattempt", 0);
			}
		break;
		case R.id.cancl:
			if(!db.status.equals("1") && !db.substatus.equals("1") )
			{
				if(validation_cancel())
				{
					upload_call_attempt(R.id.cancl);
					//cf.show_toast("Saved successfully", 0);
				}
			}
			else
			{
				cf.show_toast("Sorry already inspected record cannot make the callattempt", 0);
			}
			
		break;
		case R.id.clear:
			clear_all();
			
		break;
		case R.id.seldate:
			call_info_ed[1].setText("");
			call_info_ed[2].setText("");
			cf.getCalender();
			Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        DatePickerDialog dialog= new DatePickerDialog(this, new mDateSetListener(call_info_ed[1]),
	                   mYear, mMonth, mDay);
	        dialog.show();
			
		break;
		case R.id.currentdate:
			cf.getCalender();
			int mMonth1 = cf.mMonth+1;
			call_info_ed[1].setText("");
			call_info_ed[2].setText("");
			call_info_ed[1].setText(new StringBuilder()
			// Month is 0 based so add 1
			.append(cf.pad(mMonth1)).append("/").append(cf.pad(cf.mDay)).append("/")
			.append(cf.mYear).append(" "));
			Set_Day(cf.mDayofWeek);
		break;
		case R.id.currenttime:
			cf.getCalender();
			if (cf.amorpm == 0) {
				call_info_ed[3].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.hours).append(":").append(cf.minutes)
						.append(" ").append("AM").append(" "));
			}
			if (cf.amorpm == 1) {
				call_info_ed[3].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.hours).append(":").append(cf.minutes)
						.append(" ").append("PM").append(" "));
			}
		break;
		case R.id.hme:
			cf.go_home();
			break;
		case R.id.c_edit_H_ph:
			 in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[0].getId());
			in.putExtra("number", phone_no_tv[0].getText());
			in.putExtra("field_name", "PH_HomePhone");
			in.putExtra("table", db.policyholder);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
		break;
		case R.id.c_edit_W_ph:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[1].getId());
			in.putExtra("number", phone_no_tv[1].getText());
			in.putExtra("field_name", "PH_WorkPhone");
			in.putExtra("table", db.policyholder);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
		break;
		case R.id.c_edit_C_ph:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[2].getId());
			in.putExtra("number", phone_no_tv[2].getText());
			in.putExtra("field_name", "PH_CellPhone");
			in.putExtra("table", db.policyholder);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
		break;
		case R.id.c_edit_A_ph:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[3].getId());
			in.putExtra("number", phone_no_tv[3].getText());
			in.putExtra("field_name", "AI_AgentOffPhone");
			in.putExtra("table", db.Agent_tabble);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
		break;
		case R.id.c_edit_A_C_ph:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[4].getId());
			in.putExtra("number", phone_no_tv[4].getText());
			in.putExtra("field_name", "AI_AgentContactPhone");
			in.putExtra("table", db.Agent_tabble);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
		break;
		case R.id.ACI_mbl_edit:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[5].getId());
			in.putExtra("number", phone_no_tv[5].getText());
			in.putExtra("field_name", "AD_MobileNumber");
			in.putExtra("table", db.Additional_table);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
			
		break;
		case R.id.ACI_ph_edit:
			in =new Intent(this,EditPhoneNumber.class);
			in.putExtra("id", phone_no_tv[6].getId());
			in.putExtra("number", phone_no_tv[6].getText());
			in.putExtra("field_name", "AD_PhoneNumber");
			in.putExtra("table", db.Additional_table);
			in.putExtra("srid", cf.selectedhomeid);
			startActivityForResult(in, 12);
			
			//in.putExtra("db", db);
		break;
		case R.id.load_comments:
			if(load_comment)
			{
				load_comment=false;
				int loc[] = new int[2];
				v.getLocationOnScreen(loc);
				in =new Intent(this,Load_comments.class);
				in.putExtra("id", R.id.load_comments);
				in.putExtra("srid", cf.selectedhomeid);
				in.putExtra("question", 1);
				in.putExtra("max_length", 500);
				in.putExtra("cur_length", call_info_ed[8].getText().toString().length());
				in.putExtra("xfrom", loc[0]+10);
				in.putExtra("yfrom", loc[1]+10);
				startActivityForResult(in, cf.loadcomment_code);
			}
			else
			{
				cf.show_toast("Exceed the limit", 0);
			}
			
		break;
		default:
			break;
		}
	}
	private void clear_all() {
		// TODO Auto-generated method stub
		for(int i =0;i<phone_no_rd.length;i++)
		{
			phone_no_rd[i].setChecked(false);
			
		}
		for(int i =0;i<call_info_ed.length;i++)
		{
			call_info_ed[i].setText("");
		}
		call_info_rd[0].setChecked(false);
		call_info_rd[1].setChecked(false);
		UTS.setSelection(0);
		IR.setSelection(0);
		result_call.setSelection(0);
		title.setSelection(0);
		call_info_ed[0].setText("");
		call_info_ed[0].setTag("");
		UTS.setEnabled(false);
		UTS.setTag("");
		IR.setEnabled(false);
		IR.setTag("");
	}

	private void upload_call_attempt(final int id) {
		// TODO Auto-generated method stub
	/*	call_info_ed[0]=(EditText) findViewById(R.id.ed1);
		call_info_ed[1]=(EditText) findViewById(R.id.calldate);
		call_info_ed[2]=(EditText) findViewById(R.id.callday);
		call_info_ed[3]=(EditText) findViewById(R.id.calltime);
		call_info_ed[4]=called_no;
		call_info_ed[5]=(EditText) findViewById(R.id.resul_other);
		call_info_ed[6]=(EditText) findViewById(R.id.personanswered);
		call_info_ed[7]=(EditText) findViewById(R.id.othertitle);
		call_info_ed[8]=(EditText) findViewById(R.id.initialcomment);*/
		 String source = "<font color=#FFFFFF>Submitting Call Attempt..."	+ "</font>";
		 cf.pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);

		new Thread() {
			private boolean toastst = true;
			private int verify;
			boolean cancel=false;
			public void run() {
				Looper.prepare();
				try {
					String Chk_Inspector=wb.IsCurrentInspector(db.Insp_id,cf.selectedhomeid,"IsCurrentInspector");
					if(Chk_Inspector.equals("true"))
					{  
						
							
							SoapObject pro=null;
							String result="";
							if(id==R.id.submit)
							{
								pro=wb.export_header("ExportCallAttempt");
							}
							else
							{
								pro=wb.export_header("ExportUTSCallAttempt");
								cancel=true;
							}
							pro.addProperty("InspectorID",db.Insp_id);
							pro.addProperty("Srid",cf.selectedhomeid);
							pro.addProperty("CallDate",call_info_ed[1].getText().toString().trim());
							pro.addProperty("CallTime",call_info_ed[3].getText().toString().trim());
							pro.addProperty("NumberCalled",call_info_ed[4].getText().toString().trim());
							int re= (result_call.getSelectedItemPosition()==0)?0:result_call.getSelectedItemPosition();
							int tit= (title.getSelectedItemPosition()==0)?0:title.getSelectedItemPosition();
							pro.addProperty("CallAttemptResultId",re);
							//pro.addProperty("CallAttemptTitleId",tit);
							if(re==7)
							{
								pro.addProperty("OtherTitle",call_info_ed[5].getText().toString().trim());
								pro.addProperty("CallAttemptTitleId",re);
							}
							else if(tit==7)
							{
								pro.addProperty("OtherTitle",call_info_ed[7].getText().toString().trim());
								pro.addProperty("CallAttemptTitleId",tit);
							}
							else if(re==1)
							{
								pro.addProperty("OtherTitle","");
								pro.addProperty("CallAttemptTitleId",tit);
							}
							else
							{
								pro.addProperty("OtherTitle","");
								pro.addProperty("CallAttemptTitleId",8);// Default value to the title was 8
							}
							pro.addProperty("PersonAnswered",call_info_ed[6].getText().toString().trim());
							pro.addProperty("Comments",call_info_ed[8].getText().toString().trim());
							pro.addProperty("CreatedDate",cf.datewithtime);
							pro.addProperty("ModifiedDate",cf.datewithtime);
							if(id==R.id.submit)
							{
								System.out.println(" the property"+pro);
								wb.result=wb.export_footer(wb.envelope, pro, "ExportCallAttempt").toString();
							}
							else
							{
								
								pro.addProperty("UnableToSchedule",call_info_rd[0].isChecked());
								pro.addProperty("InspectionRefused",call_info_rd[1].isChecked());
								String sp1=(UTS.getSelectedItemPosition()==0)?"":UTS.getSelectedItem().toString();
								String sp2=(IR.getSelectedItemPosition()==0)?"":IR.getSelectedItem().toString();
								pro.addProperty("ddlReason",sp1);
								pro.addProperty("ddlRefusedReason",sp2);
								pro.addProperty("GeneralComment",call_info_ed[0].getText().toString().trim());
								System.out.println(" the property"+pro);
								wb.result=wb.export_footer(wb.envelope, pro, "ExportUTSCallAttempt").toString();
								System.out.println("result ="+wb.result);
							}
							if(id==R.id.submit){
								
								verify = 2;
								handler.sendEmptyMessage(0);
							}
							else
							{
								if (wb.result.contains("Inspection Status updated successfully"))
								{
									db.wdo_db.execSQL("UPDATE "
											+ db.policyholder
											+ " SET  PH_Status='110' where PH_SRID='"
											+ db.encode(cf.selectedhomeid) + "'");
									verify=5;
									handler.sendEmptyMessage(0);
								}
								else
								{
									verify=6;
									handler.sendEmptyMessage(0);
								}
								
								
								
							}
							

							
							
						
					}	else
						{
							verify = 3;
							handler.sendEmptyMessage(0);
							
						}
				} catch (Exception e) {

				}
			}
		

		private Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				if (verify == 2 && toastst) {
					cf.show_toast("Call Attempt has been submitted successfully.",0);
					verify = 0;
					t.run();
					clear_all();
				} else if (verify == 1) {
					cf.show_toast("Please check your network connection and try again.",0);
					t.run();
					
					verify = 0;

				} else if (verify == 3) {
					cf.show_toast("Call Attempt has not been submitted. Reallocated to another inspector.",0);
						verify = 0;
						
						cf.pd.dismiss();

				}
				else if(verify==5)
				{
					cf.show_toast(Html.fromHtml(wb.result.toString()),0);
					Intent Star = new Intent(getApplicationContext(),
							HomeScreen.class);
					Star.putExtra("keyName", 1);
					startActivity(Star);
					
				}
				else if(verify==6)
				{
					cf.show_toast(Html.fromHtml(wb.result.toString()),0);
					
					t.run();
					clear_all();
				}
				
			}
		};
		}.start();
	}
	
		
	private Handler handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {

					if (verify == 2 ) {
						show_callattempt_Value(res);
			     		txtnolist.setVisibility(View.GONE);
						//cf.show_toast("Call Attempt has been submitted successfully.",0);
						clear_all();
					} else if (verify == 1) {
					//	cf.show_toast("No call attempt available .",0);
						txtnolist.setVisibility(View.VISIBLE);
						txtnolist.setText("No call attempt information");
						verify = 0;
						//txtnolist.setVisibility(View.VISIBLE);

					} else {
						
						
						cf.show_toast("There is problem on downloadin data .",0);
							verify = 0;
						
						

					}
					cf.pd.dismiss();
				}
			};
	
		private void show_callattempt_Value(SoapObject res) {
			// TODO Auto-generated method stub
			
			int	cnt = res.getPropertyCount();
			if(cnt>0)
			{	
				dy_val.removeAllViews();
				TableRow tbl_rw;
				View  v;
				tbl_rw=new TableRow(this);
				//tbl_rw.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
				tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithsubtabbackgroundcolor));
				TextView tv_no=new TextView(this,null,R.attr.textview_200);
				
				tv_no.setText("NO");
				tbl_rw.addView(tv_no,30,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				TextView tv_date=new TextView(this,null,R.attr.textview_200);
				tv_date.setText("Call Date");
				tbl_rw.addView(tv_date,100,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
			
				TextView tv_day=new TextView(this,null,R.attr.textview_200);
				tv_day.setText("Call Day");
				tbl_rw.addView(tv_day,70,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				
				TextView tv_time=new TextView(this,null,R.attr.textview_200);
				tv_time.setText("Call Time");
				tbl_rw.addView(tv_time,80,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				
				TextView tv_nocaled=new TextView(this,null,R.attr.textview_200);
				tv_nocaled.setText("Number Called");
				tbl_rw.addView(tv_nocaled,110,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				
				TextView tv_result=new TextView(this,null,R.attr.textview_200);
				tv_result.setText("Result");
				tbl_rw.addView(tv_result,100,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				TextView tv_title=new TextView(this,null,R.attr.textview_200);
				tv_title.setText("Title");
				tbl_rw.addView(tv_title,100,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				TextView tv_comments=new TextView(this,null,R.attr.textview_200);
				tv_comments.setText("Comments");
				tbl_rw.addView(tv_comments,300,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(getResources().getColor(R.color.outline_color));
				tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				
				dy_val.addView(tbl_rw);
					for (int i = 0; i < cnt; i++) {
						tbl_rw=new TableRow(this);
					SoapObject obj = (SoapObject) res.getProperty(i);
					System.out.println("The downloaded call attempt "+obj.toString());
					
					TextView no,date,day,time,number,result,title,comments;
					
					String CADate_s="",CADay_s="",CAT_s="",CAN_s="",CAR_s="",CATi_s="",CAC_s="";
					
					CADate_s=String.valueOf(obj.getProperty("CallAttemptDate"));
					CADay_s=String.valueOf(obj.getProperty("CallAttemptDay"));
					CAT_s= String.valueOf(obj.getProperty("CallAttemptTime"));
					CAN_s=String.valueOf(obj.getProperty("CallAttemptNumberCalled"));
					CAR_s=String.valueOf(obj.getProperty("CallAttemptResult"));
					CATi_s=String.valueOf(obj.getProperty("CallAttemptTitle"));
					CAC_s=String.valueOf(obj.getProperty("CallAttemptIntialsComment"));
					

					if (CADate_s.contains("anyType{}")) {
						CADate_s = CADate_s.replace("anyType{}", "NIL");
					}
					if (CADay_s.contains("anyType{}")) {
						CADay_s = CADay_s.replace("anyType{}", "NIL");
					}
					if (CAT_s.contains("anyType{}")) {
						CAT_s = CAT_s.replace("anyType{}", "NIL");
					}
					if (CAN_s.contains("anyType{}")) {
						CAN_s = CAN_s.replace("anyType{}", "NIL");
					}
					if (CAR_s.contains("anyType{}")) {
						CAR_s = CAR_s.replace("anyType{}", "NIL");
					}
					if (CATi_s.contains("anyType{}")) {
						CATi_s = CATi_s.replace("anyType{}", "NIL");
					}
					if (CAC_s.contains("anyType{}")) {
						CAC_s = CAC_s.replace("anyType{}", "NIL");
					}
					
					
					if (CADate_s.contains("null")) {
						CADate_s = CADate_s.replace("null", "");
					}
					if (CADay_s.contains("null")) {
						CADay_s = CADay_s.replace("null", "");
					}
					if (CAT_s.contains("null")) {
						CAT_s = CAT_s.replace("null", "");
					}
					if (CAN_s.contains("null")) {
						CAN_s = CAN_s.replace("null", "");
					}
					if (CAR_s.contains("null")) {
						CAR_s = CAR_s.replace("null", "");
					}
					if (CATi_s.contains("null")) {
						CATi_s = CATi_s.replace("null", "");
					}
					if (CAC_s.contains("null")) {
						CAC_s = CAC_s.replace("null", "");
					}
					
					no= new TextView(this,null,R.attr.textview_200_inner);
				 	no.setText(String.valueOf(i+1));
				 	tbl_rw.addView(no,30,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	date=  new TextView(this,null,R.attr.textview_200_inner);
				 	date.setText(CADate_s);
				 	tbl_rw.addView(date,100,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	day=  new TextView(this,null,R.attr.textview_200_inner);
				 	if (CADay_s.equals("1")) {
				 		CADay_s = "Monday";
					} else if (CADay_s.equals("2")) {
						CADay_s = "Tuesday";
					} else if (CADay_s.equals("3")) {
						CADay_s = "Wednesday";
					} else if (CADay_s.equals("4")) {
						CADay_s = "Thursday";
					} else if (CADay_s.equals("5")) {
						CADay_s = "Friday";
					} else if (CADay_s.equals("6")) {
						CADay_s = "Saturday";
					} else if (CADay_s.equals("7")) {
						CADay_s = "Sunday";
					}
				
				 	day.setText(CADay_s);
				 	tbl_rw.addView(day,70,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
					
				 	time=  new TextView(this,null,R.attr.textview_200_inner);
				 	time.setText(CAT_s);
				 	
				 	tbl_rw.addView(time,80,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	number=  new TextView(this,null,R.attr.textview_200_inner);
				 	number.setText(CAN_s);
				 	
				 	tbl_rw.addView(number,110,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	result=  new TextView(this,null,R.attr.textview_200_inner);
				 	result.setText(CAR_s);
				 	
				 	tbl_rw.addView(result,100,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	title=  new TextView(this,null,R.attr.textview_200_inner);
				 	title.setText(CATi_s);
				 	
				 	tbl_rw.addView(title,100,LayoutParams.WRAP_CONTENT);
					v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.outline_color));
					tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
				 	
				 	comments= new TextView(this,null,R.attr.textview_200_inner);
				 	comments.setText(CAC_s);
				 	tbl_rw.addView(comments,300,LayoutParams.WRAP_CONTENT);
				 	tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithoutcurve));
				 	dy_val.addView(tbl_rw); 	
				 	
					
				}
				
				
			}else
			{
				
				dy_val.setVisibility(View.GONE);
				
				
			}
			//dy_val.setVisibility(View.GONE);
		}

	private boolean validation_cancel() {
		// TODO Auto-generated method stub
		if(va.validate(call_info_rd,"Unable to Schedule or Inspection Refused"))
		{
			if(va.validate(UTS, "Unable to Schedule","--Select--"))
			{
				if(va.validate(IR, "Inspection Refused","--Select--"))
				{
					if(va.validate(call_info_ed[0], "General Comments"))
					{
						if(validation_submit())
						{
							return true;
						}
					}
				}	
			}
			
		}
		return false;
	}

	private boolean validation_submit() {
		// TODO Auto-generated method stub
		if(va.validate(call_info_ed[1], "Call Date"))
		{
			if(cf.checkforassigndate(assigndata, call_info_ed[1].getText().toString().trim()))
			{
				if(va.validate(call_info_ed[2], "Call Day"))
				{
					if(va.validate(call_info_ed[3], "Call Time"))
					{
						if(va.validate(call_info_ed[4], "Number Called"))
						{
							if(va.validate(result_call,"Result","--Select--"))
							{
								if(va.validate_other(call_info_ed[5],"Result","Other"))
								{
									if(va.validate(call_info_ed[6], "Person Answerd"))
									{
										if(va.validate(title,"Title","--Select--"))
										{
											if(va.validate_other(call_info_ed[7],"Title","Other"))
											{
												if(va.validate(call_info_ed[8], "Comments"))
												{
													return true;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				cf.show_toast(" Call date should not be lesser than Assign date", 0);	
			}
		}
		return false;
		
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			cf.getCalender();
			cf.mYear = year;
			cf.mMonth = monthOfYear+1;
			cf.mDay = dayOfMonth;
			final Calendar c = Calendar.getInstance();
			c.set(cf.mYear, cf.mMonth-1, cf.mDay);
			cf.selmDayofWeek = c.get(Calendar.DAY_OF_WEEK);
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(cf.pad(cf.mMonth)).append("/").append(cf.pad(cf.mDay)).append("/")
					.append(cf.mYear).append(" "));
			Set_Day(cf.selmDayofWeek);
			
		}
		
	}
	
	private void Set_Day(int day) {
		// TODO Auto-generated method stub
		System.out.println("day "+day);
		if (day == 1) {
			call_info_ed[2].setText(new StringBuilder().append("Sunday"));
		}
		if (day == 2) {
			call_info_ed[2].setText(new StringBuilder().append("Monday"));
		}
		if (day  == 3) {
			call_info_ed[2].setText(new StringBuilder().append("Tuesday"));
		}
		if (day  == 4) {
			call_info_ed[2].setText(new StringBuilder().append("Wednesday"));
		}
		if (day  == 5) {
			call_info_ed[2].setText(new StringBuilder().append("Thursday"));
		}
		if (day  == 6) {
			call_info_ed[2].setText(new StringBuilder().append("Friday"));
		}
		if (day == 7) {
			call_info_ed[2].setText(new StringBuilder().append("Saturday"));
		}
	}
	public void clicker_rd(View v)
	{
		for(int i=0;i<phone_no_rd.length;i++)
		{
			if(phone_no_rd[i].getId()==v.getId())
			{
				if(!phone_no_tv[i].getText().toString().equals(""))
					called_no.setText(phone_no_tv[i].getText());
				else
				{
					cf.show_toast("Sorry no valid mobile number present", 0);
					phone_no_rd[i].setChecked(false);
				}
			}
			else{
				phone_no_rd[i].setChecked(false);
			}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goback(12);
			cf.go_back(RealtorInformation.class);
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(requestCode==12)
		{
			
			if(resultCode==RESULT_OK)
			{
				int id=data.getExtras().getInt("id");
				String number=data.getExtras().getString("number");
				((TextView)findViewById(id)).setText(number);
			}
		}
		if(requestCode==cf.loadcomment_code)
		{
				load_comment=true;
				if(resultCode==RESULT_OK)
				{
					call_info_ed[8].setText((call_info_ed[8].getText().toString()+" "+data.getExtras().getString("Comments")).trim());
					
				}
				/*else if(resultCode==RESULT_CANCELED)
				{
					cf.ShowToast("You have canceled  the comments selction ",0);
				}*/
		}	
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			SoapObject request = new SoapObject(wb.NAMESPACE, "LoadCallAttempt");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.selectedhomeid.toString());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"LoadCallAttempt", envelope);
			res = (SoapObject) envelope.getResponse();
			System.out.println("res="+res);
			
			if (res != null) {
				int	cnt =0;
				try
				{
					cnt = res.getPropertyCount();
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("the exception"+e.getMessage());
				}
				if (cnt != 0) {
					verify=2;
					handler.sendEmptyMessage(0);
					
				} else {
					verify=1;
					handler.sendEmptyMessage(0);
					
					System.out.println("comes correctle");
				}
			} else {
				//cf.pd.dismiss();
				handler.sendEmptyMessage(0);
									}
		} catch (Exception e) {
			handler.sendEmptyMessage(0);
		}
			}
	
}
	

