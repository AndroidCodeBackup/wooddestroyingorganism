package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Load_comments extends Activity{
int question;
DataBaseHelper db;
int length,maxlength,fromx,fromy,animat=0;
Bundle b;
private String selectedtag="";
LinearLayout tbl;
String s1[];
private LinearLayout com_li_fill;
private int inti=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load_comments);
		db=new DataBaseHelper(this);
		b=getIntent().getExtras();
		question=b.getInt("question");
		length=b.getInt("cur_length");
		maxlength=b.getInt("max_length");
		fromx=b.getInt("xfrom");
		fromy=b.getInt("yfrom");
		com_li_fill=(LinearLayout) findViewById(R.id.com_com_li);
		displaycomments();
		
		
	}
	private void displaycomments() {
		// TODO Auto-generated method stub
		db.CreateTable(14);
		Cursor c=db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='"+question+"' and TC_Status='true'");
		if(c.getCount()>0)
		{
			TextView tv;
			LinearLayout li;
			CheckedTextView ch;
			tbl= (LinearLayout) findViewById(R.id.com_lis);
			tbl.removeAllViews();
			c.moveToFirst();
			s1=new String[c.getCount()];
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String s =db.decode(c.getString(c.getColumnIndex("TC_Comments")));
				s1[i]=s;
				li=new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				li.setOrientation(LinearLayout.HORIZONTAL);
				li.setPadding(10, 5, 10, 5);
				tv= new TextView(this);
				tv.setText((i+1)+". ");
				tv.setTextColor(Color.BLACK);
				li.addView(tv,20,LayoutParams.WRAP_CONTENT);
				ch= new CheckedTextView(this,null,R.attr.checked_text);
				ch.setTag("checkbox_"+i);
				ch.setText(s);
				ch.setTextColor(Color.BLACK);
				ch.setOnClickListener(new liClick(s,1,i));
				ch.setGravity(Gravity.CENTER_VERTICAL);
				li.addView(ch,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				tbl.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 if(i<c.getCount()-1)
				 {
					 View v=new View(this);
					 v.setBackgroundColor(getResources().getColor(R.color.sub_menu));
					 tbl.addView(v,LayoutParams.FILL_PARENT,1);
				 }
				 RelativeLayout.LayoutParams li_p=new RelativeLayout.LayoutParams(600,LayoutParams.FILL_PARENT);
				 ((LinearLayout) findViewById(R.id.com_com_li)).setLayoutParams(li_p);
			}
			if(c!=null)
				c.close();
		}
		else
		{
			show_toast("Comments not available.", 0);
			Intent in = new Intent();
			b.putString("Comments","");
			in.putExtras(b);
			setResult(RESULT_CANCELED, in);
			finish();
		}
		
	}
	class liClick implements android.view.View.OnClickListener
	{
		int id,type;
		String cmt;
		
		liClick(String cmt,int type,int id)
		{
			this.cmt=cmt;
			this.type=type;
			this.id=id;
		}
		@Override
		public void onClick(View v) {
				if(!((CheckedTextView) v).isChecked())
				{
					if(length>=maxlength)
					{
						show_toast("You have exceeded the maximum limit.", 0);
						
					}
					else
					{
						selectedtag+=id+"~";
						((CheckedTextView) v).setChecked(true);
						length=length+cmt.length();
					}
				}
				else
				{
					
					selectedtag=selectedtag.replace(id+"~", "");
					length=length-cmt.length();
					((CheckedTextView) v).setChecked(false);
				}
			
			
		}
		
	}
	public void show_toast(String msg,int type)
	{
		String colorname = "#000000"; 
		switch(type)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		final Toast toast = new Toast(this);
		 LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(msg);
		toast.setView(layout); 
		Thread t = new Thread() {
            public void run() {
                int count = 0;
                try {
                    while (true && count < 10) {
                        toast.show();
                        sleep(3);
                        count++;

                        // do some logic that breaks out of the while loop
                    }
                } catch (Exception e) {
                   
                }
            }
        };
        t.start();
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		if(v.getId()==R.id.load_com_close)
		{
			if(animat==0)
			{
				Intent in = new Intent();
				b.putString("Comments","");
				in.putExtras(b);
				setResult(RESULT_CANCELED, in);
				Scaleoutanimation(fromx,fromy);
				//cf.ShowToast("you have not  selcted any comments", 0);
			}
		}
		else if(v.getId()==R.id.ok)
		{
			if(selectedtag.length()>1)
			{
				selectedtag=selectedtag.substring(0,selectedtag.length()-1);
				System.out.println(" the selected tag"+selectedtag);
				String[] sel=selectedtag.split("~");
				String comts="";
				for(int i=0;i<sel.length;i++)
				{
					
					//sel[i]=sel[i].replace("checkbox_", "");
					System.out.println(" te sel id "+sel[i]);
					if(comts.equals(""))
					{
						comts+=s1[Integer.parseInt(sel[i])];
					}
					else
					{
						comts+=" "+s1[Integer.parseInt(sel[i])];
					}
				}
				System.out.println(" te sel id "+comts);
				Intent in = new Intent();
				b.putString("Comments",comts);
				in.putExtras(b);
				setResult(RESULT_OK, in);
			
				Scaleoutanimation(fromx,fromy);
			}
			else
			{
				show_toast("Please select atleast one comment.", 0);
			}
		}
		else
		{
			
		}
	}
	public void Scaleinanimation(float xfrom, float yfrom) {
		AnimationSet an= new AnimationSet(false);
        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
       scale.setDuration(500);
       an.addAnimation(scale);
        TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2), TranslateAnimation.ABSOLUTE,0,
    		   TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2),TranslateAnimation.ABSOLUTE,0); 
       trans.setDuration(500);
       trans.setStartOffset(10);
       an.setFillAfter(true);
       an.addAnimation(trans);
       an.setAnimationListener(new animat_lis(1));
       com_li_fill.startAnimation(an);
    }
	public void Scaleoutanimation(float xfrom, float yfrom) {
	      
		
		
	       AnimationSet an= new AnimationSet(false);
	       ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
	       scale.setDuration(500);
	       an.addAnimation(scale);
	       TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,0, TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2),
	    		   TranslateAnimation.ABSOLUTE,0,TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2)); 
	       trans.setDuration(500);
	       trans.setStartOffset(10);
	       an.setFillAfter(true);
	       an.addAnimation(trans);
	       com_li_fill.startAnimation(an);
	       an.setAnimationListener(new animat_lis(0));
	       
	    }
		
	
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if(inti==0)
		{
			Scaleinanimation(fromx,fromy);
		 inti=1;
		}
	}
	
	class animat_lis implements AnimationListener
	{int type;
	
		animat_lis(int type)
		{
			this.type=type;
		}
		
		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			animat=1;
		}
		
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			animat=0;
			if(type==0)
			{
				finish();
			}
		}

		
	}
	
}
