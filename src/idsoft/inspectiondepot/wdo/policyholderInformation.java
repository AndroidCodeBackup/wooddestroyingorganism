package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.MultiSpinner;
import idsoft.inspectiondepot.wdo.supportclass.Option_list;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import idsoft.inspectiondepot.wdo.supportclass.phone_nowatcher;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class policyholderInformation extends Activity {
CommonFunction cf;
DataBaseHelper db;
Validation va;
EditText policy_info[]=new EditText[8],Policy_address[]=new EditText[5],Policy_mailaddress[]=new EditText[5];
Spinner policy_stat,mail_state;
CheckBox chk[]=new CheckBox[3];
MultiSpinner ms_reportrequest,ms_reportsent,ms_structureson;
Option_list op1,op2,op3;
TextView tv_reportrequest,tv_reportsent,tv_structureson;
EditText et_reportrequestother,et_reportsentother,et_structuresonother;
ArrayAdapter ad;
String fields_namepolicy_info[]={"First Name","Last Name","Home Phone No","Work Phone No","Mobile No","Contact Person","Email Address","Policy No","Report Requested By","Report sent to Reqestor and to","Structures on Property Inspected"},
fields_namePolicy_address[]={"Address #1","Address #2","City","Zipcode","County"},fields_namePolicy_mailaddress[]={"Address #1 under Mailing Address","Address #2 under Mailing Address","City under Mailing Address","Zipcode under Mailing Address","County under Mailing Address"};
String state[]={"--Select--","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","District of Columbia","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Carolina","North Dakota","Ohio","Oklahoma","Oregon","Pennsylvania","Puerto Rico","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"};
@Override
protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.policyholer_informaion);
		cf =new CommonFunction(this);
		db=new DataBaseHelper(this);
		va=new Validation(cf);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		  
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Policyholder",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,1,cf));
		cf.setTouchListener(findViewById(R.id.content));
		Declaration();
		db.CreateTable(2);
		db.CreateTable(3);
		Cursor c=db.SelectTablefunction(db.policyholder, " WHERE PH_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			show_data(c);
		}
		if(c!=null)
			c.close();
		
	}
	private void show_data(Cursor c) {
		// TODO Auto-generated method stub
		c.moveToFirst();
		policy_info[0].setText(db.decode(c.getString(c.getColumnIndex("PH_FirstName"))));
		policy_info[1].setText(db.decode(c.getString(c.getColumnIndex("PH_LastName"))));
		policy_info[2].setText(db.decode(c.getString(c.getColumnIndex("PH_HomePhone"))));
		policy_info[3].setText(db.decode(c.getString(c.getColumnIndex("PH_WorkPhone"))));
		policy_info[4].setText(db.decode(c.getString(c.getColumnIndex("PH_CellPhone"))));
		policy_info[5].setText(db.decode(c.getString(c.getColumnIndex("ContactPerson"))));
		policy_info[6].setText(db.decode(c.getString(c.getColumnIndex("PH_Email"))));
		policy_info[7].setText(db.decode(c.getString(c.getColumnIndex("PH_Policyno"))));
		/*policy_info[8].setText(db.decode(c.getString(c.getColumnIndex("PH_Policyno"))));
		policy_info[9].setText(db.decode(c.getString(c.getColumnIndex("PH_InsuranceCompany"))));*/
//		policy_info[10].setText(db.decode(c.getString(c.getColumnIndex("R_Requestedby"))));
//		policy_info[10].setText(db.decode(c.getString(c.getColumnIndex("R_Requestedto"))));
//		policy_info[11].setText(db.decode(c.getString(c.getColumnIndex("structure"))));
		
		Policy_address[0].setText(db.decode(c.getString(c.getColumnIndex("PH_Address1"))));
		Policy_address[1].setText(db.decode(c.getString(c.getColumnIndex("PH_Address2"))));
		Policy_address[2].setText(db.decode(c.getString(c.getColumnIndex("PH_City"))));
		//Policy_address[3].setText(db.decode(c.getString(c.getColumnIndex("PH_State"))));
		Policy_address[3].setText(db.decode(c.getString(c.getColumnIndex("PH_Zip"))));
		Policy_address[4].setText(db.decode(c.getString(c.getColumnIndex("PH_County"))));
		if(c.getString(c.getColumnIndex("PH_EmailChkbx")).equals("1"))
		{
			chk[0].setChecked(true);
			policy_info[6].setText("NA");
			policy_info[6].setEnabled(false);
			policy_info[6].setTag("");
			
		}
		if(ad.getPosition(db.decode(c.getString(c.getColumnIndex("PH_State"))))!=-1)
		{
			policy_stat.setSelection(ad.getPosition(db.decode(c.getString(c.getColumnIndex("PH_State")))));
		}
		Cursor cm=db.SelectTablefunction(db.MailingPolicyHolder, " WHERE ML_PH_SRID='"+cf.selectedhomeid+"'");
		if(cm.getCount()>0)
		{
			cm.moveToFirst();
			Policy_mailaddress[0].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_Address1"))));
			Policy_mailaddress[1].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_Address2"))));
			Policy_mailaddress[2].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_City"))));
			//Policy_mailaddress[3].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_State"))));
			Policy_mailaddress[3].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_Zip"))));
			Policy_mailaddress[4].setText(db.decode(cm.getString(cm.getColumnIndex("ML_PH_County"))));
			if(cm.getString(cm.getColumnIndex("ML")).equals("1"))
			{
				chk[1].setChecked(true);
			}
			else
			{
				chk[1].setChecked(false);
				
			}
			if(cm.getString(cm.getColumnIndex("ML_NA")).equals("1"))
			{
				chk[2].setChecked(true);
				for(int i=0;i<Policy_mailaddress.length;i++)
				{
					Policy_mailaddress[i].setText("NA");
					Policy_mailaddress[i].setEnabled(false);
				}
				mail_state.setSelection(0);
				mail_state.setEnabled(false);
			}
			
			if(ad.getPosition(db.decode(cm.getString(cm.getColumnIndex("ML_PH_State"))))!=-1)
			{
				mail_state.setSelection(ad.getPosition(db.decode(cm.getString(cm.getColumnIndex("ML_PH_State")))));
			}
			if((Policy_address[0].getText().toString().trim().equals(Policy_mailaddress[0].getText().toString().trim())) && (Policy_address[1].getText().toString().trim().equals(Policy_mailaddress[1].getText().toString().trim())) && 
					(Policy_address[2].getText().toString().trim().equals(Policy_mailaddress[2].getText().toString().trim())) && (Policy_address[3].getText().toString().trim().equals(Policy_mailaddress[3].getText().toString().trim())) &&
					(Policy_address[4].getText().toString().trim().equals(Policy_mailaddress[4].getText().toString().trim())) && (policy_stat.getSelectedItemPosition()== mail_state.getSelectedItemPosition())  )
			{
				
				chk[1].setChecked(true);
			}
			else
			{
				chk[1].setChecked(false);
			}
			
		}
		String dbop1 = db.decode(c.getString(c.getColumnIndex("R_Requestedby")));
		if(dbop1.trim().equals(""))
		{
			op1.set_selectionCHk("Client/Owner");
		}
		else
		{
		  op1.set_selectionCHk(db.decode(c.getString(c.getColumnIndex("R_Requestedby"))));
		}
		et_reportrequestother.setText(db.decode(c.getString(c.getColumnIndex("R_Requestedby_other"))));
		
		String dbop2 = db.decode(c.getString(c.getColumnIndex("R_Requestedto")));
		if(dbop2.trim().equals(""))
		{
			op2.set_selectionCHk("Client/Owner");
		}
		else
		{
		  op2.set_selectionCHk(db.decode(c.getString(c.getColumnIndex("R_Requestedto"))));
		}
		et_reportsentother.setText(db.decode(c.getString(c.getColumnIndex("R_Requestedto_other"))));
		
		String dbop3 = db.decode(c.getString(c.getColumnIndex("structure")));
		if(dbop3.trim().equals(""))
		{
			op3.set_selectionCHk("Main Dwelling");
		}
		else
		{
		  op3.set_selectionCHk(db.decode(c.getString(c.getColumnIndex("structure"))));
		}
		et_structuresonother.setText(db.decode(c.getString(c.getColumnIndex("structure_other"))));
		if(cm!=null)
			cm.close();
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		policy_info[0]=(EditText) findViewById(R.id.policy_fname);
		policy_info[1]=(EditText) findViewById(R.id.policy_lname);
		policy_info[2]=(EditText) findViewById(R.id.policy_hphone);
		policy_info[3]=(EditText) findViewById(R.id.policy_wphone);
		policy_info[4]=(EditText) findViewById(R.id.policy_cphone);
		policy_info[5]=(EditText) findViewById(R.id.policy_contactp);
		policy_info[6]=(EditText) findViewById(R.id.policy_email);
		policy_info[7]=(EditText) findViewById(R.id.policy_policyno);
		/*policy_info[7]=(EditText) findViewById(R.id.policy_website);
		policy_info[9]=(EditText) findViewById(R.id.policy_insurcomp);*/
//		policy_info[10]=(EditText) findViewById(R.id.policy_RRequest);
//		policy_info[10]=(EditText) findViewById(R.id.policy_RRto);
//		policy_info[11]=(EditText) findViewById(R.id.policy_struct);
		
		policy_stat =(Spinner) findViewById(R.id.policy_state);
		ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,state);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		policy_stat.setAdapter(ad);
		
		mail_state =(Spinner) findViewById(R.id.policy_mail_state);
		ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,state);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mail_state.setAdapter(ad);
		
		policy_info[2].addTextChangedListener(new phone_nowatcher(policy_info[2]));
		policy_info[3].addTextChangedListener(new phone_nowatcher(policy_info[3]));
		policy_info[4].addTextChangedListener(new phone_nowatcher(policy_info[4]));
				
		Policy_address[0]=(EditText) findViewById(R.id.policy_add1);
		Policy_address[1]=(EditText) findViewById(R.id.policy_add2);
		Policy_address[2]=(EditText) findViewById(R.id.policy_city);
		//Policy_address[3]=(EditText) findViewById(R.id.policy_state);
		Policy_address[3]=(EditText) findViewById(R.id.policy_zipcode);
		Policy_address[4]=(EditText) findViewById(R.id.policy_county);
		
		Policy_mailaddress[0]=(EditText) findViewById(R.id.policy_mail_add1);
		Policy_mailaddress[1]=(EditText) findViewById(R.id.policy_mail_add2);
		Policy_mailaddress[2]=(EditText) findViewById(R.id.policy_mail_city);
		//Policy_mailaddress[3]=(EditText) findViewById(R.id.policy_mail_state);
		Policy_mailaddress[3]=(EditText) findViewById(R.id.policy_mail_zipcode);
		Policy_mailaddress[4]=(EditText) findViewById(R.id.policy_mail_county);
		
		chk[0]=(CheckBox) findViewById(R.id.policy_turnemail);
		chk[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(chk[0].isChecked())
				{	
					policy_info[6].setTag("");
					policy_info[6].setText("NA");
					policy_info[6].setEnabled(false);
				}
				else
				{
					policy_info[6].setTag("required,email");
					policy_info[6].setEnabled(true);
					policy_info[6].setText("");
					
				}
			}
		});
		chk[1]=(CheckBox) findViewById(R.id.policy_chkmailsame);
		chk[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chk[2].setChecked(false);
				if(chk[1].isChecked())
				{
					copymailaddress();
				}
				else
				{
					clear_mailaddress();
				}
				
			}

			
		});
		chk[2]=(CheckBox) findViewById(R.id.policy_chkna);
		chk[2].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chk[1].setChecked(false);
				if(chk[2].isChecked())
				{
					for(int i=0;i<Policy_mailaddress.length;i++)
					{
						Policy_mailaddress[i].setText("NA");
						Policy_mailaddress[i].setEnabled(false);
						mail_state.setSelection(0);
						mail_state.setEnabled(false);
						
					}
				}
				else
				{
					for(int i=0;i<Policy_mailaddress.length;i++)
						{
							Policy_mailaddress[i].setText("");
							Policy_mailaddress[i].setEnabled(true);
							mail_state.setEnabled(true);
						}
				}
				
			}

			
		});
		
		ms_reportrequest=(MultiSpinner) findViewById(R.id.policy_reportrequest);
		tv_reportrequest=(TextView)findViewById(R.id.policy_tvreportrequest);
		et_reportrequestother=(EditText)findViewById(R.id.policy_etreportrequestother);
		
		op1=new Option_list(this, ms_reportrequest, 11, 1, et_reportrequestother,tv_reportrequest,"",cf);
		
		ms_reportsent=(MultiSpinner) findViewById(R.id.policy_reportsent);
		tv_reportsent=(TextView)findViewById(R.id.policy_tvreportsent);
		et_reportsentother=(EditText)findViewById(R.id.policy_etreportsentother);
		
		op2=new Option_list(this, ms_reportsent, 11, 2, et_reportsentother,tv_reportsent,"",cf);
		
		ms_structureson=(MultiSpinner) findViewById(R.id.policy_structureson);
		tv_structureson=(TextView)findViewById(R.id.policy_tvstructureson);
		et_structuresonother=(EditText)findViewById(R.id.policy_etstructuresonother);
		
		op3=new Option_list(this, ms_structureson, 11, 3, et_structuresonother,tv_structureson,"");
	}
	protected void clear_mailaddress() {
		// TODO Auto-generated method stub
		for(int i=0;i<Policy_mailaddress.length;i++)
		{
			Policy_mailaddress[i].setText("");
		}
		mail_state.setSelection(0);

	}
	private void copymailaddress() {
		// TODO Auto-generated method stub
		for(int i=0;i<Policy_mailaddress.length;i++)
		{
			Policy_mailaddress[i].setText(Policy_address[i].getText());
			Policy_mailaddress[i].setEnabled(true);
			
			
		}
		mail_state.setEnabled(true);
		mail_state.setSelection(policy_stat.getSelectedItemPosition());
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.save:
				Validation_chk();
				
				/*cf.show_toast("Saved successfully", 1);
				Intent in =new Intent(this,Agentinformation.class);
				in.putExtra("SRID", cf.selectedhomeid);
				startActivity(in);*/
				//cf.go_home();
			break;
			default:
				//cf.show_toast("Button under construction", 0);
			break;
		}
	}
	private void Validation_chk() {
		// TODO Auto-generated method stub
		for(int i=0;i<policy_info.length;i++)
		{
			boolean val=va.validate(policy_info[i],fields_namepolicy_info[i]);
			if(val!=true)
			{
				return;
			}
		}
		
		if(va.validate(tv_reportrequest,"Report Requested By","You have selected"))
		{
			if(va.validate_other(et_reportrequestother,"Report Requested By","Other"))
			{
			if(va.validate(tv_reportsent,"Send Report To","You have selected"))
			{
				if(va.validate_other(et_reportsentother,"Send Report To","Other"))
				{
				if(va.validate(tv_structureson,"Structures on Property Inspected","You have selected"))
				{
					if(va.validate_other(et_structuresonother,"Structures on Property Inspected","Other"))
					{
					for(int i=0;i<Policy_address.length;i++)
					{
						boolean val=va.validate(Policy_address[i],fields_namePolicy_address[i]);
						
							if(val!=true)
							{
								
								return;
							}
						
							
					}
					if(va.validate(policy_stat, "State", "--Select--"))
					{
						
					
					if(!chk[2].isChecked())
					{System.out.println("comesin");
						if(!Policy_mailaddress[3].getText()
								.toString()
								.trim()
								.equals("")){
							if ((Policy_mailaddress[3]
									.getText()
									.toString()
									.length() == 5)) {
								if ((!Policy_mailaddress[3]
										.getText()
										.toString()
										.contains(
												"00000"))) {
									save_data();
									
								}
								else
								{
									cf.show_toast("Please enter a valid Zip under Mailing Address", 0);
									Policy_mailaddress[3].requestFocus();
									Policy_mailaddress[3].setText("");
								}
								
							}
							else
							{
									cf.show_toast("Zip should be 5 characters under Mailing Address", 0);
									Policy_mailaddress[3].requestFocus();
									Policy_mailaddress[3].setText("");
									
							}
						}
						else
						{
							save_data();
						}
						/*for(int i=0;i<Policy_mailaddress.length;i++)
						{
							boolean val=va.validate(Policy_mailaddress[i],fields_namePolicy_mailaddress[i]);
							
								if(val!=true)
								{
									return;
								}
							
						}
						
						if(!va.validate(mail_state, "State under Mailing Address", "--Select--"))
						{
							return;
						}*/
					}
					
					save_data();
					}
				}
				}
			}
			}
			}
		}
		
		
	}
	private void save_data() {
		String reportrequested="",reportsent="",structureon="",reportrequested_o="",reportsent_o="",structureon_o="";
		String value[]=new String[policy_info.length];
		String value_add[]=new String[Policy_address.length];
		String value_madd[]=new String[Policy_mailaddress.length];
		String value_chk[]=new String[3];
		for(int i=0;i<policy_info.length;i++)
		{
			value[i]=db.encode(policy_info[i].getText().toString().trim());
		}
		for(int i=0;i<Policy_address.length;i++)
		{
			value_add[i]=db.encode(Policy_address[i].getText().toString().trim());
		}
		
		for(int i=0;i<Policy_mailaddress.length;i++)
		{
			if(!chk[2].isChecked())
			{
				value_madd[i]=db.encode(Policy_mailaddress[i].getText().toString().trim());
			}
			else
			{
				value_madd[i]="";
			}
		}
		if(chk[0].isChecked())
		{
			value_chk[0]="1";
			value[6]="";
		}
		else
		{
			value_chk[0]="0";
		}
		if(chk[1].isChecked())
		{
			value_chk[1]="1";
		}
		else
		{
			value_chk[1]="0";
		}
		if(chk[2].isChecked())
		{
			value_chk[2]="1";
		}
		else
		{
			value_chk[2]="0";
		}
		
		reportrequested=db.encode(op1.get_selected());
		if(reportrequested.contains("Other"))
		{
			reportrequested_o=db.encode(et_reportrequestother.getText().toString().trim());
		}
		
		reportsent=db.encode(op2.get_selected());
		if(reportsent.contains("Other"))
		{
			reportsent_o=db.encode(et_reportsentother.getText().toString().trim());
		}
		
		structureon=db.encode(op3.get_selected());
		if(structureon.contains("Other"))
		{
			structureon_o=db.encode(et_structuresonother.getText().toString().trim());
		}
		
	/*	System.out.println("Report requested by "+reportrequested);
		System.out.println("Report sent to "+reportsent);
		System.out.println("Structure on "+structureon);
	*/	
		try
		{
			/*System.out.println("UPDATE "+db.policyholder+" SET PH_FirstName='"+value[0]+"',PH_LastName='"+value[1]+"',PH_HomePhone='"+value[2]+"',PH_WorkPhone='"+
					value[3]+"',PH_CellPhone='"+value[4]+"',ContactPerson='"+value[5]+"',PH_Email='"+value[6]+"',PH_WEBSITE='"+value[7]+"',PH_Policyno='"+value[8]+"'" +
					",PH_InsuranceCompany='"+value[9]+"',R_Requestedby='"+reportrequested+"',R_Requestedto='"+reportsent+"',structure='"+structureon+"'," +
					"R_Requestedby_other='"+reportrequested_o+"',R_Requestedto_other='"+reportsent_o+"',structure_other='"+structureon_o+"',PH_Address1='"+value_add[0]+"'," +
					"PH_Address2='"+value_add[1]+"',PH_City='"+value_add[2]+"',PH_State='"+value_add[3]+"',PH_Zip='"+value_add[4]+"',PH_County='"+value_add[5]+"',PH_EmailChkbx='"+value_chk[0]+"' " +
							" Where PH_InspectorId='"+db.Insp_id+"' and PH_SRID='"+cf.selectedhomeid+"'");
			*/
		db.wdo_db.execSQL("UPDATE "+db.policyholder+" SET PH_FirstName='"+value[0]+"',PH_LastName='"+value[1]+"',PH_HomePhone='"+value[2]+"',PH_WorkPhone='"+
		value[3]+"',PH_CellPhone='"+value[4]+"',ContactPerson='"+value[5]+"',PH_Email='"+value[6]+"',PH_Policyno='"+value[7]+"'" +
		",R_Requestedby='"+reportrequested+"',R_Requestedto='"+reportsent+"',structure='"+structureon+"'," +
		"R_Requestedby_other='"+reportrequested_o+"',R_Requestedto_other='"+reportsent_o+"',structure_other='"+structureon_o+"',PH_Address1='"+value_add[0]+"'," +
		"PH_Address2='"+value_add[1]+"',PH_City='"+value_add[2]+"',PH_State='"+db.encode(policy_stat.getSelectedItem().toString().trim())+"',PH_Zip='"+value_add[3]+"',PH_County='"+value_add[4]+"',PH_EmailChkbx='"+value_chk[0]+"' " +
				" Where PH_InspectorId='"+db.Insp_id+"' and PH_SRID='"+cf.selectedhomeid+"'");
		
		
		/*System.out.println("UPDATE "+db.policyholder+" SET PH_FirstName='"+value[0]+"',PH_LastName='"+value[1]+"',PH_HomePhone='"+value[2]+"',PH_WorkPhone='"+
		value[3]+"',PH_CellPhone='"+value[4]+"',ContactPerson='"+value[5]+"',PH_Email='"+value[6]+"',PH_WEBSITE='"+value[7]+"',PH_Policyno='"+value[8]+"'" +
		",PH_InsuranceCompany='"+value[9]+"',R_Requestedby='"+reportrequested+"',R_Requestedto='"+reportsent+"',structure='"+structureon+"',PH_Address1='"+value_add[0]+"'," +
		"PH_Address2='"+value_add[1]+"',PH_City='"+value_add[2]+"',PH_State='"+value_add[3]+"',PH_Zip='"+value_add[4]+"',PH_County='"+value_add[5]+"',PH_EmailChkbx='"+value_chk[0]+"' " +
				" Where PH_InspectorId='"+db.Insp_id+"' and PH_SRID='"+cf.selectedhomeid+"'");*/
		
		Cursor cm=db.SelectTablefunction(db.MailingPolicyHolder, " WHERE ML_PH_SRID='"+cf.selectedhomeid+"'");
		if(cm.getCount()<=0)
		{
		/*	System.out.println("INSERT INTO "+db.MailingPolicyHolder+" (ML_PH_InspectorId,ML_PH_SRID,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County,ML) VALUES" +
					" ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+value_madd[0]+"','"+value_madd[1]+"','"+value_madd[2]+"','"+value_madd[4]+"','"+value_madd[3]+"','"+value_madd[5]+"','"+value_chk[1]+"')");*/
			db.wdo_db.execSQL("INSERT INTO "+db.MailingPolicyHolder+" (ML_PH_InspectorId,ML_PH_SRID,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County,ML,ML_NA) VALUES" +
					" ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+value_madd[0]+"','"+value_madd[1]+"','"+value_madd[2]+"','"+value_madd[3]+"','"+db.encode(mail_state.getSelectedItem().toString().trim())+"','"+value_madd[4]+"','"+value_chk[1]+"','"+value_chk[2]+"')" );
			
		}
		else
		{
			/*System.out.println("UPDATE "+db.MailingPolicyHolder+ " SET ML_PH_Address1='"+value_madd[0]+"',ML_PH_Address2='"+value_madd[1]+"',ML_PH_City='"+value_madd[2]+"'," +
					"ML_PH_Zip='"+value_madd[4]+"',ML_PH_State='"+value_madd[3]+"',ML_PH_County='"+value_madd[5]+"',ML='"+value_chk[1]+"' WHERE ML_PH_InspectorId='"+db.Insp_id+"' AND ML_PH_SRID='"+cf.selectedhomeid+"'");*/
			db.wdo_db.execSQL("UPDATE "+db.MailingPolicyHolder+ " SET ML_PH_Address1='"+value_madd[0]+"',ML_PH_Address2='"+value_madd[1]+"',ML_PH_City='"+value_madd[2]+"'," +
					"ML_PH_Zip='"+value_madd[3]+"',ML_PH_State='"+db.encode(mail_state.getSelectedItem().toString().trim())+"',ML_PH_County='"+value_madd[4]+"',ML='"+value_chk[1]+"',ML_NA='"+value_chk[2]+"' WHERE ML_PH_InspectorId='"+db.Insp_id+"' AND ML_PH_SRID='"+cf.selectedhomeid+"'");
		}
		if(cm!=null)
			cm.close();	
		
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("there issues "+e.getMessage());
			//cf.show_toast("You have problem in update policyholder information", 1);
		}
		cf.show_toast("Client information updated successfully", 1);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	// replaces the default 'Back' button action
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		//cf.goback(12);
		cf.go_back(Dashboard.class);
		
		return true;
	}
	return super.onKeyDown(keyCode, event);
}
	
	
}
