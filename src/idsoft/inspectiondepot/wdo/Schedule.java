package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class Schedule extends Activity {
CommonFunction cf;
Webservice_Function wb;
private static final int DATE_DIALOG_ID = 0;
private static final int SELECT_PICTURE = 0;

public String strhomeid; 
String isinspected;
String startspin,endspin;
Cursor c2;
int spin1,spin2,verify,SP_ST,SP_ET;
private ArrayAdapter<CharSequence> mRangeadapter;
TextView assigndate;
private EditText et_commenttxt,et_inspdate;
private CheckBox schchk;
private Button get_inspectiondate,save;
ImageView coment_addin;
private Spinner Sch_Spinnerstart,Sch_Spinnerend;
private TextView SQ_TV_type2;
///	private LinearLayout SQ_ED_type2;
private String Sh_assignedDate;
private String onlstatus="";
DataBaseHelper db;
private boolean load_comment=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		db=new DataBaseHelper(this);
		wb=new Webservice_Function(this);
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Genereal=>Schedule",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,1,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,1,5,cf));
		schchk = (CheckBox) findViewById(R.id.reschedule);
		et_inspdate = (EditText)findViewById(R.id.etinspectiondate);    
		get_inspectiondate = (Button)findViewById(R.id.getinspectiondate);
		Sch_Spinnerstart = (Spinner)findViewById(R.id.spinstarttime);
		Sch_Spinnerend = (Spinner)findViewById(R.id.spinendtime);
		Spinner_SetStarttime();
		Sch_Spinnerstart.setOnItemSelectedListener(new MyOnItemSelectedListenerstart());
		et_commenttxt = (EditText)findViewById(R.id.etcommenttxt);
		SQ_TV_type2 = (TextView) findViewById(R.id.SH_TV_ED);
		save=(Button) findViewById(R.id.save);
		coment_addin=(ImageView) findViewById(R.id.load_comments);
	    et_commenttxt.addTextChangedListener(new TextWatchLimit(et_commenttxt,1000,SQ_TV_type2));
	   cf.getInspectorId();
		cf.getCalender();
		Spinner_SetEndtime();
		Sch_Spinnerend.setOnItemSelectedListener(new MyOnItemSelectedListenerend());
		schchk.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// Perform action on clicks, depending on whether it's now
				// checked
			if (((CheckBox) v).isChecked()) {
					//et_inspdate.setEnabled(true);
					get_inspectiondate.setEnabled(true);
					Sch_Spinnerstart.setEnabled(true);
					Sch_Spinnerend.setEnabled(true);
					et_commenttxt.setEnabled(true);
					coment_addin.setEnabled(true);
					save.setEnabled(true);
					((TextView)findViewById(R.id.txtid)).setText("RE-SCHEDULE AN INSPECTION");
				} else if (!et_inspdate.getText().toString().equals("")) {
					et_inspdate.setEnabled(false);
					get_inspectiondate.setEnabled(false);
					Sch_Spinnerstart.setEnabled(false);
					Sch_Spinnerend.setEnabled(false);
					et_commenttxt.setEnabled(false);
					coment_addin.setEnabled(false);
					save.setEnabled(false);
					((TextView)findViewById(R.id.txtid)).setText("SCHEDULE AN INSPECTION");
				}else{
					((TextView)findViewById(R.id.txtid)).setText("SCHEDULE AN INSPECTION");
				}

			}

		});
		get_inspectiondate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				cf.showDialogDate(et_inspdate);
				
			}
		});
		
		cf.setTouchListener(findViewById(R.id.content));
	set_savedvalues();
 } 

	private void set_savedvalues() {
		// TODO Auto-generated method stub
		Cursor c=db.SelectTablefunction(db.policyholder, " Where PH_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			if(db.decode(c.getString(c.getColumnIndex("PH_Status"))).equals("40"))
			{
				onlstatus="Schedule";
				get_inspectiondate.setEnabled(false);
				Sch_Spinnerstart.setEnabled(false);
				Sch_Spinnerend.setEnabled(false);
				et_commenttxt.setEnabled(false);
				coment_addin.setEnabled(false);
				save.setEnabled(false);
			}
			else if(!db.decode(c.getString(c.getColumnIndex("PH_Status"))).equals("30"))
			{
				onlstatus="Assign";
				schchk.setEnabled(false);
				get_inspectiondate.setEnabled(false);
				Sch_Spinnerstart.setEnabled(false);
				Sch_Spinnerend.setEnabled(false);
				et_commenttxt.setEnabled(false);
				coment_addin.setEnabled(false);
				save.setEnabled(false);
			}
			else if(db.decode(c.getString(c.getColumnIndex("PH_Status"))).equals("30"))
			{
				onlstatus="Assign";
				schchk.setEnabled(false);
				get_inspectiondate.setEnabled(true);
				Sch_Spinnerstart.setEnabled(true);
				Sch_Spinnerend.setEnabled(true);
				et_commenttxt.setEnabled(true);
				coment_addin.setEnabled(true);
				save.setEnabled(true);
			}
			
		
			String dbinspdate = db.decode(c.getString(c.getColumnIndex("Schedule_ScheduledDate")));
			if(dbinspdate.trim().equals(""))
			{
				et_inspdate.setText(db.decode(c.getString(c.getColumnIndex("Schedule_AssignedDate"))));
			}
			else
			{
			  et_inspdate.setText(db.decode(c.getString(c.getColumnIndex("Schedule_ScheduledDate"))));
			}
			SP_ST = mRangeadapter.getPosition(db.decode(c.getString(c.getColumnIndex("Schedule_InspectionStartTime"))));
			Sch_Spinnerstart.setSelection(SP_ST);
			SP_ET = mRangeadapter.getPosition(db.decode(c.getString(c.getColumnIndex("Schedule_InspectionEndTime"))));
			Sch_Spinnerend.setSelection(SP_ET);
			et_commenttxt.setText(db.decode(c.getString(c.getColumnIndex("Schedule_Comments"))));
			((TextView) findViewById(R.id.viewassigndate)).setText(db.decode(c.getString(c.getColumnIndex("Schedule_AssignedDate"))));
			((TextView) findViewById(R.id.viewpreferdate)).setText(db.decode(c.getString(c.getColumnIndex("Schedule_AssignedDate"))));
			((TextView) findViewById(R.id.viewprefertime)).setText(db.decode(c.getString(c.getColumnIndex("prefertime"))));
			((TextView) findViewById(R.id.viewinspectionfee)).setText("$"+db.decode(c.getString(c.getColumnIndex("Inspectionfee"))));
			((TextView) findViewById(R.id.viewdiscount)).setText("$"+db.decode(c.getString(c.getColumnIndex("discount"))));
			((TextView) findViewById(R.id.viewtotalfee)).setText("$"+db.decode(c.getString(c.getColumnIndex("totalfee"))));
			Sh_assignedDate=db.decode(c.getString(c.getColumnIndex("Schedule_AssignedDate")));
			if(c!=null)
				c.close();
		}
	}

	private ProgressDialog pd;
	
	

	public void clicker(View v) {
		switch (v.getId()) {

		  case R.id.hme:
			 cf.go_home();
		  break;
		  case R.id.save:
	  if(!db.status.equals("1") && !db.substatus.equals("1") )
		{
			if(!cf.selected_insp_type.trim().equals("50"))
			{
			if (!"".equals(et_inspdate.getText().toString())) {
				/*  if (Validation_TodayDate(et_inspdate.getText().toString()) == "true") {
					  if (Validation_Assigndate(Sh_assignedDate, et_inspdate.getText().toString()) == "true") {*/
						  if (!"Select".equals(Sch_Spinnerstart.getSelectedItem().toString())) {
							  if (!"Select".equals(Sch_Spinnerend.getSelectedItem().toString())) {
								  if (spinvalidation(spin1, spin2) == "true") {
									  //||  schchk.isChecked())
									  /*  if (et_commenttxt.getText().toString().trim().equals("") || !et_commenttxt.isEnabled()) {
										if(et_commenttxt.getText().toString().trim().equals(""))
										  {
											cf.show_toast("Please enter the Scheduling Comments.", 1);
											 et_commenttxt.requestFocus();
										  }
										  else if(!et_commenttxt.isEnabled())
										  {
												cf.show_toast("Please check the reschedule option ", 1);
										  }
									  	   	
										}
										else
										{*/
											
											try
											{
											Cursor c12 = db.wdo_db.rawQuery("SELECT * FROM "
													+ db.policyholder + " WHERE PH_SRID='" + db.encode(cf.selectedhomeid)
													+ "' and PH_InspectorId='" + db.encode(db.Insp_id) + "'", null);
											
											c12.moveToFirst();
											 isinspected = c12.getString(c12.getColumnIndex("PH_IsInspected"));
											 if(c12!=null)
													c12.close();
											}
											catch(Exception e)
											{
												System.out.println("isinpecte getting "+e.getMessage());
											}
											
										if(isinspected!=null && isinspected.equals("1"))
										{ 
											cf.show_toast("You cannot schedule this record. This inspection has been moved to CIT status.", 1);
										}
										else
										{
											
											
											if (wb.isInternetOn()==true) {
												String source = "<b><font color=#00FF33>" + "Scheduling..."
														+ " . Please wait...</font></b>";
												pd = ProgressDialog.show(Schedule.this,
														"", Html.fromHtml(source), true);
												new Thread() {
													public void run() {
														try {
															String Chk_Inspector=wb.IsCurrentInspector(db.Insp_id,cf.selectedhomeid,"IsCurrentInspector");
															if(Chk_Inspector.equals("true"))
															{  
															
															  if (sendschedule() == "true") {
																db.wdo_db.execSQL("UPDATE "
																			+ db.policyholder
																			+ " SET PH_Status='40',Schedule_ScheduledDate='"
																			+ db.encode(et_inspdate.getText().toString())
																			+ "',"
																			+ "Schedule_Comments='"
																			+ db.encode(et_commenttxt.getText().toString())
																			+ "',Schedule_InspectionStartTime='"
																			+ db.encode(Sch_Spinnerstart.getSelectedItem().toString())
																			+ "',"
																			+ "Schedule_InspectionEndTime='"
																			+ db.encode(Sch_Spinnerend.getSelectedItem().toString())
																			+ "',ScheduleFlag='1' WHERE PH_InspectorId ='"
																			+ db.encode(db.Insp_id)
																			+ "' and PH_SRID='"
																			+ db.encode(cf.selectedhomeid)
																			+ "'");
																System.out.println("the status was"+onlstatus);
																if (onlstatus.equals("Assign")) {
																	
																	startActivity(new Intent(Schedule.this,Dashboard.class));
																}
																verify = 0;
																handler.sendEmptyMessage(0);
																pd.dismiss();
																
															}
															else
															{
																showerror();
															}
															}
															else
															{
																System.out
																		.println("chkins"+Chk_Inspector);
																verify = 2;
																handler.sendEmptyMessage(0);System.out.println("after handler");
																pd.dismiss();System.out.println("after dismiss");
																
															}
														} catch (SocketTimeoutException s) {
															verify = 5;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														} catch (NetworkErrorException n) {
															verify = 5;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														} catch (IOException io) {
															verify = 5;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														} catch (XmlPullParserException x) {
															verify = 5;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														} catch (Exception e) {
															verify = 5;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														}
													}

													private void showerror() {
														verify = 1;
														handler.sendEmptyMessage(0);
														pd.dismiss();
													}

													private Handler handler = new Handler() {
														@Override
														public void handleMessage(
																Message msg) {
															if (verify == 0) {
																if(schchk.isChecked()){
																cf.show_toast("Your inspection has been successfully re-scheduled.", 1);
																}
																else
																{
																	cf.show_toast("Your inspection has been successfully scheduled.", 1);
																}
																db.wdo_db.execSQL("UPDATE "
																		+ db.policyholder
																		+ " SET PH_Status='40',Schedule_ScheduledDate='"
																		+ db.encode(et_inspdate.getText().toString())
																		+ "',"
																		+ "Schedule_Comments='"
																		+ db.encode(et_commenttxt.getText().toString())
																		+ "',Schedule_InspectionStartTime='"
																		+ db.encode(Sch_Spinnerstart.getSelectedItem().toString())
																		+ "',"
																		+ "Schedule_InspectionEndTime='"
																		+ db.encode(Sch_Spinnerend.getSelectedItem().toString())
																		+ "',ScheduleFlag='1' WHERE PH_InspectorId ='"
																		+ db.encode(db.Insp_id)
																		+ "' and PH_SRID='"
																		+ db.encode(cf.selectedhomeid)
																		+ "'");
															} else if (verify == 1) {
																cf.show_toast("Your inspection has not been scheduled.", 1);
															} else if (verify == 5) {
																cf.show_toast("Please check your network connection and try again.", 1);
															}
															else if(verify == 2)
															{
																cf.show_toast("Sorry. This record has been reallocated to another inspector.", 1);
															}
														}
													};
												}.start();
												
											
												
												System.out.println("comes into the sch 6");
											} else {
												cf.show_toast("Internet connection not available.", 1);
												db.wdo_db.execSQL("UPDATE "
														+ db.policyholder
														+ " SET PH_Status='40',Schedule_ScheduledDate='"
														+ db.encode(et_inspdate.getText().toString())
														+ "',"
														+ "Schedule_Comments='"
														+ db.encode(et_commenttxt.getText().toString())
														+ "',Schedule_InspectionStartTime='"
														+ db.encode(Sch_Spinnerstart.getSelectedItem().toString())
														+ "',"
														+ "Schedule_InspectionEndTime='"
														+ db.encode(Sch_Spinnerend.getSelectedItem().toString())
														+ "',ScheduleFlag='0' WHERE PH_InspectorId ='"
														+ db.encode(db.Insp_id)
														+ "' and PH_SRID='"
														+ db.encode(cf.selectedhomeid) + "'");
												if (onlstatus.equals("Assign")) {
													startActivity(new Intent(Schedule.this,Dashboard.class));

												}
											}
											System.out.println("comes into the sch 7");
										}
										}
									//} 
								  else {
										 cf.show_toast("End Time should be greater than Start Time.", 1);
								      }
							  } else {
								   cf.show_toast("Please select End Time.", 1);
								   Sch_Spinnerend.requestFocus();
								}
						  }
						  else {
							  cf.show_toast("Please select Start Time.", 1);
							  Sch_Spinnerstart.requestFocus();
							}
						  /* }
					  else {
						  cf.show_toast("Schedule Date should be greater than or equal to Assigned Date.", 1);
						  et_inspdate.setText("");
						  et_inspdate.requestFocus();
						}
				  }
				  else {
					    cf.show_toast("Schedule Date should be greater than or equal to today date.", 1);
					    et_inspdate.setText("");
					    et_inspdate.requestFocus();
					}*/
			  }
			  else {
				  cf.show_toast("Please enter the Inspection Date.", 1);
				 et_inspdate.requestFocus();
				}
			}
			else
			{
				cf.show_toast("This record in Completed in Online Status so you can not schedule .", 1);
				 et_inspdate.requestFocus();
			}
		}
		else
		{
			cf.show_toast("Sorry already inspected record cannot Re Schedule", 0);
		}
			  break;
			case R.id.load_comments:
				int len=et_commenttxt.getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 2);
					in.putExtra("max_length", 500);
					in.putExtra("cur_length", len);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
				break;
		}
	}
 private String sendschedule() throws NetworkErrorException, IOException,
	SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
	 System.out.println("secndsc");
	    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
	    System.out.println("secndscenveloper");
		envelope.dotNet = true;
		SoapObject ad_property = new SoapObject(wb.NAMESPACE, "ExportScheduleInfo");
		System.out.println("secndscexportschedul");
		ad_property.addProperty("Srid", cf.selectedhomeid);System.out.println("srid"+cf.selectedhomeid);
		ad_property.addProperty("InspectorID", db.Insp_id);System.out.println("srid"+cf.selectedhomeid);
		ad_property.addProperty("ScheduleDate", et_inspdate.getText().toString());
		int spinnerstatPosition = mRangeadapter.getPosition(startspin);
		ad_property.addProperty("ScheduleStartTime", spinnerstatPosition);
		int spinnerendPosition = mRangeadapter.getPosition(endspin);
		ad_property.addProperty("ScheduleEndTime", spinnerendPosition);
		ad_property.addProperty("Comments", et_commenttxt.getText().toString());
		envelope.setOutputSoapObject(ad_property);
		System.out.println("the property "+ad_property);
		HttpTransportSE androidHttpTransport1 = new HttpTransportSE(wb.URL);

		androidHttpTransport1.call(wb.NAMESPACE+"ExportScheduleInfo", envelope);
		Object response = envelope.getResponse();
		System.out.println("responsee=="+response);
		if (response == null || response.toString().equals("")
				|| response.toString().equals("null")) {
			verify = 1;
		    return "false";
		} else if (response.toString().equals("true")) {
			verify = 0;
			return "true";
		} else {
			verify = 1;
			return "false";
		}
	}
private String spinvalidation(int spin12, int spin22) {
		// TODO Auto-generated method stub
		if (spin22 > spin12) {
			return "true";
		} else {
			return "false";
		}
	
	}
private String Validation_Assigndate(String sh_assignedDate,
			String string) {
		// TODO Auto-generated method stub
	 if (!sh_assignedDate.trim().equals("")
				|| sh_assignedDate.equals("N/A")
				|| sh_assignedDate.equals("Not Available")
				|| sh_assignedDate.equals("anytype")
				|| sh_assignedDate.equals("Null")) {
			String chkdate = null;
			int i1 = sh_assignedDate.indexOf("/");
			String result = sh_assignedDate.substring(0, i1);
			int i2 = sh_assignedDate.lastIndexOf("/");
			String result1 = sh_assignedDate.substring(i1 + 1, i2);
			String result2 = sh_assignedDate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = string.indexOf("/");
			String result3 = string.substring(0, i3);
			int i4 = string.lastIndexOf("/");
			String result4 = string.substring(i3 + 1, i4);
			String result5 = string.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);
			if (j > k) {
				chkdate = "false";
			} else if (j < k) {
				chkdate = "true";
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = "false";
				} else if (j1 < k1) {
					chkdate = "true";
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = "false";
					} else if (j2 < k2) {
						chkdate = "true";
					} else if (j2 == k2) {

						chkdate = "true";
					}

				}
	}

			return chkdate;
		} else {
			return "true";
		}
	}
private String Validation_TodayDate(String string) {
		// TODO Auto-generated method stub
	  	int i1 = string.indexOf("/");
		String result = string.substring(0, i1);
		int i2 = string.lastIndexOf("/");
		String result1 = string.substring(i1 + 1, i2);
		String result2 = string.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j > thsyr || (j1 > curmnth && j >= thsyr)
				|| (j2 >= curdate && j1 >= curmnth && j >= thsyr)) {
			return "true";
		} else {
			return "false";
		}

	}
public class MyOnItemSelectedListenerstart implements
	OnItemSelectedListener {

	   	public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			startspin = parent.getItemAtPosition(pos).toString();
			spin1 = parent.getSelectedItemPosition();
	    }
	
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
 }

public class MyOnItemSelectedListenerend implements OnItemSelectedListener {

     public void onItemSelected(AdapterView<?> parent, View view, int pos,
		long id) {
		endspin = parent.getItemAtPosition(pos).toString();
		spin2 = parent.getSelectedItemPosition();
    }

	public void onNothingSelected(AdapterView parent) {
		// Do nothing.
	}
}
private void Spinner_SetEndtime() {
	// TODO Auto-generated method stub
	mRangeadapter = new ArrayAdapter<CharSequence>(this,
			android.R.layout.simple_spinner_item);
	mRangeadapter
			.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	Sch_Spinnerend.setAdapter(mRangeadapter);
	mRangeadapter.add("Select");
	mRangeadapter.add("8:00 AM");
	mRangeadapter.add("8:30 AM");
	mRangeadapter.add("9:00 AM");
	mRangeadapter.add("9:30 AM");
	mRangeadapter.add("10:00 AM");
	mRangeadapter.add("10:30 AM");
	mRangeadapter.add("11:00 AM");
	mRangeadapter.add("11:30 AM");
	mRangeadapter.add("12:00 PM");
	mRangeadapter.add("12:30 PM");
	mRangeadapter.add("1:00 PM");
	mRangeadapter.add("1:30 PM");
	mRangeadapter.add("2:00 PM");
	mRangeadapter.add("2:30 PM");
	mRangeadapter.add("3:00 PM");
	mRangeadapter.add("3:30 PM");
	mRangeadapter.add("4:00 PM");
	mRangeadapter.add("4:30 PM");
	mRangeadapter.add("5:00 PM");
	mRangeadapter.add("5:30 PM");
	mRangeadapter.add("6:00 PM");
	mRangeadapter.add("6:30 PM");
	mRangeadapter.add("7:00 PM");
}
private void Spinner_SetStarttime() {
	// TODO Auto-generated method stub
	mRangeadapter = new ArrayAdapter<CharSequence>(this,
			android.R.layout.simple_spinner_item);
	mRangeadapter
			.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	Sch_Spinnerstart.setAdapter(mRangeadapter);
	mRangeadapter.add("Select");
	mRangeadapter.add("8:00 AM");
	mRangeadapter.add("8:30 AM");
	mRangeadapter.add("9:00 AM");
	mRangeadapter.add("9:30 AM");
	mRangeadapter.add("10:00 AM");
	mRangeadapter.add("10:30 AM");
	mRangeadapter.add("11:00 AM");
	mRangeadapter.add("11:30 AM");
	mRangeadapter.add("12:00 PM");
	mRangeadapter.add("12:30 PM");
	mRangeadapter.add("1:00 PM");
	mRangeadapter.add("1:30 PM");
	mRangeadapter.add("2:00 PM");
	mRangeadapter.add("2:30 PM");
	mRangeadapter.add("3:00 PM");
	mRangeadapter.add("3:30 PM");
	mRangeadapter.add("4:00 PM");
	mRangeadapter.add("4:30 PM");
	mRangeadapter.add("5:00 PM");
	mRangeadapter.add("5:30 PM");
	mRangeadapter.add("6:00 PM");
	mRangeadapter.add("6:30 PM");
	mRangeadapter.add("7:00 PM");
}
 public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goback(12);
			cf.go_back(CallAttempt.class);
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 if(requestCode==cf.loadcomment_code)
		{
				load_comment=true;
				if(resultCode==RESULT_OK)
				{
					et_commenttxt.setText((et_commenttxt.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
					
				}
				else if(resultCode==RESULT_CANCELED)
				{
					//cf.show_toast("You have canceled  the comments selction ",0);
				}
		}		 		
 		

 	}
 
}
