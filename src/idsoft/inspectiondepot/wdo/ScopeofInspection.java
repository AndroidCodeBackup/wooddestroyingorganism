package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

public class ScopeofInspection extends Activity {
CommonFunction cf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		setContentView(R.layout.scope_of_inspection);
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Inspection Information=>Scope of Inspection",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,1,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goback(12);
			cf.go_back(policyholderInformation.class);
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.Next:
				cf.go_back(InspectionsFindings.class);
			break;
		}
	}
}
