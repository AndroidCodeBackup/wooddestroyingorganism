package idsoft.inspectiondepot.wdo;


import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeScreen extends Activity {
	CommonFunction cf;
	private ProgressDialog pd;
	byte[] raw;
	DataBaseHelper db;
	Webservice_Function wb;
	int messsage;
	String newcode,newversion,apk_uri="",url;
	int vcode,show_handler;
	String versionname, newversionname, newversioncode;
	AlertDialog alertDialog;
	ScaleAnimation zoom;
	AnimationSet animSet;
	ImageView checkforupdates;
	PowerManager.WakeLock wl=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		wb=new Webservice_Function(this);
//		findViewById(R.id.head_policy_info).setVisibility(View.INVISIBLE);
//		findViewById(R.id.head_take_image).setVisibility(View.INVISIBLE);
		
		db.CreateTable(14);
		db.getInspectorId();
		try{
			 Cursor c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='1' and TC_Comments='"+db.encode("Not Applicable")+"'");
		     if(c.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','1','"+db.encode("Not Applicable")+"','true','')");	
					
		     }
		     
		     //Schedule
		     Cursor c1 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Home is on lock box")+"'");
		     if(c1.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Home is on lock box")+"','true','')");	
					
		     }
		     
		     Cursor c2 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Call client before you leave inspection")+"'");
		     if(c2.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Call client before you leave inspection")+"','true','')");	
					
		     }
		     
		     Cursor c3 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Call agent if any problems")+"'");
		     if(c3.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Call agent if any problems")+"','true','')");	
					
		     }
		     
		     Cursor c4 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Closing within one week")+"'");
		     if(c4.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Closing within one week")+"','true','')");	
					
		     }
		     
		     Cursor c5 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Prior termites disclosed")+"'");
		     if(c5.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Prior termites disclosed")+"','true','')");	
					
		     }
		     
		     Cursor c6 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='2' and TC_Comments='"+db.encode("Prior treatment disclosed")+"'");
		     if(c6.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','2','"+db.encode("Prior treatment disclosed")+"','true','')");	
					
		     }
		     
		     //locations
		     Cursor c7 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Exterior")+"'");
		     if(c7.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Exterior")+"','true','')");	
					
		     }
		     
		     Cursor c8 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Front Door")+"'");
		     if(c8.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Front Door")+"','true','')");	
					
		     }
		     
		     Cursor c9 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Garage Door")+"'");
		     if(c9.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Garage Door")+"','true','')");	
					
		     }
		     
		     Cursor c10 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Door Frame")+"'");
		     if(c10.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Door Frame")+"','true','')");	
					
		     }
		     
		     Cursor c11 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Siding along rear")+"'");
		     if(c11.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Siding along rear")+"','true','')");	
					
		     }
		     
		     Cursor c12 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Interior")+"'");
		     if(c12.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Interior")+"','true','')");	
					
		     }
		     
		     Cursor c13 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='3' and TC_Comments='"+db.encode("Baseboard")+"'");
		     if(c13.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','3','"+db.encode("Baseboard")+"','true','')");	
					
		     }
		     
		     //treatment observed
		     Cursor c14 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='4' and TC_Comments='"+db.encode("Drill holes noted")+"'");
		     if(c14.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','4','"+db.encode("Drill holes noted")+"','true','')");	
					
		     }
		     
		     Cursor c15 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='4' and TC_Comments='"+db.encode("Baiting stations present")+"'");
		     if(c15.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','4','"+db.encode("Baiting stations present")+"','true','')");	
					
		     }
		     
		     Cursor c16 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='4' and TC_Comments='"+db.encode("Past termite activity disclosed")+"'");
		     if(c16.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','4','"+db.encode("Past termite activity disclosed")+"','true','')");	
					
		     }
		     
		     //treatemnt comments
		     Cursor c17 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='5' and TC_Comments='"+db.encode("Sticker noted only partial treatment to")+"'");
		     if(c17.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','5','"+db.encode("Sticker noted only partial treatment to")+"','true','')");	
					
		     }
		     
		     Cursor c18 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='5' and TC_Comments='"+db.encode("Partial treatment to")+"'");
		     if(c18.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','5','"+db.encode("Partial treatment to")+"','true','')");	
					
		     }
		     
		     //reason
		     Cursor c19 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='6' and TC_Comments='"+db.encode("No clearance due to limited headroom")+"'");
		     if(c19.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','6','"+db.encode("No clearance due to limited headroom")+"','true','')");	
					
		     }
		     
		     Cursor c20 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='6' and TC_Comments='"+db.encode("Extensive storage present")+"'");
		     if(c20.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','6','"+db.encode("Extensive storage present")+"','true','')");	
					
		     }
		     
		     Cursor c21 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='6' and TC_Comments='"+db.encode("Storage/belongings impeded inspection")+"'");
		     if(c21.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','6','"+db.encode("Storage/belongings impeded inspection")+"','true','')");	
					
		     }
		     
		     Cursor c22 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='6' and TC_Comments='"+db.encode("Furniture present within home. Not moved as part of inspection")+"'");
		     if(c22.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','6','"+db.encode("Furniture present within home. Not moved as part of inspection")+"','true','')");	
					
		     }
		    
		    //comments
		     Cursor c23 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='7' and TC_Comments='"+db.encode("Not Applicable")+"'");
		     if(c23.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','7','"+db.encode("Not Applicable")+"','true','')");	
					
		     }
		     
		     //addendum
		     Cursor c24 =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_Question='9' and TC_Comments='"+db.encode("Not Applicable")+"'");
		     if(c24.getCount()<=0)
		     {
		    	 db.wdo_db.execSQL("INSERT INTO "
							+ db.To_addcomments
							+ " (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status,TC_CreatedOn)"
							+ " VALUES ('" + db.Insp_id+"','','1','9','"+db.encode("Not Applicable")+"','true','')");	
					
		     }
		
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("error="+e.getMessage());
		}
		
		db.CreateTable(2);
		try {
			Cursor cur = db.wdo_db.rawQuery("select * from " + db.policyholder, null);
			cur.moveToFirst();
			int result = cur.getColumnIndex("preferdate");
			if (result == -1) 
			{
				db.wdo_db.execSQL("ALTER TABLE " + db.policyholder+" ADD COLUMN preferdate VARCHAR default ''");
			}
			int result1 = cur.getColumnIndex("prefertime");
			if (result1 == -1) 
			{
				db.wdo_db.execSQL("ALTER TABLE " + db.policyholder+" ADD COLUMN prefertime VARCHAR default ''");
			}
			int result2 = cur.getColumnIndex("Inspectionfee");
			if (result2 == -1) 
			{
				db.wdo_db.execSQL("ALTER TABLE " + db.policyholder+" ADD COLUMN Inspectionfee VARCHAR default ''");
			}
			int result3 = cur.getColumnIndex("discount");
			if (result3 == -1) 
			{
				db.wdo_db.execSQL("ALTER TABLE " + db.policyholder+" ADD COLUMN discount VARCHAR default ''");
			}
			int result4 = cur.getColumnIndex("totalfee");
			if (result4 == -1) 
			{
				db.wdo_db.execSQL("ALTER TABLE " + db.policyholder+" ADD COLUMN totalfee VARCHAR default ''");
			}
		}
       catch (Exception e) {
		// TODO: handle exception
	    }
		checkforupdates=(ImageView)findViewById(R.id.home_checkforup);
		
		/*findViewById(R.id.head_insp_info).setVisibility(View.INVISIBLE);*/
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(HomeScreen.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		
//		Animation code
		
		zoom = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
		animSet = new AnimationSet(false);
		zoom.setRepeatMode(Animation.REVERSE);
		zoom.setRepeatCount(Animation.INFINITE);
		animSet.addAnimation(zoom);
		animSet.setDuration(300);
		AlphaAnimation alpha = new AlphaAnimation(1, (float)0.9);
        alpha.setDuration(300); // Make animation instant
        alpha.setRepeatMode(Animation.REVERSE);
        alpha.setRepeatCount(Animation.INFINITE);
        animSet.addAnimation(alpha);
        
//        End of Animation code
		
		show_insp_info();
		check_for_animation();
		try
		{
			TextView releasecode=(TextView) findViewById(R.id.RC_date);
			if(wb.URL.contains("idmaws.paperless-inspectors.com"))
				releasecode.setText(cf.apkrc +"  V:"+getPackageManager().getPackageInfo(getPackageName(), 0).versionName); 
	   		else
	   			releasecode.setText((cf.apkrc.replace("U", "L"))+"  V:"+getPackageManager().getPackageInfo(getPackageName(), 0).versionName); 
	   			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		cf.get_appstat();
		if(cf.application_sta)
		{
			findViewById(R.id.Log_out).setVisibility(View.GONE);
		}
		else
		{
			findViewById(R.id.Log_out).setVisibility(View.VISIBLE);
			findViewById(R.id.checknewinsp).setVisibility(View.GONE);
			findViewById(R.id.backtomainmenu).setVisibility(View.GONE);
		}
	}
	private void check_for_animation() {
		// TODO Auto-generated method stub
		/** Start animating for the  Check for updates**/
		 db.CreateTable(0);
			Cursor c12 = db.SelectTablefunction(db.wdoVersion, " ");   
			c12.moveToFirst();
		
			if (c12.getCount() >= 1) {
				if (c12.getInt(1)>getcurrentversioncode()) {
					try
					{
//							zoom = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
//							animSet = new AnimationSet(false);
//							zoom.setRepeatMode(Animation.REVERSE);
//							zoom.setRepeatCount(Animation.INFINITE);
//							animSet.addAnimation(zoom);
//							animSet.setDuration(300);
//							AlphaAnimation alpha = new AlphaAnimation(1, (float)0.9);
//					        alpha.setDuration(300); // Make animation instant
//					        alpha.setRepeatMode(Animation.REVERSE);
//					        alpha.setRepeatCount(Animation.INFINITE);
//					        animSet.addAnimation(alpha);
					    	checkforupdates.setAnimation(animSet);
							animSet.start();
					}
					catch(Exception e)
					{
						
					}
				
				} 
			}
			if(c12!=null)
				c12.close();
			db.CreateTable(23);
			Cursor c13 = db.SelectTablefunction(db.IDMAVersion, " ");   
			c13.moveToFirst();
		
			if (c13.getCount() >= 2) {
				int old=c13.getInt(1);
				c13.moveToNext();
				int new_v=c13.getInt(1);
				
				if (new_v>old) {
					try
					{
						((Button) findViewById(R.id.checknewinsp)).setAnimation(animSet);
						animSet.start();
					}
					catch(Exception e)
					{
						
					}
				
				} 
			}
			if(c13!=null)
				c13.close();
			/** End animating for the  Check foru pdates**/
		
	}
	private void show_insp_info() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("comes correct");
		((TextView) findViewById(R.id.insp_name)).setText(db.Insp_firstname+" "+db.Insp_lastname);
		File f =new File(this.getFilesDir()+"/"+db.Insp_id+db.Insp_ext);
		System.out.println("comes correct exist"+f.exists());
		if(f.exists())
		{
			Bitmap b =cf.ShrinkBitmap(this.getFilesDir()+"/"+db.Insp_id+db.Insp_ext, 150, 150);
			((ImageView) findViewById(R.id.insp_photo)).setImageBitmap(b);
		}
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("some issues "+e.getMessage());
		}
		
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.home_strt:
				Intent in =new Intent(this,Dashboard.class);
				startActivity(in);
			break;
			case R.id.addcomments:
				Intent in1 =new Intent(this,AddComments.class);
				startActivity(in1);
			break;
			case R.id.home_delete:
				Intent in12 = new Intent(this,HomwOwnerListing.class);
				in12.putExtra("status", "ALL");
				in12.putExtra("type_id", "");
				startActivity(in12);
			break;
			case R.id.home_import:
				if(wb.isInternetOn())
				{
					cf.go_back(Import.class);
				}
				else
				{
					cf.show_toast("Please enable internet connection then try import", 0);
				}
				
			break;
			case R.id.home_export:

				Intent submitint = new Intent(getApplicationContext(),Export.class);
				submitint.putExtra("type", "export");
				submitint.putExtra("classidentifier", "HomeScreen");
				startActivity(submitint);
			break;
			/*case R.id.home_search:
				online_search();
			break;*/
			case R.id.home_checkforup:
				//cf.show_toast("Button under construction", 1);
				if(wb.isInternetOn())
				{
				cf.show_ProgressDialog("Checking for updates");
				 new  Thread() {

					public void run()
					{
						try
						{
							if(check_forversion(0))
							{
								messsage=1;
								handler_version.sendEmptyMessage(0);
							}else
							{
								messsage=2;
								handler_version.sendEmptyMessage(0);
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
							messsage=3;
							handler_version.sendEmptyMessage(0);
						}
					};
				}.start();
				}else
				{
					cf.show_toast("Please enable your internet connection", 0);
				}
				
				/*if(animSet.hasStarted())
				{
//					checkforupdates.clearAnimation();
					Update_Alert_Market();
				}
				else
				{
					check_for_updates();
				}*/
				
				
				
			break;
			case R.id.backtomainmenu:
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				startActivity(intent);
			break;
			case R.id.checknewinsp:
				if(wb.isInternetOn())
				{
				cf.show_ProgressDialog("Checking for updates");
				 new  Thread() {

					public void run()
					{
						try
						{
							if(check_forversion(1))
							{
								messsage=11;
								handler_version.sendEmptyMessage(0);
							}else
							{
								messsage=21;
								handler_version.sendEmptyMessage(0);
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
							messsage=31;
							handler_version.sendEmptyMessage(0);
						}
					};
				}.start();
				}else
				{
					cf.show_toast("Please enable your internet connection", 0);
				}
			break;
			case R.id.backupcomments:
				db.CreateTable(14);
				final Cursor c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"'");
				if(c.getCount()>0)
				{
					/*if(cf.isInternetOn())
					 {*/
					AlertDialog.Builder b =new AlertDialog.Builder(HomeScreen.this);
					b.setTitle("Confirmation");
					b.setMessage("Previous backup will be cleared and new backup will be created. \n" +
							"Are you sure, Do you want to Get backup of your comments?");
					b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							 getbackup(c);
						}
					});
					b.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
					dialog.cancel();
						}
					});   
					AlertDialog al=b.create();
					al.setIcon(R.drawable.alertmsg);
					al.setCancelable(false);
					al.show(); 
					/* }
					 else
					 {
						 cf.ShowToast("Please enable your internet connection", 0);
					 }*/
				
				}
				else{
					cf.show_toast("You don't have comments to take backup",0);
				}
				if(c!=null)
					c.close();
			break;
			case R.id.restorecomments:
				/*if(cf.isInternetOn())
				 {*/
				 AlertDialog.Builder b1 =new AlertDialog.Builder(HomeScreen.this);
					b1.setTitle("Confirmation");
					b1.setMessage("Only the comments which you got backup will be restored other may be lost. \n" +
							"Are you sure, Do you want to Restore your comments?");
					b1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							restoredata();
						}
					});
					b1.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});   
					AlertDialog a=b1.create();
					a.setIcon(R.drawable.alertmsg);
					a.setCancelable(false);
					a.show(); 
				/* }
				 else
				 {
					 cf.ShowToast("Please enable your internet connection", 0);
				 }*/
			break;
			case R.id.config_option:
				Intent in2 =new Intent(this,Config_option.class);
				startActivity(in2);
			break;
			case R.id.Log_out:
				db.wdo_db.execSQL(" UPDATE "+db.inspectorlogin+" SET Fld_InspectorFlag='0'");
				startActivity(new Intent(this,Login_page.class));
			break;
			
			case R.id.placeorder:
				Intent intentorder=new Intent(HomeScreen.this,OrderInspection.class);
				startActivity(intentorder);
				finish();
			break;
			
			case R.id.emailreport:
				Intent intentemailreport=new Intent(HomeScreen.this,EmailReport.class);
				startActivity(intentemailreport);
				finish();
			break;
			
		}
	}
	
	
	 private boolean check_forversion(int i) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException {
		// TODO Auto-generated method stub
		if(i==0)
		{
		 SoapObject request = new SoapObject(wb.NAMESPACE, "GETVERSIONINFORMATION");
		 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		 envelope.dotNet = true;
		 envelope.setOutputSoapObject(request);
		 HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

		 try {
			androidHttpTransport.call(wb.NAMESPACE+"GETVERSIONINFORMATION", envelope);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 SoapObject result = (SoapObject) envelope.getResponse();
		 SoapObject obj = (SoapObject) result.getProperty(0);
		 newcode = String.valueOf(obj.getProperty("VersionCode"));
		 newversion = String.valueOf(obj.getProperty("VersionName"));
		 apk_uri= String.valueOf(obj.getProperty("Apkurl"));
		 System.out.println("its works fine"+apk_uri);
		 System.out.println("comes in the correct way1"+newversion);
		 if (!"null".equals(newcode) && null != newcode && !newcode.equals(""))
		 {
			 
			
			 if (getcurrentversioncode()<Integer.parseInt(newcode)) 
			 {
				
				 return true;
			 } 
		 }
		 return false;
		}
		else
		{
			 SoapObject request = new SoapObject(wb.NAMESPACE, "GETVERSIONINFORMATION_IDMA");
			 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			 envelope.dotNet = true;
			 envelope.setOutputSoapObject(request);
			 HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			 try {
				androidHttpTransport.call(wb.NAMESPACE+"GETVERSIONINFORMATION_IDMA", envelope);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 SoapObject result = (SoapObject) envelope.getResponse();
			 SoapObject obj = (SoapObject) result.getProperty(0);
			 newcode = String.valueOf(obj.getProperty("VersionCode"));
			 newversion = String.valueOf(obj.getProperty("VersionName"));
			 apk_uri= String.valueOf(obj.getProperty("Apkurl"));
			 System.out.println("its works fine"+apk_uri);
			 System.out.println("comes in the correct way1"+newversion);
			 int newvcode=0;
				try
				{
				newvcode=Integer.parseInt(newcode);System.out.println("newco"+newcode);
				}catch (Exception e) {
					// TODO: handle exception
				}
				
				if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
					
					Cursor c12 = db.SelectTablefunction(db.IDMAVersion," Where ID_VersionType='IDMA_Old' ");
					if(c12.getCount()>=1)
					{
						c12.moveToFirst();
						int V_code=c12.getInt(c12.getColumnIndex("ID_VersionCode"));
						if(c12!=null)
							c12.close();
						if(V_code<newvcode)
						{
							return true;
							
						}
						else
						{
							return false;
						}
						
						
					}
					else
					{
						
						return false;
					}
					
					
				}else
				{
					
					return false;
					
				}
			 
		}
	}
	 private int getcurrentversioncode() {
			// TODO Auto-generated method stub
		 int vcode=0;
			try {
				vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vcode;
		}
	private void getbackup(final Cursor c) {

		 db.CreateTable(14);
			String source = "<font color=#FFFFFF>Getting back up your comments. Please wait..."
					+ "</font>";		        
			pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
			 new Thread() {
					public void run() {
			try
			{
		
				
	      
			SQLiteDatabase Secondry_db = HomeScreen.this.openOrCreateDatabase("Temp", 1, null); // Create new database for sperate the table 
			         
			Secondry_db.execSQL("CREATE TABLE IF NOT EXISTS "+db.To_addcomments+" (TC_Id INTEGER PRIMARY KEY AUTOINCREMENT,TC_InspectorId varchar(50) NOT NULL,TC_SRID varchar(50) NOT NULL,TC_type varchar(15) NOT NULL,TC_Question varchar(15) NOT NULL,TC_Comments varchar(500),TC_Status varchar(5) NOT NULL,TC_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))"); // Created the table in the new database
			Secondry_db.execSQL("Delete from "+db.To_addcomments); //Delete if the database contains data already
		
			c.moveToFirst();
			int max =100,min=0;
			if(c.getCount()>0)
			{
				
				for(int m =0;m<c.getCount();)
				{
					
					String s="";
						for(int i=0;i<c.getColumnCount();i++)
						{
						
							if(i+1<c.getColumnCount())
							 s+="'"+c.getString(i)+"' as "+c.getColumnName(i)+" , ";
							else
								s+="'"+c.getString(i)+"' as "+c.getColumnName(i)+"";
							
						}
						
						
						c.moveToNext();
						m++;
						int j=1;
						for(j=min+1;j<c.getCount() && j<max;j++)
						{
							s+=" UNION SELECT  ";
							 s+="'"+c.getString(0)+"','"+c.getString(1)+"','"+c.getString(2)+"' ,'"+c.getString(3)+"' ,'"+c.getString(4)+"' ,'"+c.getString(5)+"' ,'"+c.getString(6)+"' ,'"+c.getString(7)+"' ";
							c.moveToNext();
							m++;
						}
						min=min+100;
						max=max+100;
						System.out.println(" the s values "+s);
						Secondry_db.execSQL("INSERT into "+db.To_addcomments+" Select "+s);//insert the data into the new table
					}
				System.out.println(" the s values 1 ");
			File data = Environment.getDataDirectory();
			String backupDBPath ="/data/idsoft.inspectiondepot.wdo/databases/Temp";
			File currentDB = new File(data, backupDBPath);
			if(currentDB.exists())
	        {
	        	 RandomAccessFile f = new RandomAccessFile(currentDB, "r");
	             try {
	                 // Get and check length
	                 long longlength = f.length();
	                 int length = (int) longlength;
	                 if (length != longlength)
	                     throw new IOException("File size >= 2 GB");
	                 // Read file and return data
	                 raw = new byte[length];
	                 f.readFully(raw);
	             }
	             finally {
	                 f.close();
	             }
	                 try
	                 {
	                	 
	                SoapObject request = new SoapObject(wb.NAMESPACE,"ANDROIDIMAGECOMMETNS"); 
	 				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	 				envelope.dotNet = true; 
	 				MarshalBase64 marshal = new MarshalBase64();
	 				request.addProperty("inspectorID",Integer.parseInt(db.Insp_id));
	 				request.addProperty("Comments",raw);
	 				request.addProperty("Filenamewithext","ARRtoaddcomments.sql");
	 				marshal.register(envelope);
	 				envelope.setOutputSoapObject(request);System.out.println("ANDROIDIMAGECOMMETNS"+request);
	 				
	 				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
	 				androidHttpTransport.call(wb.NAMESPACE+"ANDROIDIMAGECOMMETNS ",envelope);
	 				String result =  envelope.getResponse().toString();
	 				System.out.println("Res="+result);
	 				messsage=4;
	 				handler5.sendEmptyMessage(0);
	                 
	             } 
	             catch(Exception e)
	             {
	            	 System.out.println("catc"+e.getMessage());	
	            	 messsage=3;
		 				handler5.sendEmptyMessage(0);
	             }
	           }
			}
			else
			{
				messsage=3;
				handler5.sendEmptyMessage(0);
			}
	        
				}
			        catch (Exception e) {
						// TODO: handle exception
			        	messsage=0;
			        	handler5.sendEmptyMessage(0);
					}

					}
			 }.start();
			 
		}
	 
	 private void restoredata() {

			// TODO Auto-generated method stub
		 db.CreateTable(14);
				String source = "<font color=#FFFFFF>Retrieving comments. Please wait..."
						+ "</font>";		        
				pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
				 new Thread() {
						public void run() {

				try {
					
					SoapObject request = new SoapObject(wb.NAMESPACE,"GETANDROIDIMAGECOMMETNS");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("inspectorID",Integer.parseInt(db.Insp_id));
					envelope.setOutputSoapObject(request);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
					androidHttpTransport.call(wb.NAMESPACE+"GETANDROIDIMAGECOMMETNS",envelope);System.out.println("request"+request);
					String result =  envelope.getResponse().toString();System.out.println("GETANDROIDIMAGECOMMETNS"+result);
					
					if(result.equals("AAAAAAAAAAAAAA=="))
					{
						messsage=8; 
						handler5.sendEmptyMessage(0);
					}
					else if(result.equals("null"))
					{
						messsage=2; 
						handler5.sendEmptyMessage(0);						
					}
					else
					{
						byte[] b = Base64.decode(result.toString());
						String backupDBPath ="/data/idsoft.inspectiondepot.wdo/databases/Temp";
						File data = Environment.getDataDirectory();
						File backupDB = new File(data, backupDBPath);
											
						FileOutputStream out = new FileOutputStream(backupDB);
						out.write(b);
						out.close();
						
						SQLiteDatabase Secondry_db = HomeScreen.this.openOrCreateDatabase("Temp", 1, null);
						Cursor c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"'");
						Cursor c2=Secondry_db.rawQuery(" SELECT * FROM "+db.To_addcomments+ " WHERE TC_InspectorId='"+db.Insp_id+"'", null);
						db.CreateTable(14);
						
						c2.moveToFirst();
						int max =100,min=0;
						if(c2.getCount()>0)
						{
							db.wdo_db.execSQL("Delete from "+db.To_addcomments+" WHERE TC_InspectorId='"+db.Insp_id+"'");
							for(int m=0;m<c2.getCount();) // this was used only 500 insert allowed per query so we are divide the whole entry in to no of querys
							{
									String s="";
								for(int i=0;i<c2.getColumnCount();i++)
								{
									if(i+1<c2.getColumnCount())
									 s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+" , ";
									else
										s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+"";
								}
								c2.moveToNext();
								m++;
								for(int j=min+1;j<c2.getCount() && j<max;j++)
								{
									s+=" UNION SELECT  ";
									s+="'"+c2.getString(0)+"','"+c2.getString(1)+"','"+c2.getString(2)+"' ,'"+c2.getString(3)+"' ,'"+c2.getString(4)+"' ,'"+c2.getString(5)+"' ,'"+c2.getString(6)+"' ,'"+c2.getString(7)+"'  ";
									m++;
									c2.moveToNext();
								}
								min=min+100;
								max=max+100;
							
								db.wdo_db.execSQL("INSERT into "+db.To_addcomments+" Select "+s);
								messsage=1; 
							}
							
							handler5.sendEmptyMessage(0);
						}
						if(c!=null)
							c.close();
						if(c2!=null)
							c2.close();
					}

					
				}catch (Exception e) {
					// TODO: handle exception
					
				System.out.println("the error was="+e.getMessage());
				handler5.sendEmptyMessage(0);
				
			}
						}
				 }.start();
				
				  
		}
	 final Handler handler5 = new Handler() {

			

			public void handleMessage(Message msg) {
				if(messsage==1)
					cf.show_toast("Comments retrieving completed ", 0);
				else if(messsage==2)
					cf.show_toast("No backup available" , 0);
				else if(messsage==3)
					cf.show_toast("Sorry you don't have any comments to get backup", 1);
				else if(messsage==4)
					cf.show_toast("You have successfully backed up your comments", 1);
				else if(messsage==8)
					cf.show_toast("You don't have comments to restore.", 1);
				else
					cf.show_toast("Some internet problem occurred please try again later with good internet connection", 0);
				pd.dismiss();
			}
			
	};
	 final Handler handler_version = new Handler() {

			

			public void handleMessage(Message msg) {
				
				if(messsage==1)
					update_apk();
				else if(messsage==2)
					cf.show_toast("You don't have any updates to upgrade" , 0);
				else if(messsage==11)
					update_idam_apk();
				else if(messsage==21)
					cf.show_toast("You don't have any updates to upgrade" , 0);
				else if(messsage!=5)
					cf.show_toast("Some internet problem occurred. Please try again later with good internet connection.", 0);
				cf.pd.dismiss();
				if(wl!=null)
				{
				wl.release();
				wl=null;
				}
			}

		

			
			
	};
	private void update_idam_apk() {
		// TODO Auto-generated method stub
		final AlertDialog alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
		alertDialog.setMessage("New IDMA Version of "+ newversion+ " for IDMA is available,The Previous data will not be affected, Would you like to Upgrade.");
		alertDialog.setButton(Dialog.BUTTON_POSITIVE,"Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						try {
							if (alertDialog.isShowing()) {

								alertDialog.cancel();

								alertDialog.dismiss();
								cf.show_ProgressDialog("Please wait ...");
								 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
								 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
								 wl.acquire();
								new Thread(){
									public void run()
									{
										try
										{
											SoapObject request = new SoapObject(wb.NAMESPACE, "GETCURRENTAPKFILE_IDMA");
										
										request.addProperty("InspectorID",db.Insp_id);
										SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
										envelope.dotNet = true;
										envelope.setOutputSoapObject(request);
										HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
										
										androidHttpTransport.call("http://tempuri.org/GETCURRENTAPKFILE_IDMA", envelope);

										Object response = envelope.getResponse();
										
										byte[] b;
										if (wb.check_result(response.toString())) 
										{
											b = Base64.decode(response.toString());
											try {
												String PATH = Environment.getExternalStorageDirectory()+ "/Download";
												File file = new File(PATH);
												file.mkdirs();
												File outputFile = new File(PATH + "/IDMA.apk");
												FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
												fileOuputStream.write(b);
												fileOuputStream.close();
												Intent intent = new Intent(Intent.ACTION_VIEW);
												intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ "/Download/IDMA.apk")),"application/vnd.android.package-archive");
												startActivityForResult(intent, 1);
												messsage=5;
												handler_version.sendEmptyMessage(0);
											} catch (IOException e) {
												messsage=6;
												handler_version.sendEmptyMessage(0);
												//cf.show_toast("Update error!",0);
											}
										} else {
											messsage=6;
											handler_version.sendEmptyMessage(0);
											//cf.show_toast("There is a problem on your Network.\n Please try again later with better Network.",0);
										}
										}catch (Exception e) {
											// TODO: handle exception
											messsage=6;
											handler_version.sendEmptyMessage(0);
										}
										
									};
								}.start();
							}
						} catch (Exception e) {
							messsage=3;
							handler_version.sendEmptyMessage(0);
						}
					}
				});
		alertDialog.setButton(Dialog.BUTTON_NEGATIVE,"No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		}); 
			
		alertDialog.show();
				
	}
	private void update_apk() {
		// TODO Auto-generated method stub
		final AlertDialog alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
alertDialog.setMessage("New Version "+newversion+" of Wood Destroying Organism inspection is available. The Previous data will not be affected , Would you like to upgrade ?");
alertDialog.setButton(Dialog.BUTTON_POSITIVE,"Yes",
		new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				try {
					if (alertDialog.isShowing()) {

						alertDialog.cancel();

						alertDialog.dismiss();
						cf.show_ProgressDialog("Please wait ...");
						 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
						 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
						 wl.acquire();
						new Thread(){
							public void run()
							{
								try
								{
									SoapObject request = new SoapObject(wb.NAMESPACE, "GETCURRENTAPKFILE");
								
								request.addProperty("InspectorID",db.Insp_id);
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
								envelope.dotNet = true;
								envelope.setOutputSoapObject(request);
								HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
								
								androidHttpTransport.call("http://tempuri.org/GETCURRENTAPKFILE", envelope);

								Object response = envelope.getResponse();
								
								byte[] b;
								if (wb.check_result(response.toString())) 
								{
									b = Base64.decode(response.toString());
									try {
										String PATH = Environment.getExternalStorageDirectory()+ "/Download";
										File file = new File(PATH);
										file.mkdirs();
										File outputFile = new File(PATH + "/WoodDestroyingOrganism.apk");
										FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
										fileOuputStream.write(b);
										fileOuputStream.close();
										Intent intent = new Intent(Intent.ACTION_VIEW);
										intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ "/Download/WoodDestroyingOrganism.apk")),"application/vnd.android.package-archive");
										startActivity(intent);
										messsage=5;
										handler_version.sendEmptyMessage(0);
									} catch (IOException e) {
										messsage=6;
										handler_version.sendEmptyMessage(0);
										//cf.show_toast("Update error!",0);
									}
								} else {
									messsage=6;
									handler_version.sendEmptyMessage(0);
									//cf.show_toast("There is a problem on your Network.\n Please try again later with better Network.",0);
								}
								}catch (Exception e) {
									// TODO: handle exception
									messsage=6;
									handler_version.sendEmptyMessage(0);
								}
								/*if(!apk_uri.equals(""))
								{
									Intent intent = new Intent(Intent.ACTION_VIEW);
									intent.setDataAndType(Uri.parse("http://idsoftsource.in/WoodDestroyingOrganism.apk"),
											"application/vnd.android.package-archive");
									System.out.println("comes correctly");
									
									Uri myUri = Uri.parse("http://idsoftsource.in/WoodDestroyingOrganism.apk");
									//System.out.println("URI "+myUri);
									
					                Intent myIntent = new Intent(Intent.ACTION_VIEW, myUri);
					                try {
					                	startActivityForResult(myIntent,12);
					                    cf.pd.dismiss();
					                } catch (ActivityNotFoundException ex) {
					                    System.out.println("sorry you have error"+ex.getMessage());
					                }
								}*/
							};
						}.start();
					}
				} catch (Exception e) {
					messsage=3;
					handler_version.sendEmptyMessage(0);
				}
			}
		});
alertDialog.setButton(Dialog.BUTTON_NEGATIVE,"No", new DialogInterface.OnClickListener() {
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		alertDialog.dismiss();
	}
}); 
	
alertDialog.show();
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==1)
		{
			String PATH = Environment.getExternalStorageDirectory()+ "/Download/IDMA.apk";
			File file = new File(PATH);
			file.delete();
			
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if(cf.application_sta)
		{
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
			startActivity(intent);
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		}
		else
		{
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
		}
		return super.onKeyDown(keyCode, event);
	}
 
}
