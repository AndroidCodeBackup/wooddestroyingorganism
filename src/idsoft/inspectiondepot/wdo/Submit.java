package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class Submit extends Activity{
	CommonFunction cf;
	DataBaseHelper db;
	EditText etsetword,etgetword;
	private String word;
	private String[] myString;
	CheckBox handout;
	private static final Random rgenerator = new Random();
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.submit);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		db=new DataBaseHelper(this);
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Submit",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,7,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		Declarations();
		cf.setTouchListener(findViewById(R.id.content));
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		
		 myString = getResources().getStringArray(R.array.myArray);
		 etsetword = (EditText) findViewById(R.id.wrdedt);
		 etgetword = (EditText) findViewById(R.id.wrdedt1);
		 handout=(CheckBox) findViewById(R.id.sub_hanout);
		 Cursor cd =db.SelectTablefunction(db.policyholder, " WHERE PH_SRID ='"+cf.selectedhomeid+ "'");
		 if(cd.getCount()>0)
		 {
			 cd.moveToFirst();
			 if(cd.getString(cd.getColumnIndex("hand_out")).trim().equals("true"))
			{
				 handout.setChecked(true);
			}
		 }
		 if(cd!=null)
				cd.close();
		 word = myString[rgenerator.nextInt(myString.length)];
		
		etsetword.setText(word);
		if (word.contains(" ")) {
			word = word.replace(" ", "");
		}
	   ((TextView)findViewById(R.id.tv1)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ db.Insp_firstname+" "+db.Insp_lastname
						+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
						+ "</font>"));
		((TextView)findViewById(R.id.tv2)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ db.Insp_firstname+" "+db.Insp_lastname
						+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
						+ "</font>"));
	
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.submit:
			if (((RadioButton)findViewById(R.id.accept)).isChecked()) {
				if (((TextView)findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
				{
					cf.show_toast("Please enter Word Verification.",1);
					((TextView)findViewById(R.id.wrdedt1)).requestFocus();
				} else {

					if (((TextView)findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) {
					
						
						if(validation())
						{
							cf.show_toast("Submitted successfully.",1);
							checkandchangestatus();
						}
						
						
					} else {
						cf.show_toast("Please enter valid Word Verification(case sensitive).",1);
						((TextView)findViewById(R.id.wrdedt1)).setText("");
						((TextView)findViewById(R.id.wrdedt1)).requestFocus();
					}

				}

			} else {
				cf.show_toast("Please Accept Fraud Statement.",1);
				((RadioButton)findViewById(R.id.accept)).setFocusable(true);
				
				((RadioButton)findViewById(R.id.accept)).requestFocus();

			}
			break;
			case R.id.S_refresh:			
			  word = myString[rgenerator.nextInt(myString.length)];
				
				etsetword.setText(word);
				if (word.contains(" ")) {
					word = word.replace(" ", "");
				}	
			break;
			
			case R.id.hme:
				cf.go_home();
				break;

		default:
			break;
		}
	}
	private boolean validation() {
		// TODO Auto-generated method stub
//		if(db.check_value_inTable(db.Companyinformation," WHERE CI_SRID='"+cf.selectedhomeid+"'"))
//		{
			if(check_value_PHTable(db.policyholder," Where PH_InspectorId='"+db.Insp_id+"' and PH_SRID='"+cf.selectedhomeid+"'"))
			{
				if(db.check_value_inTable(db.inspection_finding," WHERE IF_SRID='"+cf.selectedhomeid+"'"))
				{
					if(db.check_value_inTable(db.treatment," WHERE T_SRID='"+cf.selectedhomeid+"'"))
					{
						if(db.check_value_inTable(db.no_access," WHERE N_SRID='"+cf.selectedhomeid+"'"))
						{
							if(db.check_value_inTable(db.CommentsandFinancials," WHERE CF_SRID='"+cf.selectedhomeid+"'"))
							{
							
								if(db.check_value_inTable(db.QA_question," WHERE QA_SRID='"+cf.selectedhomeid+"'"))
								{
									/*if(db.check_value_inTable(db.ImageTable," WHERE IM_SRID='"+cf.selectedhomeid+"'"))
									{*/
									
										if(db.check_value_inTable(db.feedback_infomation," WHERE FD_SRID='"+cf.selectedhomeid+"'"))
										{
												return true;
										}
										else
										{
											cf.show_toast(" Please save feedback information", 0);
										}
									/*}
									else
									{
										cf.show_toast(" Please Add at least one image", 0);
									}*/
								}
								else
								{
									cf.show_toast("  Please save QA_Question", 0);
								}
							}
							else
							{
								cf.show_toast("  Please save Comments and financial", 0);
							}
						
						}
						else
						{
							cf.show_toast("  Please save No Access Area", 0);
						}
						
					}
					else
					{
						cf.show_toast("  Please save Treatment", 0);
					}
					
				}
				else
				{
					cf.show_toast("  Please save Inspection Findings", 0);
				}
			}
			else
			{
				cf.show_toast("  Please save Client Information", 0);
			}
			
//		}
//		else
//		{
//			cf.show_toast("  Please save  Inspection Company informaton", 0);
//		}
		return false;
	}
	private boolean check_value_PHTable(String policyholder, String string) {
		// TODO Auto-generated method stub
		Cursor c1=db.SelectTablefunction(policyholder, string);
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			if(!c1.getString(c1.getColumnIndex("structure")).trim().equals(""))
			{
				if(c1!=null)
					c1.close();
				return true;
			}
		}
		if(c1!=null)
			c1.close();
		return false;
	}
	private void checkandchangestatus() {
		// TODO Auto-generated method stub
		  db.wdo_db.execSQL("UPDATE "
					+ db.policyholder
					+ " SET PH_IsInspected=1,PH_Status=1,PH_SubStatus='0',hand_out='"+handout.isChecked()+"' WHERE PH_SRID ='"
					+ cf.selectedhomeid	+ "'  and PH_InspectorId = '"+db.Insp_id + "'");
				//cf.show_toast("Saved successfully.", 1);
				Intent submitint = new Intent(getApplicationContext(),Export.class);
				submitint.putExtra("type", "export");
				submitint.putExtra("classidentifier", "Submit");
				startActivity(submitint);
	}
	
}
