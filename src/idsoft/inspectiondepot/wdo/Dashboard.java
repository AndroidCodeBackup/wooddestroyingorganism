package idsoft.inspectiondepot.wdo;


import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;
import android.R.integer;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Dashboard extends Activity implements Runnable{
CommonFunction cf;
DataBaseHelper db;
Webservice_Function wb;

private int v,onlassign=0,onlsch=0,onlcio=0,onluts=0,onlcan=0,onltotal=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		wb=new Webservice_Function(this);
//		findViewById(R.id.head_policy_info).setVisibility(View.INVISIBLE);
//		findViewById(R.id.head_take_image).setVisibility(View.INVISIBLE);
		((TextView)findViewById(R.id.dahs_insp_name)).setText(db.Insp_firstname+" "+db.Insp_lastname);
		/*findViewById(R.id.head_insp_info).setVisibility(View.INVISIBLE);*/
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(Dashboard.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		db.CreateTable(15);
		try {
			Cursor cur = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='1' and CP_Order='4'", null);
			if(cur.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','11','1','"+db.encode("Real Estate Agent")+"','0','4','chk','0','')");	
				
			}
			
			Cursor cur1 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='1' and CP_Option='"+db.encode("Client/Owner")+"'", null);
			cur1.moveToFirst();
			String res1 =cur1.getString(cur1.getColumnIndex("CP_Default_op"));
			if(res1.equals("0"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Default_op='1' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='1' and CP_Option='"+db.encode("Client/Owner")+"'");
						
			}
			
			Cursor cur2 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='2' and CP_Order='4'", null);
			if(cur2.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','11','2','"+db.encode("Real Estate Agent")+"','0','4','chk','0','')");	
				
			}
			
			Cursor cur3 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='2' and CP_Option='"+db.encode("Client/Owner")+"'", null);
			cur3.moveToFirst();
			String res2 =cur3.getString(cur3.getColumnIndex("CP_Default_op"));
			if(res2.equals("0"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Default_op='1' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='2' and CP_Option='"+db.encode("Client/Owner")+"'");
						
			}
			
			Cursor cur4 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='3' and CP_Option='"+db.encode("Main Dwelling")+"'", null);
			cur4.moveToFirst();
			String res3 =cur4.getString(cur4.getColumnIndex("CP_Default_op"));
			if(res3.equals("0"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Default_op='1' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='11' and CP_Question='3' and CP_Option='"+db.encode("Main Dwelling")+"'");
						
			}
			
			Cursor cur5 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='24' and CP_Question='2' and CP_Order='7'", null);
			if(cur5.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','24','2','"+db.encode("Attic Areas")+"','0','7','chk','0','')");	
				
			}
			
			Cursor cur6 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='1' and CP_Order='5'", null);
			if(cur6.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','1','"+db.encode("Not Applicable")+"','0','5','radio','0','')");	
				
			}
			
			Cursor cur12 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Drill holes")+"'", null);
			cur12.moveToFirst();
			String res12 =cur12.getString(cur12.getColumnIndex("CP_Order"));
			if(res12.equals("2"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Order='7' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Drill holes")+"'");
						
			}
			
			Cursor cur13 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Bait Station")+"'", null);
			cur13.moveToFirst();
			String res13 =cur13.getString(cur13.getColumnIndex("CP_Order"));
			if(res13.equals("3"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Order='8' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Bait Station")+"'");
						
			}
			
			Cursor cur14 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Treatment Sticker")+"'", null);
			cur14.moveToFirst();
			String res14 =cur14.getString(cur14.getColumnIndex("CP_Order"));
			if(res14.equals("4"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Order='9' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Treatment Sticker")+"'");
						
			}
			
			Cursor cur15 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Bond Documentation")+"'", null);
			cur15.moveToFirst();
			String res15 =cur15.getString(cur15.getColumnIndex("CP_Order"));
			if(res15.equals("5"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Order='10' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Bond Documentation")+"'");
						
			}
			
			Cursor cur16 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Treatment Documentation")+"'", null);
			cur16.moveToFirst();
			String res16 =cur16.getString(cur16.getColumnIndex("CP_Order"));
			if(res16.equals("6"))
			{
				db.wdo_db.execSQL("UPDATE "+db.config_option+" SET CP_Order='11' Where CP_InspectorId='"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Option='"+db.encode("Treatment Documentation")+"'");
						
			}
			
			Cursor cur7 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Order='2'", null);
			if(cur7.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','2','"+db.encode("Subterranean Termites")+"','0','2','chk','0','')");	
				
			}
			
			Cursor cur8 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Order='3'", null);
			if(cur8.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','2','"+db.encode("Powder Post Beetles")+"','0','3','chk','0','')");	
				
			}
			
			Cursor cur9 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Order='4'", null);
			if(cur9.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','2','"+db.encode("Wood Boring Beetles")+"','0','4','chk','0','')");	
				
			}
			
			Cursor cur10 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Order='5'", null);
			if(cur10.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','2','"+db.encode("Dry Wood Termites")+"','0','5','chk','0','')");	
				
			}
			
			Cursor cur11 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='23' and CP_Question='2' and CP_Order='6'", null);
			if(cur11.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','23','2','"+db.encode("Wood Decaying Fungus")+"','0','6','chk','0','')");	
				
			}
			
			Cursor cur17 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='24' and CP_Question='2' and CP_Order='8'", null);
			if(cur17.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','24','2','"+db.encode("Eaves and Corners")+"','0','8','chk','0','')");	
				
			}
			
			Cursor cur18 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='24' and CP_Question='2' and CP_Order='9'", null);
			if(cur18.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','24','2','"+db.encode("Ceiling Joists")+"','0','9','chk','0','')");	
				
			}
			
			Cursor cur19 = db.wdo_db.rawQuery("select * from " + db.config_option + " Where CP_InspectorId = '"+db.Insp_id+"' and CP_Mtype='24' and CP_Question='2' and CP_Order='10'", null);
			if(cur19.getCount()==0)
			{
				db.wdo_db.execSQL("INSERT INTO "
						+ db.config_option
						+ " (CP_InspectorId,CP_Mtype,CP_Question,CP_Option,CP_Default_op,CP_Order,CP_optiontype,CP_editable,CP_CreatedOn)"
						+ " VALUES ('" + db.Insp_id+"','24','2','"+db.encode("Under tubs and behind showers")+"','0','10','chk','0','')");	
				
			}
			
			
		}
       catch (Exception e) {
		// TODO: handle exception
	    }
		set_counts();
	}

	private void set_counts() {
		// TODO Auto-generated method stub
		TextView TV_a=(TextView) findViewById(R.id.dash_ass),
				TV_s=(TextView) findViewById(R.id.dash_sch),
				TV_ct=(TextView) findViewById(R.id.dash_cit),
				TV_co=(TextView) findViewById(R.id.dash_cio),
				TV_ut=(TextView) findViewById(R.id.dash_uts),
				//TV_can=(TextView) findViewById(R.id.dash_can),
				TV_tot=(TextView) findViewById(R.id.dash_tot);		
		int A=0,S=0,CT=0,CO=0,UT=0,CAN=0;
		db.CreateTable(2);
		Cursor c =db.SelectTablefunction(db.policyholder, " WHERE PH_InspectorId='"+db.Insp_id+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String PH_Status=cf.check_values(db.decode(c.getString(c.getColumnIndex("PH_Status")))),
					   PH_SubStatus=cf.check_values(db.decode(c.getString(c.getColumnIndex("PH_SubStatus")))),
					   PH_IsInspected=cf.check_values(db.decode(c.getString(c.getColumnIndex("PH_IsInspected")))),
					   PH_IsUploaded=cf.check_values(db.decode(c.getString(c.getColumnIndex("PH_IsUploaded"))));
					if(PH_Status.equals("30"))
					{
						A++;
					}
					if(PH_Status.equals("40") && !PH_SubStatus.equals("41")/* && !PH_IsInspected.equals("1") && !PH_IsUploaded.equals("1")*/ )
					{
						S++;
					}
					if(PH_IsInspected.equals("1") && !PH_IsUploaded.equals("1") )
					{
						CT++;
					}
					if((PH_Status.equals("2") && PH_SubStatus.equals("0")) || PH_IsUploaded.equals("1") )
					{
						CO++;
					}
					if(PH_Status.equals("110")) 
					{
						UT++;
					}
					if(PH_Status.equals("110") || PH_Status.equals("110"))
					{
						CAN++;
					}
			}
			TV_a.setText(A+"");
			TV_s.setText(S+"");
			TV_ct.setText(CT+"");
			TV_co.setText(CO+"");
			TV_ut.setText(UT+"");
			//TV_can.setText(CAN+"");
			TV_tot.setText((A+S+CT+CO+UT)+"");
		}
		if(c!=null)
			c.close();
	}

	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.dash_ass:
				movetonext(v,"30",cf.type_id);
			break;
			case R.id.dash_sch:
				movetonext(v,"40",cf.type_id);
			break;
			case R.id.dash_cit:
				movetonext(v,"1",cf.type_id);
			break;
			case R.id.dash_cio:
				if(((TextView)findViewById(R.id.dash_cio)).getText().toString().equals("0"))
				{
					cf.show_toast("No record found", 0);
				}
				else
				{
					Intent intentreportready=new Intent(Dashboard.this,View_pdf.class);
					intentreportready.putExtra("type", "emailreport");
					intentreportready.putExtra("classidentifier", "DashBoard");
					intentreportready.putExtra("total_record", Integer.parseInt(((TextView)v).getText().toString()));
					intentreportready.putExtra("current", 1);
					
					startActivity(intentreportready);
					finish();
				}
			break;
			
			case R.id.dash_uts:
				movetonext(v,"110",cf.type_id);
//				Intent intentsuspended=new Intent(Dashboard.this,OnlineList.class);
//				intentsuspended.putExtra("status", "Suspended");
//				startActivity(intentsuspended);
//				finish();
			break;
			
			case R.id.dash_sync_but:
				online_sync();
			break;
			case R.id.dash_onl_ass:
				movetoonlnext(v,"30",cf.type_id);
			break;
			case R.id.dash_onl_sch:
				movetoonlnext(v,"40",cf.type_id);
			break;
			case R.id.dash_onl_cio:
				movetoonlnext(v,"41",cf.type_id);
			break;
			default :
			//	cf.show_toast("Button under construction", 1);
			break;
		}
	}

	private void movetoonlnext(View v2, String string, String type_id) {
		// TODO Auto-generated method stub
		if(!((TextView) v2).getText().toString().trim().equals("0") && !((TextView) v2).getText().toString().trim().equals("N/A")  )
		{
		/*	Intent in = new Intent(this,OnlineListing.class);
			in.putExtra("status", string);
			in.putExtra("type_id", type_id);
			startActivity(in);*/
			cf.show_toast("Under construction", 0);
		}
		else 
		{
			cf.show_toast("No image found", 0);
		}
	}

	private void online_sync() {
		// TODO Auto-generated method stub
		    final Dialog dialog1 = new Dialog(Dashboard.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
			dialog1.findViewById(R.id.alert1).setVisibility(View.VISIBLE);
			
			Button btn_yes = (Button) dialog1.findViewById(R.id.alert_Yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.alert_no);
			ImageView close = (ImageView) dialog1.findViewById(R.id.alert_close);
			btn_yes.setOnClickListener(new OnClickListener()
			{
           	@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					if(wb.isInternetOn()==true)
					{
                      String source = "<font color=#FFFFFF>Loading data. Please wait..."
								+ "</font>";
						cf.pd = ProgressDialog.show(Dashboard.this,"", Html.fromHtml(source), true);
						Thread thread = new Thread(Dashboard.this);
						thread.start();
					}
					else
					{
						cf.show_toast("Internet connection not available.",0);
					}
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					
				}
				
			});
			close.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();
	}

	private void movetonext(View v, String i,String typ_id) {
		// TODO Auto-generated method stub
		if(!((TextView) v).getText().toString().trim().equals("0"))
		{
			Intent in = new Intent(this,HomwOwnerListing.class);
			in.putExtra("status", i);
			in.putExtra("type_id", typ_id);
			startActivity(in);
		}
		else 
		{
			cf.show_toast("No record found", 0);
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent in = new Intent(this,HomeScreen.class);
			startActivity(in);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		// TODO Auto-generated method stub
					try {
						
						SoapObject onlresult=wb.Calling_service(db.Insp_id, "UpdateMobileDBCount");
						
						 retrieveonlinedata(onlresult);
						 
		                 v=0;
		                
					} catch (Exception e) {
		               v=1;
		               System.out.println(" the issues "+e.getMessage());
					}
					 handler.sendEmptyMessage(0);
			}

			private Handler handler = new Handler() {
				public void handleMessage(Message msg) {
					cf.pd.dismiss();
					TextView TV_a=(TextView) findViewById(R.id.dash_onl_ass),
							TV_s=(TextView) findViewById(R.id.dash_onl_sch),
							TV_co=(TextView) findViewById(R.id.dash_onl_cio),
							TV_ut=(TextView) findViewById(R.id.dash_onl_uts),
							TV_can=(TextView) findViewById(R.id.dash_onl_can),
							TV_tot=(TextView) findViewById(R.id.dash_onl_tot);	
		         if(v==0){
					if (onlassign != 0) {
						TV_a.setText(String.valueOf(onlassign));

					} else {
						TV_a.setText("0");
					}
					if (onlsch != 0) {
						TV_s.setText(String.valueOf(onlsch));

					} else {
						TV_s.setText("0");
					}
					if (onlcio != 0) {
						TV_co.setText(String.valueOf(onlcio));

					} else {
						TV_co.setText("0");
					}
					if (onluts != 0) {
						TV_ut.setText(String.valueOf(onluts));

					} else {
						TV_ut.setText("0");
					}
					if (onlcan != 0) {
						TV_can.setText(String.valueOf(onlcan));

					} else {
						TV_can.setText("0");
					}
					if (onltotal != 0) {
						TV_tot.setText(String.valueOf(onltotal));

					} else {
						TV_tot.setText("0");
					}
				 }
				 else if(v==1)
				 {
					 cf.show_toast("There is a problem in the server. Please contact paperless admin.", 0);
				 }
				}
			};
			public void retrieveonlinedata(SoapObject onlresult)
			{
				
				onlassign=0;onlsch=0;onlcio=0;onluts=0;onlcan=0;onltotal=0;
				int Cnt = onlresult.getPropertyCount();
				System.out.println("property count "+Cnt);
				if (Cnt >= 1) {
					
					for (int i = 0; i < Cnt; i++) 
					{
						SoapObject obj1 = (SoapObject) onlresult.getProperty(i);
						System.out.println(" the object values"+obj1);
						int Cnt1 = obj1.getPropertyCount();
						for (int j = 0; j < Cnt1; j++) 
						{
							PropertyInfo pi = new PropertyInfo();
							obj1.getPropertyInfo(j, pi);
							if (!obj1.getProperty("i_maininspectiontype").toString().equals("")) 
							{
								
								if(pi.name.equals("Assign"))
								{
									
									onlassign = onlassign+ Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
								if(pi.name.equals("Sch"))
								{
									
									onlsch = onlsch+Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
								if(pi.name.equals("Comp_Ins_in_Online"))
								{
									 
									onlcio = onlcio+Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
								if(pi.name.equals("UTS"))
								{
									
									onluts = onluts+Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
								if(pi.name.equals("Can"))
								{
									
									onlcan = onlcan+Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
								if(pi.name.equals("Total"))
								{
									
									onltotal = onltotal+Integer.parseInt(obj1.getProperty(j).toString());							
								}
								
							}
						}
					}
				}
			}
			
}
