package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CommentsAndFinancial extends Activity {
	CommonFunction cf;
	EditText ed;
	DataBaseHelper db;
	private boolean load_comment=true;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comments_financial);
		cf=new CommonFunction(this);
		Bundle b=getIntent().getExtras();
		db=new DataBaseHelper(this);
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Inspection Information=>Treatment",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,5,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		LayoutParams lp2=((EditText) findViewById(R.id.commentsandfinancial)).getLayoutParams();
		lp2.width=cf.wd-100;
		ed=(EditText) findViewById(R.id.commentsandfinancial);
		ed.setLayoutParams(lp2);
		ed.addTextChangedListener(new TextWatchLimit(ed,300,((TextView) findViewById(R.id.commentsandfinancial_limit)),cf.addendm_txt));
		cf.setTouchListener(findViewById(R.id.content));
		db.CreateTable(10);
		Cursor c =db.SelectTablefunction(db.CommentsandFinancials, " WHERE CF_SRID='"+cf.selectedhomeid+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			ed.setText(db.decode(c.getString(c.getColumnIndex("CF_Commnents"))));
			
		}
		if(c!=null)
			c.close();
	
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
						ed.setText((ed.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						
					}
					else if(resultCode==RESULT_CANCELED)
					{
						cf.show_toast("You have canceled  the comments selction ",0);
					}
			}		 		
	 		

	 	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.clear:
				((EditText) findViewById(R.id.commentsandfinancial)).setText("");
				//clear_all();
			break;case R.id.save:
				if(!ed.getText().toString().trim().equals(""))
				{
					Cursor c =db.SelectTablefunction(db.CommentsandFinancials, " WHERE CF_SRID='"+cf.selectedhomeid+"'");
					if(c.getCount()>0)
					{
						db.wdo_db.execSQL(" UPDATE "+db.CommentsandFinancials+" SET CF_Commnents='"+db.encode(ed.getText().toString().trim())+"' WHERE CF_SRID='"+cf.selectedhomeid+"'");
						
					}
					else
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.CommentsandFinancials+" (CF_InspectorId,CF_SRID,CF_Commnents) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+db.encode(ed.getText().toString().trim())+"')");
					}
					cf.show_toast("Financial Disclosure Comments saved successfully", 0);
					cf.go_back(Addendum.class);
					if(c!=null)
						c.close();
					
				}
				else
				{
					cf.show_toast(" Please enter  Financial Disclosure Comments", 0);
				}
				//cf.go_back(NoAccess.class);
			break;
			case R.id.load_comments:
				int len=ed.getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in =new Intent(this,Load_comments.class);
					in.putExtra("id", R.id.load_comments);
					in.putExtra("srid", cf.selectedhomeid);
					in.putExtra("question", 7);
					in.putExtra("max_length", 300);
					in.putExtra("cur_length", len);
					in.putExtra("xfrom", loc[0]+10);
					in.putExtra("yfrom", loc[1]+10);
					startActivityForResult(in, cf.loadcomment_code);
				}
				break;
			
		}	
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_back(NoAccess.class);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
