package idsoft.inspectiondepot.wdo.imagezoom;

public interface IDisposable {

	void dispose();
}
