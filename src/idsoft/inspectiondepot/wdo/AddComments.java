package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.TouchFoucs;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AddComments extends Activity {
	CommonFunction cf;
	Spinner sp;
	EditText ed;
	CheckBox chk[]=new CheckBox[2];
	LinearLayout li_insp;
	TableLayout tbl;
	DataBaseHelper db;
	String ques[]={"--Select--","Initial Comments","Scheduling Comments","Live/Evidence/Damage Locations","Treatment Observed","Method of Treatment comments","No Access Reason","Comments and financial disclosure","Feedback Comments","Addendum"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcomments);
		cf=new CommonFunction(this);
		Declaration();
		db=new DataBaseHelper(this);
		db.CreateTable(14);
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		sp=(Spinner) findViewById(R.id.addcomment_SPquestype);
		chk[0]=(CheckBox) findViewById(R.id.addcomment_CHKcarrier);
		chk[1]=(CheckBox) findViewById(R.id.addcomment_CHKretail);
		ed=(EditText) findViewById(R.id.addcomment_EDcomment);
		tbl=(TableLayout) findViewById(R.id.addcomment_LLlistingcomments);
		li_insp=(LinearLayout) findViewById(R.id.addcomment_LLinsptype);
		ed.setOnTouchListener(new TouchFoucs(ed));
		ed.addTextChangedListener(new  TextWatchLimit(ed, 300,((TextView)findViewById(R.id.addcomment_TVcommetslimit))));
		ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_spinner_item,ques);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp.setAdapter(ad);
//		findViewById(R.id.head_policy_info).setVisibility(View.INVISIBLE);
//		findViewById(R.id.head_take_image).setVisibility(View.INVISIBLE);
		
		/*findViewById(R.id.head_insp_info).setVisibility(View.INVISIBLE);*/
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(AddComments.this,PolicyholdeInfoHead.class);
//				insp_info.putExtra("homeid", cf.selectedhomeid);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		
		sp.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp.getSelectedItem().toString().equals("--Select--"))
				{
					tbl.removeAllViews();
				}
				else
				{
					view_comments();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_home(); 
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.addcomment_BTclear:
				clearall();
			break;
			case R.id.addcomment_BTsave:
				//clearall();
				save_comments();
				
			break;
			case R.id.addcomment_BTview:
				if(sp.getSelectedItemPosition()!=0)
				{
					if(chk[0].isChecked()|| chk[1].isChecked())
						{
							ed.setText("");
							view_comments();
						}
							else
							{
								cf.show_toast("Please select Inspection Type", 0);
							}
						}
						else
						{
							cf.show_toast("Please select Question Type", 0);
						}
			break;
		default:
			break;
		}
	}
	private void save_comments() {
		// TODO Auto-generated method stub
		if(sp.getSelectedItemPosition()!=0)
		{
			
			if(chk[0].isChecked() || chk[1].isChecked())
			{
				if(!ed.getText().toString().trim().equals(""))
				{
					int type =0;
					if(!((Button) findViewById(R.id.addcomment_BTsave)).getText().equals("Update"))
					{
					if(chk[0].isChecked())
					{
							Cursor c=	db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_SRID='"+cf.selectedhomeid+"' and TC_Comments='"+db.encode(ed.getText().toString().trim())+"' and TC_type='0' and TC_Question='"+sp.getSelectedItemPosition()+"'");
							if(c.getCount()>0)
							{
								cf.show_toast(" This Comment is already available in the Question Type ", 0);
								return;
							}
							c.close();
					}
					if(chk[1].isChecked())
					{
							Cursor c=	db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_SRID='"+cf.selectedhomeid+"' and TC_Comments='"+db.encode(ed.getText().toString().trim())+"' and TC_type='1' and TC_Question='"+sp.getSelectedItemPosition()+"'");
							if(c.getCount()>0)
							{
								cf.show_toast(" This Comment is already available in the Question Type ", 0);
								return;
							}
							c.close();
					}
					
					if(chk[0].isChecked())
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.To_addcomments+" (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status) VALUES " +
								"('"+db.Insp_id+"','"+cf.selectedhomeid+"','0','"+sp.getSelectedItemPosition()+"','"+db.encode(ed.getText().toString().trim())+"','true')");
					}
					
					if(chk[1].isChecked())
					{
						db.wdo_db.execSQL(" INSERT INTO "+db.To_addcomments+" (TC_InspectorId,TC_SRID,TC_type,TC_Question,TC_Comments,TC_Status) VALUES " +
								"('"+db.Insp_id+"','"+cf.selectedhomeid+"','1','"+sp.getSelectedItemPosition()+"','"+db.encode(ed.getText().toString().trim())+"','true')");
					}	
					}
					else
					{
						db.wdo_db.execSQL(" UPDATE  "+db.To_addcomments+" SET TC_Question='"+sp.getSelectedItemPosition()+"',TC_Comments='"+db.encode(ed.getText().toString().trim())+"' WHERE TC_Id='"+((TextView) findViewById(R.id.edit_id)).getText().toString()+"'");
						((TextView) findViewById(R.id.edit_id)).setText("");
						//li_insp.setVisibility(View.VISIBLE);
						((Button) findViewById(R.id.addcomment_BTsave)).setText("Save/Add More Comments");
						((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.VISIBLE);
					}
					//clearall();
					
					ed.setText("");
					cf.show_toast("Comments saved succesfully", 0);
					view_comments();
				}
				else
				{
					cf.show_toast("Please enter Comments", 0);
				}
			}
			else
			{
				cf.show_toast("Please select Inspection Type", 0);
			}
		}
		else
		{
			cf.show_toast("Please select Question Type", 0);
		}
	}
	private void view_comments() {
		// TODO Auto-generated method stub
		TableRow tbl_rw;
		View  v;
		Cursor c=null;;
		tbl.removeAllViews();
		String insp_typ="";
		
		if(chk[0].isChecked())
		{
			if(chk[1].isChecked())
			{
				 c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_SRID='"+cf.selectedhomeid+"' and TC_type in (0,1) and TC_Question='"+sp.getSelectedItemPosition()+"'");
				 insp_typ="Carrier/Retail";
			
			}
			else
			{
				 c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_SRID='"+cf.selectedhomeid+"' and TC_type ='0' and TC_Question='"+sp.getSelectedItemPosition()+"'");
				 insp_typ="Carrier";
			}
		}
		else if(chk[1].isChecked())
		{
			c =db.SelectTablefunction(db.To_addcomments, " WHERE TC_InspectorId='"+db.Insp_id+"' and TC_SRID='"+cf.selectedhomeid+"' and TC_type ='1' and TC_Question='"+sp.getSelectedItemPosition()+"'");
				insp_typ="Retail";
		}
		if(c!=null)
		{
			if(c.getCount()>0)
			{
				((TextView) findViewById(R.id.no_comments)).setVisibility(View.GONE);
				
				TextView TV_breadcum=new TextView(this);
				TV_breadcum.setText(Html.fromHtml("<b> Question Type</b>:&nbsp;"+sp.getSelectedItem().toString()));
				TV_breadcum.setTextSize(16);
				TV_breadcum.setPadding(0, 10, 0, 10);
				TV_breadcum.setGravity(Gravity.CENTER);
				tbl.addView(TV_breadcum,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				tbl_rw=new TableRow(this);
			
			tbl_rw.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
			tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithsubtabbackgroundcolor));
			TextView tv_no=new TextView(this,null,R.attr.textview_200);
			
			tv_no.setText("No");
			tbl_rw.addView(tv_no,50,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_area=new TextView(this,null,R.attr.textview_200);
			tv_area.setText("Type");
			tv_area.setVisibility(View.GONE);/**only retail record present so we no need show this**/ 
			tbl_rw.addView(tv_area,150,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setVisibility(View.GONE);/**only retail record present so we no need show this**/
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_sparea=new TextView(this,null,R.attr.textview_200);
			tv_sparea.setText("Comments");
			tbl_rw.addView(tv_sparea,550,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			
			TextView tv_status=new TextView(this,null,R.attr.textview_200);
			tv_status.setText("Active Status");
			tbl_rw.addView(tv_status,100,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			
			
			TextView tv_Edit=new TextView(this,null,R.attr.textview_200);
			tv_Edit.setText("Edit/Delete");
			tbl_rw.addView(tv_Edit,150,LayoutParams.WRAP_CONTENT);
			tbl.addView(tbl_rw);
			c.moveToFirst();
		
	for(int i =0;i<c.getCount();i++,c.moveToNext())
	{
		String comments,type = "",status;
		int id=c.getInt(c.getColumnIndex("TC_Id"));
		comments=db.decode(c.getString(c.getColumnIndex("TC_Comments")));
		if(c.getString(c.getColumnIndex("TC_type")).equals("0"))
		{
			type=chk[0].getText().toString();
		}
		else if(c.getString(c.getColumnIndex("TC_type")).equals("1"))
		{
			type=chk[1].getText().toString();
		}
		status=c.getString(c.getColumnIndex("TC_Status"));
		tbl_rw=new TableRow(this);
		TextView tv_no1=new TextView(this,null,R.attr.textview_200_inner);
		tv_no1.setText((i+1)+"");
		tbl_rw.addView(tv_no1);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		TextView tv_area1=new TextView(this,null,R.attr.textview_200_inner);
		tv_area1.setText(type);
		tv_area1.setVisibility(View.GONE);/**only retail record present so we no need show this**/
		tbl_rw.addView(tv_area1,150,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setVisibility(View.GONE);/**only retail record present so we no need show this**/
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		TextView tv_sparea1=new TextView(this,null,R.attr.textview_200_inner);
		tv_sparea1.setText(comments);
		tbl_rw.addView(tv_sparea1,550,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		
		LinearLayout li1=new LinearLayout(this);
		CheckBox chk =new CheckBox(this);
		chk.setOnClickListener(new clickerlistenr(id,status));
		chk.setTag(id);//set the id here
		if(status.equals("true"))
		{
		chk.setChecked(true);
		}
		else
		{
		chk.setChecked(false);
		}
		li1.addView(chk);
		li1.setGravity(Gravity.CENTER_HORIZONTAL);
		tbl_rw.addView(li1,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		
		LinearLayout li=new LinearLayout(this);
		ImageView im =new ImageView(this);
		im.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofedit));
		ImageView im1 =new ImageView(this);
		im1.setBackgroundDrawable(getResources().getDrawable(R.drawable.close));
		li.addView(im,43,43);
		li.addView(im1,50,50);
		im.setOnClickListener(new edit_clicker(id,comments));
		im1.setOnClickListener(new del_clickerlistenr(id));
		li.setGravity(Gravity.CENTER);
		tbl_rw.addView(li);
		li.setPadding(5, 10, 5, 10);
		tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithoutcurve));
		tbl.addView(tbl_rw);
		}
			}
			else
			{
				((TextView) findViewById(R.id.no_comments)).setVisibility(View.VISIBLE);
			}
			
		}
		if(c!=null)
		{
			c.close();
		}
	}
	class clickerlistenr implements OnClickListener
	{
		int id;
		String status;
		String sta="Activate";
		String sta_val="true";
		
		public clickerlistenr(int id, String status) {
			// TODO Auto-generated constructor stub
			this.id=id;
			this.status=status;
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			System.out.println("status"+status);
			if(status.equals("true"))
			{
				sta="DeActivate";
				sta_val="false";
			}
			else
			{
				sta="Activate";
				sta_val="true";
			}
			AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to change the status to "+ sta+"?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wdo_db.execSQL(" UPDATE  "+db.To_addcomments+" SET TC_Status='"+sta_val+"' WHERE TC_Id='"+id+"'");
						
						status=sta_val;
						cf.show_toast("Status changed successfully", 1);
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					if(status.equals("true"))
						((CheckBox)v).setChecked(true);
					else
						((CheckBox)v).setChecked(false);
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
		}
		
	}
	class del_clickerlistenr implements OnClickListener
	{
		int id;
		
		
		public del_clickerlistenr(int id) {
			// TODO Auto-generated constructor stub
			this.id=id;
		
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			
			AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to delete the selected comments?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wdo_db.execSQL(" DELETE FROM  "+db.To_addcomments+"  WHERE TC_Id='"+id+"'");
						cf.show_toast("Selected comment deleted successfully", 1);
						((TextView) findViewById(R.id.edit_id)).setText("");
						((Button) findViewById(R.id.addcomment_BTsave)).setText("Save/Add More Comments");
						((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.VISIBLE);
						ed.setText("");
						view_comments();
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
		}
		
	}
	class edit_clicker implements OnClickListener
	{
		int id;
		String comments;
		edit_clicker(int id,String comment)
		{
			this.id=id;
			comments=comment;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			((TextView) findViewById(R.id.edit_id)).setText(id+"");
			ed.setText(comments);
			//li_insp.setVisibility(View.GONE);
			((Button) findViewById(R.id.addcomment_BTsave)).setText("Update");
			((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.GONE);
		}
		
	}
	private void clearall() {
		// TODO Auto-generated method stub
		sp.setSelection(0);
		ed.setText("");
		/*chk[0].setChecked(false);
		chk[1].setChecked(false);*/
		//li_insp.setVisibility(View.VISIBLE);
		tbl.removeAllViews();
		((TextView) findViewById(R.id.edit_id)).setText("");
		//li_insp.setVisibility(View.VISIBLE);
		((Button) findViewById(R.id.addcomment_BTsave)).setText("Save/Add More Comments");
		((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.VISIBLE);
	}
	}
