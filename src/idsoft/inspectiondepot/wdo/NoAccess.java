package idsoft.inspectiondepot.wdo;
import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.MultiSpinner;
import idsoft.inspectiondepot.wdo.supportclass.Option_list;
import idsoft.inspectiondepot.wdo.supportclass.TextWatchLimit;
import idsoft.inspectiondepot.wdo.supportclass.Validation;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class NoAccess extends Activity {
CommonFunction cf;
DataBaseHelper db;
Validation va;
 Spinner sp_areas;
TextView tv_areas;
EditText Other_area,reason,specific_other;
MultiSpinner specificarea;
TableLayout tbl;
Option_list op1,op2;
private boolean load_comment=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		System.out.println("comes correctly");
		setContentView(R.layout.no_access);
		System.out.println("comes correctly1");
		Bundle b=getIntent().getExtras();
		cf=new CommonFunction(this);
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("SRID");
		}
		db=new DataBaseHelper(this);   
		System.out.println("comes correctly1");
		
	
		System.out.println("comes correctly2");
		LinearLayout header=(LinearLayout)findViewById(R.id.header);
		header.addView(new HeaderInflater(this,"Inspection Information=>Noaccess Area",1,cf.selectedhomeid));
		LinearLayout menu=(LinearLayout)findViewById(R.id.general_menu);
		menu.addView(new MenuInflater(this,2,cf));
		LinearLayout submenu=(LinearLayout)findViewById(R.id.general_submenu);
		submenu.addView(new SubMenuInflater(this,2,4,cf));
		cf.getDeviceDimensions();
		LayoutParams lp=((LinearLayout) findViewById(R.id.content)).getLayoutParams();
		lp.width=cf.wd-40;
		((LinearLayout) findViewById(R.id.content)).setLayoutParams(lp);
		Declaration();
		cf.setTouchListener(findViewById(R.id.content));
		va=new Validation(cf);
		System.out.println("comes correctly3");
		showsaved();
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		
        Other_area=(EditText) findViewById(R.id.noaccess_areas_other);
		sp_areas=(Spinner) findViewById(R.id.noaccess_areas);
		tv_areas=(TextView) findViewById(R.id.noaccess_areas_tv);
		op1=new Option_list(this, sp_areas, 24, 1,null);
		
		specificarea=(MultiSpinner) findViewById(R.id.noaccess_specificareas);
		specific_other=(EditText)findViewById(R.id.noaccess_specificareas_other);
		op2=new Option_list(this, specificarea, 24, 2, specific_other,((TextView)findViewById(R.id.Noaccess_tv_sp)),"");
		
		reason=(EditText) findViewById(R.id.noaccess_reason);
		reason.addTextChangedListener(new TextWatchLimit(reason, 150,((TextView) findViewById(R.id.limit_2))));
		
		tbl=(TableLayout) findViewById(R.id.dynamic_tbl);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.go_back(Treatment.class);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.clear:
				op1.clear_selectionRD();
				op2.clear_selectionCHK();
				clear_all();
			break;
			case R.id.save:
				if(validate())
				{
					save_data();
				}
			break;
			case R.id.load_comments:
			int len=reason.getText().toString().length();
			
			if(load_comment )
			{
				load_comment=false;
				int loc[] = new int[2];
				v.getLocationOnScreen(loc);
				Intent in =new Intent(this,Load_comments.class);
				in.putExtra("id",reason.getId());
				in.putExtra("srid", cf.selectedhomeid);
				in.putExtra("question", 6);
				in.putExtra("max_length", 150);
				in.putExtra("cur_length", len);
				in.putExtra("xfrom", loc[0]+10);
				in.putExtra("yfrom", loc[1]+10);
				startActivityForResult(in, cf.loadcomment_code);
			}
			break;
			case R.id.hme:
				cf.go_home();
			break;
			case R.id.next:
				cf.go_back(CommentsAndFinancial.class);
			break;
		default:
			
			break;
		}
	}
	private void save_data() {
		// TODO Auto-generated method stub
		String N_Area=db.encode(op1.get_selected());
		String N_Area_other=db.encode(Other_area.getText().toString().trim());
		String N_SArea=db.encode(op2.get_selected());
		String N_SArea_other=db.encode(specific_other.getText().toString().trim());
		String N_Reason=db.encode(reason.getText().toString().trim());
		if(((Button) findViewById(R.id.save)).getText().toString().trim().equals("Update"))
		{
			db.wdo_db.execSQL(" UPDATE "+db.no_access+" SET N_Area='"+N_Area+"',N_Area_other='"+N_Area_other+"',N_SArea='"+N_SArea+"',N_SArea_other='"+N_SArea_other+"',N_Reason='"+N_Reason+"' WHERE N_SRID='"+cf.selectedhomeid+"' AND N_Id='"+((TextView) findViewById(R.id.noaccess_id)).getText().toString()+"'");
		}
		else
		{
			db.wdo_db.execSQL(" INSERT INTO  "+db.no_access+" (N_InspectorId,N_SRID,N_Area,N_Area_other,N_SArea,N_SArea_other,N_Reason) VALUES ('"+db.Insp_id+"','"+cf.selectedhomeid+"','"+N_Area+"','"+N_Area_other+"','"+N_SArea+"','"+N_SArea_other+"','"+N_Reason+"')");
		}
		op1.setDefaultRD();
		op2.setDefaultCHK();
		clear_all();
		showsaved();
	}
	private boolean validate() {
		// TODO Auto-generated method stub
		
		if(va.validate(sp_areas, "In Accessible Area", "-Select-"))
		{
			if(check_duplicate(sp_areas))
			{
				/*if(va.validate_other(Other_area, "Not Accessible area" , "Other"))
				{*/
					if(va.validate(op2, "Specific areas","-Select-"))
					{
						if(va.validate_other(specific_other, "Specific Areas", "Other"))
						{
							
							/*if(va.validate(reason, "No Access Reason"))
							{*/
								return true;
							//}
						}
					}
				//}
			}
		}
		return false;
	}
	private boolean check_duplicate(Spinner sp_areas2) {
		// TODO Auto-generated method stub
		if(!((Button) findViewById(R.id.save)).getText().toString().trim().equals("Update"))
		{
		Cursor c =db.SelectTablefunction(db.no_access, " Where N_SRID='"+cf.selectedhomeid+"' and N_Area='"+db.encode(sp_areas.getSelectedItem().toString())+"' ");
		if(c.getCount()>0)
		{
			cf.show_toast("In Accessible Area already exists!.", 0);
			if(c!=null)
				c.close();
			return false;
		
		}
		else
		{
			if(c!=null)
				c.close();
			return true;
		}
		
		}
		else{
			return true;
		}
	}
	private void showsaved() {
		// TODO Auto-generated method stub
		db.CreateTable(9);
		String N_Area,N_Area_other,N_SArea ,N_Reason,N_SArea_other;
		Cursor c =db.SelectTablefunction(db.no_access, " Where N_SRID='"+cf.selectedhomeid+"'");
		tbl.removeAllViews();
		if(c.getCount()>0)
		{
			findViewById(R.id.norecord).setVisibility(View.GONE);
			c.moveToFirst();
		TableRow tbl_rw;
		View  v;
		
			tbl_rw=new TableRow(this);
			
			tbl_rw.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
			tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithsubtabbackgroundcolor));
			TextView tv_no=new TextView(this,null,R.attr.textview_200);
			
			tv_no.setText("No");
			tbl_rw.addView(tv_no,50,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_area=new TextView(this,null,R.attr.textview_200);
			tv_area.setText("Not Accessible Area");
			tbl_rw.addView(tv_area,150,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_sparea=new TextView(this,null,R.attr.textview_200);
			tv_sparea.setText("Specific Areas");
			tbl_rw.addView(tv_sparea,250,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_reason=new TextView(this,null,R.attr.textview_200);
			tv_reason.setText("No Access Reason");
			tbl_rw.addView(tv_reason,250,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(getResources().getColor(R.color.outline_color));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_Edit=new TextView(this,null,R.attr.textview_200);
			tv_Edit.setText("Edit/Delete");
			tbl_rw.addView(tv_Edit,150,LayoutParams.WRAP_CONTENT);
			tbl.addView(tbl_rw);
			
		
		for(int i=0;i<c.getCount();i++,c.moveToNext())
		{
			N_Area=db.decode(c.getString(c.getColumnIndex("N_Area")));
		    N_Area_other=db.decode(c.getString(c.getColumnIndex("N_Area_other")));
		    N_SArea=db.decode(c.getString(c.getColumnIndex("N_SArea")));
		    N_SArea_other=db.decode(c.getString(c.getColumnIndex("N_SArea_other")));
		    N_Reason=db.decode(c.getString(c.getColumnIndex("N_Reason")));
		tbl_rw=new TableRow(this);
		tv_no=new TextView(this,null,R.attr.textview_200_inner);
		tv_no.setText((i+1)+"");
		
		tbl_rw.addView(tv_no);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		 tv_area=new TextView(this,null,R.attr.textview_200_inner);
		if(N_Area_other.trim().equals(""))
		{
			tv_area.setText(N_Area);
		}
		else
		{
			tv_area.setText(N_Area+"("+N_Area_other+")");
		}
		tbl_rw.addView(tv_area,150,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		 tv_sparea=new TextView(this,null,R.attr.textview_200_inner);
		 if(N_SArea.contains("Other"))
		 {
			 N_SArea+="("+N_SArea_other+")";
		 }
		tv_sparea.setText(N_SArea);
		tbl_rw.addView(tv_sparea,250,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		 tv_reason=new TextView(this,null,R.attr.textview_200_inner);
		tv_reason.setText(N_Reason);
		tbl_rw.addView(tv_reason,250,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(getResources().getColor(R.color.outline_color));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		LinearLayout li=new LinearLayout(this);
		ImageView im =new ImageView(this);
		im.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofedit));
		ImageView im1 =new ImageView(this);
		im1.setBackgroundDrawable(getResources().getDrawable(R.drawable.close));
		li.addView(im,43,43);
		li.addView(im1,50,50);
		li.setGravity(Gravity.CENTER);
		tbl_rw.addView(li);
		li.setPadding(5, 10, 5, 10);
		tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithoutcurve));
		tbl.addView(tbl_rw);
		im.setTag(c.getString(c.getColumnIndex("N_Id")));
		im1.setTag(c.getString(c.getColumnIndex("N_Id")));
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				edit_value(v.getTag().toString());
			}
		});
		im1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder b =new AlertDialog.Builder(NoAccess.this);
				b.setTitle("Confirmation");
				b.setMessage("Do you want to delete the selected NotAccessibleArea?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						try
						{
							db.wdo_db.execSQL(" DELETE FROM "+db.no_access+" WHERE N_Id='"+v.getTag().toString()+"'");
							specificarea.setSelection(0);
							sp_areas.setSelection(0);
							op1.clear_selectionRD();
							op2.clear_selectionCHK();
							clear_all();
							showsaved();
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("the exeption in delete"+e.getMessage());
						}
						
						cf.show_toast("The selected NoAccessArea has been deleted successfully.", 1);
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();  
				
			}
		});
		}
		}
		else
		{
			 findViewById(R.id.norecord).setVisibility(View.VISIBLE);
		}
		if(c!=null)
			c.close();
	}
	protected void edit_value(String tag) {
		// TODO Auto-generated method stub
		Cursor c =db.SelectTablefunction(db.no_access, " WHERE N_SRID='"+cf.selectedhomeid+"' AND N_Id='"+tag+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String N_Area=db.decode(c.getString(c.getColumnIndex("N_Area")));
			String N_Area_other=db.decode(c.getString(c.getColumnIndex("N_Area_other")));
			String N_SArea=db.decode(c.getString(c.getColumnIndex("N_SArea")));
			String N_SArea_other=db.decode(c.getString(c.getColumnIndex("N_SArea_other")));
			String N_Reason=db.decode(c.getString(c.getColumnIndex("N_Reason")));
			op1.set_selectionRD(N_Area);
			sp_areas.setEnabled(false);
			((TextView) findViewById(R.id.noaccess_id)).setText(c.getString(c.getColumnIndex("N_Id")));
			//Other_area.setText(N_Area_other);
			specific_other.setText(N_SArea_other);
			op2.set_selectionCHk(N_SArea);
			reason.setText(N_Reason);
			((Button) findViewById(R.id.save)).setText("Update"); 
		}
		if(c!=null)
			c.close();
	}
	private void clear_all() {
		// TODO Auto-generated method stub
		
		//Other_area.setText("");
		//Other_area.setTag("required");
		
		sp_areas.setEnabled(true);
		reason.setText("");
		((TextView) findViewById(R.id.noaccess_id)).setText("");
		((Button) findViewById(R.id.save)).setText("Save/Add More Areas");
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
							reason.setText((reason.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
					}
					/*else if(resultCode==RESULT_CANCELED)
					{
						cf.show_toast("You have canceled  the comments selction ",0);
					}*/
			}		 		
	 		

	 	}
	
}
