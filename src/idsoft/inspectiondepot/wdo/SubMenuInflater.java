package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;

import java.text.AttributedCharacterIterator.Attribute;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class SubMenuInflater extends LinearLayout {
Context con;
int menu_no;
int submenu_no;
CommonFunction cf;
	public SubMenuInflater(Context context,int menu,int submenu,CommonFunction cf) {
		super(context);
		con=context;
		menu_no=menu;
		submenu_no=submenu;
		this.cf=cf;
		create_menu();
		// TODO Auto-generated constructor stub
	}
	public SubMenuInflater(Context context,Attribute attr,int menu,int submenu,CommonFunction cf) {
		super(context);
		con=context;
		menu_no=menu;
		submenu_no=submenu;
		this.cf=cf;
		create_menu();
		// TODO Auto-generated constructor stub
	}
	private void create_menu() {
		// TODO Auto-generated method stub
		LayoutInflater ly=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ly.inflate(R.layout.submenus, this, true);
		setselected();
		

	}
	private void setselected() {
		// TODO Auto-generated method stub
		if(menu_no==1)
		{
			
			((RelativeLayout) findViewById(R.id.submenu_general)).setVisibility(View.VISIBLE);
			Button sb[]=new Button[7];
			sb[0]=(Button) findViewById(R.id.submenu_policy);
			sb[1]=(Button) findViewById(R.id.submenu_agent);
			sb[2]=(Button) findViewById(R.id.submenu_realtor);
			sb[3]=(Button) findViewById(R.id.submenu_call);
			sb[4]=(Button) findViewById(R.id.submenu_schedule);
			sb[5]=(Button) findViewById(R.id.submenu_contact);
			sb[6]=(Button) findViewById(R.id.submenu_generaldata);
			
			sb[0].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,policyholderInformation.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[1].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,Agentinformation.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[2].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,RealtorInformation.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[3].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,CallAttempt.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[4].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,Schedule.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[5].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,AdditionalContacts.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[6].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,Generaldata.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			switch (submenu_no) {
			case 1:
				sb[0].setTextColor(Color.BLACK);
				sb[0].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 2:
				sb[1].setTextColor(Color.BLACK);
				sb[1].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 3:
				sb[2].setTextColor(Color.BLACK);
				sb[2].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 4:
				sb[3].setTextColor(Color.BLACK);
				sb[3].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 5:
				sb[4].setTextColor(Color.BLACK);
				sb[4].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 6:
				sb[5].setTextColor(Color.BLACK);
				sb[5].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[6].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 7:
				sb[6].setTextColor(Color.BLACK);
				sb[6].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			default:
				break;
			}
		}
		else if(menu_no==2)
		{
			((RelativeLayout) findViewById(R.id.submenu_insp)).setVisibility(View.VISIBLE);
			Button sb[]=new Button[6];
			sb[0]=(Button) findViewById(R.id.submenu_scope);
			sb[1]=(Button) findViewById(R.id.submenu_finding);
			sb[2]=(Button) findViewById(R.id.submenu_treatment);
			sb[3]=(Button) findViewById(R.id.submenu_noaccess);
			sb[4]=(Button) findViewById(R.id.submenu_comments);
			sb[5]=(Button) findViewById(R.id.submenu_Addendum);
			
			
			sb[0].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,ScopeofInspection.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[1].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,InspectionsFindings.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[2].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,Treatment.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[3].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,NoAccess.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[4].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,CommentsAndFinancial.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			sb[5].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in = new Intent(con,Addendum.class);
					in.putExtra("SRID", cf.selectedhomeid);
					con.startActivity(in);
				}
			});
			
			switch (submenu_no) {
			case 1:
				sb[0].setTextColor(Color.BLACK);
				sb[0].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 2:
				sb[1].setTextColor(Color.BLACK);
				sb[1].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 3:
				sb[2].setTextColor(Color.BLACK);
				sb[2].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 4:
				sb[3].setTextColor(Color.BLACK);	
				sb[3].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 5:
				sb[4].setTextColor(Color.BLACK);	
				sb[4].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[5].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			case 6:
				sb[5].setTextColor(Color.BLACK);	
				sb[5].setBackgroundResource(R.drawable.submenu_button_bg);
				sb[1].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[2].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[3].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[0].setBackgroundColor(getResources().getColor(R.color.sub_menu));
				sb[4].setBackgroundColor(getResources().getColor(R.color.sub_menu));
			break;
			default:
				break;
			}
		}
	}
	
	
}
