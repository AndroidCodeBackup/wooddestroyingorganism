package idsoft.inspectiondepot.wdo;

import idsoft.inspectiondepot.wdo.supportclass.CommonFunction;
import idsoft.inspectiondepot.wdo.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.wdo.supportclass.Webservice_Function;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Import extends Activity
{
	CommonFunction cf;
	DataBaseHelper db;
	Webservice_Function wb;
	private String newcode,newversion;
	private int total,vcode,usercheck,mState,delay,RUNNING=1;
	private ProgressDialog progDialog;
	public ProgressThread progThread;
	PowerManager.WakeLock wl=null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.import_data);
		//System.out.println("comes ocrrectly");
		 progDialog = new ProgressDialog(this);
		 wb=new Webservice_Function(this);
		 db=new DataBaseHelper(this);
		 cf=new CommonFunction(this);
		 getcurrentversioncode();
		total = 0;
		showDialog(1);
		total=5;
		start_import();
		Bundle b =getIntent().getExtras();
		
		
	}
	 private int getcurrentversioncode() {
			// TODO Auto-generated method stub

			try {
				vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vcode;
		}
	 @Override
	protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(100);	
				String source = "<font color=#FFFFFF>Importing please wait..."	+ "</font>";
		        progDialog.setMessage(Html.fromHtml(source));	
				progDialog.setCancelable(false);	
				progThread = new ProgressThread(handler1);	
				progThread.start();	
				return progDialog;
			default:
				return null;
			}
		}

	 	final Handler handler1 = new Handler() {
			public void handleMessage(Message msg) {
				// Get the current value of the variable total from the message data
				// and update the progress bar.
				int total = msg.getData().getInt("total");
				progDialog.setProgress(total);
				//progDialog.setProgressStyle(R.style.progtext);
				
				
				if (total == 100) {
					progDialog.setCancelable(true);
					removeDialog(1);
					handler.sendEmptyMessage(0);
					progThread.setState(ProgressThread.DONE);
			 	}

			}
		};
		 final Handler handler = new Handler() {

				public void handleMessage(Message msg) {
					 if(usercheck==1)
					 {
						 usercheck = 0;
						 cf.show_toast("Internet connection not available.",0);
					//	 startActivity(new Intent(Import.this, Import.class));
						 finish();
					 }
					 else if(usercheck==2)
					 {
						 usercheck = 0;
						 cf.show_toast("There is a problem on your network, so please try again later with better network.",0);
						 //startActivity(new Intent(Import.this, Import.class));
						 finish();
					 }
					 else if(usercheck==3)
					 {
						 usercheck = 0;
						 cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",0);
						// startActivity(new Intent(Import.this, Import.class));
						 finish();
					 }
					 else if(usercheck==4)
					 {
						 usercheck=0;
						 progDialog.dismiss();
						 final Dialog dialog1 = new Dialog(Import.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alert);
							dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
							LinearLayout li=(LinearLayout)dialog1.findViewById(R.id.help_alert);
							li.setVisibility(View.VISIBLE);
							TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
							txttitle.setText("Import");
							TextView txt = (TextView) li.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml("Import successfully completed"));
							ImageView btn_helpclose = (ImageView) li.findViewById(R.id.helpclose);
							btn_helpclose.setOnClickListener(new OnClickListener()
							{
							public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									finish();
								}
								
							});
							Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
							btn_ok.setVisibility(View.VISIBLE);
							btn_ok.setOnClickListener(new OnClickListener()
							{
							public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									startActivity(new Intent(Import.this,Dashboard.class));
								}
								
							});
							
							dialog1.setCancelable(false);
							dialog1.show();
					
						// startActivity(new Intent(Import.this, Import.class));
					 }
					if(wl!=null)
						{
						wl.release();
						wl=null;
						}
				}

			};
		private class ProgressThread extends Thread {

			// Class constants defining state of the thread
			final static int DONE = 0;

			Handler mHandler;

			
			ProgressThread(Handler h) {
				mHandler = h;
			}

			@Override
			public void run() {
				mState = RUNNING;
				while (mState == RUNNING) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e) {
						Log.e("ERROR", "Thread was Interrupted");
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("total", total);
					msg.setData(b);
					mHandler.sendMessage(msg);

				}
			}
			public void setState(int state) {
				mState = state;
			}

		}

	 private void start_import() {
			// TODO Auto-generated method stub
		 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		 wl.acquire();
			 new Thread() {
				public void run() {
					System.out.println("comes in the correct way");
					try {
						if (getversioncodefromweb() == true) 
						{
							System.out.println("not come here ");
							 total = 10;
					  		 try
							 {
					  			 
									SoapObject policy_holder = wb.Calling_service(db.Insp_id,"UpdateMobileDB");
									
									if (policy_holder.equals("anyType{}")) 
									{
										cf.show_toast("Server is busy. Please try again later.",0);
										finish();
									} 
									else 
									{
										total=30;
										GetPolicyholderInfo(policy_holder);
										total=50;
										GetAgentinformation();
										total=70;
										GetRealtorinformation();
										total=90;
										GetAdditionalService();										
//										total=80;
//										ExportReportsReady();
										total =100;
										usercheck = 4;
									}
									
								}
								catch (SocketTimeoutException s) {
									System.out.println("s "+s.getMessage());
									    usercheck = 2;
										total = 100;
										
								 } catch (NetworkErrorException n) {
									 System.out.println("n "+n.getMessage());
									    usercheck = 2;
									    total = 100;
									   
								 } catch (IOException io) {
									 System.out.println("io "+io.getMessage());
									    usercheck = 2;
									    total = 100;
									    
								 } catch (XmlPullParserException x) {
									 System.out.println("x "+x.getMessage());
									 	usercheck = 2;
									 	total = 100;
									    
								 }
			               	     catch (Exception e) {
			               	    	 System.out.println("e "+e.getMessage());
			               	    	    usercheck = 3;
										total = 100;									
								}
							}
						    else 
						    {
						    	total = 100;
						    }
						 }
						 catch (Exception e) {
							 System.out.println("e "+e.getMessage());
							 	usercheck = 2;
							 	total = 100;
							 System.out.println("commeonexcep"+e.getMessage());
						}
					}
			 }
			 .start();
		}
	 public boolean getversioncodefromweb() throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException 
	 {
		 System.out.println("comes correctly1");
		 SoapObject request = new SoapObject(wb.NAMESPACE, "GETVERSIONINFORMATION");
		 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		 envelope.dotNet = true;
		 envelope.setOutputSoapObject(request);
		 System.out.println("comes correctly1"+request);
		 
		 HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

		 androidHttpTransport.call(wb.NAMESPACE+"GETVERSIONINFORMATION", envelope);
		 SoapObject result = (SoapObject) envelope.getResponse();
		 System.out.println("comes correctly2"+result);
		 SoapObject obj = (SoapObject) result.getProperty(0);
		 
		 newcode =String.valueOf(obj.getProperty("VersionCode"));
		 newversion =String.valueOf(obj.getProperty("VersionName"));
		 
		 if (!"null".equals(newcode) && null != newcode && !newcode.equals(""))
		 {
			 
			
			 if (Integer.toString(vcode).equals(newcode)) 
			 {
				
				// return true;
			 } 
			 else 
			 {
				 
					 try 
					 {
						 db.CreateTable(0);
						 Cursor c12 = db.SelectTablefunction(db.wdoVersion," ");
						 if (c12.getCount() < 1) 
						 {
							 try 
							 {
								 db.wdo_db.execSQL("INSERT INTO "+ db.wdoVersion + "(VId,VersionCode,VersionName,ModifiedDate)"
									+ " VALUES ('1','"+newcode+"','" + newversion+ "',date('now'))");
							 } 
							 catch (Exception e) 
							 {
								
							 }
						 } 
						 else 
						 {
							 try 
							 {
								 db.wdo_db.execSQL("UPDATE " + db.wdoVersion + " set VersionCode='"+newcode+"',VersionName='" + newversion + "',ModifiedDate =date('now')");
							 }
							 catch (Exception e) 
							 {
								
							 }
						 }
						 if(c12!=null)
								c12.close();
						 
					 }
					 catch (Exception e) 
					 {
						 System.out.println("some isseus occies "+e.getMessage());
						//return true;
					 }
			 }
		 }
		System.out.println("comes here 15");
		try {  
		 	SoapObject request1 = new SoapObject(wb.NAMESPACE, "GETVERSIONINFORMATION_IDMA");
			SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope1.dotNet = true;
			envelope1.setOutputSoapObject(request1);
			System.out.println("comes here 16"+request1);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(wb.URL);
	    	androidHttpTransport1.call(wb.NAMESPACE+"GETVERSIONINFORMATION_IDMA", envelope1);
	    	
	    	SoapObject result1 = (SoapObject) envelope1.getResponse();
	    	System.out.println("comes here 17"+result1);
			SoapObject obj1 = (SoapObject) result1.getProperty(0);
			String newcode =String.valueOf(obj1.getProperty("VersionCode"));
			String newversion = String.valueOf(obj1.getProperty("VersionName"));
			
			if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
				   
						db.CreateTable(23);
						Cursor c12 = db.SelectTablefunction(db.IDMAVersion," Where ID_VersionType='IDMA_New' ");

						if (c12.getCount() < 1) {
							
								db.wdo_db.execSQL("INSERT INTO "
										+ db.IDMAVersion
										+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
										+ " VALUES ('2','"+newcode+"','" + newversion
										+ "','IDMA_New')");

							

						} else {
								db.wdo_db.execSQL("UPDATE " + db.IDMAVersion
										+ " set ID_VersionCode='"+newcode+"',ID_VersionName='"
										+ newversion
										+ "' Where  ID_VersionType='IDMA_New'");
							
						}
						if(c12!=null)
							c12.close();
				
					

				}  
		} catch (Exception e) {
			
			System.out.println("new data  error  "+e.getMessage());
		}

		 return true;
}
	 public void GetPolicyholderInfo(SoapObject objInsert) throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException
	 {
		 System.out.println("comes corectly 1");
		 String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
	     PH_Fname,PH_Lname,PH_Mname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees = "",PH_CONTPERSON,CompID,YearBuilt,
	     PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,PH_Noofstories,IsInspected,BuildingSize,
	     PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0",PH_InspectionName,insp_name="",latitude="",longtitude="",PH_MainInspectionTypeId="",
	    		  MailingAddress="",MailingAddress2="",CommercialFlag="",Mailingcity="",MailingState="",MailingCounty="",Mailingzip="",preferdate="",
	    	    		  prefertime="",inspectionfee="",discount="",totalfee="";
		 int Cnt;
		 db.CreateTable(2);
		 if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
		 {
				Cnt=0;total = 50;
		 }
		 else
		 {
			Cnt = objInsert.getPropertyCount();		System.out.println("Cnt="+Cnt);	
			double Totaltmp = 0.0;
			int countchk = Cnt;
			Totaltmp = (10.00 / (double) countchk);
			double temptot = Totaltmp;
			int temptotal = total;
			for (int i = 0; i < Cnt; i++) 
			{System.out.println("forloop");
				SoapObject obj = (SoapObject) objInsert.getProperty(i);
				try 
				{
					//System.out.println( "property "+obj.toString());
					 HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
					
					 PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
					 PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
					 PH_Mname = (String.valueOf(obj.getProperty("MiddleName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("NA"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("N/A"))?"":String.valueOf(obj.getProperty("MiddleName"));
					 PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
					 PH_Address2 = (String.valueOf(obj.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address2")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address2")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address2"));
					 PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
					 PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
					 PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
					 PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
					 PH_HPhone = (String.valueOf(obj.getProperty("HomePhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("HomePhone"));
					 PH_CPhone = (String.valueOf(obj.getProperty("CellPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("CellPhone"));
					 PH_WPhone = (String.valueOf(obj.getProperty("WorkPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("WorkPhone"));
					 PH_Email = (String.valueOf(obj.getProperty("Email")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Email")).equals("NA"))?"":(String.valueOf(obj.getProperty("Email")).equals("N/A"))?"":String.valueOf(obj.getProperty("Email"));
					 PH_CONTPERSON = (String.valueOf(obj.getProperty("ContactPerson")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("NA"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("N/A"))?"":String.valueOf(obj.getProperty("ContactPerson"));
					 PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
					 PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
					 PH_SubStatus = (String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
					 CompID = (String.valueOf(obj.getProperty("CompanyId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("NA"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("N/A"))?"":String.valueOf(obj.getProperty("CompanyId"));
					 InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
					 ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
					 YearBuilt = (String.valueOf(obj.getProperty("YearBuilt")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("NA"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("N/A"))?"":String.valueOf(obj.getProperty("YearBuilt"));
					 PH_Noofstories = (String.valueOf(obj.getProperty("Nstories")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("NA"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("N/A"))?"":String.valueOf(obj.getProperty("Nstories"));
					// PH_MainInspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MaininspectiontypeId")).equals("NA"))?"":(String.valueOf(obj.getProperty("MaininspectiontypeId")).equals("N/A"))?"":String.valueOf(obj.getProperty("MaininspectiontypeId"));
					 //PH_InspectionName = (String.valueOf(obj.getProperty("InspectionNames")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionNames")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionNames")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionNames"));
					 ScheduledCreatedDate = (String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduleCreatedDate"));
					 AssignedDate = (String.valueOf(obj.getProperty("AssignedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("AssignedDate"));
					 InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
					 InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
					 Schedulecomments = (String.valueOf(obj.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionComment"));
					 IsInspected= (String.valueOf(obj.getProperty("IsInspected")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("NA"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("N/A"))?"":String.valueOf(obj.getProperty("IsInspected"));
					 PH_InsuranceCompany = (String.valueOf(obj.getProperty("InsuranceCompany")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuranceCompany"));
					 BuildingSize = (String.valueOf(obj.getProperty("BuildingSize")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingSize"));
					 latitude = (String.valueOf(obj.getProperty("Latitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Latitude"));
					 longtitude = (String.valueOf(obj.getProperty("Longitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Longitude"));				
					 preferdate = (String.valueOf(obj.getProperty("Prefeereddate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Prefeereddate")).equals("NA"))?"":(String.valueOf(obj.getProperty("Prefeereddate")).equals("N/A"))?"":String.valueOf(obj.getProperty("Prefeereddate"));
					 System.out.println("preferdate"+preferdate);
					 prefertime = (String.valueOf(obj.getProperty("Preferredtime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Preferredtime")).equals("NA"))?"":(String.valueOf(obj.getProperty("Preferredtime")).equals("N/A"))?"":String.valueOf(obj.getProperty("Preferredtime"));
					 System.out.println("prefertime"+prefertime);
					 inspectionfee = (String.valueOf(obj.getProperty("InspectionFee")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionFee")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionFee")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionFee"));
					 discount = (String.valueOf(obj.getProperty("Discount")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Discount")).equals("NA"))?"":(String.valueOf(obj.getProperty("Discount")).equals("N/A"))?"":String.valueOf(obj.getProperty("Discount"));
					 totalfee = (String.valueOf(obj.getProperty("TotalInspectionFee")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("TotalInspectionFee")).equals("NA"))?"":(String.valueOf(obj.getProperty("TotalInspectionFee")).equals("N/A"))?"":String.valueOf(obj.getProperty("TotalInspectionFee"));
					
					 // PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
					 MailingAddress= (String.valueOf(obj.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress"));
					 MailingAddress2 = (String.valueOf(obj.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress2"));
					 Mailingcity = (String.valueOf(obj.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingcity"));
					 MailingState = (String.valueOf(obj.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingState"));
					 MailingCounty = (String.valueOf(obj.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingCounty"));				
					 Mailingzip = (String.valueOf(obj.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingzip"));
					 
					/* <MailingAddress>string</MailingAddress>
			          <MailingAddress2>string</MailingAddress2>
			          <Mailingcity>string</Mailingcity>
			          <MailingState>string</MailingState>
			          <MailingCounty>string</MailingCounty>
			          <Mailingzip>string</Mailingzip>*/
					 //CommercialFlag = (String.valueOf(obj.getProperty("i_COMMflag")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("i_COMMflag")).equals("NA"))?"":(String.valueOf(obj.getProperty("i_COMMflag")).equals("N/A"))?"":String.valueOf(obj.getProperty("i_COMMflag"));
						
					 //System.out.println("HomeId="+HomeId+"PH_HPhone="+PH_HPhone+"PH_WPhone"+PH_WPhone+"PH_CPhone"+PH_CPhone+"MailingState"+MailingState);
					 
					 
					 Cursor c1 = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.encode(db.Insp_id) + "' and PH_SRID='"+db.encode(HomeId)+"'");
					if(c1.getCount() >= 1)
					 {
						 c1.moveToFirst();
								 if(c1.getString(c1.getColumnIndex("PH_IsInspected")).equals("0"))
								 {
									 db.wdo_db.execSQL("UPDATE "
												+ db.policyholder
												+ " SET  PH_FirstName ='"+ db.encode(PH_Fname)+"'," +
												       " PH_LastName  ='"+ db.encode(PH_Lname)+"'," +
												       " PH_Address1 ='"+ db.encode(PH_Address1)+"'," +
												       " PH_Address2 ='"+ db.encode(PH_Address2)+"'," +
												       " PH_City ='"+ db.encode(PH_City)+"'," +
												       " PH_Zip ='"+ db.encode(PH_Zip)+"'," +
												       " PH_State ='"+ db.encode(PH_State)+"', "+
												       " PH_County ='"+ db.encode(PH_County)+"',"+
												       " PH_Policyno ='"+ db.encode(PH_Policyno)+"',"+
												       " PH_Inspectionfees ='"+ db.encode(PH_Inspectionfees)+"',"+
												       " PH_InsuranceCompany ='"+ db.encode(PH_InsuranceCompany)+"',"+
												       " PH_HomePhone ='"+ db.encode(PH_HPhone)+"',"+
												       " PH_WorkPhone ='"+ db.encode(PH_WPhone)+"',"+
												       " PH_CellPhone ='"+ db.encode(PH_CPhone)+"', "+
												       " PH_NOOFSTORIES ='"+db.encode(PH_Noofstories)+"',"+
												       " PH_WEBSITE  ='',"+
												       " PH_Email ='"+ db.encode(PH_Email)+"', "+										       
												       " PH_EmailChkbx ='"+ db.encode(PH_EmailChk)+"', "+
												       " PH_IsInspected ='"+ db.encode(PH_IsInspected)+"',"+
												       " PH_IsUploaded ='"+ db.encode(PH_IsUploaded)+"',"+
												       " PH_Status='"+ db.encode(PH_Status)+"',"+
												       " PH_SubStatus='"+ db.encode(PH_SubStatus)+"'," +
												       " Schedule_ScheduledDate ='"+ db.encode(ScheduledDate)+"'," +
												       " Schedule_InspectionStartTime='"+db.encode(InspectionStartTime)+"'," +
												       " Schedule_InspectionEndTime ='"+db.encode(InspectionEndTime)+"'," +
												       " Schedule_Comments ='"+db.encode(Schedulecomments)+"',"+
												       " Schedule_ScheduleCreatedDate ='"+db.encode(ScheduledCreatedDate)+"',"+
												       " Schedule_AssignedDate ='"+db.encode(AssignedDate)+"',"+
												       " ScheduleFlag =0 ," +
												       " YearBuilt ='"+YearBuilt+"',"+
												       " ContactPerson ='"+db.encode(PH_CONTPERSON)+"',BuidingSize='"+db.encode(BuildingSize)+"',fld_latitude='"+latitude+"',fld_longitude='"+longtitude+"',"+
												       "preferdate='"+preferdate+"',prefertime='"+prefertime+"',Inspectionfee='"+inspectionfee+"',discount='"+discount+"',totalfee='"+totalfee+"' where PH_SRID='" + db.encode(HomeId)+ "' and PH_InspectorId='"+ db.encode(InspectorId)+"'");
									 } } else
									 	{
							 /*INSERTING THE TABLE - POLICYHOLDER*/
							
							 db.wdo_db.execSQL("INSERT INTO "
										+ db.policyholder
										+ " (PH_InspectorId,PH_SRID,PH_FirstName,PH_LastName,PH_Address1,PH_Address2," +
										" PH_City,PH_Zip,PH_State,PH_County,PH_Policyno,PH_Inspectionfees,PH_InsuranceCompany," +
										" PH_HomePhone,PH_WorkPhone,PH_CellPhone,PH_Email,PH_EmailChkbx," +
										" PH_IsInspected,PH_IsUploaded,PH_Status,PH_SubStatus,Schedule_ScheduledDate,Schedule_InspectionStartTime,"+
									  " Schedule_InspectionEndTime,Schedule_Comments,Schedule_ScheduleCreatedDate,Schedule_AssignedDate,ScheduleFlag,YearBuilt,ContactPerson,BuidingSize,PH_NOOFSTORIES,PH_WEBSITE,fld_latitude,fld_longitude,fld_homeownersign,fld_homewonercaption,fld_paperworksign,fld_paperworkcaption,preferdate,prefertime,Inspectionfee,discount,totalfee)"
										+ " VALUES ('"+ db.encode(InspectorId)+"','"
										+ db.encode(HomeId)+"','"
										+ db.encode(PH_Fname)+"','"
										+ db.encode(PH_Lname)+"','"
										+ db.encode(PH_Address1)+"','"
										+ db.encode(PH_Address2)+"','"
										+ db.encode(PH_City)+"','"
										+ db.encode(PH_Zip)+"','"
										+ db.encode(PH_State)+"','"
										+ db.encode(PH_County)+"','"
										+ db.encode(PH_Policyno)+"','"
										+ db.encode(PH_Inspectionfees)+"','"
										+ db.encode(PH_InsuranceCompany)+"','"
										+ db.encode(PH_HPhone)+"','"
										+ db.encode(PH_WPhone)+"','"
										+ db.encode(PH_CPhone)+"','"
										+ db.encode(PH_Email)+"','"
										+ db.encode(PH_EmailChk)+"','"
										+ db.encode(PH_IsInspected)+"','"
										+ db.encode(PH_IsUploaded)+"','"
										+ db.encode(PH_Status)+"','"
										+ db.encode(PH_SubStatus)+"','"
										+ db.encode(ScheduledDate)+"','"+db.encode(InspectionStartTime)+"','"+db.encode(InspectionEndTime)+"','"
										+ db.encode(Schedulecomments)+"','"+db.encode(ScheduledCreatedDate)+"','"+db.encode(AssignedDate)+"','0','"+YearBuilt+"','"+db.encode(PH_CONTPERSON)+"','"+db.encode(BuildingSize)+"','"+db.encode(PH_Noofstories)+"','','"+latitude+"','"+longtitude+"','','','','','"+preferdate+"','"+prefertime+"','"+inspectionfee+"','"+discount+"','"+totalfee+"')");
						 }
					if(c1!=null)
						c1.close();
					 
					 db.CreateTable(3);
				//	 System.out.println("the mail address"+MailingAddress);
					        Cursor c21 = db.SelectTablefunction(db.MailingPolicyHolder," where ML_PH_InspectorId='" + db.encode(db.Insp_id) + "' and ML_PH_SRID='"+db.encode(HomeId)+"'");
							int rws = c21.getCount();
							if(c21!=null)
								c21.close();
							if(rws >= 1)
							{
								db.wdo_db.execSQL("UPDATE " + db.MailingPolicyHolder
										+ " SET ML_PH_Address1='" + db.encode(MailingAddress)
										+ "',ML_PH_Address2='"
										+ db.encode(MailingAddress2)
										+ "',ML_PH_City='"
										+ db.encode(Mailingcity)
										+ "',ML_PH_Zip='"
										+ db.encode(Mailingzip) + "',ML_PH_State='"
										+ db.encode(MailingState) + "',ML_PH_County='"
										+ db.encode(MailingCounty) + "' WHERE ML_PH_SRID ='" + db.encode(HomeId)
										+ "'");
							}
							else
							{
								db.wdo_db.execSQL("INSERT INTO "
										+ db.MailingPolicyHolder
										+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
										+ "VALUES ('"+db.encode(HomeId)+"','"+db.encode(InspectorId)+"','0','"+db.encode(MailingAddress)+"','"
									    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
									    + db.encode(Mailingzip)+"','"+db.encode(MailingState)+"','"
									    + db.encode(MailingCounty)+"')");
							}
					
					 

				}
				catch(Exception e)
				{
					System.out.println("Exception"+e.getMessage());
				}
			}
			}
	 }
	 public void GetAgentinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 System.out.println("comes in correct place");
			 	int Cnt;
				String SRID,AgencyName,AgentName,AgentAddress,AgentAddress2,AgentCity,AgentCounty,AgentRole,AgentState,AgentZip,AgentOffPhone,AgentContactPhone,AgentFax,AgentEmail,AgentWebSite;
			 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadAgentInformation");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID",db.Insp_id);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				androidHttpTransport.call(wb.NAMESPACE+"LoadAgentInformation",envelope);
				SoapObject agentinfo = (SoapObject) envelope.getResponse();
				
				System.out.println("the agent info result is "+agentinfo);
				
				
				if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
				{
					Cnt=0;total = 40;
				}
				else
				{
				 	 Cnt = agentinfo.getPropertyCount();
					 double Totaltmp = 0.0;
					 int countchk = Cnt;
					 Totaltmp = (20.00 / (double) countchk);
					 double temptot = Totaltmp;
					 int temptotal = total;
					db.CreateTable(5);
					
					for(int i=0;i<agentinfo.getPropertyCount();i++)
					{
						SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
						SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
						AgencyName=(String.valueOf(temp_result.getProperty("AgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgencyName"));
						AgentName=(String.valueOf(temp_result.getProperty("AgentName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentName"));
						AgentAddress=(String.valueOf(temp_result.getProperty("AgentAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress"));
						AgentAddress2=(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress2"));
						AgentCity=(String.valueOf(temp_result.getProperty("AgentCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCity"));
						AgentCounty=(String.valueOf(temp_result.getProperty("AgentCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCounty"));
						AgentRole=(String.valueOf(temp_result.getProperty("AgentRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentRole"));
						AgentState=(String.valueOf(temp_result.getProperty("AgentState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentState"));
						AgentZip=(String.valueOf(temp_result.getProperty("AgentZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentZip"));
						AgentOffPhone=(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentOffPhone"));
						AgentContactPhone=(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentContactPhone"));
						AgentFax=(String.valueOf(temp_result.getProperty("AgentFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentFax"));
						AgentEmail=(String.valueOf(temp_result.getProperty("AgentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentEmail"));
						AgentWebSite=(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentWebSite"));
						
						try
						{
							Cursor c2=db.SelectTablefunction(db.Agent_tabble," where AI_SRID ='"+SRID+"'");
							if(c2.getCount()==0)
							{
								db.wdo_db.execSQL("Insert into "+db.Agent_tabble+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
										"('"+SRID+"','"+AgencyName+"','"+AgentName+"','"+AgentAddress+"','"+AgentAddress2+"','"+AgentCity+"','"+AgentCounty+"','"+AgentRole+"','"+AgentState+"','"+AgentZip+"','"+db.encode(AgentOffPhone)+"','"+db.encode(AgentContactPhone)+"','"+AgentFax+"','"+AgentEmail+"','"+AgentWebSite+"') ");
							}
							else if(c2.getCount()>=1)
							{
								db.wdo_db.execSQL("UPDATE "+db.Agent_tabble+" SET AI_SRID='"+SRID+"',AI_AgencyName='"+AgencyName+"',AI_AgentName='"+AgentName+"',AI_AgentAddress='"+AgentAddress+"',AI_AgentAddress2='"+AgentAddress2+"'," +
								"AI_AgentCity='"+AgentCity+"',AI_AgentCounty='"+AgentCounty+"',AI_AgentRole='"+AgentRole+"',AI_AgentState='"+AgentState+"',AI_AgentZip='"+AgentZip+"',AI_AgentOffPhone='"+db.decode(AgentOffPhone)+"'," +
								"AI_AgentContactPhone='"+db.decode(AgentContactPhone)+"',AI_AgentFax='"+AgentFax+"',AI_AgentEmail='"+AgentEmail+"',AI_AgentWebSite='"+AgentWebSite+"' WHERE AI_SRID='"+SRID+"'");
							}
							if(c2!=null)
								c2.close();
						}
						catch(Exception e)
						{
							System.out.println("he issues is "+e.getMessage());
						//	cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}
//						if (total <= 41) {
//		
//							total = temptotal + (int) (Totaltmp);
//						} else {
//							total = 40;
//						}
//						Totaltmp += temptot;
						
					}
				 }
		}
	 
	 public void GetRealtorinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 System.out.println("comes in correct place");
			 	int Cnt;
				String RealtorSRID,RealtorAgencyName,RealtorName,RealtorAddress,RealtorAddress2,RealtorCity,RealtorCounty,RealtorRole,RealtorState,RealtorZip,RealtorOffPhone,RealtorContactPhone,RealtorFax,RealtorEmail,RealtorWebSite;
			 	SoapObject request = new SoapObject(wb.NAMESPACE,"LoadRealtorInformation");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID",db.Insp_id);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				androidHttpTransport.call(wb.NAMESPACE+"LoadRealtorInformation",envelope);
				SoapObject agentinfo = (SoapObject) envelope.getResponse();
				
				System.out.println("the realtor info result is "+agentinfo);
				
				
				if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
				{
					Cnt=0;total = 60;
				}
				else
				{
				 	 Cnt = agentinfo.getPropertyCount();
					 double Totaltmp = 0.0;
					 int countchk = Cnt;
					 Totaltmp = (20.00 / (double) countchk);
					 double temptot = Totaltmp;
					 int temptotal = total;
					db.CreateTable(25);
					
					for(int i=0;i<agentinfo.getPropertyCount();i++)
					{
						SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
						RealtorSRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
						RealtorAgencyName=(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAgencyName"));
						RealtorName=(String.valueOf(temp_result.getProperty("RealtorName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorName"));
						RealtorAddress=(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress"));
						RealtorAddress2=(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress2"));
						RealtorCity=(String.valueOf(temp_result.getProperty("RealtorCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCity"));
						RealtorCounty=(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCounty"));
						RealtorRole=(String.valueOf(temp_result.getProperty("RealtorRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorRole"));
						RealtorState=(String.valueOf(temp_result.getProperty("RealtorState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorState"));
						RealtorZip=(String.valueOf(temp_result.getProperty("RealtorZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorZip"));
						RealtorOffPhone=(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorOffPhone"));
						RealtorContactPhone=(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorContactPhone"));
						RealtorFax=(String.valueOf(temp_result.getProperty("RealtorFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorFax"));
						RealtorEmail=(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorEmail"));
						RealtorWebSite=(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorWebSite"));
						
						try
						{
							Cursor c2=db.SelectTablefunction(db.Realtor_table," where AI_SRID ='"+RealtorSRID+"'");
							if(c2.getCount()==0)
							{
								db.wdo_db.execSQL("Insert into "+db.Realtor_table+" (AI_SRID,AI_AgencyName,AI_AgentName,AI_AgentAddress,AI_AgentAddress2,AI_AgentCity,AI_AgentCounty,AI_AgentRole,AI_AgentState,AI_AgentZip,AI_AgentOffPhone,AI_AgentContactPhone,AI_AgentFax,AI_AgentEmail,AI_AgentWebSite) values" +
										"('"+RealtorSRID+"','"+RealtorAgencyName+"','"+RealtorName+"','"+RealtorAddress+"','"+RealtorAddress2+"','"+RealtorCity+"','"+RealtorCounty+"','"+RealtorRole+"','"+RealtorState+"','"+RealtorZip+"','"+db.encode(RealtorOffPhone)+"','"+db.encode(RealtorContactPhone)+"','"+RealtorFax+"','"+RealtorEmail+"','"+RealtorWebSite+"') ");
							}
							else if(c2.getCount()>=1)
							{
								db.wdo_db.execSQL("UPDATE "+db.Realtor_table+" SET AI_SRID='"+RealtorSRID+"',AI_AgencyName='"+RealtorAgencyName+"',AI_AgentName='"+RealtorName+"',AI_AgentAddress='"+RealtorAddress+"',AI_AgentAddress2='"+RealtorAddress2+"'," +
								"AI_AgentCity='"+RealtorCity+"',AI_AgentCounty='"+RealtorCounty+"',AI_AgentRole='"+RealtorRole+"',AI_AgentState='"+RealtorState+"',AI_AgentZip='"+RealtorZip+"',AI_AgentOffPhone='"+RealtorOffPhone+"'," +
								"AI_AgentContactPhone='"+RealtorContactPhone+"',AI_AgentFax='"+RealtorFax+"',AI_AgentEmail='"+RealtorEmail+"',AI_AgentWebSite='"+RealtorWebSite+"' WHERE AI_SRID='"+RealtorSRID+"'");
							}
							if(c2!=null)
								c2.close();
						}
						catch(Exception e)
						{
							System.out.println("he issues is "+e.getMessage());
						//	cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}
//						if (total <= 61) {
//		
//							total = temptotal + (int) (Totaltmp);
//						} else {
//							total = 60;
//						}
//						Totaltmp += temptot;
						
					}
				 }
		}	 
	 
	 public void GetAdditionalService() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException 
	 {
		 int Cnt;
		 	String SRID="",IsRecord="",ContactEmail="",UserTypeName="",PhoneNumber="",MobileNumber="",BestTimetoCallYou="",BestDay="",FirstChoice="",SecondChoice="",ThirdChoice="",FeedbackComments="";
		 	db.CreateTable(4);
		 	SoapObject request = new SoapObject(wb.NAMESPACE,"GETADDITIONALSERVICEINFO");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",Integer.parseInt(db.Insp_id));
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"GETADDITIONALSERVICEINFO",envelope);
			SoapObject addiserviceres = (SoapObject) envelope.getResponse();
			
			 if(String.valueOf(addiserviceres).equals("null") || String.valueOf(addiserviceres).equals(null) || String.valueOf(addiserviceres).equals("anytype{}"))
			 {
					Cnt=0;total = 100;
			  }
			 else
			 {
				 Cnt = addiserviceres.getPropertyCount();
				
				for(int i=0;i<Cnt;i++)
				{					
					SoapObject temp_result=(SoapObject) addiserviceres.getProperty(i);
					SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					IsRecord=(String.valueOf(temp_result.getProperty("IsRecord")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("IsRecord")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("IsRecord")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("IsRecord"));
					UserTypeName=(String.valueOf(temp_result.getProperty("UserTypeName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("UserTypeName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("UserTypeName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("UserTypeName"));
					ContactEmail=(String.valueOf(temp_result.getProperty("ContactEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ContactEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ContactEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ContactEmail"));
					PhoneNumber=(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("PhoneNumber"));
					MobileNumber=(String.valueOf(temp_result.getProperty("MobileNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("MobileNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("MobileNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("MobileNumber"));
					BestTimetoCallYou=(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("BestTimetoCallYou"));
					BestDay=(String.valueOf(temp_result.getProperty("BestDay")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("BestDay")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("BestDay")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("BestDay"));
					FirstChoice=(String.valueOf(temp_result.getProperty("FirstChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("FirstChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("FirstChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("FirstChoice"));
					SecondChoice=(String.valueOf(temp_result.getProperty("SecondChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SecondChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SecondChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SecondChoice"));
					ThirdChoice=(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ThirdChoice"));
					FeedbackComments=(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("FeedbackComments"));
					
					try
					{
					Cursor c1 = db.SelectTablefunction(db.Additional_table, " where AD_INSPID='" + db.Insp_id + "' and AD_SRID='"+SRID+"'");
					if(c1.getCount()>0)
					{
						
						db.wdo_db.execSQL("UPDATE "+db.Additional_table+" set AD_UserTypeName='"+db.encode(UserTypeName)+"',AD_ContactEmail='"+db.encode(ContactEmail)+"'," +
										"AD_PhoneNumber='"+db.encode(PhoneNumber)+"',AD_MobileNumber='"+db.encode(MobileNumber)+"',AD_BestTimetoCallYou='"+db.encode(BestTimetoCallYou)+"'," +
											"AD_Bestdaycall='"+db.encode(BestDay)+"',AD_FirstChoice='"+db.encode(FirstChoice)+"',AD_SecondChoice='"+db.encode(SecondChoice)+"',AD_ThirdChoice='"+db.encode(ThirdChoice)+"'," +
											"SF_AD_FeedbackComments='"+db.encode(FeedbackComments)+"' WHERE AD_SRID='"+SRID+"' and " +
											"AD_INSPID='"+db.Insp_id+"'");
					}
					else
					{
							db.wdo_db.execSQL("INSERT INTO "
									+ db.Additional_table
									+ "(AD_SRID,AD_INSPID,AD_IsRecord,AD_UserTypeName,AD_ContactEmail,AD_PhoneNumber,AD_MobileNumber,AD_BestTimetoCallYou,AD_Bestdaycall,AD_FirstChoice,AD_SecondChoice,AD_ThirdChoice,SF_AD_FeedbackComments)"
									+ " VALUES ('"+SRID+"','"+db.Insp_id+"','','"+db.encode(IsRecord)+"','"+db.encode(ContactEmail)+"','"+db.encode(PhoneNumber)+"','"+db.encode(MobileNumber)+"','"+db.encode(BestTimetoCallYou)+"','"+db.encode(BestDay)+"','"+db.encode(FirstChoice)+"','"+db.encode(SecondChoice)+"','"+db.encode(ThirdChoice)+"','"+db.encode(FeedbackComments)+"')");
					}
					if(c1!=null)
						c1.close();
					}
					catch(Exception e)
					{
						System.out.println("eee"+e.getMessage());
					}
					
				}
			}
	 }
	 
	
	 
}
